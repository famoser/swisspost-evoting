/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */

const {ImmutableBigInteger} = require("crypto-primitives-ts/lib/cjs/immutable_big_integer");
const {
	deserializeElGamalMultiRecipientPublicKey,
	deserializeImmutableBigInteger,
	deserializeGqGroup
} = require("../../src/services/primitives-deserializer");
const {serializeElGamalMultiRecipientPublicKey} = require("../../src/services/primitives-serializer");

describe('Primitives deserializer', function () {
	'use strict';

	const testData = require('./mocks/primitives.json');
	const gqGroup = deserializeGqGroup(testData.gqGroup);

	it('should deserialize ImmutableBigInteger', function () {
		expect(ImmutableBigInteger.fromNumber(testData.gqGroup.expectedG)).toEqual(deserializeImmutableBigInteger(testData.gqGroup.g));
	});

	it('should deserialize ElGamalMultiRecipientPublicKey', function () {
		const pk = deserializeElGamalMultiRecipientPublicKey(testData.elGamalMultiRecipientPublicKey, gqGroup);
		expect(JSON.stringify(testData.elGamalMultiRecipientPublicKey)).toEqual(serializeElGamalMultiRecipientPublicKey(pk));
	});
});
