/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */

const primitivesParamsParser = require("../../src/services/primitives-params-service");
const authResponse = require('../tools/data/authResponse.json');

describe('Primitives params parser', function () {
	'use strict';

	it('should parse all the primitives', function () {
		expect(() => primitivesParamsParser.parsePrimitivesParams(authResponse)).not.toThrow();
	});
});
