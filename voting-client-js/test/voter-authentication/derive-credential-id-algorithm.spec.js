/*
 * (c) Copyright 2023 Swiss Post Ltd
 */

/* jshint maxlen: 6666 */

const deriveCredentialIdAlgorithm = require("../../src/voter-authentication/derive-credential-id-algorithm");

describe('Derive credentialId algorithm', function () {

	const electionEventId = '34caee78ed3d4cf981ca06b659f558eb';
	const startVotingKey = '4d65ej2adb4ia6ghhzb52kg6';
	const credentialId = '9660D63A4AB22ECCEF143D213BAF3EF2';

	it('should return expected credentialId', async function () {
		deriveCredentialIdAlgorithm.deriveCredentialId(
			electionEventId,
			startVotingKey
		).then(actualCredentialId => expect(credentialId).toBe(actualCredentialId));
	}, 30000);

});


