/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */

/* jshint maxlen: 6666 */

const {ImmutableBigInteger} = require("crypto-primitives-ts/lib/cjs/immutable_big_integer");
const realValuesJson = require('../tools/data/write-in-to-integer.json');
const {GqGroup} = require("crypto-primitives-ts/lib/cjs/math/gq_group");
const {ZqElement} = require("crypto-primitives-ts/lib/cjs/math/zq_element");
const {ZqGroup} = require("crypto-primitives-ts/lib/cjs/math/zq_group");
const writeInToIntegerAlgorithm = require("../../src/write-ins/write-in-to-integer-algorithm");
const primitivesParamsParser = require("../../src/services/primitives-params-service");
const authResponse = require("../tools/data/authResponse.json");
const {WRITE_IN_ALPHABET} = require("../../src/write-ins/write-in-alphabet");
const {getGqGroup} = require("../tools/data/group-test-data");

describe('WriteIn to integer algorithm', () => {

  describe('with real values', () => {
    const args = [];
    const parametersList = JSON.parse(JSON.stringify(realValuesJson));
    parametersList.forEach(testParameters => {

      // Context.
      const p = readValue(testParameters.context.p);
      const q = readValue(testParameters.context.q);
      const g = readValue(testParameters.context.g);

      const gqGroup = new GqGroup(p, q, g);
      const zqGroup = new ZqGroup(q);

      // Parse input parameter.
      const s = testParameters.input.s;

      // Parse output parameters.
      const output = ZqElement.create(readValue(testParameters.output.output), zqGroup);

      args.push({
        encryptionGroup: gqGroup,
        s: s,
        output: output,
        description: testParameters.description
      });
    });


    args.forEach((arg) => {
      it(arg.description, () => {
        const actual = writeInToIntegerAlgorithm.writeInToInteger({encryptionGroup: arg.encryptionGroup}, arg.s);
        expect(arg.output.equals(actual)).toBe(true);
      });
    });
  });
});

describe('checkExpLength method', () => {
  const primitivesParams = primitivesParamsParser.parsePrimitivesParams(authResponse);
  const encryptionGroup = primitivesParams.encryptionGroup;
  const a_int = WRITE_IN_ALPHABET.length;
  const s_length_limit = Math.ceil(encryptionGroup.q.bitLength() / Math.log2(a_int));

  it('should return true with exponential result smaller than q', function () {
    const a = ImmutableBigInteger.fromNumber(a_int);
    const s_length = ImmutableBigInteger.fromNumber(s_length_limit - 1);

    const actual = writeInToIntegerAlgorithm.checkExpLength(a, s_length, encryptionGroup);

    expect(actual).toBe(true);
  });

  it('should return false with exponential result bigger than q', function () {
    const a = ImmutableBigInteger.fromNumber(a_int);
    const s_length = ImmutableBigInteger.fromNumber(s_length_limit + 1);

    const actual = writeInToIntegerAlgorithm.checkExpLength(a, s_length, encryptionGroup);

    expect(actual).toBe(false);
  });

  it('should return a result with exponential result equal to q', function () {
    for (let i = 0; i < 100; i++) {
      const smallGroup = getGqGroup();
      const q_bitLength = smallGroup.q.bitLength();
      const a = ImmutableBigInteger.fromNumber(2 ** q_bitLength);
      const s_length = ImmutableBigInteger.fromNumber(1);

      const actual = writeInToIntegerAlgorithm.checkExpLength(a, s_length, smallGroup);

      expect(actual).toBe(false);
    }
  });
});

function readValue(value) {
  return ImmutableBigInteger.fromString(value.substring(2), 16);
}

