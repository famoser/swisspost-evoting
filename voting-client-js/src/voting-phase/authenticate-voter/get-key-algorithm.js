/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */

const {validateUUID, validateBase32StringWithoutPadding, validateBase64String, validateActualVotingOptions} = require("../../services/validation-service");
const {ZqGroup} = require("crypto-primitives-ts/lib/cjs/math/zq_group");
const {ZqElement} = require("crypto-primitives-ts/lib/cjs/math/zq_element");
const {HashService} = require("crypto-primitives-ts/lib/cjs/hashing/hash_service");
const {Argon2Profile} = require("crypto-primitives-ts/lib/cjs/hashing/argon2_profile");
const {Argon2Service} = require("crypto-primitives-ts/lib/cjs/hashing/argon2_service");
const {Base64Service} = require("crypto-primitives-ts/lib/cjs/math/base64_service");
const {ImmutableArray} = require("crypto-primitives-ts/lib/cjs/immutable_array");
const {SymmetricService} = require("crypto-primitives-ts/lib/cjs/symmetric/symmetric_service");
const {ImmutableUint8Array} = require("crypto-primitives-ts/lib/cjs/immutable_uint8Array");
const {checkNotNull, checkArgument} = require("crypto-primitives-ts/lib/cjs/validation/preconditions");
const {SymmetricEncryptionAlgorithm} = require("crypto-primitives-ts/lib/cjs/symmetric/symmetric_encryption_algorithm");
const {byteArrayToInteger, stringToByteArray} = require("crypto-primitives-ts/lib/cjs/conversions");

module.exports = (function () {
  'use strict';

  const VERIFICATION_CARD_KEYSTORE_MAX_LENGTH = 600;
  const VERIFICATION_CARD_KEYSTORE_MIN_LENGTH = 56;

  /**
   * @typedef {object} GetKeyContext
   * @property {GqGroup} encryptionGroup, the encryption parameters p, q and g.
   * @property {string} electionEventId ee, the identifier of the election event.
   * @property {string} verificationCardSetId vcs, the identifier of the verification card set.
   * @property {ElGamalMultiRecipientPublicKey} electionPublicKey EL_pk, the election public key.
   * @property {ElGamalMultiRecipientPublicKey} choiceReturnCodesEncryptionPublicKey pk_CCR, the Choice Return Codes encryption public key.
   * @property {GroupVector<PrimeGqElement>} encodedVotingOptions p_tilde, the encoded voting options.
   * @property {string[]} actualVotingOptions v_tilde, the actual voting options.
   * @property {string[]} semanticInformation sigma, the semantic information.
   * @property {string[]} ciSelections ciSelections, the correctness information for selections.
   * @property {string[]} ciVotingOptions ciVotingOptions, the correctness information for voting options.
   * @property {number} characterLengthOfTheStartVotingKey l_SVK, the length of the Start Voting Key.
   */

  /**
   * Retrieves the verification card secret key from the verification card keystore.
   *
   * @param {GetKeyContext} context, the getKey context.
   * @param {string} startVotingKey SVK_id, the start voting key.
   * @param {string} verificationCardKeystore VCks_id, the verification card keystore.
   * @param {string} verificationCardId vc_id, the identifier of the verification card.
   * @return {ZqElement} k_id, the verification card secret key.
   */
  async function getKey(
    context,
    startVotingKey,
    verificationCardKeystore,
    verificationCardId
  ) {
    checkNotNull(context);
    const encryptionGroup = checkNotNull(context.encryptionGroup);
    const p = encryptionGroup.p;
    const q = encryptionGroup.q;
    const g = encryptionGroup.generator.value;
    const ee = validateUUID(context.electionEventId);
    const vcs = validateUUID(context.verificationCardSetId);
    const EL_pk = checkNotNull(context.electionPublicKey);
    const pk_CCR = checkNotNull(context.choiceReturnCodesEncryptionPublicKey);
    const p_tilde = checkNotNull(context.encodedVotingOptions);
    const v_tilde = checkNotNull(context.actualVotingOptions);
    validateActualVotingOptions(v_tilde);
    const sigma = checkNotNull(context.semanticInformation);
    const ciSelections = checkNotNull(context.ciSelections);
    const ciVotingOptions = checkNotNull(context.ciVotingOptions);
    const l_SVK = checkNotNull(context.characterLengthOfTheStartVotingKey);

    const SVK_id = validateBase32StringWithoutPadding(startVotingKey, l_SVK);
    const VCks_id = validateBase64String(verificationCardKeystore);
    checkArgument(VERIFICATION_CARD_KEYSTORE_MIN_LENGTH <= VCks_id.length && VCks_id.length <= VERIFICATION_CARD_KEYSTORE_MAX_LENGTH,
      `The verification card keystore length must be between ${VERIFICATION_CARD_KEYSTORE_MIN_LENGTH} and ${VERIFICATION_CARD_KEYSTORE_MAX_LENGTH}.`);
    const vc_id = validateUUID(verificationCardId);

    const hashService = new HashService();
    const base64Service = new Base64Service();
    const symmetricService = new SymmetricService();
    const nonceLength = SymmetricEncryptionAlgorithm.AES256_GCM_NOPADDING().nonceLength;
    const argon2Service = new Argon2Service(Argon2Profile.LESS_MEMORY);
    const saltLength = 16;

    // Cross-checks.
    checkArgument(encryptionGroup.equals(EL_pk.group), "The context encryption group must equal the election public key group.");
    checkArgument(encryptionGroup.equals(pk_CCR.group), "The context encryption group must equal the choice return codes encryption public key group.");
    checkArgument(encryptionGroup.equals(p_tilde.group), "The context encryption group must equal the encoded voting options group.");
    checkArgument(p_tilde.size === v_tilde.length, "The encoded voting options and the actual voting options must have the same size.");
    checkArgument(ciVotingOptions.length === v_tilde.length, "The correctness information voting options and the actual voting options" +
      " must have the same size.");

    // Operation.
    const h_aux = ImmutableArray.of(
      "Context", p, q, g, ee, vcs, "ELpk", ...EL_pk.stream().map(e => e.value),
      "pkCCR", ...pk_CCR.stream().map(e => e.value),
      "EncodedVotingOptions", ...p_tilde.elements.map(e => e.value),
      "ActualVotingOptions", ...v_tilde,
      "SemanticInformation", ...sigma,
      "ciSelections", ...ciSelections,
      "ciVotingOptions", ...ciVotingOptions);
    const i_aux = base64Service.base64Encode(hashService.recursiveHash(h_aux).value());

    const VCks_id_combined = base64Service.base64Decode(VCks_id);

    const length = VCks_id_combined.byteLength;
    const split_ciphertext = length - (nonceLength + saltLength);
    const split_nonce = split_ciphertext + nonceLength;
    const VCks_id_ciphertext = ImmutableUint8Array.from(VCks_id_combined.subarray(0, split_ciphertext));
    const VCks_id_nonce = ImmutableUint8Array.from(VCks_id_combined.subarray(split_ciphertext, split_nonce));
    const VCks_id_salt = ImmutableUint8Array.from(VCks_id_combined.subarray(split_nonce, length));

    const dSVK_id = await argon2Service.getArgon2id(stringToByteArray(SVK_id), VCks_id_salt);

    const KSkey_id = hashService.recursiveHash("VerificationCardKeystore", ee, vcs, vc_id, dSVK_id);

    const k_id_bytes = await symmetricService.getPlaintextSymmetric(KSkey_id, VCks_id_ciphertext, VCks_id_nonce, [i_aux]);

    const k_id = byteArrayToInteger(k_id_bytes);

    return ZqElement.create(k_id, ZqGroup.sameOrderAs(encryptionGroup));
  }

  return {
    getKey: getKey
  }
})();
