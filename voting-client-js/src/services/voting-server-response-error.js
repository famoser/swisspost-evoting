/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.VotingServerResponseError = void 0;

class VotingServerResponseError extends Error {
	constructor(errorResponse) {
		super("Voting Server response error.");
		this.errorResponse = errorResponse;
		// Set the prototype explicitly.
		Object.setPrototypeOf(this, VotingServerResponseError.prototype);
	}
}
exports.VotingServerResponseError = VotingServerResponseError;
//# sourceMappingURL=illegal_argument_error.js.map