/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */

const {FailedValidationError} = require("./failed_validation_error");
const {checkNotNull, checkArgument} = require("crypto-primitives-ts/lib/cjs/validation/preconditions");
const {CHARACTER_LENGTH_OF_BALLOT_CASTING_KEY} = require("../voter-authentication/constants");

module.exports = (function () {
  'use strict';

  const UUID_LENGTH = 32;
  const BASE64_REGEX = '^(?:[A-Za-z0-9+\/]{4})*(?:[A-Za-z0-9+\/]{3}=|[A-Za-z0-9+\/]{2}={2})$';
  const BASE16_ALPHABET = 'a-fA-F0-9';
  const VALID_XML_TOKEN_PATTERN = '^[\\w\\-]{1,50}$'
  const ACTUAL_VOTING_OPTION_MAX_LENGTH = 50;
  const BASE32_LOWERCASE_ALPHABET_WITHOUT_PADDING = 'a-z2-7';

  /**
   * Validates that the input string is in Base16 alphabet with lowercase and has length of 32.
   * The alphabet corresponds to the uppercase and lowercase version of "Table 5: The Base 16 Alphabet" from RFC3548.
   *
   * @param {string} toValidate, the string to validate. Must be non-null.
   * @throws NullPointerError if the input string is null.
   * @throws FailedValidationError if the input string validation fails.
   * @returns {string} the validated input string.
   */
  function validateUUID(toValidate) {
    checkNotNull(toValidate);
    return validateInAlphabet(toValidate, `^[${BASE16_ALPHABET}]{${UUID_LENGTH}}$`);
  }

  /**
   * Validates that the input string is in Base32 lowercase alphabet excluding padding "=" with the specified length.
   * The alphabet corresponds to the lowercase version excluding padding "=" of "Table 3: The Base 32 Alphabet" from RFC3548.
   *
   * @param {string} toValidate, the string to validate. Must be non-null.
   * @param {number} length, the length of the string to validate. Must be greater than 0.
   * @throws NullPointerError if the input string is null.
   * @throws IllegalArgumentError if the length is smaller than or equal to zero.
   * @throws FailedValidationError if the input string validation fails.
   * @returns {string} the validated input string.
   */
  function validateBase32StringWithoutPadding(toValidate, length) {
    checkNotNull(toValidate);
    checkArgument(length > 0);
    return validateInAlphabet(toValidate, `^[${BASE32_LOWERCASE_ALPHABET_WITHOUT_PADDING}]{${length.toString()}}$`);
  }

  /**
   * Validates that the input string is in Base64 lowercase alphabet including padding "=".
   * The alphabet corresponds to "Table 1: The Base 64 Alphabet" from RFC3548.
   *
   * @param {string} toValidate, the string to validate. Must be non-null.
   * @throws NullPointerError if the input string is null.
   * @throws IllegalArgumentError if the input length is not a multiple of 4.
   * @throws FailedValidationError if the input string validation fails.
   * @returns {string} the validated input string.
   */
  function validateBase64String(toValidate) {
    checkNotNull(toValidate);
    checkArgument(toValidate.length % 4 === 0);
    return validateInAlphabet(toValidate, BASE64_REGEX);
  }

  /**
   * Validates the extended authentication factor. Must be non-null.
   *
   * @param toValidate the extended authentication factor.
   * @returns {string} the validated extended authentication factor.
   */
  function validateExtendedAuthenticationFactor(toValidate) {
    checkNotNull(toValidate);
    return validateInAlphabet(toValidate, "^(\\d{4})(\\d{4})?$");
  }

  /**
   * Validates all actual voting options are a valid xml xs:token.
   * @param {string[]} actualVotingOptions, the list of actual voting options. Must be non-null.
   */
  function validateActualVotingOptions(actualVotingOptions) {
    checkNotNull(actualVotingOptions);
    actualVotingOptions.forEach(actualVotingOption => {
      checkNotNull(actualVotingOption);

      const identifications = actualVotingOption.split("\|");
      checkArgument(identifications.length === 1 || identifications.length === 2,
        "The actual voting option should be either one identification or two identifications concatenated using |.");

      const validXmlToken = new RegExp(VALID_XML_TOKEN_PATTERN, 'gm');

      identifications.forEach(identification => {
        checkArgument(0 < identification.length && identification.length <= ACTUAL_VOTING_OPTION_MAX_LENGTH,
          "The length of each actual voting option must be in between 1 and 50.");
        checkArgument(identification.match(validXmlToken) !== null, "The actual voting options must match a valid xml xs:token.");
      });
    });
  }

  /**
   * Validates a Ballot Casting Key.
   * @param {string} ballotCastingKey, the ballot casting key. Must be non-null.
   */
  function validateBallotCastingKey(ballotCastingKey) {
    checkNotNull(ballotCastingKey);
    checkArgument(ballotCastingKey.length === CHARACTER_LENGTH_OF_BALLOT_CASTING_KEY, "The ballot casting key must have the correct size")
    checkArgument(ballotCastingKey.match("^\\d+$") != null, "The ballot casting key must be a numeric value");
    checkArgument(ballotCastingKey.match("^0+$") == null, "The ballot casting key must contain one non-zero element");
  }


  /**
   * @returns {string}
   */
  const validateInAlphabet = function (toValidate, pattern) {
    checkNotNull(toValidate);
    checkNotNull(pattern);

    const regExp = new RegExp(pattern);
    if (!toValidate.match(regExp)) {
      throw new FailedValidationError(`The given string does not comply with the required format. [string: ${toValidate}, format: ${pattern}].`);
    }
    return toValidate;
  }

  return {
    validateUUID: validateUUID,
    validateBase64String: validateBase64String,
    validateActualVotingOptions: validateActualVotingOptions,
    validateBase32StringWithoutPadding: validateBase32StringWithoutPadding,
    validateExtendedAuthenticationFactor: validateExtendedAuthenticationFactor,
    validateBallotCastingKey: validateBallotCastingKey
  };

})();
