/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
const path = require('path');

module.exports = {
  mode: 'production',
  entry: './src/api.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'ov-api.min.js',
  },
  resolve: {
    fallback: {crypto: false},
  },
  performance: {
    maxAssetSize: 500000,
    maxEntrypointSize: 500000,
  },
  optimization: {
    usedExports: true,
  }
};