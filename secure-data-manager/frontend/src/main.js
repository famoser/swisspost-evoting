/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */

// dependencies
import 'angular';
import 'angular-ui-bootstrap/dist/ui-bootstrap-tpls';
import 'angular-ui-router';
import 'angular-animate';
import 'angular-aria';
import 'angular-gettext/dist/angular-gettext';
import 'angular-material';
import 'angular-promise-extras';
import 'ng-file-upload/dist/ng-file-upload';
import 'oclazyload';

// app
import './app/app';
import './app/3rd-party';
import './app/config-election-constants';

// directives
import './app/directives/datatable-data';
import './app/directives/folder-input';
import './app/directives/sv-sidebar/sv-sidebar';
import './app/directives/tabbar/tabbar';
import './app/directives/toolbar/toolbar';
import './app/directives/ui-bootstrap-collapse';

// filters
import './app/filters/array-join';

// routes
import './app/routes';

// services
import './app/services/active-filters';
import './app/services/database';
import './app/services/dialogs-custom';
import './app/services/endpoints';
import './app/services/errors';
import './app/services/i18n';
import './app/services/navigation-service';
import './app/services/session';
import './app/services/settler';
import './app/services/status-box';
import './app/services/toast-custom';
import './app/services/voting-card-set';
import './app/services/admin-board-activation';

// controllers (not lazy loaded)
import './app/views/dialogs/dialogs';
import './app/views/home-manage/uploadZip';
import './app/views/board-members-passwords/board-members-passwords';
import './app/views/splash/splash';

// styles
import './scss/style.scss';