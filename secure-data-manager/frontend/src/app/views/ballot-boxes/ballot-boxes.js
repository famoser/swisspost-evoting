/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
/*jshint maxparams: 12 */

import confirmMixingTemplate from './confirm-mixing.html';
import boardMembersPasswordsTemplate from "../board-members-passwords/board-members-passwords.html";

export default angular
	.module('ballot-boxes', ['app.dialogs'])
	.controller('ballot-boxes', function (
		$scope,
		$mdDialog,
		$mdToast,
		toastCustom,
		sessionService,
		endpoints,
		$http,
		$q,
		settler,
		CustomDialog,
		gettextCatalog,
		$rootScope,
		ErrorsDict,
		statusBox,
		activeFilters,
		configElectionConstants,
		_,
		adminBoardActivationService
	) {
		'use strict';

		let ballotBoxElectoralBoardId;

		const electionEventIdPattern = '{electionEventId}';
		const ballotBoxIdPattern = '{ballotBoxId}';
		const electoralBoardIdPattern = '{electoralBoardId}';

		const BALLOT_BOX_SELECT_MSG_ID = 'Please select first a Ballot box';
		const BALLOT_BOX_TEST_MSG_ID = 'Test';
		const BALLOT_BOX_REGULAR_MSG_ID = 'Regular';
		const BALLOT_BOX_DOWNLOAD_MSG_ID = 'Ballot box download';
		const BALLOT_BOX_SELECT_DOWNLOAD_MSG_ID = 'Please, select some ballot box(es) to download from the list';
		const BALLOT_BOX_DOWNLOADED_MSG_ID = 'Ballot box(es) downloaded!';
		const BALLOT_BOX_MIX_MSG_ID = 'Ballot box mix';
		const BALLOT_BOX_SELECT_MIX_MSG_ID = 'Please, select some ballot box(es) to be mixed from the list';
		const BALLOT_BOX_MIXED_MSG_ID = 'Mixing started!';
		const BALLOT_BOX_MIX_ERROR_MSG_ID = 'Failed to send ballot box for mixing';
		const BALLOT_BOX_MIX_ERROR_NOT_CLOSED_MSG_ID = 'Ballot box not yet closed and cannot be mixed';
		const BALLOT_BOX_MSG_ID = 'Ballot Box(es)';
		const BALLOT_BOX_DECRYPTING_MSG_ID = 'Ballot Box decrypting';
		const BALLOT_BOX_SELECT_DECRYPTING_MSG_ID = 'Please, select some ballot boxes to decrypt from the list';
		const BALLOT_BOX_SELECT_SINGLE_EA_MSG_ID = 'Please, select ballot boxes for a single electoral authority';
		const BALLOT_BOX_DECRYPTING_COMPLETE_MSG_ID = 'Decrypting complete!';
		const BALLOT_BOX_DECRYPTING_STARTED_MSG_ID = 'Decrypting started';
		const BALLOT_BOX_SIGNING_MSG_ID = 'Ballot Box signing';
		const BALLOT_BOX_SIGNED_MSG_ID = 'Ballot box(es) signed!';
		const ADMIN_BOARD_ACTIVATE_MSG_ID = 'Please, activate the administration board.';
		const ADMIN_BOARD_ACTIVATE_OK_MSG_ID = 'Activate now';
		const TALLY_FILES_MSG_ID = 'Tally files generation';
		const TALLY_FILES_GENERATED_MSG_ID = 'Tally files generation success';
		const TALLY_FILES_GENERATION_STARTED_MSG_ID = 'Tally files generation started';
		const TALLY_FILES_GENERATION_BUTTON_MSG_ID = 'Tally files generation button hint';
		const CONTACT_SUPPORT_MSG_ID = 'Something went wrong. Contact with Support';
		const ERROR_CODE_MSG_ID = 'Error code';

		$scope.ballotBoxMixStatuses = [];
		$scope.isCollapsed = false;
		$scope.uploadInProgress = false;
		$scope.isDecryptInProgress = false;
		$scope.isGenerateTallyFilesInProgress = false;

		// manage status filters
		$scope.filterItem = 'ballotBoxes';
		$scope.filterTabs = [
			configElectionConstants.STATUS_LOCKED,
			configElectionConstants.STATUS_READY,
			configElectionConstants.STATUS_SIGNED,
			configElectionConstants.STATUS_MIXING,
			configElectionConstants.STATUS_MIXED,
			configElectionConstants.STATUS_DOWNLOADED,
			configElectionConstants.STATUS_DECRYPTED
		];
		$scope.onTabSelected = function (filter) {
			$scope.filterActive = filter.text;
			$scope.tableFilter = filter.code;
			activeFilters.setActiveFilter($scope.filterItem, filter);
			$scope.typeCount = $scope.countTestInFilterActive(filter);
			$scope.unselectAll();
		};

		// manage test filter
		$scope.filterBBTest = 'ballotBoxesTest';
		$scope.filterTestTabs = [
			configElectionConstants.TEST_REGULAR,
			configElectionConstants.TEST_TEST,
		];
		$scope.onTestSelected = function (filter) {
			$scope.filterActiveTest = filter.text;
			$scope.tableFilterTest = filter.code;
			activeFilters.setActiveFilter($scope.filterBBTest, filter);
			$scope.typeCount = $scope.countTestInFilterActive(
				activeFilters.getActiveFilter($scope.filterItem),
			);
			$scope.unselectAll();
		};
		$scope.parseFilterActiveTest = function (filter) {
			return filter === 'Test' ? 'true' : 'false';
		};
		$scope.formatDate = function (dateStr) {
			try {
				return new Date(dateStr).toLocaleString('fr-CH', {
					day: '2-digit',
					month: '2-digit',
					year: 'numeric',
					hour: '2-digit',
					minute: '2-digit',
					second: '2-digit',
					timeZone: 'Europe/Zurich'
				});
			} catch (error) {
				return dateStr;
			}
		}
		$scope.countTestInFilterActive = function (filter) {
			const testStatus = {};
			testStatus.false = _.filter($scope.ballotBoxes.result, {
				status: filter.code,
				test: 'false',
			}).length;
			testStatus.true = _.filter($scope.ballotBoxes.result, {
				status: filter.code,
				test: 'true',
			}).length;
			return testStatus;
		};

		// init status filter
		if (!activeFilters.getActiveFilter($scope.filterItem)) {
			activeFilters.setActiveFilter($scope.filterItem, $scope.filterTabs[0]);
		}
		$scope.filterActive = activeFilters.getActiveFilter($scope.filterItem).text;
		$scope.tableFilter = activeFilters.getActiveFilter($scope.filterItem).code;

		// init test filter
		if (!activeFilters.getActiveFilter($scope.filterBBTest)) {
			activeFilters.setActiveFilter(
				$scope.filterBBTest,
				$scope.filterTestTabs[0],
			);
		}
		$scope.filterActiveTest = activeFilters.getActiveFilter(
			$scope.filterBBTest,
		).text;
		$scope.tableFilterTest = activeFilters.getActiveFilter(
			$scope.filterBBTest,
		).code;

		$scope.selectBallotBoxText = '';

		$scope.listBallotBoxes = function () {
			$scope.errors.ballotBoxesFailed = false;

			const ballotBoxesURL = endpoints.host() + endpoints.ballotboxes
				.replace(electionEventIdPattern, $scope.selectedElectionEventId);

			$http.get(ballotBoxesURL).then(
				function (response) {
					const data = response.data;
					try {
						$scope.ballotBoxes = data;

						// Keep ballot boxes with MIXING_ERROR in the MIXING tab.
						$scope.ballotBoxes.result.forEach(function (ballotBox) {
							if (ballotBox.status === 'MIXING_ERROR') {
								ballotBox.status = 'MIXING';
								$scope.ballotBoxMixStatuses[ballotBox.id] = 'ERROR';
							}
						});

						$scope.statusCount = _.countBy(data.result, 'status');
						$scope.typeCount = $scope.countTestInFilterActive(
							activeFilters.getActiveFilter($scope.filterItem),
						);
					} catch (e) {
						$scope.data.message = e.message;
						$scope.errors.ballotBoxesFailed = true;
						$scope.statusCount = null;
						$scope.typeCount = null;
					}
				},
				function () {
					$scope.errors.ballotBoxesFailed = true;
					$scope.statusCount = null;
					$scope.typeCount = null;
				},
			);
		};

		// download ballotBox(es)
		// -------------------------------------------------------------

		const showBallotBoxDownloadError = function () {
			new CustomDialog()
				.title(gettextCatalog.getString(BALLOT_BOX_DOWNLOAD_MSG_ID))
				.error()
				.show();
		};

		$scope.downloadBallotBox = function (ev) {
			$scope.errors.ballotBoxDownloadError = false;

			// collect ballot box to download

			const ballotBoxesToDownload = [];
			const ballotBoxesSelected = [];
			$scope.ballotBoxes.result.forEach(function (ballotBox) {
				if (ballotBox.selected && ballotBox.status === 'MIXED') {
					ballotBoxesToDownload.push(ballotBox);
				} else if (
					ballotBox.selected &&
					ballotBox.status === 'SIGNED' &&
					ballotBox.synchronized === 'true' &&
					ballotBox.test === 'true'
				) {
					ballotBoxesToDownload.push(ballotBox);
				}
				if (ballotBox.selected) {
					ballotBoxesSelected.push(ballotBox);
				}
			});

			// no selection?

			if (ballotBoxesSelected.length <= 0) {
				$mdDialog.show(
					$mdDialog.customAlert({
						locals: {
							title: gettextCatalog.getString(BALLOT_BOX_DOWNLOAD_MSG_ID),
							content: gettextCatalog.getString(BALLOT_BOX_SELECT_DOWNLOAD_MSG_ID),
						},
					}),
				);
				return;
			}

			if (ballotBoxesToDownload.length <= 0) {
				new CustomDialog()
					.title(gettextCatalog.getString(BALLOT_BOX_DOWNLOAD_MSG_ID))
					.cannotPerform(gettextCatalog.getString(BALLOT_BOX_MSG_ID))
					.show();
				return;
			}

			// download individual ballot box

			$scope.ballotBoxDownloadResults = [];
			$scope.ballotBoxDownloadError = false;

			$q.allSettled(
				ballotBoxesToDownload.map(function (ballotBox) {
					const url = (endpoints.host() + endpoints.downloadBallotBox)
						.replace(electionEventIdPattern, $scope.selectedElectionEventId)
						.replace(ballotBoxIdPattern, ballotBox.id);
					return $http.get(url);
				})
			).then(function (responses) {
				const settled = settler.settle(responses);
				if (settled.ok) {
					$scope.ballotBoxDownloadResults = settled.fulfilled.map(function (
						response,
					) {
						return response.data.result;
					});
					if (!$scope.checkOkBecauseExtraFastClick(settled.fulfilled)) {
						$mdToast.show(
							toastCustom.topCenter(
								gettextCatalog.getString(BALLOT_BOX_DOWNLOADED_MSG_ID),
								'success',
							),
						);
					}
				}
				if (settled.error) {
					$scope.errors.ballotBoxDownloadError = true;
					showBallotBoxDownloadError();
				}
				$scope.listBallotBoxes();
				$scope.unselectAll();
			});
		};

		$scope.uploadConfigurations = function () {
			$scope.uploadInProgress = true;
			$http.post(endpoints.host() + endpoints.uploadConfigurations.replace(
				electionEventIdPattern, sessionService.getSelectedElectionEvent().id)
			).then(function (_response) {
				$rootScope.$broadcast('refresh-ballots');
				$rootScope.$broadcast('refresh-voting-card-sets');
				$rootScope.$broadcast('refresh-authorities');
				$rootScope.$broadcast('refresh-ballot-boxes');
				$mdToast.show(
					toastCustom.topCenter(
						gettextCatalog.getString('Data uploaded successfully'),
						'success'
					)
				);
				$scope.uploadInProgress = false;
			}, function (_error) {
				$mdToast.show(
					toastCustom.topCenter(
						gettextCatalog.getString('Error while uploading configuration.'),
						'error'
					)
				);
				$scope.uploadInProgress = false;
			});
		}

		$scope.isOnline = function () {
			return sessionService.isOnlineMode();
		};

		function mixBallotBox(url, ballotBoxId) {
			$http.put(url).then(function (response) {
				$scope.mixingOnGoing = false;
				$scope.ballotBoxMixStatuses[ballotBoxId] = 'PROCESSING';

				$scope.listBallotBoxes();
				$scope.unselectAll();
			}, function (response) {
				$scope.mixingOnGoing = false;
				$scope.ballotBoxMixStatuses[ballotBoxId] = 'ERROR';

				if (response.status === 409) {
					$mdToast.show(
						toastCustom.topCenter(
							gettextCatalog.getString(BALLOT_BOX_MIX_ERROR_NOT_CLOSED_MSG_ID),
							'error',
						),
					);
				} else {
					$mdToast.show(
						toastCustom.topCenter(
							gettextCatalog.getString(BALLOT_BOX_MIX_ERROR_MSG_ID),
							'error',
						),
					);
				}
			});
		}

		$scope.mixBallotBoxes = function () {
			$scope.mixingOnGoing = true;

			// Collect ballot box to mix.
			const ballotBoxesToMix = [];
			const ballotBoxesSelected = [];
			$scope.ballotBoxes.result.forEach(function (ballotBox) {
				if ($scope.ballotBoxMixStatuses[ballotBox.id] !== 'MIXING') {
					if (ballotBox.selected && ballotBox.status === 'SIGNED' && ballotBox.synchronized === 'true') {
						ballotBoxesToMix.push(ballotBox.id);
					}
					if (ballotBox.selected) {
						ballotBoxesSelected.push(ballotBox);
					}
				}
			});

			// no selection?

			if (ballotBoxesSelected.length <= 0) {
				$mdDialog.show(
					$mdDialog.customAlert({
						locals: {
							title: gettextCatalog.getString(BALLOT_BOX_MIX_MSG_ID),
							content: gettextCatalog.getString(BALLOT_BOX_SELECT_MIX_MSG_ID),
						},
					}),
				);
				$scope.mixingOnGoing = false;
				return;
			}

			if (ballotBoxesToMix.length <= 0) {
				new CustomDialog()
					.title(gettextCatalog.getString(BALLOT_BOX_MIX_MSG_ID))
					.cannotPerform(gettextCatalog.getString(BALLOT_BOX_MSG_ID))
					.show();
				$scope.mixingOnGoing = false;
				return;
			}

			ballotBoxesToMix.forEach(ballotBoxId => {
				const url = (endpoints.host() + endpoints.mixingOnline)
					.replace(electionEventIdPattern, $scope.selectedElectionEventId)
					.replace(ballotBoxIdPattern, ballotBoxId);

				mixBallotBox(url, ballotBoxId);
			});
			$mdToast.show(
				toastCustom.topCenter(
					gettextCatalog.getString(BALLOT_BOX_MIXED_MSG_ID),
					'success',
				),
			);
		};

		$scope.getMixingStatus = function (ballotBoxId) {
			return $scope.ballotBoxMixStatuses[ballotBoxId];
		};

		$scope.dialogController = function () {
			$scope.hide = function () {
				$mdDialog.hide();
			};
			$scope.cancel = function () {
				$mdDialog.cancel();
			};
			$scope.answer = function (answer) {
				$mdDialog.hide(answer);
			};
		}

		$scope.confirmMixing = function (ev) {
			$scope.inputMixingConfirmationCode = '';

			// Generate a simple 5-digit confirmation code.
			$scope.expectedMixingConfirmationCode = (new Date().getMilliseconds().toString() + new Date().getSeconds().toString()).padStart(5, "0");

			$mdDialog.show({
				controller: $scope.dialogController,
				template: confirmMixingTemplate,
				parent: angular.element(document.body),
				targetEvent: ev,
				clickOutsideToClose: false,
				escapeToClose: false,
				scope: $scope,
				preserveScope: true
			}).then(function () {
				if ($scope.expectedMixingConfirmationCode === $scope.inputMixingConfirmationCode) {
					$scope.mixBallotBoxes();
				}
			}, function () {
				$scope.inputMixingConfirmationCode = '';
			});
		};

		$scope.updateMixingStatuses = function () {
			const ballotBoxesMixing = [];
			$scope.ballotBoxes.result.forEach(function (ballotBox) {
				if (ballotBox.status === 'MIXING') {
					ballotBoxesMixing.push(ballotBox.id);
				}
			});

			ballotBoxesMixing.forEach(ballotBoxId => {
				const url = (endpoints.host() + endpoints.updateMixingStatus)
					.replace(electionEventIdPattern, $scope.selectedElectionEventId)
					.replace(ballotBoxIdPattern, ballotBoxId);

				updateMixingStatus(url);
			});
		}

		function updateMixingStatus(url) {
			$http.put(url).then(function () {
				$scope.listBallotBoxes();
				$scope.unselectAll();
			});
		}

		$scope.unselectAll = function () {
			$scope.selectAll = false;
			$scope.ballotBoxes.result.forEach(function (ballotBox) {
				ballotBox.selected = false;
			});
		};

		$scope.onSelectAll = function (value) {
			$scope.ballotBoxes.result.forEach(function (ballotBox) {
				const status = activeFilters.getActiveFilter($scope.filterItem).code;
				const test = activeFilters.getActiveFilter($scope.filterBBTest).code;
				if (ballotBox.status === status && ballotBox.test === test) {
					ballotBox.selected = value;
				}
			});
		};

		$scope.updateSelectAll = function (value) {
			if (!value) {
				$scope.selectAll = false;
			}
		};

		$scope.$on('refresh-ballot-boxes', function () {
			$scope.listBallotBoxes();
			$scope.selectAll = false;
		});

		$scope.decryptBallotBoxes = function () {
			// collect ballot boxes to decrypt
			const boxesToDecrypt = [];

			$scope.ballotBoxes.result.forEach(function (ballotBox) {
				if (ballotBox.selected && ballotBox.status === 'DOWNLOADED') {
					boxesToDecrypt.push(ballotBox);
				}
			});

			// no selection?

			if (boxesToDecrypt.length === 0) {
				$mdDialog.show(
					$mdDialog.customAlert({
						locals: {
							title: gettextCatalog.getString(BALLOT_BOX_DECRYPTING_MSG_ID),
							content: gettextCatalog.getString(BALLOT_BOX_SELECT_DECRYPTING_MSG_ID),
						},
					}),
				);
				return;
			}

			// all from the same authority?

			ballotBoxElectoralBoardId = boxesToDecrypt[0].electoralBoard.id;
			let same = true;
			boxesToDecrypt.forEach(function (ballotBox) {
				same = same && ballotBox.electoralBoard.id === ballotBoxElectoralBoardId;
			});
			if (!same) {
				$mdDialog.show(
					$mdDialog.customAlert({
						locals: {
							title: gettextCatalog.getString(BALLOT_BOX_DECRYPTING_MSG_ID),
							content: gettextCatalog.getString(BALLOT_BOX_SELECT_SINGLE_EA_MSG_ID),
						},
					}),
				);
				return;
			}

			if (!adminBoardActivationService.isActivated()) {
				const p = $mdDialog.show(
					$mdDialog.customConfirm({
						locals: {
							title: gettextCatalog.getString(BALLOT_BOX_DECRYPTING_MSG_ID),
							content: gettextCatalog.getString(ADMIN_BOARD_ACTIVATE_MSG_ID),
							ok: gettextCatalog.getString(ADMIN_BOARD_ACTIVATE_OK_MSG_ID),
						},
					}),
				);
				p.then(
					function () {
						adminBoardActivationService.activate($scope, 'tally').then(() => activateElectoralBoard(boxesToDecrypt));
					},
					function (error) {
						//Not possible to open the $mdDialog
					},
				);

			} else {
				activateElectoralBoard(boxesToDecrypt);
			}
		};

		function activateElectoralBoard(boxesToDecrypt) {
			const electoralBoards = sessionService.getElectoralBoards();
			const electoralBoard = electoralBoards.find(el => el.id === ballotBoxElectoralBoardId);

			$scope.bmpConfig = {
				membersPasswordsMode: 'activation',
				selectedBoard: electoralBoard,
				selectedBoardMembers: electoralBoard.boardMembers,
				membersPasswordsThreshold: electoralBoard.boardMembers.length,
				membersPasswordsValidationUrl: (endpoints.host() + endpoints.electoralBoardPasswordValidate)
					.replace(electionEventIdPattern, $scope.selectedElectionEventId)
					.replace(electoralBoardIdPattern, electoralBoard.id),
				boardType: 'electoral'
			};

			$mdDialog.show({
				controller: 'boardMembersPasswordsController',
				controllerAs: 'ctrl',
				template: boardMembersPasswordsTemplate,
				parent: angular.element(document.body),
				clickOutsideToClose: false,
				escapeToClose: false,
				preserveScope: true,
				scope: $scope
			}).then(
				(membersPasswords) => {
					if (membersPasswords.length === electoralBoard.boardMembers.length) {
						const passwords = [];
						membersPasswords.forEach(pwd => passwords.push([...pwd]));
						$scope.membersPasswords = [];
						mixOffline(boxesToDecrypt, passwords);
					}
				},
				() => null
			);
		}

		function mixOffline(boxesToDecrypt, passwords) {
			$scope.isDecryptInProgress = true;
			$q.allSettled(
				boxesToDecrypt.map(function (ballotBox) {
					const url =
						endpoints.host() +
						endpoints.mixingOffline
							.replace(electionEventIdPattern, $scope.selectedElectionEventId)
							.replace(ballotBoxIdPattern, ballotBox.id);
					return $http.put(url, passwords);
				}),
			).then(function (responses) {
				const settled = settler.settle(responses);
				if (settled.ok) {
					if (!$scope.checkOkBecauseExtraFastClick(settled.fulfilled)) {
						$mdToast.show(
							toastCustom.topCenter(
								gettextCatalog.getString(BALLOT_BOX_DECRYPTING_COMPLETE_MSG_ID),
								'success',
							),
						);
					}
				}
				if (settled.error) {
					new CustomDialog()
						.title(gettextCatalog.getString(BALLOT_BOX_DECRYPTING_MSG_ID))
						.error()
						.show();
				}
				$scope.listBallotBoxes();
				$scope.isDecryptInProgress = false;
			});

			$mdToast.show(
				toastCustom.topCenter(
					gettextCatalog.getString(BALLOT_BOX_DECRYPTING_STARTED_MSG_ID),
					'success',
				),
			);
			$scope.unselectAll();
		}

		function capitalizeWord(string) {
			const lowerCaseString = string.toLowerCase();
			return lowerCaseString.charAt(0).toUpperCase() + lowerCaseString.slice(1);
		}

		$scope.capitalizeFirstLetter = function (string) {
			const words = string.split('_');
			const out = [];
			angular.forEach(words, function (word) {
				out.push(capitalizeWord(word));
			});
			return out.join(' ');
		};

		const showSigningError = function () {
			new CustomDialog()
				.title(gettextCatalog.getString(BALLOT_BOX_SIGNING_MSG_ID))
				.error()
				.show();
		};


		$scope.signBallotBoxes = function () {
			const ballotBoxesToSign = [];
			$scope.ballotBoxes.result.forEach(function (bb) {
				if (bb.status === 'READY') {
					ballotBoxesToSign.push(bb);
				}
			});

			if (ballotBoxesToSign.length <= 0) {
				new CustomDialog()
					.title(gettextCatalog.getString(BALLOT_BOX_SIGNING_MSG_ID))
					.cannotPerform(gettextCatalog.getString(BALLOT_BOX_MSG_ID))
					.show();
				return;
			}

			if (!adminBoardActivationService.isActivated()) {
				const p = $mdDialog.show(
					$mdDialog.customConfirm({
						locals: {
							title: gettextCatalog.getString(BALLOT_BOX_SIGNING_MSG_ID),
							content: gettextCatalog.getString(ADMIN_BOARD_ACTIVATE_MSG_ID),
							ok: gettextCatalog.getString(ADMIN_BOARD_ACTIVATE_OK_MSG_ID),
						},
					}),
				);
				p.then(
					function () {
						adminBoardActivationService.activate($scope, 'setup').then(() => $scope.signBallotBoxes());
					},
					function (error) {
						//Not possible to open the $mdDialog
					},
				);
				return;
			}

			$q.allSettled(
				ballotBoxesToSign.map(function (bb) {
					const url = (endpoints.host() + endpoints.ballotBoxSign)
						.replace(electionEventIdPattern, $scope.selectedElectionEventId)
						.replace(ballotBoxIdPattern, bb.id);

					return $http.put(url);
				}),
			).then(function (responses) {
				const settled = settler.settle(responses);
				$scope.ballotBoxesResults = settled.fulfilled;

				if (settled.ok) {
					$mdToast.show(
						toastCustom.topCenter(
							gettextCatalog.getString(BALLOT_BOX_SIGNED_MSG_ID),
							'success',
						),
					);
					$scope.listBallotBoxes();
				}
				if (settled.error) {
					showSigningError();
				}
			});

			$scope.unselectAll();
		};

		$scope.areAllBallotBoxesSigned = function () {
			return $scope.ballotBoxes.result.every(bb => bb.status === 'SIGNED');
		};

		$scope.areSomeBallotBoxesSigned = function () {
			return $scope.ballotBoxes.result.some(bb => bb.status === 'SIGNED');
		};

		$scope.areAllBallotBoxesReady = function () {
			return $scope.ballotBoxes.result.every(bb => bb.status === 'READY');
		};

		$scope.areAllBallotBoxesUploaded = function() {
			return $scope.ballotBoxes.result.every(bb => bb.synchronized === 'true');
		}

		$scope.areSomeBallotBoxesMixing = function () {
			return $scope.ballotBoxes.result.some(bb => bb.status === 'MIXING');
		};

		$scope.isThereNoBallotBoxSelected = function () {
			let generatedBallotBoxSelected = false;

			if ($scope.ballotBoxes) {
				$scope.ballotBoxes.result.forEach(function (bb) {
					if (bb.selected) {
						generatedBallotBoxSelected = true;
					}
				});
			}

			return !generatedBallotBoxSelected;
		};

		$scope.isSetupMode = function () {
			return sessionService.isSetupMode();
		};

		$scope.isOnlineMode = function () {
			return sessionService.isOnlineMode();
		};

		$scope.isTallyMode = function () {
			return sessionService.isTallyMode();
		}

		$scope.getTextByBallotBoxSelected = function () {
			const selectBallotBoxText = gettextCatalog.getString(BALLOT_BOX_SELECT_MSG_ID);
			const check = $scope.isThereNoBallotBoxSelected();
			return check ? selectBallotBoxText : '';
		};

		$scope.getTextTallyFilesGeneration = function () {
			const text = gettextCatalog.getString(TALLY_FILES_GENERATION_BUTTON_MSG_ID);
			const check = $scope.areSomeBallotBoxesReadyForTallyFilesGeneration();
			return check ? '' : text;
		};

		/**
		 * Check if the responses might be ok because a extra fast double click in  an action
		 * @param responses
		 * @returns {boolean}
		 */
		$scope.checkOkBecauseExtraFastClick = function (responses) {
			responses.forEach(function (r) {
				if (r.idle) {
					return true;
				}
			});
			return false;
		};

		/*
			ballot test get descriptiontext
		   */
		$scope.getBallotBoxTypeDesc = function (val) {
			const ballotBoxTestDesc = gettextCatalog.getString(BALLOT_BOX_TEST_MSG_ID);
			const ballotBoxRegularDesc = gettextCatalog.getString(BALLOT_BOX_REGULAR_MSG_ID);
			return val === 'true' ? ballotBoxTestDesc : ballotBoxRegularDesc;
		};

		$scope.areSomeBallotBoxesReadyForTallyFilesGeneration = function () {
			const isDecryptedAndSynchronized = (box) => box.status === 'DECRYPTED' && box.synchronized === 'true';
			if ($scope.ballotBoxes) {
				return $scope.ballotBoxes.result.some(isDecryptedAndSynchronized);
			}
			return false;
		};

		$scope.generateTallyFiles = function () {

			$mdToast.show(toastCustom.topCenter(gettextCatalog.getString(TALLY_FILES_GENERATION_STARTED_MSG_ID), 'success'));

			const url =
				endpoints.host() +
				endpoints.generateTallyFiles
					.replace(electionEventIdPattern, $scope.selectedElectionEventId);

			$scope.isGenerateTallyFilesInProgress = true;

			$http
				.post(url)
				.then(function () {
					$mdToast.show(toastCustom.topCenter(gettextCatalog.getString(TALLY_FILES_GENERATED_MSG_ID), 'success'));
					$scope.isGenerateTallyFilesInProgress = false;
				})
				.catch(function (e) {
					$scope.isGenerateTallyFilesInProgress = false;
					$mdToast.show(
						toastCustom.topCenter(
							`${gettextCatalog.getString(TALLY_FILES_MSG_ID)}: 
							${gettextCatalog.getString(CONTACT_SUPPORT_MSG_ID)}. ${gettextCatalog.getString(ERROR_CODE_MSG_ID)}: 
							${e.data.error}, ${gettextCatalog.getString(ErrorsDict(e.data.error))}`, 'error'
						)
					);
				});
		}

//initialize && populate view
// -------------------------------------------------------------

		$scope.alert = '';
		$scope.errors = {};
		$scope.selectedElectionEventId = sessionService.getSelectedElectionEvent().id;
		$scope.listBallotBoxes();
		$scope.statusBox = statusBox;
	})
;
