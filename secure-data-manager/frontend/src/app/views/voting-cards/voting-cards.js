/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
/*jshint maxparams: 13 */
/*jshint maxlen: 1800 */
export default angular
	.module('voting-cards', [])
	.controller('voting-cards', function (
		$scope,
		$rootScope,
		$mdDialog,
		$mdToast,
		toastCustom,
		sessionService,
		endpoints,
		$http,
		$q,
		settler,
		$timeout,
		votingCardSetService,
		CustomDialog,
		gettextCatalog,
		ErrorsDict,
		statusBox,
		activeFilters,
		configElectionConstants,
		_,
		adminBoardActivationService
	) {
		'use strict';

		const refreshVotingCardSetsState = 'refresh-voting-card-sets';
		const electionEventIdPattern = '{electionEventId}';
		const votingCardSetIdPattern = '{votingCardSetId}';

		const VOTING_CARDS_SELECTION_MSG_ID = 'Voting cards selection';

		const PRINT_FILES_MSG_ID = 'Print file generation';
		const PRINT_FILES_GENERATED_MSG_ID = 'Print file generation success';
		const PRINT_FILES_GENERATION_STARTED_MSG_ID = 'Print file generation started';
		const PRINT_FILES_GENERATION_IN_PROGRESS_MSG_ID = 'Print file generation in progress';
		const PRINT_FILES_GENERATION_BUTTON_MSG_ID = 'Print file generation button hint';

		const CONTACT_SUPPORT_MSG_ID = 'Something went wrong. Contact with Support';
		const ERROR_CODE_MSG_ID = 'Error code';


		$scope.printFilesGenerationStatus = [];
		$scope.isCollapsed = false;
		$scope.signInProgress = false;
		$scope.uploadInProgress = false;
		$scope.isPrecomputeInProgress = false;
		$scope.isDownloadInProgress = false;
		$scope.isBallotsFetchingInProgress = false;

		// manage status filters
		$scope.filterItem = 'votingCards';
		$scope.filterTabs = [
			configElectionConstants.STATUS_LOCKED,
			configElectionConstants.STATUS_PRECOMPUTED,
			configElectionConstants.STATUS_COMPUTING,
			configElectionConstants.STATUS_COMPUTED,
			configElectionConstants.STATUS_VCS_DOWNLOADED,
			configElectionConstants.STATUS_GENERATING,
			configElectionConstants.STATUS_GENERATED,
			configElectionConstants.STATUS_SIGNED,
		];
		$scope.onTabSelected = function (filter) {
			$scope.filterActive = filter.text;
			$scope.tableFilter = filter.code;
			activeFilters.setActiveFilter($scope.filterItem, filter);
			$scope.unselectAll();
		};

		// init status filter
		if (!activeFilters.getActiveFilter($scope.filterItem)) {
			activeFilters.setActiveFilter($scope.filterItem, $scope.filterTabs[0]);
		}
		$scope.filterActive = activeFilters.getActiveFilter($scope.filterItem).text;
		$scope.tableFilter = activeFilters.getActiveFilter($scope.filterItem).code;

		$scope.data = {};
		$scope.ballots = {};

		/**
		 * Out of the selected voting card sets, gather a list of the ones in a
		 * particular state, and warn if the gathering does not succeed
		 * completely.
		 *
		 * @param {string} status the status the selected voting card sets must
		 *                        be in, in order to be selected
		 *
		 * @returns a promise, successful if there are chosen voting card sets.
		 */
		const findVotingCardSetsWithStatus = function (status) {
			const deferred = $q.defer();

			const chosenVotingCardSets = [];
			// Put all selected voting cards with the selected status in a list.
			$scope.data.votingCardSets.result.forEach(function (votingCardSet) {
				if (votingCardSet.status === status) {
					chosenVotingCardSets.push(votingCardSet);
				}
			});

			if (chosenVotingCardSets.length < 1) {
				new CustomDialog()
					.title(gettextCatalog.getString(VOTING_CARDS_SELECTION_MSG_ID))
					.cannotPerform(gettextCatalog.getString('Voting Card Set(s)'))
					.show()
					.then(null, function () {
						deferred.reject('No chosen voting card sets');
					});
			} else {
				deferred.resolve(chosenVotingCardSets);
			}

			return deferred.promise;
		};

		const showVotingCardSetTransitionError = function () {
			$mdDialog.show(
				$mdDialog.customAlert({
					locals: {
						title: gettextCatalog.getString('Voting Card Set Error'),
						content: gettextCatalog.getString(
							'Current operation failed for some Voting Card Set(s), please review your list',
						),
					},
				}),
			);
		};

		/**
		 * Run the computation processes to transition.
		 *
		 * @param {string} endpoint the endpoint of the operation (precompute, compute or download).
		 * @param {array} votingCardSets the voting card sets to transition.
		 * @param {string} newStatus the new status to assign.
		 * @param {object} options additional options for the call.
		 * @return a promise resolved once all voting card sets have been processed.
		 */
		const transitionVotingCardSets = function (
			endpoint,
			votingCardSets,
			newStatus,
			options,
		) {
			// Start precomputation for each of the voting card sets.
			votingCardSets.forEach(function (votingCardSet) {
				// Set 'in progress' status.
				votingCardSet.processing = true;
				votingCardSetService
					.changeStatus(endpoint, votingCardSet, options)
					.then(
						function () {
							// Remove the processing flag.
							delete votingCardSet.processing;
							// Set the voting card set's new status
							votingCardSet.status = newStatus;
							// Unselect the finished voting card set, which has
							// the desired side effect of updating the view.
							votingCardSet.selected = false;
							// Refresh VCs list after status change to update tab counters
							$rootScope.$broadcast(refreshVotingCardSetsState);

							$scope.isDownloadInProgress = false;
						},
						function () {
							// Remove the processing flag.
							delete votingCardSet.processing;
							// Show error message
							showVotingCardSetTransitionError();

							$scope.isDownloadInProgress = false;
						},
					);
			});
		};

		/**
		 * Run the pre-computation.
		 *
		 * @param {string} electionEventId the election event id.
		 * @param {array} votingCardSets the voting card sets to pre-compute.
		 * @return a promise resolved once all voting card sets have been processed.
		 */
		const votingCardSetsPreComputation = function (
			electionEventId,
			votingCardSets,
		) {

			$scope.isPrecomputeInProgress = true;

			const adminBoard = sessionService.getActivatedAdminBoard();
			const options = {
				adminBoardId: adminBoard.id
			};

			votingCardSets.forEach(function (votingCardSet) {
				votingCardSet.processing = true;
			});

			// Precompute the voting card sets.
			votingCardSetService.precompute(electionEventId, options).then(
				function () {
					votingCardSets.forEach(function (votingCardSet) {
						// Remove the processing flag.
						delete votingCardSet.processing;
					});
					// Refresh VCs list after status change to update tab counters
					$scope.listVotingCardSets();

					$scope.isPrecomputeInProgress = false;
				},
				function () {
					// Remove the processing flag.
					votingCardSets.forEach(function (votingCardSet) {
						delete votingCardSet.processing;
					});
					// Refresh VCs list after status change to update tab counters
					$scope.listVotingCardSets();
					// Show error message
					showVotingCardSetTransitionError();

					$scope.isPrecomputeInProgress = false;
				},
			)
		};

		$scope.listVotingCardSets = function () {
			$scope.errors.votingCardSetsFailed = false;
			const url =
				endpoints.host() +
				endpoints.votingCardSets.replace(
					electionEventIdPattern,
					$scope.selectedElectionEventId,
				);
			$http.get(url).then(
				function (response) {
					const data = response.data;
					try {
						$scope.data.votingCardSets = data;
						$scope.statusCount = _.countBy(data.result, 'status');
					} catch (e) {
						$scope.data.message = e.message;
						$scope.errors.votingCardSetsFailed = true;
						$scope.statusCount = null;
					}
				},
				function () {
					$scope.errors.votingCardSetsFailed = true;
					$scope.statusCount = null;
				},
			);
		};

		const showVotingCardsGenerationError = function () {
			new CustomDialog()
				.title(gettextCatalog.getString('Voting cards generation'))
				.error()
				.show();
		};

		$scope.precomputeVotingCardSets = function (ev) {
			// Get all relevant voting card sets.
			const votingCardSets = $scope.getLockedVotingCardSets();
			const electionEventId = sessionService.getSelectedElectionEvent().id;

			// Ensure the admin board is active.
			if (!adminBoardActivationService.isActivated()) {
				// It is not, ask the user to activate it.
				$mdDialog
					.show(
						$mdDialog.customConfirm({
							locals: {
								title: gettextCatalog.getString('Precomputation'),
								content: gettextCatalog.getString(
									'Please, activate the administration board.',
								),
								ok: gettextCatalog.getString('Activate now'),
							},
						}),
					)
					.then(() => adminBoardActivationService.activate($scope, 'setup')
						.then(() => votingCardSetsPreComputation(electionEventId, votingCardSets)));
			} else {
				votingCardSetsPreComputation(electionEventId, votingCardSets);
			}
		};

		$scope.getLockedVotingCardSets = function () {
			if ($scope.data && $scope.data.votingCardSets) {
				return $scope.data.votingCardSets.result.filter(vcs => vcs.status === 'LOCKED');
			} else {
				return [];
			}
		};

		$scope.areAllBallotsUpdated = function () {
			if ($scope.isBallotsFetchingInProgress) {
				return false;
			}

			if(Object.keys($scope.ballots).length !== 0) {
				return $scope.isEveryBallotUpdated($scope.ballots);
			}

			$scope.isBallotsFetchingInProgress = true;

			const url =
				endpoints.host() +
				endpoints.ballots.replace(
					electionEventIdPattern,
					$scope.selectedElectionEventId,
				);
			$http.get(url).then(
				function (response) {
					$scope.ballots = response.data.result;
					$scope.isBallotsFetchingInProgress = false;
					return $scope.isEveryBallotUpdated($scope.ballots);
				},
				function (error) {
					// By default, in error case, we assume the ballots are updated.
					$scope.isBallotsFetchingInProgress = false;
					return true;
				},
			);
		};

		$scope.isEveryBallotUpdated = function (ballots) {
			return ballots.every(ballot => ballot.status === 'UPDATED')
		};

		$scope.precomputeInProgress = function () {
			if ($scope.data && $scope.data.votingCardSets) {
				return $scope.data.votingCardSets.result.some(vcs => vcs.processing === true);
			} else {
				return false;
			}
		};

		$scope.someVotingCardSetsPrecomputed = function () {
			if ($scope.data && $scope.data.votingCardSets) {
				return $scope.data.votingCardSets.result.some(vcs => vcs.status === 'PRECOMPUTED');
			} else {
				return false;
			}
		};

		$scope.allVotingCardSetsGenerated = function () {
			return $scope.data.votingCardSets.result.every(vcs => vcs.status === 'GENERATED');
		};

		$scope.someVotingCardSetsComputed = function () {
			return $scope.data.votingCardSets.result.some(vcs => vcs.status === 'COMPUTED');
		};

		$scope.allVotingCardSetsDownloaded = function () {
			return $scope.data.votingCardSets.result.every(vcs => vcs.status === 'VCS_DOWNLOADED');
		};

		$scope.computeVotingCardSet = function (ev) {
			findVotingCardSetsWithStatus('PRECOMPUTED').then(function (
				votingCardSets,
			) {
				// Launch computation.
				transitionVotingCardSets(endpoints.votingCardSetCompute, votingCardSets, 'COMPUTING');
			});
		};

		$scope.downloadVotingCardSet = function (ev) {
			findVotingCardSetsWithStatus('COMPUTED').then(function (votingCardSets) {
				$scope.isDownloadInProgress = true;
				// Start downloading.
				transitionVotingCardSets(endpoints.votingCardSetDownload, votingCardSets, 'VCS_DOWNLOADED');
			});
		};

		$scope.customFilter = function (votingCardSet) {
			const selectedTab = $scope.tableFilter;
			const status = votingCardSet.status;
			return (
				status === selectedTab
			);
		};

		$scope.generateVotingCardSet = function (ev) {
			$scope.errors = {};

			findVotingCardSetsWithStatus('VCS_DOWNLOADED').then(function (
				votingCardsToGenerate,
			) {
				// generate the voting cards
				$scope.generateVotingCardSetError = false;


				let errorAlreadyShown = false;

				$q.allSettled(
					votingCardsToGenerate.map(function (votingCardSet) {
						const url = endpoints.votingCardSet
							.replace(electionEventIdPattern, $scope.selectedElectionEventId)
							.replace(votingCardSetIdPattern, votingCardSet.id);

						return $http.post(endpoints.host() + url).then(
							function success(data) {
								return data;
							},
							function error() {
								$scope.errors.generateVotingCardSetError = true;
								if (!errorAlreadyShown) {
									errorAlreadyShown = true;
									showVotingCardsGenerationError();
								}
							},
						);
					}),
				).then(() => {
					$scope.listVotingCardSets();
					$mdToast.show(
						toastCustom.topCenter(
							gettextCatalog.getString('Voting cards submitted for generation...'),
							'success'
						)
					);
				});
				$scope.unselectAll();
			});
		};

		$scope.updateGeneratingStatus = function () {
			$scope.listVotingCardSets();
			$mdToast.show(
				toastCustom.topCenter(
					gettextCatalog.getString('Voting cards generation status updated.'),
					'success'
				)
			);
		};

		$scope.updateComputingStatus = function () {
			$http.post(endpoints.host() + endpoints.votingCardSetComputationStatus
				.replace(electionEventIdPattern, $scope.selectedElectionEventId)
			).then(function (_responses) {
				$scope.listVotingCardSets();
				$mdToast.show(
					toastCustom.topCenter(
						gettextCatalog.getString('Voting cards computation status updated.'),
						'success'
					))
			});
		};

		$scope.$on(refreshVotingCardSetsState, function () {
			$scope.listVotingCardSets();
			$scope.selectAll = false;
		});

		$scope.capitalizeFirstLetter = function (string) {
			const lowerCaseString = string.toLowerCase();
			return lowerCaseString.charAt(0).toUpperCase() + lowerCaseString.slice(1);
		};

		$scope.signVotingCardSets = function () {
			$scope.signInProgress = true
			const electionEventId = sessionService.getSelectedElectionEvent().id;

			// Ensure the admin board is active.
			if (!adminBoardActivationService.isActivated()) {
				// It is not, ask the user to activate it.
				$mdDialog
					.show(
						$mdDialog.customConfirm({
							locals: {
								title: gettextCatalog.getString('Sign'),
								content: gettextCatalog.getString(
									'Please, activate the administration board.',
								),
								ok: gettextCatalog.getString('Activate now'),
							},
						}),
					)
					.then(() => adminBoardActivationService.activate($scope, 'setup')
						.then(() => votingCardSetsSign(electionEventId)));
			} else {
				votingCardSetsSign(electionEventId);
			}
		};

		/**
		 * Run the sign.
		 *
		 * @param {string} electionEventId the election event id.
		 * @return a promise resolved once all voting card sets have been processed.
		 */
		const votingCardSetsSign = function (electionEventId) {
			// Sign the voting card sets.
			votingCardSetService.sign(electionEventId).then(
				function () {
					// Refresh VCs list after status change to update tab counters
					$scope.listVotingCardSets();
					$scope.signInProgress = false;
				},
				function () {
					// Refresh VCs list after status change to update tab counters
					$scope.listVotingCardSets();
					// Show error message
					showVotingCardSetTransitionError();
					$scope.signInProgress = false;
				},
			)
		};

		$scope.uploadConfigurations = function () {
			$scope.uploadInProgress = true;
			$http.post(endpoints.host() + endpoints.uploadConfigurations.replace(
				electionEventIdPattern, sessionService.getSelectedElectionEvent().id)
			).then(function (_response) {
				$rootScope.$broadcast('refresh-ballots');
				$rootScope.$broadcast('refresh-voting-card-sets');
				$rootScope.$broadcast('refresh-authorities');
				$rootScope.$broadcast('refresh-ballot-boxes');
				$mdToast.show(
					toastCustom.topCenter(
						gettextCatalog.getString('Data uploaded successfully'),
						'success'
					)
				);
				$scope.uploadInProgress = false;
			}, function (_error) {
				$mdToast.show(
					toastCustom.topCenter(
						gettextCatalog.getString('Error while uploading configuration.'),
						'error'
					)
				);
				$scope.uploadInProgress = false;
			});
		}

		$scope.isThereNoVCSSelected = function () {
			let noVCSSelected = true;

			if ($scope.data.votingCardSets) {
				$scope.data.votingCardSets.result.forEach(function (votingCardSet) {
					if (votingCardSet.selected) {
						noVCSSelected = false;
					}
				});
			}

			return noVCSSelected;
		};

		$scope.isThereNoVCSComputing = function () {
			let noVCSComputing = true;
			if ($scope.data.votingCardSets) {
				$scope.data.votingCardSets.result.forEach(function (votingCardSet) {
					if (votingCardSet.status === configElectionConstants.STATUS_COMPUTING.code) {
						noVCSComputing = noVCSComputing && false;
					}
				});
			}
			return noVCSComputing;
		};

		$scope.isThereNoVCSGenerating = function () {
			let noVCSGenerating = true;
			if ($scope.data.votingCardSets) {
				$scope.data.votingCardSets.result.forEach(function (votingCardSet) {
					if (votingCardSet.status === configElectionConstants.STATUS_GENERATING.code) {
						noVCSGenerating = noVCSGenerating && false;
					}
				});
			}
			return noVCSGenerating;
		};

		$scope.getSelectVCSText = function () {
			const text = gettextCatalog.getString(
				'Please select first a Voting card set',
			);

			const check = $scope.isThereNoVCSSelected();
			return check ? text : '';
		};

		$scope.getTextPrintFilesGeneration = function () {
			const text = gettextCatalog.getString(PRINT_FILES_GENERATION_BUTTON_MSG_ID);
			const check = $scope.areAllVotingCardsReadyForPrintFilesGeneration();
			return check ? '' : text;
		};

		$scope.areAllVotingCardsReadyForPrintFilesGeneration = function () {
			const isSigned = (vcs) => vcs.status === configElectionConstants.STATUS_SIGNED.code;
			if ($scope.data.votingCardSets) {
				return $scope.data.votingCardSets.result.every(isSigned)
			}
			return false;
		};

		$scope.isGeneratingPrintFiles = function () {
			return $scope.printFilesGenerationStatus[$scope.selectedElectionEventId];
		}

		$scope.isSetupMode = function () {
			return sessionService.isSetupMode();
		};

		$scope.isOnlineMode = function () {
			return sessionService.isOnlineMode();
		};

		$scope.completePrintFilesGeneration = function () {
			return $scope.printFilesGenerationStatus[$scope.selectedElectionEventId] = false;
		}

		$scope.startPrintFilesGeneration = function () {
			return $scope.printFilesGenerationStatus[$scope.selectedElectionEventId] = true;
		}

		$scope.generatePrintFiles = function () {
			if ($scope.isGeneratingPrintFiles()) {
				$mdToast.show(
					toastCustom.topCenter(
						gettextCatalog.getString(PRINT_FILES_GENERATION_IN_PROGRESS_MSG_ID),
						'warning',
					),
				);
				return;
			} else {
				$scope.startPrintFilesGeneration();
				$mdToast.show(
					toastCustom.topCenter(
						gettextCatalog.getString(PRINT_FILES_GENERATION_STARTED_MSG_ID),
						'success',
					),
				);
			}

			const url = endpoints.host() + endpoints.generatePrintFiles.replace(electionEventIdPattern, $scope.selectedElectionEventId);

			$http
				.post(url)
				.then(function () {
					$scope.completePrintFilesGeneration();
					$mdToast.show(
						toastCustom.topCenter(
							gettextCatalog.getString(PRINT_FILES_GENERATED_MSG_ID),
							'success',
						),
					);
				})
				.catch(function (e) {
					$scope.completePrintFilesGeneration();
					$mdToast.show(
						toastCustom.topCenter(
							`${gettextCatalog.getString(PRINT_FILES_MSG_ID)}: 
							${gettextCatalog.getString(CONTACT_SUPPORT_MSG_ID)}. ${gettextCatalog.getString(ERROR_CODE_MSG_ID)}: 
							${e.data.error}, ${gettextCatalog.getString(ErrorsDict(e.data.error))}`
							, 'error'
						),
					);
				});
		}

		$scope.unselectAll = function () {
			$scope.selectAll = false;
			$scope.data.votingCardSets.result.forEach(function (votingCardSet) {
				votingCardSet.selected = false;
			});
		};

		$scope.onSelectAll = function (value) {
			$scope.data.votingCardSets.result.forEach(function (votingCardSet) {
				const status = activeFilters.getActiveFilter($scope.filterItem).code;
				if (votingCardSet.status === status) {
					votingCardSet.selected = value;
				}
			});
		};

		$scope.isOnline = function () {
			return sessionService.isOnlineMode();
		};

		$scope.updateSelectAll = function (value) {
			if (!value) {
				$scope.selectAll = false;
			}
		};

		//initialize && populate view
		// -------------------------------------------------------------
		$scope.alert = '';
		$scope.errors = {};
		$scope.selectedElectionEventId = sessionService.getSelectedElectionEvent().id;
		$scope.listVotingCardSets();
		$scope.statusBox = statusBox;
	})
	.filter('elapsed', function (gettextCatalog) {
		'use strict';
		return function (time) {
			if (!time) {
				return gettextCatalog.getString('Estimating');
			}

			const seconds = Math.floor(time / 1000);
			let minutes = Math.floor(seconds / 60);
			const minutesRest = Math.floor(seconds % 60);
			let hours = Math.floor(minutes / 60);
			const hoursRest = Math.floor(minutes % 60);
			let days = Math.floor(hours / 24);
			const daysRest = Math.floor(hours % 24);

			if (minutes && minutesRest) {
				minutes++;
			}

			if (hours && hoursRest) {
				hours++;
			}

			if (days && daysRest) {
				days++;
			}

			if (days > 1) {
				return `${days} ${gettextCatalog.getString('days')}`;
			} else if (hours > 1) {
				return `${hours} ${gettextCatalog.getString('hours')}`;
			} else if (hours === 1) {
				return `1 ${gettextCatalog.getString('hour')}`;
			} else if (minutes > 1) {
				return `${minutes} ${gettextCatalog.getString('minutes')}`;
			} else if (minutes === 1) {
				return `1 ${gettextCatalog.getString('minute')}`;
			} else {
				return `${seconds} ${gettextCatalog.getString('seconds')}`;
			}
		};
	});
