/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
angular
    .module('uploadZip', [
        // 'conf.globals'
    ])
    .factory('uploadZip', function (Upload) {
        'use strict';
		const upload = function (url, zip) {
			return Upload.upload({
				url: url,
				data: {
					file: zip
				},
			});
		};
		return {
            upload: upload,
        };
    });
