/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
/*jshint maxparams: 13 */
/*jshint maxlen: 1800 */

import dialogShowMembersTemplate from '../dialogs/dialog-show-members.html';
import boardMembersPasswordsTemplate from "../board-members-passwords/board-members-passwords.html";

export default angular
  .module('administration-board-list', ['app.dialogs'])
  .controller('administration-board-list', function (
    $rootScope,
    $scope,
    $mdDialog,
    sessionService,
    endpoints,
    $http,
    $mdToast,
    $timeout,
    $q,
    CustomDialog,
    gettextCatalog,
    toastCustom,
  ) {

    // initialize & populate view
    $scope.administrationBoardsList = [];

    $scope.isConstituting = false;

    $scope.listAdminBoards = function () {
      $http.get(endpoints.host() + endpoints.adminBoardList).then(
        function (response) {
          const data = response.data;
          $rootScope.safeApply(() => {
            $scope.administrationBoardsList = data.result;
            sessionService.setAdminBoards(data.result);
          });
        }
      );
    };

    $scope.listAdminBoards();

    $scope.showMembers = function (adminBoard, ev) {
      $scope.showMembersDialogTitle = gettextCatalog.getString(
        'Administration Board Members',
      );

      $scope.listOfMembers = adminBoard.boardMembers;
      $scope.closeDialog = () => $mdDialog.cancel();
      $mdDialog.show({
        template: dialogShowMembersTemplate,
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose: true,
        scope: $scope,
        escapeToClose: true,
        preserveScope: true,
      });
    };

    $scope.isAdminBoardSelected = function () {
      return getSelectedAdminBoard() !== undefined;
    };

    $scope.isConstituteVisible = function () {
      return sessionService.isSetupMode();
    };

    $scope.checkSelectedText = function () {
      const check = $scope.isAdminBoardSelected();
      const selectAdminBoardText = gettextCatalog.getString(
        'Please select first an Administration board',
      );

      return check ? selectAdminBoardText : '';
    };

    $scope.uniqueSelection = function (adminBoard) {
      if (adminBoard.selected) {
        $scope.administrationBoardsList.forEach(function (ab) {
          if (ab.id !== adminBoard.id) {
            ab.selected = false;
          }
        });
      }
    };

    $scope.constituteAdminBoard = function () {
      const selectedAdminBoard = getSelectedAdminBoard();
      if (selectedAdminBoard.status !== 'LOCKED') {
        new CustomDialog()
          .title(gettextCatalog.getString('Constitute administration board'))
          .cannotPerform(gettextCatalog.getString('Administration Board'))
          .show();
        return;
      }
      $scope.bmpConfig = {
        membersPasswordsMode: 'constitution',
        selectedBoard: selectedAdminBoard,
        selectedBoardMembers: selectedAdminBoard.boardMembers,
        boardType: 'administration'
      };

      $scope.isConstituting = true;

      // Members passwords modal display
      $mdDialog.show({
        controller: 'boardMembersPasswordsController',
        controllerAs: 'ctrl',
        template: boardMembersPasswordsTemplate,
        parent: angular.element(document.body),
        clickOutsideToClose: false,
        escapeToClose: false,
        preserveScope: true,
        scope: $scope
      }).then(
        (membersPasswords) => {
          if (membersPasswords.length === selectedAdminBoard.boardMembers.length) {
            const passwords = [];
            membersPasswords.forEach(pwd => passwords.push([...pwd]));

            const constituteEndpoint = (endpoints.host() + endpoints.adminBoardConstitution)
              .replace('{adminBoardId}', selectedAdminBoard.id);

            $http.post(constituteEndpoint, passwords).then(constituteSuccess, constituteError);
          }
        },
        () => {
          $scope.isConstituting = false;
        }
      )
    };

    function constituteSuccess() {
      $scope.listAdminBoards();

      $mdToast.show(
        toastCustom.topCenter(
          gettextCatalog.getString('Administration board successfully constituted'),
          'success',
        ),
      );

      $scope.isConstituting = false;
    }

    function constituteError() {
      new CustomDialog()
        .title('Error')
        .error()
        .show();

      $scope.isConstituting = false;
    }

    function getSelectedAdminBoard() {
      return $scope.administrationBoardsList.find(ab => ab.selected);
    }

    $scope.capitalizeFirstLetter = function (string) {
      const lowerCaseString = string.toLowerCase();
      return (
        lowerCaseString.charAt(0).toUpperCase() + lowerCaseString.slice(1)
      );
    };
  });
