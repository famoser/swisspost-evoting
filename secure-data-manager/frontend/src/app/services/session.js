/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
angular
	.module('app.sessionService', [
		// 'conf.globals'
	])

	.factory('sessionService', function ($timeout) {
		'use strict';

		const setServerMode = function (serverMode) {
			this.serverMode = serverMode;
		};

		const getServerMode = function () {
			return this.serverMode;
		};

		const isOnlineMode = function () {
			return this.serverMode === 'SERVER_MODE_ONLINE' || this.serverMode === 'SERVER_MODE_TESTING';
		};

		const isTallyMode = function () {
			return this.serverMode === 'SERVER_MODE_TALLY' || this.serverMode === 'SERVER_MODE_TESTING';
		};

		const isSetupMode = function () {
			return this.serverMode === 'SERVER_MODE_SETUP' || this.serverMode === 'SERVER_MODE_TESTING';
		};

		const setAdminBoards = function (adminBoards) {
			this.adminBoards = adminBoards;
		};

		const getAdminBoards = function () {
			return this.adminBoards;
		};

		const setElectoralBoards = function (electoralBoards) {
			this.electoralBoards = electoralBoards;
		};

		const getElectoralBoards = function () {
			return this.electoralBoards;
		};

		const setSelectedElectionEvent = function (selectedElectionEvent) {
			this.selectedElectionEvent = selectedElectionEvent;
		};

		const getSelectedElectionEvent = function () {
			return this.selectedElectionEvent;
		};

		const clearSelectedElectionEvent = function () {
			this.selectedElectionEvent = undefined;
		};

		const setActivatedAdminBoard = function (activatedAdminBoard) {
			this.activatedAdminBoard = activatedAdminBoard;
		};

		const getActivatedAdminBoard = function () {
			return this.activatedAdminBoard;
		};

		const doesActivatedABBelongToSelectedEE = function () {
			const electionEvent = this.getSelectedElectionEvent();
			const adminBoard = this.getActivatedAdminBoard();

			if (electionEvent && adminBoard) {
				return electionEvent.administrationBoard.id === adminBoard.id;
			}
		};

		return {
			setServerMode: setServerMode,
			getServerMode: getServerMode,
			isOnlineMode: isOnlineMode,
			isSetupMode: isSetupMode,
			isTallyMode: isTallyMode,
			setAdminBoards: setAdminBoards,
			getAdminBoards: getAdminBoards,
			setElectoralBoards: setElectoralBoards,
			getElectoralBoards: getElectoralBoards,
			setActivatedAdminBoard: setActivatedAdminBoard,
			getActivatedAdminBoard: getActivatedAdminBoard,
			getSelectedElectionEvent: getSelectedElectionEvent,
			setSelectedElectionEvent: setSelectedElectionEvent,
			clearSelectedElectionEvent: clearSelectedElectionEvent,
			doesActivatedABBelongToSelectedEE: doesActivatedABBelongToSelectedEE
		};
	});
