/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */

import dialogCustomAlertTemplateTemplate from '../views/dialogs/dialog-custom-alert-template.html';
import dialogCustomConfirmTemplateTemplate from '../views/dialogs/dialog-custom-confirm-template.html';

angular
	.module('dialogsCustom', [])
	.controller('DialogController', DialogController)
	.config([
		'$mdDialogProvider',
		function ($mdDialogProvider) {
			$mdDialogProvider.addPreset('customAlert', {
				options: function () {
					return {
						template: dialogCustomAlertTemplateTemplate,
						controller: 'DialogController',
						clickOutsideToClose: true,
						escapeToClose: true,
					};
				},
			});
			$mdDialogProvider.addPreset('customConfirm', {
				options: function () {
					return {
						template: dialogCustomConfirmTemplateTemplate,
						controller: 'DialogController',
						clickOutsideToClose: false,
						escapeToClose: true,
					};
				},
			});
		},
	]);

function DialogController($scope, $mdDialog, locals) {
	$scope.title = locals.title;
	$scope.content = locals.content;
	$scope.ok = locals.ok;
	$scope.cancel = function () {
		$mdDialog.cancel();
	};
	$scope.hide = function () {
		$mdDialog.hide();
	};
}
