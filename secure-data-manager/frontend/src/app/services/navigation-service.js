/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
angular
    .module('entitiesCounterService', [
        // 'conf.globals'
    ])

    .factory('entitiesCounterService', function (
        $http,
        endpoints,
        sessionService,
    ) {
        'use strict';

		const model = {};
		const ELECTION_EVENT_ID = '{electionEventId}';

		// init
        model.allowElectionEventNav = false;

		const setElectionEventNav = function (value) {
			model.allowElectionEventNav = value;
		};

		const getTheMainItemsCount = function () {
			$http({
				method: 'GET',
				url: endpoints.host() + endpoints.electionEvents,
			}).then(
				function successCallback(response) {
					model.numberOfElectionEvents = response.data.result.length;
				},
				function errorCallback(response) {
					model.numberOfElectionEvents = '?';
				},
			);

			$http({
				method: 'GET',
				url: endpoints.host() + endpoints.adminBoardList,
			}).then(
				function successCallback(response) {
					model.numberOfAdminBoards = response.data.result.length;
				},
				function errorCallback(data) {
					model.numberOfAdminBoards = '?';
				},
			);
		};

		const getElectionEventInfo = function (electionEvent) {
			model.selectedElectionEvent = electionEvent.defaultTitle;

			$http({
				method: 'GET',
				url:
					endpoints.host() +
					endpoints.ballots.replace(ELECTION_EVENT_ID, electionEvent.id),
			}).then(
				function successCallback(response) {
					model.numberOfBallots = response.data.result.length;
				},
				function errorCallback(response) {
					model.numberOfBallots = '?';
				},
			);

			$http({
				method: 'GET',
				url:
					endpoints.host() +
					endpoints.votingCardSets.replace(
						ELECTION_EVENT_ID,
						electionEvent.id,
					),
			}).then(
				function successCallback(response) {
					model.numberOfVotingCards = response.data.result.length;
				},
				function errorCallback(response) {
					model.numberOfVotingCards = '?';
				},
			);

			$http({
				method: 'GET',
				url:
					endpoints.host() +
					endpoints.electoralBoardList.replace(
						ELECTION_EVENT_ID,
						electionEvent.id,
					),
			}).then(
				function successCallback(response) {
					model.numberOfElectoralBoards = response.data.result.length;
				},
				function errorCallback(response) {
					model.numberOfElectoralBoards = '?';
				},
			);

			$http({
				method: 'GET',
				url:
					endpoints.host() +
					endpoints.ballotboxes.replace(ELECTION_EVENT_ID, electionEvent.id),
			}).then(
				function successCallback(response) {
					model.numberOfBallotBoxes = response.data.result.length;
				},
				function errorCallback(response) {
					model.numberOfBallotBoxes = '?';
				},
			);
		};

		const resetElectionEventInfo = function () {
			model.selectedElectionEvent = undefined;
			model.numberOfBallots = '-';
			model.numberOfVotingCards = '-';
			model.numberOfElectoralBoards = '-';
			model.numberOfBallotBoxes = '-';
		};

		return {
            model,
            setElectionEventNav,
            getTheMainItemsCount,
            getElectionEventInfo,
            resetElectionEventInfo,
        };
    });
