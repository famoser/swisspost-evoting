/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import boardMembersPasswordsTemplate from "../views/board-members-passwords/board-members-passwords.html";

angular
  .module('adminBoardActivation', [])
  .service('adminBoardActivationService', function ($http, endpoints, sessionService, $q, $mdDialog, $mdToast, toastCustom, gettextCatalog) {
    'use strict';

    function activateAdminBoard(controllerScope, phase) {
      const deferred = $q.defer();
      const selectedEvent = sessionService.getSelectedElectionEvent();
      const adminBoardId = selectedEvent.administrationBoard.id;
      const adminBoard = sessionService.getAdminBoards().find(el => el.id === adminBoardId);

      const phaseValidationEndpoint = phase === 'setup' ? endpoints.adminBoardPasswordValidateSetup : endpoints.adminBoardPasswordValidateTally;
      const validationUrl = (endpoints.host() + phaseValidationEndpoint).replace('{adminBoardId}', adminBoard.id)

      controllerScope.bmpConfig = {
        membersPasswordsMode: 'activation',
        selectedBoard: adminBoard,
        selectedBoardMembers: adminBoard.boardMembers,
        membersPasswordsThreshold: adminBoard.minimumThreshold,
        membersPasswordsValidationUrl: validationUrl,
        boardType: 'administration'
      };

      $mdDialog.show({
        controller: 'boardMembersPasswordsController',
        controllerAs: 'ctrl',
        template: boardMembersPasswordsTemplate,
        parent: angular.element(document.body),
        clickOutsideToClose: false,
        escapeToClose: false,
        preserveScope: true,
        scope: controllerScope
      }).then(
        (membersPasswords) => {
          if (membersPasswords.length >= adminBoard.minimumThreshold) {
            const passwords = [];
            membersPasswords.forEach(pwd => passwords.push([...pwd]));

            const phaseActivationEndpoint = phase === 'setup' ? endpoints.adminBoardActivationSetup : endpoints.adminBoardActivationTally;
            const activationEndpoint = endpoints.host() + phaseActivationEndpoint.replace('{adminBoardId}', adminBoard.id);

            $http.put(activationEndpoint, passwords).then(() => {
              sessionService.setActivatedAdminBoard(adminBoard);
              $mdToast.show(
                toastCustom.topCenter(
                  gettextCatalog.getString('Administration board successfully activated'),
                  'success',
                ),
              );
              deferred.resolve();
            });
          }
        },
        () => null
      );
      return deferred.promise;
    }

    function deactivateAdminBoard() {
      sessionService.setActivatedAdminBoard(null);
    }

    function isAdminBoardActivated() {
      const adminBoard = sessionService.getActivatedAdminBoard();
      return adminBoard && adminBoard.id;
    }

    return {
      activate: activateAdminBoard,
      deactivate: deactivateAdminBoard,
      isActivated: isAdminBoardActivated
    };
  });
