/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */

const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyPlugin = require('copy-webpack-plugin');

const isProduction = process.env.NODE_ENV === 'production';
const stylesHandler = isProduction ? MiniCssExtractPlugin.loader : 'style-loader';

const config = {
	entry: './src/main.js',
	output: {
		filename: '[name].[contenthash].js',
		path: path.resolve(__dirname, 'dist'),
	},
	plugins: [
		// Render index.html
		new HtmlWebpackPlugin({
			template: 'src/index.html'
		}),

		// Copy assets
		new CopyPlugin({
			patterns: [
				{
					from: 'assets/**/*',
					context: path.resolve(__dirname, 'src'),
				}
			],
		}),
	],
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/i,
				loader: "babel-loader",
				exclude: /node_modules/
			},
			{
				test: /\.html$/i,
				loader: 'html-loader',
			},
			{
				test: /\.s[ac]ss$/i,
				use: [stylesHandler, 'css-loader', 'postcss-loader', 'sass-loader'],
			},
			{
				test: /\.(eot|svg|ttf|woff|woff2|png|jpg|gif)$/i,
				type: 'asset',
			}
		]
	},
	resolve: {
		alias: {
			node_modules: path.resolve(__dirname, 'node_modules'),
		},
	},
	performance: {
		hints: false,
	},
	optimization: {
		runtimeChunk: 'single',
	}
};

module.exports = () => {
	'use strict';

	if (isProduction) {
		config.mode = 'production';

		config.plugins.push(new MiniCssExtractPlugin({
			filename: 'styles.[contenthash].css',
		}));
	} else {
		config.mode = 'development';
	}

	return config;
};
