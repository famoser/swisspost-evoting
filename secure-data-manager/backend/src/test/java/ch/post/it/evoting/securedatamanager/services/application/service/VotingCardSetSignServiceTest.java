/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.application.service;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import ch.post.it.evoting.securedatamanager.VotingCardSetServiceTestBase;
import ch.post.it.evoting.securedatamanager.services.application.exception.ResourceNotFoundException;
import ch.post.it.evoting.securedatamanager.services.infrastructure.votingcardset.VotingCardSetRepository;

@ExtendWith(MockitoExtension.class)
@SpringJUnitConfig(VotingCardSetServiceTestSpringConfig.class)
class VotingCardSetSignServiceTest extends VotingCardSetServiceTestBase {

	private static final String ELECTION_EVENT_ID = "a3d790fd1ac543f9b0a05ca79a20c9e2";
	@Autowired
	private VotingCardSetSignService votingCardSetSignService;

	@Autowired
	private VotingCardSetRepository votingCardSetRepositoryMock;

	@BeforeEach
	void setup() {
		Mockito.reset(votingCardSetRepositoryMock);
	}

	@Test
	void signReturnsFalseWhenTheRequestedSetIsAlreadySigned() {
		assertDoesNotThrow(() -> votingCardSetSignService.sign(ELECTION_EVENT_ID));
	}

	@Test
	void sign() throws ResourceNotFoundException {
		setStatusForVotingCardSetFromRepository("GENERATED", votingCardSetRepositoryMock);

		assertDoesNotThrow(() -> votingCardSetSignService.sign(ELECTION_EVENT_ID));
	}
}
