/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.application.service;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

import java.nio.file.Paths;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.domain.tally.BallotBoxStatus;
import ch.post.it.evoting.securedatamanager.services.domain.model.status.SynchronizeStatus;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;
import ch.post.it.evoting.securedatamanager.services.infrastructure.ballotbox.BallotBoxRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.service.ConfigurationEntityStatusService;

class BallotBoxServiceTest {

	public static final String BALLOT_BOX_READY = "{\"result\": [{\"id\": \"96e\", \"ballot\" : { " + "\"id\": \"96f\"}, \"status\": \"READY\"}]}";
	private static final String BALLOT_BOX_ID = "96e";
	private static final String BALLOT_ID = "96f";
	private static final String ELECTION_EVENT_ID = "989";

	private static PathResolver pathResolver;
	private static BallotBoxService ballotBoxService;
	private static BallotBoxRepository ballotBoxRepositoryMock;
	private static ConfigurationEntityStatusService statusServiceMock;

	@BeforeAll
	static void setUpAll() {
		pathResolver = mock(PathResolver.class);
		statusServiceMock = mock(ConfigurationEntityStatusService.class);
		ballotBoxRepositoryMock = mock(BallotBoxRepository.class);

		ballotBoxService = new BallotBoxService(DomainObjectMapper.getNewInstance(), ballotBoxRepositoryMock, statusServiceMock, true);
	}

	@AfterEach
	void tearDown() {
		reset(pathResolver, statusServiceMock, ballotBoxRepositoryMock);
	}

	@Test
	void getBallotBoxesId() {
		// given
		when(ballotBoxRepositoryMock.list(anyMap())).thenReturn(BALLOT_BOX_READY);
		when(pathResolver.resolveBallotBoxPath(any(), any(), any())).thenReturn(Paths.get(
				"src/test/resources/ballotboxservice/" + ELECTION_EVENT_ID + "/ONLINE/electionInformation/ballots/" + BALLOT_ID + "/ballotBoxes/"
						+ BALLOT_BOX_ID));
		when(statusServiceMock.updateWithSynchronizedStatus(BallotBoxStatus.SIGNED.name(), BALLOT_BOX_ID, ballotBoxRepositoryMock,
				SynchronizeStatus.PENDING))
				.thenReturn("");

		// when
		final List<String> ballotBoxes = ballotBoxService.getBallotBoxesId("12e590cc85ad49af96b15ca761dfe49d");

		// then
		Assertions.assertThat(ballotBoxes)
				.isNotNull()
				.hasSize(1)
				.containsExactly("96e");
	}

	@Test
	void sign() {
		when(ballotBoxRepositoryMock.list(anyMap())).thenReturn(BALLOT_BOX_READY);
		when(statusServiceMock.updateWithSynchronizedStatus(BallotBoxStatus.SIGNED.name(), BALLOT_BOX_ID, ballotBoxRepositoryMock,
				SynchronizeStatus.PENDING))
				.thenReturn("");

		assertDoesNotThrow(() -> ballotBoxService.sign(BALLOT_BOX_ID));
	}

}
