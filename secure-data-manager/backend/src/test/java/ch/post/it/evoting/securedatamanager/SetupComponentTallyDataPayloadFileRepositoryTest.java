/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager;

import static ch.post.it.evoting.securedatamanager.commons.Constants.CONFIG_FILE_NAME_SETUP_COMPONENT_TALLY_DATA_PAYLOAD;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Locale;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.io.TempDir;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashFactory;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.evotinglibraries.domain.SerializationUtils;
import ch.post.it.evoting.evotinglibraries.domain.configuration.SetupComponentTallyDataPayload;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

@DisplayName("A SetupComponentTallyDataPayloadFileRepository")
class SetupComponentTallyDataPayloadFileRepositoryTest {
	private static final Random random = RandomFactory.createRandom();
	private static final Hash hashService = HashFactory.createHash();

	private static final String NON_EXISTING_ELECTION_EVENT_ID = random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH);
	private static final String EXISTING_ELECTION_EVENT_ID = random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH);
	private static final String VERIFICATION_CARD_SET_ID = random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH);

	private static ObjectMapper objectMapper;
	private static SetupComponentTallyDataPayloadFileRepository setupComponentTallyDataPayloadFileRepository;

	@BeforeAll
	static void setUpAll(
			@TempDir
			final Path tempDir) throws IOException {

		objectMapper = DomainObjectMapper.getNewInstance();

		createDirectories(tempDir, EXISTING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID);

		final PathResolver pathResolver = new PathResolver(tempDir.toString());

		setupComponentTallyDataPayloadFileRepository = new SetupComponentTallyDataPayloadFileRepository(objectMapper, pathResolver);

		final SetupComponentTallyDataPayloadFileRepository repository = new SetupComponentTallyDataPayloadFileRepository(objectMapper, pathResolver);

		repository.save(validSetupComponentTallyDataPayload(EXISTING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID));
	}

	private static SetupComponentTallyDataPayload validSetupComponentTallyDataPayload(final String electionEventId,
			final String verificationCardSetId) {
		final GqGroup encryptionGroup = SerializationUtils.getGqGroup();
		final List<String> verificationCardIds = List.of(
				random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH),
				random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH),
				random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH));
		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> verificationCardPublicKeys = GroupVector.of(
				SerializationUtils.getPublicKey(),
				SerializationUtils.getPublicKey(),
				SerializationUtils.getPublicKey());

		final String ballotBoxAlias = random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH);
		final SetupComponentTallyDataPayload setupComponentTallyDataPayload = new SetupComponentTallyDataPayload(electionEventId,
				verificationCardSetId, ballotBoxAlias, encryptionGroup, verificationCardIds, verificationCardPublicKeys);

		final byte[] payloadHash = hashService.recursiveHash(setupComponentTallyDataPayload);
		final CryptoPrimitivesSignature signature = new CryptoPrimitivesSignature(payloadHash);
		setupComponentTallyDataPayload.setSignature(signature);

		return setupComponentTallyDataPayload;
	}

	private static void createDirectories(final Path tempDir, final String electionEventId, final String verificationCardSetId) throws IOException {
		Files.createDirectories(
				tempDir.resolve("sdm/config").resolve(electionEventId).resolve("ONLINE").resolve("voteVerification").resolve(verificationCardSetId));
	}

	@Nested
	@DisplayName("saving")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class SaveTest {

		private SetupComponentTallyDataPayloadFileRepository setupComponentTallyDataPayloadFileRepositoryTemp;
		private SetupComponentTallyDataPayload setupComponentTallyDataPayload;

		@BeforeAll
		void setUpAll(
				@TempDir
				final Path tempDir) throws IOException {

			createDirectories(tempDir, EXISTING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID);

			final PathResolver pathResolver = new PathResolver(tempDir.toString());
			setupComponentTallyDataPayloadFileRepositoryTemp = new SetupComponentTallyDataPayloadFileRepository(objectMapper, pathResolver);
		}

		@BeforeEach
		void setUp() {
			setupComponentTallyDataPayload = validSetupComponentTallyDataPayload(EXISTING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID);
		}

		@Test
		@DisplayName("valid setup component tally data payload creates file")
		void save() {
			final Path savedPath = setupComponentTallyDataPayloadFileRepositoryTemp.save(setupComponentTallyDataPayload);

			assertTrue(Files.exists(savedPath));
		}

		@Test
		@DisplayName("null setup component tally data payload throws NullPointerException")
		void saveNullSetupComponentTallyData() {
			assertThrows(NullPointerException.class, () -> setupComponentTallyDataPayloadFileRepositoryTemp.save(null));
		}

		@Test
		@DisplayName("invalid path throws UncheckedIOException")
		void invalidPath() {
			final PathResolver pathResolver = new PathResolver("invalidPath");
			final SetupComponentTallyDataPayloadFileRepository repository =
					new SetupComponentTallyDataPayloadFileRepository(DomainObjectMapper.getNewInstance(), pathResolver);

			final UncheckedIOException exception = assertThrows(UncheckedIOException.class, () -> repository.save(setupComponentTallyDataPayload));

			final Path voteVerificationPath = pathResolver.resolveElectionEventPath(EXISTING_ELECTION_EVENT_ID).resolve("ONLINE")
					.resolve("voteVerification").resolve(VERIFICATION_CARD_SET_ID).resolve(CONFIG_FILE_NAME_SETUP_COMPONENT_TALLY_DATA_PAYLOAD);
			final String errorMessage = String.format(
					"Failed to serialize setup component tally data payload. [electionEventId: %s, verificationCardSetId: %s, path: %s]",
					EXISTING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, voteVerificationPath);

			assertEquals(errorMessage, exception.getMessage());
		}
	}

	@Nested
	@DisplayName("calling existsById")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class ExistsByIdTest {

		@Test
		@DisplayName("for existing setup component tally data payload returns true")
		void existingSetupComponentTallyData() {
			assertTrue(setupComponentTallyDataPayloadFileRepository.existsById(EXISTING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID));
		}

		@Test
		@DisplayName("with null input throws NullPointerException")
		void nullInput() {
			assertThrows(NullPointerException.class,
					() -> setupComponentTallyDataPayloadFileRepository.existsById(null, VERIFICATION_CARD_SET_ID));
			assertThrows(NullPointerException.class,
					() -> setupComponentTallyDataPayloadFileRepository.existsById(EXISTING_ELECTION_EVENT_ID, null));
		}

		@Test
		@DisplayName("with invalid input throws FailedValidationException")
		void invalidInput() {
			assertThrows(FailedValidationException.class,
					() -> setupComponentTallyDataPayloadFileRepository.existsById("invalidId", VERIFICATION_CARD_SET_ID));
			assertThrows(FailedValidationException.class,
					() -> setupComponentTallyDataPayloadFileRepository.existsById(EXISTING_ELECTION_EVENT_ID, "invalidId"));
		}

		@Test
		@DisplayName("for non existing setup component tally data payload returns false")
		void nonExistingSetupComponentTallyData() {
			assertFalse(setupComponentTallyDataPayloadFileRepository.existsById(NON_EXISTING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID));
		}

	}

	@Nested
	@DisplayName("calling findById")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class FindByIdTest {

		@Test
		@DisplayName("for existing setup component tally data payload returns it")
		void existingSetupComponentTallyData() {
			assertTrue(setupComponentTallyDataPayloadFileRepository.findById(EXISTING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID).isPresent());
		}

		@Test
		@DisplayName("for non existing setup component tally data payload return empty optional")
		void nonExistingSetupComponentTallyData() {
			assertFalse(setupComponentTallyDataPayloadFileRepository.findById(NON_EXISTING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID).isPresent());
		}

	}

}
