/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.domain.config;

import static org.mockito.Mockito.mock;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.web.client.RestTemplate;

import ch.post.it.evoting.securedatamanager.configuration.setupvoting.EncryptedNodeLongReturnCodeSharesService;
import ch.post.it.evoting.securedatamanager.configuration.setupvoting.ReturnCodesPayloadsGeneratedService;
import ch.post.it.evoting.securedatamanager.services.application.service.ElectionEventContextPersistenceService;
import ch.post.it.evoting.securedatamanager.services.application.service.EncryptedLongReturnCodeSharesService;
import ch.post.it.evoting.securedatamanager.services.application.service.IdleStatusService;
import ch.post.it.evoting.securedatamanager.services.application.service.VotingCardSetDownloadService;
import ch.post.it.evoting.securedatamanager.services.application.service.VotingCardSetGenerateBallotService;
import ch.post.it.evoting.securedatamanager.services.application.service.VotingCardSetGenerateService;
import ch.post.it.evoting.securedatamanager.services.application.service.VotingCardSetSignService;
import ch.post.it.evoting.securedatamanager.services.domain.service.BallotDataGeneratorService;
import ch.post.it.evoting.securedatamanager.services.domain.service.ElectionEventDataGeneratorService;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;
import ch.post.it.evoting.securedatamanager.services.infrastructure.ballotbox.BallotBoxRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.cc.SetupComponentVerificationDataPayloadFileRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.service.ConfigurationEntityStatusService;

/**
 * MVC Configuration
 */
@Configuration
@ComponentScan(basePackages = { "ch.post.it.evoting.securedatamanager.services.infrastructure" })
@PropertySource("classpath:config/application.properties")
@Profile("test")
public class SdmDatabaseConfig {

	@Value("${sdm.workspace}")
	private String workspace;

	@Bean
	public static PropertySourcesPlaceholderConfigurer propertiesResolver() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	@Bean
	public PathResolver pathResolver() {
		return new PathResolver(workspace);
	}

	@Bean
	public VotingCardSetDownloadService votingCardSetDownloadService(
			final PathResolver pathResolver,
			final IdleStatusService idleStatusService,
			final EncryptedLongReturnCodeSharesService encryptedLongReturnCodeSharesService,
			final ConfigurationEntityStatusService configurationEntityStatusService,
			final SetupComponentVerificationDataPayloadFileRepository setupComponentVerificationDataPayloadFileRepository) {
		return new VotingCardSetDownloadService(pathResolver, idleStatusService, encryptedLongReturnCodeSharesService,
				configurationEntityStatusService, setupComponentVerificationDataPayloadFileRepository);
	}

	@Bean
	public VotingCardSetGenerateBallotService votingCardSetGenerateBallotService(
			final BallotBoxRepository ballotBoxRepository,
			final BallotDataGeneratorService ballotDataGeneratorService,
			final ConfigurationEntityStatusService configurationEntityStatusService) {
		return new VotingCardSetGenerateBallotService(ballotBoxRepository, ballotDataGeneratorService, configurationEntityStatusService);
	}

	@Bean
	public VotingCardSetGenerateService votingCardSetGenerateService(
			final IdleStatusService idleStatusService,
			final ConfigurationEntityStatusService configurationEntityStatusService,
			final ReturnCodesPayloadsGeneratedService returnCodesPayloadsGeneratedService,
			final VotingCardSetGenerateBallotService votingCardSetGenerateBallotService) {
		return new VotingCardSetGenerateService(idleStatusService, configurationEntityStatusService, votingCardSetGenerateBallotService,
				returnCodesPayloadsGeneratedService);
	}

	@Bean
	public VotingCardSetSignService votingCardSetSignService(
			final ElectionEventContextPersistenceService electionEventContextPersistenceService,
			final ConfigurationEntityStatusService configurationEntityStatusService) {
		return new VotingCardSetSignService(electionEventContextPersistenceService, configurationEntityStatusService);
	}

	@Bean
	public BallotDataGeneratorService getBallotDataGeneratorService() {
		return mock(BallotDataGeneratorService.class);
	}

	@Bean
	public ElectionEventDataGeneratorService getElectionEventDataGeneratorService() {
		return mock(ElectionEventDataGeneratorService.class);
	}

	@Bean
	public RestTemplate getRestTemplate() {
		return mock(RestTemplate.class);
	}

	@Bean
	EncryptedNodeLongReturnCodeSharesService encryptedNodeLongCodeSharesService() {
		return mock(EncryptedNodeLongReturnCodeSharesService.class);
	}
}
