/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.infrastructure.preconfiguration;

import static ch.post.it.evoting.cryptoprimitives.domain.election.Question.WRITE_IN_ATTRIBUTE_NO_WRITE_IN;
import static ch.post.it.evoting.securedatamanager.services.infrastructure.preconfiguration.ElectionsConfigFactory.ElectionTypeAttributeIds;
import static ch.post.it.evoting.securedatamanager.services.infrastructure.preconfiguration.ElectionsConfigFactory.VoteTypeAttributeIds;
import static ch.post.it.evoting.securedatamanager.services.infrastructure.preconfiguration.TranslationsFactory.Translation;
import static ch.post.it.evoting.securedatamanager.services.infrastructure.preconfiguration.TranslationsFactory.createTranslationsWithoutBallotId;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import ch.post.it.evoting.cryptoprimitives.domain.election.Contest;
import ch.post.it.evoting.cryptoprimitives.domain.election.ElectionAttributes;
import ch.post.it.evoting.cryptoprimitives.domain.election.ElectionOption;
import ch.post.it.evoting.cryptoprimitives.domain.election.Question;
import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.AnswerInformationType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.BallotQuestionType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.BallotType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.Configuration;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ContestType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.LanguageType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.StandardAnswerType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.StandardBallotType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.VoteDescriptionInformationType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.VoteInformationType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.VoteType;
import ch.post.it.evoting.securedatamanager.services.infrastructure.JsonConstants;

class TranslationsFactoryTest {

	private static final String STANDARD_BALLOT_QUESTION_IDENTIFICATION = "1000000000-StandardBallot";
	private static final List<String> ANSWER_TYPE = List.of("YES", "NO", "EMPTY");
	private static final String ANSWER_IDENTIFICATION = "57a30570-1722-3a7e-a8f9-7dd643d7f33";
	private static final String DOMAIN_OF_INFLUENCE = "doid-ch1-mu";
	private static final String VOTE_IDENTIFICATION = "1000000000-1";
	private static final Random RANDOM = RandomFactory.createRandom();
	private static final ObjectMapper OBJECT_MAPPER = DomainObjectMapper.getNewInstance();
	private static final Configuration configuration = getConfiguration();
	private static final Map<String, ElectionTypeAttributeIds> electionTypeAttributeIdsMap = Map.of();
	private static final Map<String, VoteTypeAttributeIds> standardBallotAttributeIdsMap = Map.of(STANDARD_BALLOT_QUESTION_IDENTIFICATION,
			new VoteTypeAttributeIds(
					RANDOM.genRandomBase16String(32),
					IntStream.range(0, ANSWER_TYPE.size()).boxed()
							.collect(Collectors.toMap(i -> ANSWER_IDENTIFICATION + i, ignored -> RANDOM.genRandomBase16String(32)))));
	private static final String attributeId = RANDOM.genRandomBase16String(32);
	private static final ElectionOption option = new ElectionOption(RANDOM.genRandomBase16String(32), attributeId,
			"Fünf-Kandidat-Eberhard-1992-01-01", "1");
	private static final ElectionAttributes attribute = new ElectionAttributes(attributeId, "3a89019a-87ba-45b6-ac92-501ace75d05f", List.of(), true);
	private static final Question question = new Question(RANDOM.genRandomBase16String(32), 1, 0, 1, false, RANDOM.genRandomBase16String(32),
			WRITE_IN_ATTRIBUTE_NO_WRITE_IN, attributeId, List.of(), "2", false, "7a89719a-87ba-45b6-ac72-771ace77d05f");
	private static final Map<String, List<BallotContestFactory.ContestWithPosition>> domainOfInfluenceToContests = Map.of(DOMAIN_OF_INFLUENCE,
			List.of(new BallotContestFactory.ContestWithPosition(
					new Contest(RANDOM.genRandomBase16String(32), "default", "default", VOTE_IDENTIFICATION, Contest.VOTES_TEMPLATE, false,
							List.of(option), List.of(attribute), List.of(question)), 1)));

	@Test
	@DisplayName("null parameters throws NullPointerException.")
	void nullParametersThrows() {
		assertAll(
				() -> assertThrows(NullPointerException.class,
						() -> createTranslationsWithoutBallotId(null, standardBallotAttributeIdsMap, domainOfInfluenceToContests,
								electionTypeAttributeIdsMap)),
				() -> assertThrows(NullPointerException.class,
						() -> createTranslationsWithoutBallotId(configuration, null, domainOfInfluenceToContests, electionTypeAttributeIdsMap)),
				() -> assertThrows(NullPointerException.class,
						() -> createTranslationsWithoutBallotId(configuration, standardBallotAttributeIdsMap, null, electionTypeAttributeIdsMap)),
				() -> assertThrows(NullPointerException.class,
						() -> createTranslationsWithoutBallotId(configuration, standardBallotAttributeIdsMap, domainOfInfluenceToContests, null))
		);
	}

	@Test
	@DisplayName("valid parameters returns expected result.")
	void validParametersDoesNotThrow() {
		final Map<String, List<Translation>> result = assertDoesNotThrow(
				() -> createTranslationsWithoutBallotId(configuration, standardBallotAttributeIdsMap, domainOfInfluenceToContests,
						electionTypeAttributeIdsMap));

		assertEquals(getExpectedTranslations(), result);
	}

	private Map<String, List<Translation>> getExpectedTranslations() {
		final VoteType vote = configuration.getContest().getVoteInformation().get(0).getVote();
		final Map<LanguageType, String> voteDescription = vote.getVoteDescription().getVoteDescriptionInfo().stream()
				.collect(Collectors.toMap(VoteDescriptionInformationType.VoteDescriptionInfo::getLanguage,
						VoteDescriptionInformationType.VoteDescriptionInfo::getVoteDescription));
		final String voteDomainOfInfluence = vote.getDomainOfInfluence();
		final StandardBallotType standardBallotType = vote.getBallot().get(0).getStandardBallot();
		final Map<String, Map<LanguageType, String>> answerInformation = standardBallotType.getAnswer().stream()
				.collect(Collectors.toMap(StandardAnswerType::getAnswerIdentification,
						standardAnswerType -> standardAnswerType.getAnswerInfo().stream()
								.collect(Collectors.toMap(AnswerInformationType::getLanguage, AnswerInformationType::getAnswer))));
		final List<Translation> standardBallotTranslations = standardBallotType.getBallotQuestion().getBallotQuestionInfo().stream()
				.map(ballotQuestionInfo -> {
					final LanguageType languageType = ballotQuestionInfo.getLanguage();
					VoteTypeAttributeIds voteTypeAttributeIds = standardBallotAttributeIdsMap.get(standardBallotType.getQuestionIdentification());

					final ObjectNode questionIdObject = OBJECT_MAPPER.createObjectNode()
							.put(JsonConstants.QUESTION_TYPE_TEXT, ballotQuestionInfo.getBallotQuestion());
					final ObjectNode contestIdObject = OBJECT_MAPPER.createObjectNode()
							.put(JsonConstants.TITLE, voteDescription.get(languageType))
							.put(JsonConstants.DESCRIPTION, voteDescription.get(languageType))
							.put(JsonConstants.HOW_TO_VOTE, (String) null)
							.set(JsonConstants.ATTRIBUTE_KEYS, OBJECT_MAPPER.createObjectNode()
									.put(JsonConstants.ANSWER_TYPE_TEXT, "text")
									.put(JsonConstants.QUESTION_TYPE_TEXT, "text"));

					final ObjectNode answerIdObjects = standardBallotType.getAnswer().stream()
							.map(standardAnswerType -> {
								final String answerIdentification = standardAnswerType.getAnswerIdentification();
								final ObjectNode answerIdObject = OBJECT_MAPPER.createObjectNode()
										.put(JsonConstants.ANSWER_TYPE_TEXT, answerInformation.get(answerIdentification).get(languageType));
								return (ObjectNode) OBJECT_MAPPER.createObjectNode()
										.set(voteTypeAttributeIds.attributeAnswerId().get(answerIdentification),
												OBJECT_MAPPER.createArrayNode().add(answerIdObject));
							}).reduce(OBJECT_MAPPER.createObjectNode(), ObjectNode::setAll);

					final ObjectNode texts = OBJECT_MAPPER.createObjectNode();
					texts.set(voteTypeAttributeIds.attributeQuestionId(), OBJECT_MAPPER.createArrayNode().add(questionIdObject));
					texts.set(domainOfInfluenceToContests.get(voteDomainOfInfluence).get(0).contest().id(), contestIdObject);
					texts.setAll(answerIdObjects);
					return new Translation(languageType.value() + "-CH", texts);
				}).toList();

		return Map.of(voteDomainOfInfluence, standardBallotTranslations);
	}

	private static Configuration getConfiguration() {
		// Create standard ballot
		final BallotQuestionType ballotQuestionType = new BallotQuestionType();
		ballotQuestionType.setBallotQuestionInfo(Arrays.stream(LanguageType.values())
				.map(languageType -> {
					final BallotQuestionType.BallotQuestionInfo ballotQuestionInfo = new BallotQuestionType.BallotQuestionInfo();
					ballotQuestionInfo.setLanguage(languageType);
					ballotQuestionInfo.setBallotQuestion(languageType.value() + ": standard ballot question");
					return ballotQuestionInfo;
				})
				.toList());
		final StandardBallotType standardBallotType = new StandardBallotType();
		standardBallotType.setQuestionIdentification(STANDARD_BALLOT_QUESTION_IDENTIFICATION);
		standardBallotType.setAnswerType(BigInteger.TWO);
		standardBallotType.setBallotQuestion(ballotQuestionType);
		standardBallotType.setAnswer(IntStream.range(0, ANSWER_TYPE.size())
				.mapToObj(i -> {
					final StandardAnswerType standardAnswerType = new StandardAnswerType();
					standardAnswerType.setAnswerIdentification(ANSWER_IDENTIFICATION + i);
					standardAnswerType.setAnswerPosition(i + 1);
					standardAnswerType.setStandardAnswerType(ANSWER_TYPE.get(i));
					standardAnswerType.setAnswerInfo(Arrays.stream(LanguageType.values())
							.map(languageType -> {
								final AnswerInformationType answerInformationType = new AnswerInformationType();
								answerInformationType.setLanguage(languageType);
								answerInformationType.setAnswer(languageType.value() + ANSWER_TYPE.get(i) + i);
								return answerInformationType;
							})
							.toList());
					return standardAnswerType;
				})
				.toList());

		// Create ballot
		final BallotType ballotType = new BallotType();
		ballotType.setBallotIdentification("1000000000");
		ballotType.setBallotPosition(1);
		ballotType.setStandardBallot(standardBallotType);

		// Create vote
		final VoteDescriptionInformationType voteDescriptionType = new VoteDescriptionInformationType();
		voteDescriptionType.setVoteDescriptionInfo(Arrays.stream(LanguageType.values())
				.map(languageType -> {
					final VoteDescriptionInformationType.VoteDescriptionInfo voteDescriptionInfo = new VoteDescriptionInformationType.VoteDescriptionInfo();
					voteDescriptionInfo.setLanguage(languageType);
					voteDescriptionInfo.setVoteDescription(languageType.value() + ": Proposition");
					return voteDescriptionInfo;
				})
				.toList());
		final VoteType voteType = new VoteType();
		voteType.setVoteIdentification(VOTE_IDENTIFICATION);
		voteType.setDomainOfInfluence(DOMAIN_OF_INFLUENCE);
		voteType.setVoteDescription(voteDescriptionType);
		voteType.setBallot(List.of(ballotType));

		// Create configuration
		final VoteInformationType voteInformationType = new VoteInformationType();
		voteInformationType.setVote(voteType);
		final ContestType contestType = new ContestType();
		contestType.setVoteInformation(List.of(voteInformationType));
		final Configuration configuration = new Configuration();
		configuration.setContest(contestType);

		return configuration;
	}
}