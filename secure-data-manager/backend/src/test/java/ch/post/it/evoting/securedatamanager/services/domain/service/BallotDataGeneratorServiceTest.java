/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.domain.service;

import static java.nio.file.Files.createDirectories;
import static java.nio.file.Files.createFile;
import static java.nio.file.Files.createTempDirectory;
import static java.nio.file.Files.deleteIfExists;
import static java.nio.file.Files.exists;
import static java.nio.file.Files.getLastModifiedTime;
import static java.nio.file.Files.isDirectory;
import static java.nio.file.Files.newDirectoryStream;
import static java.nio.file.Files.readAllBytes;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.FileTime;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.securedatamanager.commons.Constants;
import ch.post.it.evoting.securedatamanager.services.infrastructure.JsonConstants;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;
import ch.post.it.evoting.securedatamanager.services.infrastructure.ballot.BallotRepository;

/**
 * Tests of {@link BallotDataGeneratorService}.
 */
class BallotDataGeneratorServiceTest {

	private static final String JSON = "{\"ballot\": \"Test Ballot\"}";
	private static final String ID = "id";
	private static final String ELECTION_EVENT_ID = "electionEventId";

	private BallotRepository repository;
	private Path baseFolder;
	private Path file;
	private BallotDataGeneratorService service;

	private static void deleteRecursively(final Path file) throws IOException {
		if (isDirectory(file)) {
			try (final DirectoryStream<Path> children = newDirectoryStream(file)) {
				for (final Path child : children) {
					deleteRecursively(child);
				}
			}
		}
		deleteIfExists(file);
	}

	@BeforeEach
	void setUp() throws IOException {
		repository = mock(BallotRepository.class);
		when(repository.find(ID)).thenReturn(JSON);
		baseFolder = createTempDirectory("user");
		final Path ballotPath = baseFolder.resolve(ELECTION_EVENT_ID).resolve(Constants.CONFIG_DIR_NAME_ONLINE)
				.resolve(Constants.CONFIG_DIR_NAME_ELECTIONINFORMATION)
				.resolve(Constants.CONFIG_DIR_NAME_BALLOTS).resolve(ID);
		file = ballotPath.resolve(Constants.CONFIG_FILE_NAME_BALLOT_JSON);
		final PathResolver pathResolver = mock(PathResolver.class);
		when(pathResolver.resolveBallotPath(ELECTION_EVENT_ID, ID)).thenReturn(ballotPath);
		service = new BallotDataGeneratorService(pathResolver, repository);
	}

	@AfterEach
	void tearDown() throws IOException {
		deleteRecursively(baseFolder);
	}

	@Test
	void testGenerate() throws IOException {
		assertTrue(service.generate(ID, ELECTION_EVENT_ID));
		assertArrayEquals(JSON.getBytes(StandardCharsets.UTF_8), readAllBytes(file));
	}

	@Test
	void testGenerateBallotNotFound() {
		when(repository.find(ID)).thenReturn(JsonConstants.EMPTY_OBJECT);

		assertFalse(service.generate(ID, ELECTION_EVENT_ID));
		assertFalse(exists(file));
	}

	@Test
	void testGenerateFailedToCreateFolder() throws IOException {
		createFile(baseFolder.resolve(ELECTION_EVENT_ID));

		assertFalse(service.generate(ID, ELECTION_EVENT_ID));
	}

	@Test
	void testGenerateFileAlreadyExists() throws IOException, InterruptedException {
		createDirectories(file.getParent());
		Files.writeString(file, JSON);
		final FileTime time = getLastModifiedTime(file);
		Thread.sleep(1000);

		assertTrue(service.generate(ID, ELECTION_EVENT_ID));
		assertEquals(time, getLastModifiedTime(file));
	}

	@Test
	void testGenerateFileAlreadyExistsDifferent() throws IOException {
		createDirectories(file.getParent());
		Files.writeString(file, "Something different");

		assertTrue(service.generate(ID, ELECTION_EVENT_ID));
		assertArrayEquals(JSON.getBytes(StandardCharsets.UTF_8), readAllBytes(file));
	}

	@Test
	void testGenerateIOException() throws IOException {
		createDirectories(file);

		assertFalse(service.generate(ID, ELECTION_EVENT_ID));
	}
}
