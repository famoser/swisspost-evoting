/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration;

import static ch.post.it.evoting.securedatamanager.commons.Constants.CONFIG_DIR_NAME_VERIFICATION_CARD_SECRET_KEYS;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Locale;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.io.TempDir;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.evotinglibraries.domain.SerializationUtils;
import ch.post.it.evoting.securedatamanager.configuration.setupvoting.VerificationCardSecretKey;
import ch.post.it.evoting.securedatamanager.configuration.setupvoting.VerificationCardSecretKeyPayload;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

@DisplayName("A VerificationCardSecretKeyFileRepository")
class VerificationCardSecretKeyPayloadFileRepositoryTest {
	private static final Random random = RandomFactory.createRandom();

	private static final String NON_EXISTING_ELECTION_EVENT_ID = random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH);
	private static final String EXISTING_ELECTION_EVENT_ID = random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH);
	private static final String VERIFICATION_CARD_SET_ID = random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH);
	private static final String VERIFICATION_CARD_ID = random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH);

	private static ObjectMapper objectMapper;
	private static VerificationCardSecretKeyPayloadFileRepository verificationCardSecretKeyPayloadFileRepository;

	@BeforeAll
	static void setUpAll(
			@TempDir
			final Path tempDir) throws IOException {

		objectMapper = DomainObjectMapper.getNewInstance();

		createDirectories(tempDir, EXISTING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID);

		final PathResolver pathResolver = new PathResolver(tempDir.toString());

		verificationCardSecretKeyPayloadFileRepository = new VerificationCardSecretKeyPayloadFileRepository(objectMapper, pathResolver);

		final VerificationCardSecretKeyPayloadFileRepository repository = new VerificationCardSecretKeyPayloadFileRepository(objectMapper,
				pathResolver);

		repository.save(validVerificationCardSecretKeyPayload(EXISTING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, VERIFICATION_CARD_ID));
	}

	private static VerificationCardSecretKeyPayload validVerificationCardSecretKeyPayload(final String electionEventId,
			final String verificationCardSetId, final String verificationCardId) {
		final List<VerificationCardSecretKey> verificationCardSecretKeys = List.of(
				new VerificationCardSecretKey(verificationCardId, SerializationUtils.getPrivateKey()));
		return new VerificationCardSecretKeyPayload(SerializationUtils.getGqGroup(), electionEventId, verificationCardSetId,
				verificationCardSecretKeys);
	}

	private static void createDirectories(final Path tempDir, final String electionEventId, final String verificationCardSetId) throws IOException {
		Files.createDirectories(
				tempDir.resolve("sdm/config").resolve(electionEventId).resolve("OFFLINE")
						.resolve(CONFIG_DIR_NAME_VERIFICATION_CARD_SECRET_KEYS).resolve(verificationCardSetId));
	}

	@Nested
	@DisplayName("saving")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class SaveTest {

		private VerificationCardSecretKeyPayloadFileRepository verificationCardSecretKeyPayloadFileRepositoryTemp;
		private VerificationCardSecretKeyPayload verificationCardSecretKeyPayload;

		@BeforeAll
		void setUpAll(
				@TempDir
				final Path tempDir) throws IOException {

			createDirectories(tempDir, EXISTING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID);

			final PathResolver pathResolver = new PathResolver(tempDir.toString());
			verificationCardSecretKeyPayloadFileRepositoryTemp = new VerificationCardSecretKeyPayloadFileRepository(objectMapper, pathResolver);
		}

		@BeforeEach
		void setUp() {
			verificationCardSecretKeyPayload = validVerificationCardSecretKeyPayload(EXISTING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID,
					VERIFICATION_CARD_ID);
		}

		@Test
		@DisplayName("valid verification card secret key creates file")
		void save() {
			final Path savedPath = verificationCardSecretKeyPayloadFileRepositoryTemp.save(verificationCardSecretKeyPayload);

			assertTrue(Files.exists(savedPath));
		}

		@Test
		@DisplayName("null verification card secret key throws NullPointerException")
		void saveNullVerificationCardSecretKeyPayload() {
			assertThrows(NullPointerException.class, () -> verificationCardSecretKeyPayloadFileRepositoryTemp.save(null));
		}
	}

	@Nested
	@DisplayName("calling findById")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class FindByIdTest {

		@Test
		@DisplayName("for existing verification card secret key returns it")
		void existingVerificationCardSecretKeyPayload() {
			assertTrue(verificationCardSecretKeyPayloadFileRepository.findById(EXISTING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID
			).isPresent());
		}

		@Test
		@DisplayName("for non existing verification card secret key return empty optional")
		void nonExistingVerificationCardSecretKeyPayload() {
			assertFalse(
					verificationCardSecretKeyPayloadFileRepository.findById(NON_EXISTING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID
					).isPresent());
		}

	}
}
