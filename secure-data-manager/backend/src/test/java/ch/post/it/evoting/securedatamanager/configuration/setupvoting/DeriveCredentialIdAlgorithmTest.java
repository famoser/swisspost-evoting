/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Locale;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.hashing.Argon2Factory;
import ch.post.it.evoting.cryptoprimitives.hashing.Argon2Profile;
import ch.post.it.evoting.cryptoprimitives.hashing.HashFactory;
import ch.post.it.evoting.cryptoprimitives.math.BaseEncodingFactory;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.securedatamanager.commons.Constants;

/**
 * Tests of DeriveCredentialIdAlgorithm.
 */
@DisplayName("DeriveCredentialIdAlgorithm")
class DeriveCredentialIdAlgorithmTest {

	private final Random random = RandomFactory.createRandom();
	private final String electionEventId = random.genRandomBase16String(Constants.BASE16_ID_LENGTH);
	private final String startVotingKey = random.genRandomBase32String(Constants.SVK_LENGTH).toLowerCase(Locale.ENGLISH);

	private final DeriveCredentialIdAlgorithm deriveCredentialIdAlgorithm = new DeriveCredentialIdAlgorithm(HashFactory.createHash(),
			BaseEncodingFactory.createBase16(), Argon2Factory.createArgon2(Argon2Profile.TEST.getContext()));

	@Test
	@DisplayName("calling deriveCredentialId with null parameters throws a NullPointerException.")
	void deriveCredentialIdWithNullParametersThrows() {
		assertThrows(NullPointerException.class, () -> deriveCredentialIdAlgorithm.deriveCredentialId(null, startVotingKey));
		assertThrows(NullPointerException.class, () -> deriveCredentialIdAlgorithm.deriveCredentialId(electionEventId, null));
	}

	@Test
	@DisplayName("calling deriveCredentialId with invalid parameters throws a FailedValidationException.")
	void deriveCredentialIdWithInvalidParametersThrows() {
		assertThrows(FailedValidationException.class, () -> deriveCredentialIdAlgorithm.deriveCredentialId("not UUID", startVotingKey));
		assertThrows(FailedValidationException.class,
				() -> deriveCredentialIdAlgorithm.deriveCredentialId(electionEventId, "not base32 of length 24."));

		final String base32WithPadAlphabet = "4d65ej2adb4ia6ghhzb52k==";
		assertThrows(FailedValidationException.class, () -> deriveCredentialIdAlgorithm.deriveCredentialId(electionEventId, base32WithPadAlphabet));
	}

	@Test
	@DisplayName("calling deriveCredentialId with invalid SVK_id length throws a IllegalArgumentException.")
	void deriveCredentialIdWithInvalidSVKLengthThrows() {
		final String tooSmallSVK = startVotingKey.substring(1);
		assertThrows(IllegalArgumentException.class, () -> deriveCredentialIdAlgorithm.deriveCredentialId(electionEventId, tooSmallSVK));
	}

	@Test
	@DisplayName("calling deriveCredentialId with correct parameters does not throw.")
	void deriveCredentialId() {
		assertDoesNotThrow(() -> deriveCredentialIdAlgorithm.deriveCredentialId(electionEventId, startVotingKey));
	}

	@Test
	@DisplayName("calling deriveCredentialId gives expected output.")
	void deriveCredentialIdExpectedOutput() {
		final String expectedCredentialId = "FE6B585A1EB23C1AFF76B3C7535587D3";
		final String electionEventId = "0ad226bdfbe84a32bc8808234d83e7b4";
		final String startVotingKey = "fsfoon4x7js5xbzeyfyd3qpu";

		assertEquals(expectedCredentialId, deriveCredentialIdAlgorithm.deriveCredentialId(electionEventId, startVotingKey));
	}
}
