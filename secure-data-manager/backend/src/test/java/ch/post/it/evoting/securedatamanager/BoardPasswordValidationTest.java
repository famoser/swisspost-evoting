/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.google.common.base.Throwables;

@DisplayName("BoardPasswordValidation")
class BoardPasswordValidationTest {

	@Test
	@DisplayName("a null password throws a NullPointerException.")
	void nullPasswordThrowsNullPointerException() {
		assertThrows(NullPointerException.class, () -> BoardPasswordValidation.validate(null));
	}

	@Test
	@DisplayName("a password with small size throws an IllegalArgumentException.")
	void smallSizePasswordThrowsIllegalArgumentException() {
		final char[] smallSizePW = "Password1_small".toCharArray();
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> BoardPasswordValidation.validate(smallSizePW));
		assertTrue(Throwables.getRootCause(exception).getMessage().contains("The password must be at least of size "));
	}

	@Test
	@DisplayName("a password with no upper case letter throws an IllegalArgumentException.")
	void noUpperCasePasswordThrowsIllegalArgumentException() {
		final char[] noUpperCasePW = "password_eb1_nouppercase".toCharArray();
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> BoardPasswordValidation.validate(noUpperCasePW));
		assertEquals("The password must contain at least one upper case letter.", Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("a password with no lower case letter throws an IllegalArgumentException.")
	void noLowerCasePasswordThrowsIllegalArgumentException() {
		final char[] noLowerCasePW = "PASSWORD_EB1_NOLOWERCASE".toCharArray();
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> BoardPasswordValidation.validate(noLowerCasePW));
		assertEquals("The password must contain at least one lower case letter.", Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("a password with no special characters throws an IllegalArgumentException.")
	void noSpecialCharacterPasswordThrowsIllegalArgumentException() {
		final char[] noSpecialCharPW = "PasswordEB1NoSpecialChar".toCharArray();
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> BoardPasswordValidation.validate(noSpecialCharPW));
		assertEquals("The password must contain at least one special character.", Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("a password with no digits throws an IllegalArgumentException.")
	void noDigitPasswordThrowsIllegalArgumentException() {
		final char[] noDigitPW = "Password_ElectoralBoard_NoDigit".toCharArray();
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> BoardPasswordValidation.validate(noDigitPW));
		assertEquals("The password must contain at least one digit.", Throwables.getRootCause(exception).getMessage());
	}
}
