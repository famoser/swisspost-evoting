/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.cryptoprimitives.domain.VotingOptionsConstants.MAXIMUM_NUMBER_OF_SELECTABLE_VOTING_OPTIONS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamal;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalFactory;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPrivateKey;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashFactory;
import ch.post.it.evoting.cryptoprimitives.math.Base64;
import ch.post.it.evoting.cryptoprimitives.math.BaseEncodingFactory;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupMatrix;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.test.tools.TestGroupSetup;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;

/**
 * Tests of CombineEncLongCodeSharesAlgorithm.
 */
@DisplayName("CombineEncLongCodeSharesAlgorithm")
class CombineEncLongCodeSharesAlgorithmTest extends TestGroupSetup {

	private static final int NUMBER_OF_VOTING_OPTIONS = MAXIMUM_NUMBER_OF_SELECTABLE_VOTING_OPTIONS;
	private static final int CONFIRMATION_KEYS_CIPHERTEXT_SIZE = 1;
	private static final List<String> VERIFICATION_CARD_IDS = List.of("e3318008e47d439a92577fcb2c738192", "4f51188102c2421385d250bf48b8b8dd",
			"9b5be5f5068a499d9998d48cb394aee1", "f51188102c2421385d250bf48b845b8a");

	private final GqGroup encryptionGroup = GroupTestData.getGqGroup();
	private final GqGroup otherEncryptionGroup = GroupTestData.getDifferentGqGroup(encryptionGroup);
	private final ElGamalGenerator elGamalGeneratorGrp2 = new ElGamalGenerator(otherEncryptionGroup);
	private final ElGamalGenerator elGamalGeneratorGrp1 = new ElGamalGenerator(encryptionGroup);
	private final ElGamal elGamal = ElGamalFactory.createElGamal();

	@Nested
	@DisplayName("calling combineEncLongCodeShares with")
	class CombineEncLongCodeSharesTest {
		private final int NUM_ROWS = 4;
		private final int NUM_COLS = 4;
		final GroupMatrix<ElGamalMultiRecipientCiphertext, GqGroup> exponentiatedEncryptedPartialChoiceReturnCodesMatrix =
				elGamalGeneratorGrp1.genRandomCiphertextMatrix(NUM_ROWS, NUM_COLS, NUMBER_OF_VOTING_OPTIONS);
		final GroupMatrix<ElGamalMultiRecipientCiphertext, GqGroup> exponentiatedEncryptedConfirmationKeysMatrix =
				elGamalGeneratorGrp1.genRandomCiphertextMatrix(NUM_ROWS, NUM_COLS, CONFIRMATION_KEYS_CIPHERTEXT_SIZE);
		private final CombineEncLongCodeSharesInput input =
				new CombineEncLongCodeSharesInput.Builder()
						.setExponentiatedEncryptedChoiceReturnCodesMatrix(exponentiatedEncryptedPartialChoiceReturnCodesMatrix)
						.setExponentiatedEncryptedConfirmationKeysMatrix(exponentiatedEncryptedConfirmationKeysMatrix)
						.setVerificationCardIds(VERIFICATION_CARD_IDS)
						.build();
		private final Hash hash = HashFactory.createHash();
		private final ElGamal elGamal = ElGamalFactory.createElGamal();
		private final Base64 base64 = BaseEncodingFactory.createBase64();
		private final CombineEncLongCodeSharesAlgorithm combineEncLongCodeSharesAlgorithm = new CombineEncLongCodeSharesAlgorithm(hash,
				elGamal, base64);
		private final String electionEventId = "0123456789abcdef0123456789abcdef";
		private final String verificationCardSetId = "abcdef0123456789abcdef0123456789";
		private final ElGamalMultiRecipientPrivateKey setupSecretKey = elGamalGeneratorGrp1.genRandomPrivateKey(1);
		private final CombineEncLongCodeSharesContext context =
				new CombineEncLongCodeSharesContext.Builder()
						.setEncryptionGroup(encryptionGroup)
						.setElectionEventId(electionEventId)
						.setVerificationCardSetId(verificationCardSetId)
						.setSetupSecretKey(setupSecretKey)
						.setNumberOfVotingOptions(NUMBER_OF_VOTING_OPTIONS)
						.setNumberOfEligibleVoters(NUM_ROWS)
						.build();

		@Test()
		@DisplayName("The context is null")
		void testWithNullContext() {
			assertThrows(NullPointerException.class, () -> combineEncLongCodeSharesAlgorithm.combineEncLongCodeShares(null, input));
		}

		@Test()
		@DisplayName("The input is null")
		void testWithNullInput() {
			assertThrows(NullPointerException.class, () -> combineEncLongCodeSharesAlgorithm.combineEncLongCodeShares(context, null));
		}

		@Test()
		@DisplayName("The context and input are not null")
		void testWithNonNullContextAndInput() {
			final CombineEncLongCodeSharesOutput output = combineEncLongCodeSharesAlgorithm.combineEncLongCodeShares(context, input);

			assertNotNull(output);

			final int N_E = input.getExponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix().numRows();

			final List<Integer> sizes = Arrays.asList(
					input.getExponentiatedEncryptedHashedConfirmationKeysMatrix().numRows(),
					input.getExponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix().numRows(),
					input.getVerificationCardIds().size());

			assertTrue(sizes.stream().allMatch(size -> size == N_E));
		}
	}

	@Nested
	@DisplayName("CombineEncLongCodeSharesContext with")
	class CombineEncLongCodeSharesContextTest {

		private final String electionEventId = "0123456789abcdef0123456789abcdef";
		private final String verificationCardSetId = "abcdef0123456789abcdef0123456789";
		private final ElGamalMultiRecipientPrivateKey setupSecretKey = elGamalGeneratorGrp1.genRandomPrivateKey(1);

		@Test()
		@DisplayName("The encryptionParameters null")
		void contextWithNullEncryptionParameters() {
			final CombineEncLongCodeSharesContext.Builder builder = new CombineEncLongCodeSharesContext.Builder()
					.setEncryptionGroup(null)
					.setElectionEventId(electionEventId)
					.setVerificationCardSetId(verificationCardSetId)
					.setSetupSecretKey(setupSecretKey)
					.setNumberOfVotingOptions(NUMBER_OF_VOTING_OPTIONS)
					.setNumberOfEligibleVoters(CONFIRMATION_KEYS_CIPHERTEXT_SIZE);

			assertThrows(NullPointerException.class, builder::build);
		}

		@Test()
		@DisplayName("The Election Event Id null")
		void contextWithNullElectionEventId() {
			final CombineEncLongCodeSharesContext.Builder builder = new CombineEncLongCodeSharesContext.Builder()
					.setEncryptionGroup(encryptionGroup)
					.setElectionEventId(null)
					.setVerificationCardSetId(verificationCardSetId)
					.setSetupSecretKey(setupSecretKey)
					.setNumberOfVotingOptions(NUMBER_OF_VOTING_OPTIONS)
					.setNumberOfEligibleVoters(CONFIRMATION_KEYS_CIPHERTEXT_SIZE);

			assertThrows(NullPointerException.class, builder::build);
		}

		@Test()
		@DisplayName("The Verification Card Set Id null")
		void contextWithNullVerificationCardSetId() {
			final CombineEncLongCodeSharesContext.Builder builder = new CombineEncLongCodeSharesContext.Builder()
					.setEncryptionGroup(encryptionGroup)
					.setElectionEventId(electionEventId)
					.setVerificationCardSetId(null)
					.setSetupSecretKey(setupSecretKey)
					.setNumberOfVotingOptions(NUMBER_OF_VOTING_OPTIONS)
					.setNumberOfEligibleVoters(CONFIRMATION_KEYS_CIPHERTEXT_SIZE);

			assertThrows(NullPointerException.class, builder::build);
		}

		@Test()
		@DisplayName("The Setup Secret Key null")
		void contextWithNullSetupSecretKey() {
			final CombineEncLongCodeSharesContext.Builder builder = new CombineEncLongCodeSharesContext.Builder()
					.setEncryptionGroup(encryptionGroup)
					.setElectionEventId(electionEventId)
					.setVerificationCardSetId(verificationCardSetId)
					.setSetupSecretKey(null)
					.setNumberOfVotingOptions(NUMBER_OF_VOTING_OPTIONS)
					.setNumberOfEligibleVoters(CONFIRMATION_KEYS_CIPHERTEXT_SIZE);

			assertThrows(NullPointerException.class, builder::build);
		}

		@Test()
		@DisplayName("The Setup Secret Key has different group")
		void contextWithDifferentGroupForSetupSecretKey() {
			final CombineEncLongCodeSharesContext.Builder builder = new CombineEncLongCodeSharesContext.Builder()
					.setEncryptionGroup(otherEncryptionGroup)
					.setElectionEventId(electionEventId)
					.setVerificationCardSetId(verificationCardSetId)
					.setSetupSecretKey(setupSecretKey)
					.setNumberOfVotingOptions(NUMBER_OF_VOTING_OPTIONS)
					.setNumberOfEligibleVoters(CONFIRMATION_KEYS_CIPHERTEXT_SIZE);

			final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, builder::build);

			final String expectedMessage = "The setup secret key must have the same order as the encryption group.";
			final String actualMessage = exception.getMessage();

			assertEquals(expectedMessage, actualMessage);
		}

		@Test()
		@DisplayName("The Election Event Id invalid UUID")
		void contextWithInvalidElectionEventId() {
			final String invalidElectionEventId = "zdiauzdi134";

			final CombineEncLongCodeSharesContext.Builder builder = new CombineEncLongCodeSharesContext.Builder()
					.setEncryptionGroup(encryptionGroup)
					.setElectionEventId(invalidElectionEventId)
					.setVerificationCardSetId(verificationCardSetId)
					.setSetupSecretKey(setupSecretKey)
					.setNumberOfVotingOptions(NUMBER_OF_VOTING_OPTIONS)
					.setNumberOfEligibleVoters(CONFIRMATION_KEYS_CIPHERTEXT_SIZE);

			final FailedValidationException exception = assertThrows(FailedValidationException.class, builder::build);

			final String expectedMessage = String.format(
					"The given string does not comply with the required format. [string: %s, format: ^[0123456789abcdefABCDEF]{32}$].",
					invalidElectionEventId);
			final String actualMessage = exception.getMessage();

			assertEquals(expectedMessage, actualMessage);
		}

		@Test()
		@DisplayName("The Verification Card Set Id invalid UUID")
		void contextWithInvalidVerificationCardSetId() {
			final String invalidVerificationCardSetId = "zdiauzdi134";

			final CombineEncLongCodeSharesContext.Builder builder = new CombineEncLongCodeSharesContext.Builder()
					.setEncryptionGroup(encryptionGroup)
					.setElectionEventId(electionEventId)
					.setVerificationCardSetId(invalidVerificationCardSetId)
					.setSetupSecretKey(setupSecretKey)
					.setNumberOfVotingOptions(NUMBER_OF_VOTING_OPTIONS)
					.setNumberOfEligibleVoters(CONFIRMATION_KEYS_CIPHERTEXT_SIZE);

			final FailedValidationException exception = assertThrows(FailedValidationException.class, builder::build);

			final String expectedMessage = String.format(
					"The given string does not comply with the required format. [string: %s, format: ^[0123456789abcdefABCDEF]{32}$].",
					invalidVerificationCardSetId);
			final String actualMessage = exception.getMessage();

			assertEquals(expectedMessage, actualMessage);
		}

		@Test()
		@DisplayName("The number of voting options is negative")
		void contextWithNegativeNumberOfVotingOptions() {
			final CombineEncLongCodeSharesContext.Builder builder = new CombineEncLongCodeSharesContext.Builder()
					.setEncryptionGroup(encryptionGroup)
					.setElectionEventId(electionEventId)
					.setVerificationCardSetId(verificationCardSetId)
					.setSetupSecretKey(setupSecretKey)
					.setNumberOfVotingOptions(-50)
					.setNumberOfEligibleVoters(CONFIRMATION_KEYS_CIPHERTEXT_SIZE);

			final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, builder::build);

			final String expectedMessage = "The number of voting options must be strictly positive.";
			final String actualMessage = exception.getMessage();

			assertEquals(expectedMessage, actualMessage);
		}

		@Test()
		@DisplayName("The number of eligible voters is negative")
		void contextWithNegativeNumberOfEligibleVoters() {
			final CombineEncLongCodeSharesContext.Builder builder = new CombineEncLongCodeSharesContext.Builder()
					.setEncryptionGroup(encryptionGroup)
					.setElectionEventId(electionEventId)
					.setVerificationCardSetId(verificationCardSetId)
					.setSetupSecretKey(setupSecretKey)
					.setNumberOfVotingOptions(NUMBER_OF_VOTING_OPTIONS)
					.setNumberOfEligibleVoters(-50);

			final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, builder::build);

			final String expectedMessage = "The number of eligible voters must be strictly positive.";
			final String actualMessage = exception.getMessage();

			assertEquals(expectedMessage, actualMessage);
		}

		@Test()
		@DisplayName("output for input with same size and Gq group")
		void contextWithCorrectValues() {
			final CombineEncLongCodeSharesContext context =
					new CombineEncLongCodeSharesContext.Builder()
							.setEncryptionGroup(encryptionGroup)
							.setElectionEventId(electionEventId)
							.setVerificationCardSetId(verificationCardSetId)
							.setSetupSecretKey(setupSecretKey)
							.setNumberOfVotingOptions(NUMBER_OF_VOTING_OPTIONS)
							.setNumberOfEligibleVoters(CONFIRMATION_KEYS_CIPHERTEXT_SIZE)
							.build();

			assertEquals(encryptionGroup, context.getEncryptionGroup(), "encryptionGroup expected");
			assertEquals(electionEventId, context.getElectionEventId(), "electionEventId expected");
			assertEquals(verificationCardSetId, context.getVerificationCardSetId(), "verificationCardSetId expected");
			assertEquals(setupSecretKey, context.getSetupSecretKey(), "setupSecretKey expected");
			assertEquals(NUMBER_OF_VOTING_OPTIONS, context.getNumberOfVotingOptions(), "numberOfVotingOptions expected");
			assertEquals(CONFIRMATION_KEYS_CIPHERTEXT_SIZE, context.getNumberOfEligibleVoters(), "numberOfEligibleVoters expected");
		}
	}

	@Nested
	@DisplayName("CombineEncLongCodeSharesInput with")
	class CombineEncLongCodeSharesInputTest {

		private GroupMatrix<ElGamalMultiRecipientCiphertext, GqGroup> exponentiatedEncryptedPartialChoiceReturnCodesMatrix_2x2;
		private GroupMatrix<ElGamalMultiRecipientCiphertext, GqGroup> exponentiatedEncryptedPartialChoiceReturnCodesMatrix_3x4;
		private GroupMatrix<ElGamalMultiRecipientCiphertext, GqGroup> exponentiatedEncryptedPartialChoiceReturnCodesMatrix_4x4;
		private GroupMatrix<ElGamalMultiRecipientCiphertext, GqGroup> exponentiatedEncryptedPartialConfirmationKeysMatrix_3x2;
		private GroupMatrix<ElGamalMultiRecipientCiphertext, GqGroup> exponentiatedEncryptedPartialConfirmationKeysMatrix_4x4_grp_1;
		private GroupMatrix<ElGamalMultiRecipientCiphertext, GqGroup> exponentiatedEncryptedPartialConfirmationKeysMatrix_4x4_grp_2;
		private List<String> verificationCardIds_4;
		private List<String> verificationCardIds_3;

		@BeforeEach
		void setUp() {
			exponentiatedEncryptedPartialChoiceReturnCodesMatrix_2x2 = elGamalGeneratorGrp1.genRandomCiphertextMatrix(2, 2, NUMBER_OF_VOTING_OPTIONS);
			exponentiatedEncryptedPartialChoiceReturnCodesMatrix_3x4 = elGamalGeneratorGrp1.genRandomCiphertextMatrix(3, 4, NUMBER_OF_VOTING_OPTIONS);
			exponentiatedEncryptedPartialChoiceReturnCodesMatrix_4x4 = elGamalGeneratorGrp1.genRandomCiphertextMatrix(4, 4, NUMBER_OF_VOTING_OPTIONS);
			exponentiatedEncryptedPartialConfirmationKeysMatrix_3x2 = elGamalGeneratorGrp1.genRandomCiphertextMatrix(3, 2,
					CONFIRMATION_KEYS_CIPHERTEXT_SIZE);
			exponentiatedEncryptedPartialConfirmationKeysMatrix_4x4_grp_1 = elGamalGeneratorGrp1.genRandomCiphertextMatrix(4, 4,
					CONFIRMATION_KEYS_CIPHERTEXT_SIZE);
			exponentiatedEncryptedPartialConfirmationKeysMatrix_4x4_grp_2 = elGamalGeneratorGrp2.genRandomCiphertextMatrix(4, 4,
					CONFIRMATION_KEYS_CIPHERTEXT_SIZE);
			verificationCardIds_4 = VERIFICATION_CARD_IDS;
			verificationCardIds_3 = VERIFICATION_CARD_IDS.subList(0, 2);
		}

		@Test()
		@DisplayName("Constructor with Matrix Choice Return Codes null")
		void inputWithChoiceReturnCodesMatrixNull() {
			final CombineEncLongCodeSharesInput.Builder builder = new CombineEncLongCodeSharesInput.Builder()
					.setExponentiatedEncryptedChoiceReturnCodesMatrix(null)
					.setExponentiatedEncryptedConfirmationKeysMatrix(exponentiatedEncryptedPartialConfirmationKeysMatrix_4x4_grp_1)
					.setVerificationCardIds(verificationCardIds_4);

			assertThrows(NullPointerException.class, builder::build);
		}

		@Test()
		@DisplayName("Constructor with Matrix of Confirmation Keys null")
		void inputWithNullMatrixConfirmationKeys() {
			final CombineEncLongCodeSharesInput.Builder builder = new CombineEncLongCodeSharesInput.Builder()
					.setExponentiatedEncryptedChoiceReturnCodesMatrix(exponentiatedEncryptedPartialChoiceReturnCodesMatrix_2x2)
					.setExponentiatedEncryptedConfirmationKeysMatrix(null)
					.setVerificationCardIds(verificationCardIds_4);

			assertThrows(NullPointerException.class, builder::build);
		}

		@Test()
		@DisplayName("Constructor with Vector of Verification Card Ids null")
		void inputWithNullVectorVerificationCardIds() {
			final CombineEncLongCodeSharesInput.Builder builder = new CombineEncLongCodeSharesInput.Builder()
					.setExponentiatedEncryptedChoiceReturnCodesMatrix(exponentiatedEncryptedPartialChoiceReturnCodesMatrix_2x2)
					.setExponentiatedEncryptedConfirmationKeysMatrix(exponentiatedEncryptedPartialConfirmationKeysMatrix_4x4_grp_1)
					.setVerificationCardIds(null);

			assertThrows(NullPointerException.class, builder::build);
		}

		@Test()
		@DisplayName("Constructor with Vector of Verification Card Ids of invalid UUID format")
		void inputWithInvalidVerificationCardIds() {
			final String verificationCardId_invalid = "f51188102c2421385zS";
			final CombineEncLongCodeSharesInput.Builder builder = new CombineEncLongCodeSharesInput.Builder()
					.setExponentiatedEncryptedChoiceReturnCodesMatrix(exponentiatedEncryptedPartialChoiceReturnCodesMatrix_2x2)
					.setExponentiatedEncryptedConfirmationKeysMatrix(exponentiatedEncryptedPartialConfirmationKeysMatrix_4x4_grp_1)
					.setVerificationCardIds(Collections.singletonList(verificationCardId_invalid));

			final FailedValidationException exception = assertThrows(FailedValidationException.class, builder::build);

			final String expectedMessage = String.format(
					"The given string does not comply with the required format. [string: %s, format: ^[0123456789abcdefABCDEF]{32}$].",
					verificationCardId_invalid);
			final String actualMessage = exception.getMessage();

			assertEquals(expectedMessage, actualMessage);
		}

		@Test()
		@DisplayName("Constructor with different column size of both matrices")
		void inputWithDifferentMatrixColumnSizes() {
			final CombineEncLongCodeSharesInput.Builder builder = new CombineEncLongCodeSharesInput.Builder()
					.setExponentiatedEncryptedChoiceReturnCodesMatrix(exponentiatedEncryptedPartialChoiceReturnCodesMatrix_2x2)
					.setExponentiatedEncryptedConfirmationKeysMatrix(exponentiatedEncryptedPartialConfirmationKeysMatrix_4x4_grp_1)
					.setVerificationCardIds(verificationCardIds_4);

			final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, builder::build);

			final String expectedMessage = "C_expPCC and C_expCK must have the same number of columns.";
			final String actualMessage = exception.getMessage();

			assertEquals(expectedMessage, actualMessage);
		}

		@Test()
		@DisplayName("Constructor with matrices column size != 4")
		void inputWithWrongMatrixColumnSize() {
			final CombineEncLongCodeSharesInput.Builder builder = new CombineEncLongCodeSharesInput.Builder()
					.setExponentiatedEncryptedChoiceReturnCodesMatrix(exponentiatedEncryptedPartialChoiceReturnCodesMatrix_2x2)
					.setExponentiatedEncryptedConfirmationKeysMatrix(exponentiatedEncryptedPartialConfirmationKeysMatrix_3x2)
					.setVerificationCardIds(verificationCardIds_4);

			final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, builder::build);

			final String expectedMessage = String.format(
					"The matrix of exponentiated, encrypted, hashed partial Choice Return Codes does not have the expected number of columns. [expected: %s, actual: %s]",
					NODE_IDS.size(), exponentiatedEncryptedPartialChoiceReturnCodesMatrix_2x2.numColumns());
			final String actualMessage = exception.getMessage();

			assertEquals(expectedMessage, actualMessage);
		}

		@Test()
		@DisplayName("Constructor with matrix with different row numbers")
		void inputWithDifferentMatrixRowSizes() {
			final CombineEncLongCodeSharesInput.Builder builder = new CombineEncLongCodeSharesInput.Builder()
					.setExponentiatedEncryptedChoiceReturnCodesMatrix(exponentiatedEncryptedPartialChoiceReturnCodesMatrix_3x4)
					.setExponentiatedEncryptedConfirmationKeysMatrix(exponentiatedEncryptedPartialConfirmationKeysMatrix_4x4_grp_1)
					.setVerificationCardIds(verificationCardIds_4);

			final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, builder::build);

			final String expectedMessage = "C_expPCC and C_expCK must have the same number of rows.";
			final String actualMessage = exception.getMessage();

			assertEquals(expectedMessage, actualMessage);
		}

		@Test()
		@DisplayName("Constructor with matrices having different group")
		void inputWithDifferentGroupMatrix() {
			final CombineEncLongCodeSharesInput.Builder builder = new CombineEncLongCodeSharesInput.Builder()
					.setExponentiatedEncryptedChoiceReturnCodesMatrix(exponentiatedEncryptedPartialChoiceReturnCodesMatrix_4x4)
					.setExponentiatedEncryptedConfirmationKeysMatrix(exponentiatedEncryptedPartialConfirmationKeysMatrix_4x4_grp_2)
					.setVerificationCardIds(verificationCardIds_4);

			final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, builder::build);

			final String expectedMessage = "All input must have the same encryption group.";
			final String actualMessage = exception.getMessage();

			assertEquals(expectedMessage, actualMessage);
		}

		@Test()
		@DisplayName("Constructor with Vector of Verification Card Ids with different columns")
		void inputWithDifferentSizeVerificationCardIds() {
			final CombineEncLongCodeSharesInput.Builder builder = new CombineEncLongCodeSharesInput.Builder()
					.setExponentiatedEncryptedChoiceReturnCodesMatrix(exponentiatedEncryptedPartialChoiceReturnCodesMatrix_4x4)
					.setExponentiatedEncryptedConfirmationKeysMatrix(exponentiatedEncryptedPartialConfirmationKeysMatrix_4x4_grp_1)
					.setVerificationCardIds(verificationCardIds_3);

			final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, builder::build);

			final String expectedMessage = "The number of rows of each matrix C_expPCC and C_expCK must be equal to the number of verification card ids.";
			final String actualMessage = exception.getMessage();

			assertEquals(expectedMessage, actualMessage);
		}

		@Test()
		@DisplayName("Constructor with Vector of Verification Card Ids with duplicates")
		void inputWithDuplicatesInVerificationCardIds() {
			final List<String> verificationCardSetIds = List.of(verificationCardIds_4.get(0), verificationCardIds_4.get(0));
			final CombineEncLongCodeSharesInput.Builder builder = new CombineEncLongCodeSharesInput.Builder()
					.setExponentiatedEncryptedChoiceReturnCodesMatrix(exponentiatedEncryptedPartialChoiceReturnCodesMatrix_4x4)
					.setExponentiatedEncryptedConfirmationKeysMatrix(exponentiatedEncryptedPartialConfirmationKeysMatrix_4x4_grp_1)
					.setVerificationCardIds(verificationCardSetIds);

			final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, builder::build);

			final String expectedMessage = "The verification card id list must not have duplicates.";
			final String actualMessage = exception.getMessage();

			assertEquals(expectedMessage, actualMessage);
		}

		@Test()
		@DisplayName("Constructor with correct values")
		void inputWithCrrectValues() {
			final CombineEncLongCodeSharesInput input =
					new CombineEncLongCodeSharesInput.Builder()
							.setExponentiatedEncryptedChoiceReturnCodesMatrix(exponentiatedEncryptedPartialChoiceReturnCodesMatrix_4x4)
							.setExponentiatedEncryptedConfirmationKeysMatrix(exponentiatedEncryptedPartialConfirmationKeysMatrix_4x4_grp_1)
							.setVerificationCardIds(VERIFICATION_CARD_IDS)
							.build();

			assertNotNull(input);

			assertEquals(exponentiatedEncryptedPartialChoiceReturnCodesMatrix_4x4,
					input.getExponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix());
			assertEquals(exponentiatedEncryptedPartialConfirmationKeysMatrix_4x4_grp_1,
					input.getExponentiatedEncryptedHashedConfirmationKeysMatrix());
			assertEquals(VERIFICATION_CARD_IDS, input.getVerificationCardIds());
		}
	}

	@Nested
	@DisplayName("CombineEncLongCodeSharesOutput with")
	class CombineEncLongCodeSharesOutputTest {

		private GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> encryptedPreChoiceReturnCodesVector_4;
		private GroupVector<GqElement, GqGroup> preVoteCastReturnCodes4G1;
		private GroupVector<GqElement, GqGroup> preVoteCastReturnCodes3G1;
		private GroupVector<GqElement, GqGroup> preVoteCastReturnCodes4G2;
		private List<String> longVoteCastReturnCodesVector_4;
		private List<String> longVoteCastReturnCodesVector_3;

		@BeforeEach
		void setUp() {
			encryptedPreChoiceReturnCodesVector_4 = GroupVector.of(elGamal.neutralElement(NUMBER_OF_VOTING_OPTIONS, encryptionGroup),
					elGamal.neutralElement(NUMBER_OF_VOTING_OPTIONS, encryptionGroup),
					elGamal.neutralElement(NUMBER_OF_VOTING_OPTIONS, encryptionGroup),
					elGamal.neutralElement(NUMBER_OF_VOTING_OPTIONS, encryptionGroup));

			preVoteCastReturnCodes4G1 = GroupVector.of(encryptionGroup.getGenerator(), encryptionGroup.getGenerator(), encryptionGroup.getGenerator(),
					encryptionGroup.getGenerator());

			preVoteCastReturnCodes3G1 = GroupVector.of(encryptionGroup.getGenerator(), encryptionGroup.getGenerator(),
					encryptionGroup.getGenerator());

			preVoteCastReturnCodes4G2 = GroupVector.of(otherEncryptionGroup.getGenerator(), otherEncryptionGroup.getGenerator(),
					otherEncryptionGroup.getGenerator(),
					otherEncryptionGroup.getGenerator());

			longVoteCastReturnCodesVector_4 = Arrays.asList("lVCC1", "lVCC2", "lVCC3", "longVCC4");

			longVoteCastReturnCodesVector_3 = Arrays.asList("lVCC1", "lVCC2", "lVCC3");
		}

		@Test()
		@DisplayName("The Vector encrypted pre-Choice Return Codes null")
		void outputWithNullEncryptedPreChoiceReturnCodes() {
			final CombineEncLongCodeSharesOutput.Builder builder = new CombineEncLongCodeSharesOutput.Builder()
					.setEncryptedPreChoiceReturnCodesVector(null)
					.setPreVoteCastReturnCodesVector(preVoteCastReturnCodes4G1)
					.setLongVoteCastReturnCodesAllowList(longVoteCastReturnCodesVector_4);

			final Exception exception = assertThrows(NullPointerException.class, builder::build);

			final String expectedMessage = "The vector of encrypted pre-Choice Return Codes must not be null.";
			final String actualMessage = exception.getMessage();

			assertEquals(expectedMessage, actualMessage);
		}

		@Test()
		@DisplayName("The Vector pre-Choice Return Codes null")
		void outputWithNullPreVoteCastReturnCodes() {
			final CombineEncLongCodeSharesOutput.Builder builder = new CombineEncLongCodeSharesOutput.Builder()
					.setEncryptedPreChoiceReturnCodesVector(encryptedPreChoiceReturnCodesVector_4)
					.setPreVoteCastReturnCodesVector(null)
					.setLongVoteCastReturnCodesAllowList(longVoteCastReturnCodesVector_4);

			final Exception exception = assertThrows(NullPointerException.class, builder::build);

			final String expectedMessage = "The vector of pre-Vote Cast Return Codes must not be null.";
			final String actualMessage = exception.getMessage();

			assertEquals(expectedMessage, actualMessage);
		}

		@Test()
		@DisplayName("The Vector long Vote Cast Return Codes null")
		void outputWithNullLongVoteCastReturnCodesVector() {
			final CombineEncLongCodeSharesOutput.Builder builder = new CombineEncLongCodeSharesOutput.Builder()
					.setEncryptedPreChoiceReturnCodesVector(encryptedPreChoiceReturnCodesVector_4)
					.setPreVoteCastReturnCodesVector(preVoteCastReturnCodes4G1)
					.setLongVoteCastReturnCodesAllowList(null);
			final Exception exception = assertThrows(NullPointerException.class, builder::build);

			final String expectedMessage = "The long Vote Cast Return Codes allow list must not be null.";
			final String actualMessage = exception.getMessage();

			assertEquals(expectedMessage, actualMessage);
		}

		@Test()
		@DisplayName("The Vector Encrypted Pre-Choice Return Codes empty")
		void outputWithEmptyPreChoiceReturnCodes() {
			final CombineEncLongCodeSharesOutput.Builder builder = new CombineEncLongCodeSharesOutput.Builder()
					.setEncryptedPreChoiceReturnCodesVector(GroupVector.from(Collections.emptyList()))
					.setPreVoteCastReturnCodesVector(preVoteCastReturnCodes4G1)
					.setLongVoteCastReturnCodesAllowList(longVoteCastReturnCodesVector_3);

			final Exception exception = assertThrows(IllegalArgumentException.class, builder::build);

			final String expectedMessage = "The vector of encrypted pre-Choice Return Codes must have more than zero elements.";
			final String actualMessage = exception.getMessage();

			assertEquals(expectedMessage, actualMessage);
		}

		@Test()
		@DisplayName("The Vector of pre-Vote Cast Return Codes empty")
		void outputWithEmptyPreVoteCastReturnCodes() {
			final CombineEncLongCodeSharesOutput.Builder builder = new CombineEncLongCodeSharesOutput.Builder()
					.setEncryptedPreChoiceReturnCodesVector(encryptedPreChoiceReturnCodesVector_4)
					.setPreVoteCastReturnCodesVector(GroupVector.from(Collections.emptyList()))
					.setLongVoteCastReturnCodesAllowList(longVoteCastReturnCodesVector_3);

			final Exception exception = assertThrows(IllegalArgumentException.class, builder::build);

			final String expectedMessage = String.format(
					"The vector of pre-Vote Cast Return Codes is of incorrect size [size: expected: %s, actual: %s].",
					encryptedPreChoiceReturnCodesVector_4.size(), 0);
			final String actualMessage = exception.getMessage();

			assertEquals(expectedMessage, actualMessage);
		}

		@Test()
		@DisplayName("Vector of long Vote Cast Return Codes empty")
		void outputWithEmptyLongVoteCastReturnCodesVector() {
			final CombineEncLongCodeSharesOutput.Builder builder = new CombineEncLongCodeSharesOutput.Builder()
					.setEncryptedPreChoiceReturnCodesVector(encryptedPreChoiceReturnCodesVector_4)
					.setPreVoteCastReturnCodesVector(preVoteCastReturnCodes4G1)
					.setLongVoteCastReturnCodesAllowList(Collections.emptyList());

			final Exception exception = assertThrows(IllegalArgumentException.class, builder::build);

			final String expectedMessage = String.format(
					"The long Vote Cast Return Codes allow list is of incorrect size [size: expected: %s, "
							+ "actual: %s].", encryptedPreChoiceReturnCodesVector_4.size(), 0);
			final String actualMessage = exception.getMessage();

			assertEquals(expectedMessage, actualMessage);
		}

		@Test()
		@DisplayName("Vector of pre-Vote Cast Return Codes is of incorrect size")
		void buildInputWithWrongChoiceReturnCodesMatrixColumnSize() {
			final CombineEncLongCodeSharesOutput.Builder builder = new CombineEncLongCodeSharesOutput.Builder()
					.setEncryptedPreChoiceReturnCodesVector(encryptedPreChoiceReturnCodesVector_4)
					.setPreVoteCastReturnCodesVector(preVoteCastReturnCodes3G1)
					.setLongVoteCastReturnCodesAllowList(longVoteCastReturnCodesVector_4);

			final Exception exception = assertThrows(IllegalArgumentException.class, builder::build);

			final String expectedMessage = String.format(
					"The vector of pre-Vote Cast Return Codes is of incorrect size [size: expected: %s, actual: %s].",
					encryptedPreChoiceReturnCodesVector_4.size(), preVoteCastReturnCodes3G1.size());
			final String actualMessage = exception.getMessage();

			assertEquals(expectedMessage, actualMessage);
		}

		@Test()
		@DisplayName("Vector of long Vote Cast Return Codes is of incorrect size")
		void buildInputWithWrongLongVoteCastReturnCodesSize() {
			final CombineEncLongCodeSharesOutput.Builder builder = new CombineEncLongCodeSharesOutput.Builder()
					.setEncryptedPreChoiceReturnCodesVector(encryptedPreChoiceReturnCodesVector_4)
					.setPreVoteCastReturnCodesVector(preVoteCastReturnCodes4G1)
					.setLongVoteCastReturnCodesAllowList(longVoteCastReturnCodesVector_3);

			final Exception exception = assertThrows(IllegalArgumentException.class, builder::build);

			final String expectedMessage = String.format(
					"The long Vote Cast Return Codes allow list is of incorrect size [size: expected: %s, actual: %s].",
					encryptedPreChoiceReturnCodesVector_4.size(), longVoteCastReturnCodesVector_3.size());
			final String actualMessage = exception.getMessage();

			assertEquals(expectedMessage, actualMessage);
		}

		@Test()
		@DisplayName("the Vector of pre-Choice Return Codes and the Vector of pre-Vote Cast Return Codes have different Gd groups")
		void buildInputWithWrongConfirmationKeysMatrixRowSize() {
			final CombineEncLongCodeSharesOutput.Builder builder = new CombineEncLongCodeSharesOutput.Builder()
					.setEncryptedPreChoiceReturnCodesVector(encryptedPreChoiceReturnCodesVector_4)
					.setPreVoteCastReturnCodesVector(preVoteCastReturnCodes4G2)
					.setLongVoteCastReturnCodesAllowList(longVoteCastReturnCodesVector_4);

			final Exception exception = assertThrows(IllegalArgumentException.class, builder::build);

			final String expectedMessage = "The vector of encrypted pre-Choice Return Codes and the vector of pre-Vote Cast Return Codes do not have the same group order.";
			final String actualMessage = exception.getMessage();

			assertEquals(expectedMessage, actualMessage);
		}

		@Test()
		@DisplayName("output for input with same size and Gd group")
		void outputWithCorrectValues() {
			final CombineEncLongCodeSharesOutput.Builder builder = new CombineEncLongCodeSharesOutput.Builder()
					.setEncryptedPreChoiceReturnCodesVector(encryptedPreChoiceReturnCodesVector_4)
					.setPreVoteCastReturnCodesVector(preVoteCastReturnCodes4G1)
					.setLongVoteCastReturnCodesAllowList(longVoteCastReturnCodesVector_4);

			final CombineEncLongCodeSharesOutput output = builder.build();

			assertEquals(encryptedPreChoiceReturnCodesVector_4, output.getEncryptedPreChoiceReturnCodesVector(),
					"EncryptedPreChoiceReturnCodesVector expected");
			assertEquals(preVoteCastReturnCodes4G1, output.getPreVoteCastReturnCodesVector(),
					"PreVoteCastReturnCodesVector expected");
			assertEquals(longVoteCastReturnCodesVector_4, output.getLongVoteCastReturnCodesAllowList(),
					"LongVoteCastReturnCodesVector expected");
		}
	}
}
