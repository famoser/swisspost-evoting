/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.VotingOptionsConstants.MAXIMUM_NUMBER_OF_VOTING_OPTIONS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientKeyPair;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;

/**
 * Tests of GenSetupEncryptionKeysAlgorithm.
 */
@DisplayName("GenSetupEncryptionKeysAlgorithm")
class GenSetupEncryptionKeysAlgorithmTest {

	private final GenSetupEncryptionKeysAlgorithm genSetupEncryptionKeysAlgorithm = new GenSetupEncryptionKeysAlgorithm(RandomFactory.createRandom());

	@Test
	@DisplayName("calling genSetupEncryptionKeys with null argument throws a NullPointerException.")
	void genSetupEncryptionKeysWithNullGroupThrows() {
		assertThrows(NullPointerException.class, () -> genSetupEncryptionKeysAlgorithm.genSetupEncryptionKeys(null));
	}

	@Test
	@DisplayName("calling genSetupEncryptionKeys with a correct argument generates a keypair with the right size.")
	void genSetupEncryptionKeys() {
		final GqGroup group = GroupTestData.getGqGroup();
		final ElGamalMultiRecipientKeyPair elGamalMultiRecipientKeyPair;
		elGamalMultiRecipientKeyPair = genSetupEncryptionKeysAlgorithm.genSetupEncryptionKeys(group);
		assertEquals(MAXIMUM_NUMBER_OF_VOTING_OPTIONS, elGamalMultiRecipientKeyPair.size());
	}
}
