/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.sdmonline.tally.mixonline;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.util.Locale;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.domain.tally.BallotBoxStatus;
import ch.post.it.evoting.securedatamanager.services.application.service.BallotBoxService;

@ExtendWith(MockitoExtension.class)
class MixDecryptOnlineControllerTest {

	private static final Random random = RandomFactory.createRandom();
	private static final String ELECTION_EVENT_ID = random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH);
	private static final String BALLOT_BOX_ID = random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH);

	@Mock
	private MixDecryptOnlineService mixDecryptOnlineService;

	@Mock
	private BallotBoxService ballotBoxService;

	@InjectMocks
	private MixDecryptOnlineController mixDecryptOnlineController;

	@Test
	void invalidParametersMixOnlineThrows() {
		assertAll(
				() -> assertThrows(NullPointerException.class, () -> mixDecryptOnlineController.mixOnline(null, BALLOT_BOX_ID)),
				() -> assertThrows(NullPointerException.class, () -> mixDecryptOnlineController.mixOnline(ELECTION_EVENT_ID, null)),
				() -> assertThrows(FailedValidationException.class, () -> mixDecryptOnlineController.mixOnline("not UUID", BALLOT_BOX_ID)),
				() -> assertThrows(FailedValidationException.class, () -> mixDecryptOnlineController.mixOnline(ELECTION_EVENT_ID, "not UUID"))
		);
	}

	@Test
	void mixOnlineHappyPath() {
		when(mixDecryptOnlineService.startOnlineMixing(ELECTION_EVENT_ID, BALLOT_BOX_ID)).thenReturn(true);

		final ResponseEntity<Void> actual = mixDecryptOnlineController.mixOnline(ELECTION_EVENT_ID, BALLOT_BOX_ID);

		assertEquals(HttpStatus.OK, actual.getStatusCode());
	}

	@Test
	void mixOnlineBallotBoxNotClosed() {
		when(mixDecryptOnlineService.startOnlineMixing(ELECTION_EVENT_ID, BALLOT_BOX_ID)).thenReturn(false);

		final ResponseEntity<Void> actual = mixDecryptOnlineController.mixOnline(ELECTION_EVENT_ID, BALLOT_BOX_ID);

		assertEquals(HttpStatus.CONFLICT, actual.getStatusCode());
	}

	@Test
	void invalidParametersOnlineMixingStatusThrows() {
		assertAll(
				() -> assertThrows(NullPointerException.class, () -> mixDecryptOnlineController.onlineMixingStatus(null, BALLOT_BOX_ID)),
				() -> assertThrows(NullPointerException.class, () -> mixDecryptOnlineController.onlineMixingStatus(ELECTION_EVENT_ID, null)),
				() -> assertThrows(FailedValidationException.class, () -> mixDecryptOnlineController.onlineMixingStatus("not UUID", BALLOT_BOX_ID)),
				() -> assertThrows(FailedValidationException.class,
						() -> mixDecryptOnlineController.onlineMixingStatus(ELECTION_EVENT_ID, "not UUID"))
		);
	}

	@Test
	void onlineMixingStatusMixingHappyPath() {
		when(ballotBoxService.getBallotBoxStatus(BALLOT_BOX_ID)).thenReturn(BallotBoxStatus.MIXING);
		when(mixDecryptOnlineService.getOnlineStatus(ELECTION_EVENT_ID, BALLOT_BOX_ID)).thenReturn(BallotBoxStatus.MIXING_NOT_STARTED);

		final ResponseEntity<BallotBoxStatus> actual = mixDecryptOnlineController.onlineMixingStatus(ELECTION_EVENT_ID, BALLOT_BOX_ID);

		assertEquals(HttpStatus.OK, actual.getStatusCode());
		assertEquals(BallotBoxStatus.MIXING, actual.getBody());
	}

	@Test
	void onlineMixingStatusMixingErrorHappyPath() {
		when(ballotBoxService.getBallotBoxStatus(BALLOT_BOX_ID)).thenReturn(BallotBoxStatus.MIXING_ERROR);
		when(mixDecryptOnlineService.getOnlineStatus(ELECTION_EVENT_ID, BALLOT_BOX_ID)).thenReturn(BallotBoxStatus.MIXING_ERROR);
		when(ballotBoxService.updateBallotBoxStatus(BALLOT_BOX_ID, BallotBoxStatus.MIXING_ERROR)).thenReturn(BallotBoxStatus.MIXING_ERROR);

		final ResponseEntity<BallotBoxStatus> actual = mixDecryptOnlineController.onlineMixingStatus(ELECTION_EVENT_ID, BALLOT_BOX_ID);

		assertEquals(HttpStatus.OK, actual.getStatusCode());
		assertEquals(BallotBoxStatus.MIXING_ERROR, actual.getBody());
	}

	@Test
	void onlineMixingStatusMixedHappyPath() {
		when(ballotBoxService.getBallotBoxStatus(BALLOT_BOX_ID)).thenReturn(BallotBoxStatus.MIXED);

		final ResponseEntity<BallotBoxStatus> actual = mixDecryptOnlineController.onlineMixingStatus(ELECTION_EVENT_ID, BALLOT_BOX_ID);

		assertEquals(HttpStatus.OK, actual.getStatusCode());
		assertEquals(BallotBoxStatus.MIXED, actual.getBody());
	}

	@Test
	void invalidParametersDownloadMixedOnlineBallotsThrows() {
		assertAll(
				() -> assertThrows(NullPointerException.class, () -> mixDecryptOnlineController.downloadMixedOnlineBallots(null, BALLOT_BOX_ID)),
				() -> assertThrows(NullPointerException.class, () -> mixDecryptOnlineController.downloadMixedOnlineBallots(ELECTION_EVENT_ID, null)),
				() -> assertThrows(FailedValidationException.class,
						() -> mixDecryptOnlineController.downloadMixedOnlineBallots("not UUID", BALLOT_BOX_ID)),
				() -> assertThrows(FailedValidationException.class,
						() -> mixDecryptOnlineController.downloadMixedOnlineBallots(ELECTION_EVENT_ID, "not UUID"))
		);
	}

	@Test
	void downloadMixedOnlineBallotsHappyPath() {
		final ResponseEntity<Void> actual = mixDecryptOnlineController.downloadMixedOnlineBallots(ELECTION_EVENT_ID, BALLOT_BOX_ID);

		assertEquals(HttpStatus.OK, actual.getStatusCode());
	}
}