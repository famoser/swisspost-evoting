/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration;

import static ch.post.it.evoting.cryptoprimitives.domain.ControlComponentConstants.NODE_IDS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.SignatureException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.domain.election.ControlComponentPublicKeys;
import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.evotinglibraries.domain.configuration.ControlComponentPublicKeysPayload;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

@DisplayName("A ControlComponentPublicKeysConfigService")
class ControlComponentPublicKeysConfigServiceTest {

	private static final String ELECTION_EVENT_ID = "314bd34dcf6e4de4b771a92fa3849d3d";
	private static final String NOT_ENOUGH_ELECTION_EVENT_ID = "614bd34dcf6e4de4b771a92fa3849d3d";
	private static final String TOO_MANY_ELECTION_EVENT_ID = "714bd34dcf6e4de4b771a92fa3849d3d";
	private static final String INVALID_ID = "invalidId";

	private static ControlComponentPublicKeysConfigService controlComponentPublicKeysConfigService;
	private static SignatureKeystore<Alias> signatureKeystore;

	@BeforeAll
	static void setUpAll() throws URISyntaxException {
		final ObjectMapper objectMapper = DomainObjectMapper.getNewInstance();
		final Path path = Paths.get(
				Objects.requireNonNull(ControlComponentPublicKeysConfigServiceTest.class.getResource("/controlComponentPublicKeysTest/")).toURI());
		final PathResolver pathResolver = new PathResolver(path.toString());

		final ControlComponentPublicKeysPayloadFileRepository controlComponentPublicKeysPayloadFileRepository = new ControlComponentPublicKeysPayloadFileRepository(
				objectMapper, pathResolver);

		signatureKeystore = mock(SignatureKeystore.class);

		controlComponentPublicKeysConfigService = new ControlComponentPublicKeysConfigService(controlComponentPublicKeysPayloadFileRepository,
				signatureKeystore);
	}

	@Nested
	@DisplayName("loading")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class LoadTest {

		@BeforeEach
		void setUp() throws SignatureException {
			when(signatureKeystore.verifySignature(any(), any(), any(), any())).thenReturn(true);
		}

		@Test
		@DisplayName("existing election event returns all payloads")
		void loadExistingElectionEvent() {
			final List<ControlComponentPublicKeys> controlComponentPublicKeys = controlComponentPublicKeysConfigService.loadOrderByNodeId(
					ELECTION_EVENT_ID);

			final List<Integer> payloadsNodeIds = controlComponentPublicKeys.stream()
					.map(ControlComponentPublicKeys::nodeId)
					.toList();

			assertEquals(NODE_IDS.size(), controlComponentPublicKeys.size());
			assertTrue(payloadsNodeIds.containsAll(NODE_IDS));
		}

		@Test
		@DisplayName("invalid election event id throws FailedValidationException")
		void loadInvalidElectionEventId() {
			assertThrows(FailedValidationException.class, () -> controlComponentPublicKeysConfigService.loadOrderByNodeId(INVALID_ID));
		}

		@Test
		@DisplayName("existing election event with missing payloads throws IllegalStateException")
		void loadMissingPayloads() {
			final IllegalStateException exception = assertThrows(IllegalStateException.class,
					() -> controlComponentPublicKeysConfigService.loadOrderByNodeId(NOT_ENOUGH_ELECTION_EVENT_ID));

			final String errorMessage = String.format("Wrong number of control component public keys payloads. [required node ids: %s, found: %s]",
					NODE_IDS, Collections.singletonList(1));
			assertEquals(errorMessage, Throwables.getRootCause(exception).getMessage());
		}

		@Test
		@DisplayName("existing election event with too many payloads throws IllegalStateException")
		void loadTooManyPayloads() {
			final IllegalStateException exception = assertThrows(IllegalStateException.class,
					() -> controlComponentPublicKeysConfigService.loadOrderByNodeId(TOO_MANY_ELECTION_EVENT_ID));

			final String errorMessage = String.format("Wrong number of control component public keys payloads. [required node ids: %s, found: %s]",
					NODE_IDS, Arrays.asList(1, 2, 3, 4, 4));
			assertEquals(errorMessage, Throwables.getRootCause(exception).getMessage());
		}

		@Test
		@DisplayName("payload with invalid signature throws IllegalStateException")
		void loadInvalidPayloadSignature() throws SignatureException {
			when(signatureKeystore.verifySignature(any(), any(), any(), any())).thenReturn(false);

			final InvalidPayloadSignatureException exception = assertThrows(InvalidPayloadSignatureException.class,
					() -> controlComponentPublicKeysConfigService.loadOrderByNodeId(ELECTION_EVENT_ID));

			final String errorMessage = String.format(
					"Signature of payload %s is invalid. [electionEventId: %s, nodeId: %s]",
					ControlComponentPublicKeysPayload.class.getSimpleName(), ELECTION_EVENT_ID, 1);
			assertEquals(errorMessage, Throwables.getRootCause(exception).getMessage());
		}
	}
}