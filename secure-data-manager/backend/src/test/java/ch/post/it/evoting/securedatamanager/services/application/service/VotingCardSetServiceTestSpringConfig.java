/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.application.service;

import static org.mockito.Mockito.mock;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.securedatamanager.configuration.setupvoting.EncryptedNodeLongReturnCodeSharesService;
import ch.post.it.evoting.securedatamanager.configuration.setupvoting.ReturnCodesPayloadsGeneratedService;
import ch.post.it.evoting.securedatamanager.configuration.setupvoting.SetupComponentCMTablePayloadService;
import ch.post.it.evoting.securedatamanager.services.domain.service.BallotDataGeneratorService;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;
import ch.post.it.evoting.securedatamanager.services.infrastructure.ballot.BallotRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.ballotbox.BallotBoxRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.cc.SetupComponentVerificationDataPayloadFileRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.electionevent.ElectionEventRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.electoralboard.ElectoralBoardRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.service.ConfigurationEntityStatusService;
import ch.post.it.evoting.securedatamanager.services.infrastructure.votingcardset.VotingCardSetRepository;

@Configuration
public class VotingCardSetServiceTestSpringConfig {

	@Bean
	public IdleStatusService idleStatusService() {
		return mock(IdleStatusService.class);
	}

	@Bean
	public ConfigurationEntityStatusService configurationEntityStatusService() {
		return mock(ConfigurationEntityStatusService.class);
	}

	@Bean
	public PathResolver pathResolver() {
		return mock(PathResolver.class);
	}

	@Bean
	public VotingCardSetRepository votingCardSetRepository() {
		return mock(VotingCardSetRepository.class);
	}

	@Bean
	public ElectionEventRepository electionEventRepository() {
		return mock(ElectionEventRepository.class);
	}

	@Bean
	public ObjectMapper objectMapper() {
		return mock(ObjectMapper.class);
	}

	@Bean
	public BallotRepository ballotRepository() {
		return mock(BallotRepository.class);
	}

	@Bean
	public BallotBoxRepository ballotBoxRepository() {
		return mock(BallotBoxRepository.class);
	}

	@Bean
	public BallotDataGeneratorService ballotDataGeneratorService() {
		return mock(BallotDataGeneratorService.class);
	}

	@Bean
	public EncryptedLongReturnCodeSharesService votingCardSetChoiceCodesService() {
		return mock(EncryptedLongReturnCodeSharesService.class);
	}

	@Bean
	public SetupComponentVerificationDataPayloadFileRepository setupComponentVerificationDataPayloadFileSystemRepository() {
		return mock(SetupComponentVerificationDataPayloadFileRepository.class);
	}

	@Bean
	public ElectoralBoardRepository electoralAuthorityRepository() {
		return mock(ElectoralBoardRepository.class);
	}

	@Bean
	public EncryptedNodeLongReturnCodeSharesService encryptedNodeLongCodeSharesService() {
		return mock(EncryptedNodeLongReturnCodeSharesService.class);
	}

	@Bean
	public VotingCardSetDownloadService votingCardSetDownloadService(
			final PathResolver pathResolver,
			final IdleStatusService idleStatusService,
			final EncryptedLongReturnCodeSharesService encryptedLongReturnCodeSharesService,
			final ConfigurationEntityStatusService configurationEntityStatusService,
			final SetupComponentVerificationDataPayloadFileRepository setupComponentVerificationDataPayloadFileRepository) {
		return new VotingCardSetDownloadService(pathResolver, idleStatusService, encryptedLongReturnCodeSharesService,
				configurationEntityStatusService, setupComponentVerificationDataPayloadFileRepository);
	}

	@Bean
	public VotingCardSetGenerateBallotService votingCardSetGenerateBallotService(
			final BallotBoxRepository ballotBoxRepository,
			final BallotDataGeneratorService ballotDataGeneratorService,
			final ConfigurationEntityStatusService configurationEntityStatusService) {
		return new VotingCardSetGenerateBallotService(ballotBoxRepository, ballotDataGeneratorService, configurationEntityStatusService);
	}

	@Bean
	public VotingCardSetGenerateService votingCardSetGenerateService(
			final IdleStatusService idleStatusService,
			final ConfigurationEntityStatusService configurationEntityStatusService,
			final ReturnCodesPayloadsGeneratedService returnCodesPayloadsGeneratedService,
			final VotingCardSetGenerateBallotService votingCardSetGenerateBallotService) {
		return new VotingCardSetGenerateService(idleStatusService, configurationEntityStatusService,
				votingCardSetGenerateBallotService, returnCodesPayloadsGeneratedService);
	}

	@Bean
	public VotingCardSetSignService votingCardSetSignService(
			final ElectionEventContextPersistenceService electionEventContextPersistenceService,
			final ConfigurationEntityStatusService configurationEntityStatusService) {
		return new VotingCardSetSignService(electionEventContextPersistenceService, configurationEntityStatusService);
	}

	@Bean
	public ElectionEventContextPersistenceService electionEventContextPersistenceService() {
		return mock(ElectionEventContextPersistenceService.class);
	}

	@Bean
	public SetupComponentCMTablePayloadService returnCodesMappingTablePayloadService() {
		return mock(SetupComponentCMTablePayloadService.class);
	}

	@Bean
	public ReturnCodesPayloadsGeneratedService returnCodesMappingTablePayloadGenerationService() {
		return mock(ReturnCodesPayloadsGeneratedService.class);
	}

}
