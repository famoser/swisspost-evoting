/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.domain.config;

import java.io.IOException;
import java.util.Objects;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;

import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.hashing.Argon2;
import ch.post.it.evoting.cryptoprimitives.hashing.Argon2Factory;
import ch.post.it.evoting.cryptoprimitives.hashing.Argon2Profile;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystoreFactory;
import ch.post.it.evoting.securedatamanager.EncryptionParametersPayloadFileRepository;
import ch.post.it.evoting.securedatamanager.EncryptionParametersPayloadService;
import ch.post.it.evoting.securedatamanager.KeystoreRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.DatabaseManager;
import ch.post.it.evoting.securedatamanager.services.infrastructure.DatabaseManagerFactory;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

/**
 * MVC Configuration
 */
@Configuration
@ComponentScan(basePackages = { "ch.post.it.evoting.securedatamanager.services.infrastructure" })
@PropertySource("classpath:config/application.properties")
@Profile("test")
public class Config {

	@Value("${database.type}")
	private String databaseType;

	@Value("${database.path}")
	private String databasePath;

	@Value("${database.password.location}")
	private String passwordFile;

	@Autowired
	private DatabaseManagerFactory databaseManagerFactory;
	@Value("${direct.trust.keystore.location.config}")
	private String keystoreLocationConfig;
	@Value("${direct.trust.keystore.password.location.config}")
	private String keystorePasswordLocationConfig;
	@Value("${direct.trust.keystore.location.tally}")
	private String keystoreLocationTally;
	@Value("${direct.trust.keystore.password.location.tally}")
	private String keystorePasswordLocationTally;

	@Bean
	public static PropertySourcesPlaceholderConfigurer propertiesResolver() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	@Bean
	public DatabaseManagerFactory createDatabaseManagerFactory() {
		return new DatabaseManagerFactory(databaseType, databasePath, passwordFile);
	}

	@Bean
	public ObjectMapper objectMapper() {
		return new ObjectMapper();
	}

	@Bean(initMethod = "createDatabase")
	DatabaseManager databaseManager() {
		final String name = UUID.randomUUID().toString();
		return databaseManagerFactory.newDatabaseManager(name);
	}

	@Bean
	ObjectReader jsonReader() {
		return new ObjectMapper().reader();
	}

	@Bean
	public KeystoreRepository keystoreRepository() {
		return new KeystoreRepository(keystoreLocationConfig, keystorePasswordLocationConfig, keystoreLocationTally, keystorePasswordLocationTally);
	}

	@Bean
	public EncryptionParametersPayloadService encryptionParametersPayloadService(
			@Value("${sdm.workspace}")
			final String workspace) {
		return new EncryptionParametersPayloadService(new EncryptionParametersPayloadFileRepository(new PathResolver(workspace), objectMapper()));
	}

	@Bean
	@Qualifier("keystoreServiceSdmConfig")
	SignatureKeystore<Alias> keystoreServiceSdmConfig(final KeystoreRepository repository) throws IOException {
		return SignatureKeystoreFactory.createSignatureKeystore(repository.getConfigKeyStore(), "PKCS12", repository.getConfigKeystorePassword(),
				Objects::nonNull, Alias.SDM_CONFIG);
	}

	@Bean
	@Qualifier("keystoreServiceSdmTally")
	SignatureKeystore<Alias> keystoreServiceSdmTally(final KeystoreRepository repository) throws IOException {
		return SignatureKeystoreFactory.createSignatureKeystore(repository.getTallyKeyStore(), "PKCS12", repository.getTallyKeystorePassword(),
				Objects::nonNull, Alias.SDM_TALLY);
	}

	@Bean
	Argon2 argon2Standard() {
		// the CompressionService needs a bean with name "argon2Standard", for testing purposes we use the TEST context
		return Argon2Factory.createArgon2(Argon2Profile.TEST.getContext());
	}
}
