/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.UncheckedIOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

@DisplayName("NodeContributionsResponsesFileRepository")
class NodeContributionsResponsesFileRepositoryTest {

	private static final String WRONG_ID = "0123456789abcdef0123456789abcdef";
	private static final String ELECTION_EVENT_ID = "7E6225DF3A10F4A5D63D76EA7E0E7916";
	private static final String VERIFICATION_CARD_SET_ID = "5B78F34995FAE5EA69DBD3A7608F5397";

	private static final ObjectMapper objectMapper = DomainObjectMapper.getNewInstance();

	private static NodeContributionsResponsesFileRepository nodeContributionsResponsesRepository;

	@BeforeAll
	static void setUpAll() throws URISyntaxException {

		final Path path = Paths.get(
				NodeContributionsResponsesFileRepository.class.getResource("/nodeContributionsResponsesFileRepositoryTest/valid").toURI());
		final PathResolver pathResolver = new PathResolver(path.toString());

		nodeContributionsResponsesRepository = new NodeContributionsResponsesFileRepository(objectMapper, pathResolver);
	}

	@Test
	@DisplayName("Find all")
	void findAll() {
		final List<Path> nodeContributionsPath = nodeContributionsResponsesRepository.findAllPathsOrderByChunkId(
				ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID);

		assertEquals(1, nodeContributionsPath.size());
	}

	@Test
	@DisplayName("Find all with invalid ids throws")
	void findAllWithInvalidIds() {
		assertAll(
				() -> assertThrows(FailedValidationException.class,
						() -> nodeContributionsResponsesRepository.findAllPathsOrderByChunkId("invalidId", VERIFICATION_CARD_SET_ID)),
				() -> assertThrows(FailedValidationException.class,
						() -> nodeContributionsResponsesRepository.findAllPathsOrderByChunkId(ELECTION_EVENT_ID, "invalidId"))
		);
	}

	@Test
	@DisplayName("Find all with wrong path return empty list")
	void findAllWithWrongPath() {
		assertAll(
				() -> assertEquals(Collections.emptyList(),
						nodeContributionsResponsesRepository.findAllPathsOrderByChunkId(WRONG_ID, VERIFICATION_CARD_SET_ID)),
				() -> assertEquals(Collections.emptyList(), nodeContributionsResponsesRepository.findAllPathsOrderByChunkId(ELECTION_EVENT_ID, WRONG_ID))
		);
	}

	@Test
	@DisplayName("get payload with invalid node contribution throws")
	void getPayloadsWithInvalidNodeContributions() throws URISyntaxException {
		final Path path = Paths.get(
				NodeContributionsResponsesFileRepository.class.getResource("/nodeContributionsResponsesFileRepositoryTest/invalid").toURI());
		final PathResolver pathResolver = new PathResolver(path.toString());

		final NodeContributionsResponsesFileRepository repository = new NodeContributionsResponsesFileRepository(objectMapper, pathResolver);

		final Path verificationCardSetPath = pathResolver.resolveVerificationCardSetPath(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID);

		final UncheckedIOException exception = assertThrows(UncheckedIOException.class,
				() -> repository.getControlComponentCodeSharesPayloads(verificationCardSetPath.resolve("controlComponentCodeSharesPayload.0.json")));

		assertTrue(exception.getMessage().startsWith("Failed to deserialize the ControlComponentCodeSharesPayloads."));
	}

}
