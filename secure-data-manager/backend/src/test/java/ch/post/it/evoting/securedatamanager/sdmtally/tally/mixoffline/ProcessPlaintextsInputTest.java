/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.sdmtally.tally.mixoffline;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.security.SecureRandom;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientMessage;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.securedatamanager.sdmtally.tally.mixoffline.ProcessPlaintextsInput;

@DisplayName("Constructing a ProcessPlaintextsInput object with")
class ProcessPlaintextsInputTest {

	private static final SecureRandom RANDOM = new SecureRandom();
	private static final GqGroup GQ_GROUP = GroupTestData.getLargeGqGroup();
	private static final ElGamalGenerator elGamalGenerator = new ElGamalGenerator(GQ_GROUP);

	private GroupVector<ElGamalMultiRecipientMessage, GqGroup> plaintextVotes;

	private GroupVector<PrimeGqElement, GqGroup> writeInVotingOptions;
	private int numberOfSelectableVotes;
	private int numberOfAllowedWriteInsPlusOne;

	@BeforeEach
	void setup() {
		numberOfSelectableVotes = RANDOM.nextInt(120) + 1; // range [1, 120]
		numberOfAllowedWriteInsPlusOne = RANDOM.nextInt(5) + 1;
		final int n = RANDOM.nextInt(4) + 2;
		plaintextVotes = elGamalGenerator.genRandomMessageVector(n, numberOfAllowedWriteInsPlusOne);
		writeInVotingOptions = numberOfAllowedWriteInsPlusOne == 1 ?
				GroupVector.of() :
				PrimeGqElement.PrimeGqElementFactory.getSmallPrimeGroupMembers(GQ_GROUP, numberOfAllowedWriteInsPlusOne - 1);
	}

	@Test
	@DisplayName("null arguments throws a NullPointerException")
	void processPlaintextsInputWithNullArgumentsThrows() {
		ProcessPlaintextsInput.Builder builder = new ProcessPlaintextsInput.Builder()
				.setPlaintextVotes(plaintextVotes)
				.setNumberOfSelectableVotingOptions(numberOfSelectableVotes)
				.setNumberOfAllowedWriteInsPlusOne(numberOfAllowedWriteInsPlusOne);
		assertThrows(NullPointerException.class, builder::build);

		builder = new ProcessPlaintextsInput.Builder()
				.setWriteInVotingOptions(writeInVotingOptions)
				.setNumberOfSelectableVotingOptions(numberOfSelectableVotes)
				.setNumberOfAllowedWriteInsPlusOne(numberOfAllowedWriteInsPlusOne);
		assertThrows(NullPointerException.class, builder::build);
	}

	@Test
	@DisplayName("number of selectable votes less than 1 throws an IllegalArgumentException")
	void processPlaintextsInputWithTooSmallNumberOfSelectableVotesThrows() {
		final int tooSmallNumberOfSelectableVotes = 0;
		final ProcessPlaintextsInput.Builder builder = new ProcessPlaintextsInput.Builder()
				.setPlaintextVotes(plaintextVotes)
				.setWriteInVotingOptions(writeInVotingOptions)
				.setNumberOfSelectableVotingOptions(tooSmallNumberOfSelectableVotes)
				.setNumberOfAllowedWriteInsPlusOne(numberOfAllowedWriteInsPlusOne);
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, builder::build);
		assertEquals(
				String.format("The number of selectable voting options must be greater or equal to 1. [psi: %s]", tooSmallNumberOfSelectableVotes),
				Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("number of write-ins + 1 less than 1 throws an IllegalArgumentException")
	void processPlaintextsInputWithTooSmallNumberOfAllowedWriteInsPlusOneThrows() {
		final int tooSmallNumberOfWriteInsPlusOne = 0;
		final ProcessPlaintextsInput.Builder builder = new ProcessPlaintextsInput.Builder()
				.setPlaintextVotes(plaintextVotes)
				.setWriteInVotingOptions(writeInVotingOptions)
				.setNumberOfSelectableVotingOptions(numberOfSelectableVotes)
				.setNumberOfAllowedWriteInsPlusOne(tooSmallNumberOfWriteInsPlusOne);
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, builder::build);
		assertEquals(String.format("The number of allowed write-ins + 1 must be strictly greater than 0. [delta_hat: %s]",
				tooSmallNumberOfWriteInsPlusOne), Throwables.getRootCause(exception).getMessage());
	}
}
