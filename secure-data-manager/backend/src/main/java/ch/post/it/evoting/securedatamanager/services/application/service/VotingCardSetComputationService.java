/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.application.service;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.returncodes.SetupComponentVerificationDataPayload;
import ch.post.it.evoting.domain.configuration.setupvoting.ComputingStatus;
import ch.post.it.evoting.securedatamanager.services.domain.model.status.InvalidStatusTransitionException;
import ch.post.it.evoting.securedatamanager.services.infrastructure.cc.PayloadStorageException;
import ch.post.it.evoting.securedatamanager.services.infrastructure.cc.SetupComponentVerificationDataPayloadFileRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.service.ConfigurationEntityStatusService;

/**
 * This is an application service that deals with the computation of voting card data.
 */
@Service
public class VotingCardSetComputationService extends BaseVotingCardSetService {

	private static final Logger LOGGER = LoggerFactory.getLogger(VotingCardSetComputationService.class);

	private final IdleStatusService idleStatusService;
	private final ConfigurationEntityStatusService configurationEntityStatusService;
	private final EncryptedLongReturnCodeSharesService encryptedLongReturnCodeSharesService;
	private final SetupComponentVerificationDataPayloadFileRepository setupComponentVerificationDataPayloadFileRepository;

	public VotingCardSetComputationService(final IdleStatusService idleStatusService,
			final ConfigurationEntityStatusService configurationEntityStatusService,
			final EncryptedLongReturnCodeSharesService encryptedLongReturnCodeSharesService,
			final SetupComponentVerificationDataPayloadFileRepository setupComponentVerificationDataPayloadFileRepository) {
		this.idleStatusService = idleStatusService;
		this.configurationEntityStatusService = configurationEntityStatusService;
		this.encryptedLongReturnCodeSharesService = encryptedLongReturnCodeSharesService;
		this.setupComponentVerificationDataPayloadFileRepository = setupComponentVerificationDataPayloadFileRepository;
	}

	/**
	 * Compute a voting card set.
	 *
	 * @param votingCardSetId the identifier of the voting card set
	 * @param electionEventId the identifier of the election event
	 * @throws InvalidStatusTransitionException if the original status does not allow computing
	 * @throws PayloadStorageException          if the payload could not be store
	 */
	public void compute(final String votingCardSetId, final String electionEventId)
			throws InvalidStatusTransitionException, PayloadStorageException {

		if (!idleStatusService.getIdLock(votingCardSetId)) {
			return;
		}

		try {
			LOGGER.info("Starting computation of voting card set {}...", votingCardSetId);

			validateUUID(votingCardSetId);
			validateUUID(electionEventId);

			final ComputingStatus toStatus = ComputingStatus.COMPUTING;

			final String verificationCardSetId = votingCardSetRepository.getVerificationCardSetId(votingCardSetId);

			final int chunkCount = setupComponentVerificationDataPayloadFileRepository.getCount(electionEventId, verificationCardSetId);
			for (int chunkId = 0; chunkId < chunkCount; chunkId++) {
				// Retrieve the payload.
				final SetupComponentVerificationDataPayload payload = setupComponentVerificationDataPayloadFileRepository.retrieve(
						electionEventId, verificationCardSetId, chunkId);

				// Send chunk for processing.
				encryptedLongReturnCodeSharesService.computeGenEncLongCodeShares(payload);
			}

			// All chunks have been sent, update status.
			configurationEntityStatusService.update(toStatus.name(), votingCardSetId, votingCardSetRepository);
			LOGGER.info("Computation of voting card set {} started", votingCardSetId);

		} finally {
			idleStatusService.freeIdLock(votingCardSetId);
		}
	}
}
