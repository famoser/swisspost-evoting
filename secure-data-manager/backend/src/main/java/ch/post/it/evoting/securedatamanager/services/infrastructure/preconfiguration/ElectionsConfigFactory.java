/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.infrastructure.preconfiguration;

import static ch.post.it.evoting.cryptoprimitives.domain.election.ElectionAttributesAliasConstants.ALIAS_PREFIX_WRITE_IN;
import static ch.post.it.evoting.securedatamanager.services.infrastructure.preconfiguration.BallotContestFactory.ContestWithPosition;
import static ch.post.it.evoting.securedatamanager.services.infrastructure.preconfiguration.BallotContestFactory.createContest;
import static ch.post.it.evoting.securedatamanager.services.infrastructure.preconfiguration.TranslationsFactory.Translation;
import static ch.post.it.evoting.securedatamanager.services.infrastructure.preconfiguration.TranslationsFactory.createTranslationsWithoutBallotId;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.function.Predicate.not;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import javax.xml.datatype.XMLGregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.MoreCollectors;

import ch.post.it.evoting.cryptoprimitives.domain.election.AdministrationBoard;
import ch.post.it.evoting.cryptoprimitives.domain.election.Ballot;
import ch.post.it.evoting.cryptoprimitives.domain.election.BallotBox;
import ch.post.it.evoting.cryptoprimitives.domain.election.Contest;
import ch.post.it.evoting.cryptoprimitives.domain.election.ElectionEvent;
import ch.post.it.evoting.cryptoprimitives.domain.election.ElectoralBoard;
import ch.post.it.evoting.cryptoprimitives.domain.election.Identifier;
import ch.post.it.evoting.cryptoprimitives.domain.election.VotingCardSet;
import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.AdminBoardType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.AuthorizationObjectType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.AuthorizationType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.CandidatePositionType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.CandidateType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.Configuration;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ContestDescriptionInformationType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ContestType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.DomainOfInfluenceType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ElectionGroupBallotType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ElectionInformationType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ElectoralBoardType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.LanguageType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ListType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.StandardAnswerType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.StandardBallotType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.StandardQuestionType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.TieBreakQuestionType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.TiebreakAnswerType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.VariantBallotType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.VoteInformationType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.VoteType;
import ch.post.it.evoting.securedatamanager.commons.Constants;
import ch.post.it.evoting.securedatamanager.services.infrastructure.JsonConstants;

/**
 * Creates the elections_config.json from the configuration-anonymized.xml and generates the election ids.
 */
public class ElectionsConfigFactory {

	private static final Logger LOGGER = LoggerFactory.getLogger(ElectionsConfigFactory.class);
	private static final String STATUS_LOCKED = "LOCKED";
	private static final String RESOURCE_TYPE_CONFIGURATION = "configuration";

	private static final Random random = RandomFactory.createRandom();
	private static final ObjectMapper objectMapper = DomainObjectMapper.getNewInstance();

	private ElectionsConfigFactory() {
		// static usage only.
	}

	/**
	 * Creates the elections_config file and generates the election ids.
	 *
	 * @param configuration the {@link Configuration} representing the configuration-anonymized. Must be non-null.
	 * @return the elections_config as a {@link ObjectNode}.
	 */
	public static ObjectNode createElectionsConfig(final Configuration configuration) {
		LOGGER.debug("Creating the elections_config...");
		checkNotNull(configuration);

		final List<AuthorizationType> authorizationTypes = configuration.getAuthorizations().getAuthorization();

		final ObjectNode electionEventId = generateIdObjectFor(JsonConstants.ELECTION_EVENT);
		final ObjectNode electoralBoardId = generateIdObjectFor(JsonConstants.ELECTORAL_BOARD);
		final ObjectNode administrationBoardId = generateIdObjectFor(JsonConstants.ADMINISTRATION_BOARD);
		final Map<String, AuthorizationTypeIds> authorizationTypeIdsMap = generateAuthorizationTypeIds(authorizationTypes);
		final Map<String, VoteTypeAttributeIds> voteTypeAttributeIdsMap = generateVoteTypeAttributeIdsMap(configuration);
		final Map<String, ElectionTypeAttributeIds> electionTypeAttributeIdsMap = generateElectionTypeAttributeIds(configuration);

		final Map<String, List<ContestWithPosition>> domainOfInfluenceToContests = createContest(configuration, voteTypeAttributeIdsMap,
				electionTypeAttributeIdsMap);
		final Map<String, List<Translation>> domainOfInfluenceToTranslations = createTranslationsWithoutBallotId(configuration,
				voteTypeAttributeIdsMap, domainOfInfluenceToContests, electionTypeAttributeIdsMap);

		final ArrayNode electionEvents = createElectionEvents(configuration, electionEventId, administrationBoardId);
		final ArrayNode settings = createSettings(electionEventId);
		final ArrayNode ballots = createBallots(authorizationTypes, electionEventId, authorizationTypeIdsMap, domainOfInfluenceToContests);
		final ArrayNode ballotBoxes = createBallotBoxes(authorizationTypes, electionEventId, authorizationTypeIdsMap, electoralBoardId);
		final ArrayNode votingCardSets = createVotingCardSets(configuration, electionEventId, authorizationTypeIdsMap);
		final ArrayNode electoralBoards = createElectoralBoards(configuration, electionEventId, electoralBoardId);
		final ArrayNode administrationBoards = createAdministrationBoards(configuration, administrationBoardId);
		final ArrayNode translations = createTranslations(authorizationTypes, authorizationTypeIdsMap, domainOfInfluenceToTranslations);

		final ObjectNode electionsConfig = createGenericElectionsConfig(electionEvents, settings, ballots, ballotBoxes, votingCardSets,
				electoralBoards, administrationBoards, translations);

		LOGGER.info("Successfully created the elections_config.");

		return electionsConfig;
	}

	private static Map<String, AuthorizationTypeIds> generateAuthorizationTypeIds(final List<AuthorizationType> authorizationTypes) {
		return authorizationTypes.stream()
				.collect(Collectors.toMap(
						AuthorizationType::getAuthorizationIdentification,
						authorizationType -> {
							final ObjectNode ballotId = generateIdObjectFor(JsonConstants.BALLOT);
							final ObjectNode ballotBoxId = generateIdObjectFor(JsonConstants.BALLOT_BOX);
							final String votingCardSetId = random.genRandomBase16String(Constants.BASE16_ID_LENGTH);

							return new AuthorizationTypeIds(ballotId, ballotBoxId, votingCardSetId);
						}));
	}

	private static ObjectNode generateIdObjectFor(final String object) {
		return objectMapper.createObjectNode()
				.set(object, objectMapper.createObjectNode()
						.put(JsonConstants.ID, random.genRandomBase16String(Constants.BASE16_ID_LENGTH)));
	}

	private static Map<String, VoteTypeAttributeIds> generateVoteTypeAttributeIdsMap(final Configuration configuration) {
		return configuration.getContest().getVoteInformation().stream()
				.map(VoteInformationType::getVote)
				.map(VoteType::getBallot)
				.flatMap(List::stream)
				.map(ballotType -> {
					final Map<String, VoteTypeAttributeIds> voteTypeAttributeIdsMap = new HashMap<>();

					final StandardBallotType standardBallot = ballotType.getStandardBallot();
					if (standardBallot != null) {
						voteTypeAttributeIdsMap.putAll(generateVoteTypeAttributeIds(Stream.of(standardBallot),
								StandardBallotType::getQuestionIdentification, StandardBallotType::getAnswer,
								StandardAnswerType::getAnswerIdentification));
					}

					final VariantBallotType variantBallot = ballotType.getVariantBallot();
					if (variantBallot != null) {
						voteTypeAttributeIdsMap.putAll(generateVoteTypeAttributeIds(variantBallot.getStandardQuestion().stream(),
								StandardQuestionType::getQuestionIdentification, StandardQuestionType::getAnswer,
								StandardAnswerType::getAnswerIdentification));

						final List<TieBreakQuestionType> tieBreakQuestion = variantBallot.getTieBreakQuestion();
						if (tieBreakQuestion != null) {
							voteTypeAttributeIdsMap.putAll(generateVoteTypeAttributeIds(variantBallot.getTieBreakQuestion().stream(),
									TieBreakQuestionType::getQuestionIdentification, TieBreakQuestionType::getAnswer,
									TiebreakAnswerType::getAnswerIdentification));
						}
					}

					return voteTypeAttributeIdsMap;
				}).flatMap(map -> map.entrySet().stream())
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
	}

	private static <S, T> Map<String, VoteTypeAttributeIds> generateVoteTypeAttributeIds(final Stream<S> questions,
			final Function<S, String> getQuestionIdentification, final Function<S, List<T>> getAnswer,
			final Function<T, String> getAnswerIdentification) {
		return questions
				.collect(Collectors.toMap(
						getQuestionIdentification,
						question -> {
							final String attributeQuestionId = random.genRandomBase16String(Constants.BASE16_ID_LENGTH);
							final Map<String, String> answerTypeAttributeId = getAnswer.apply(question).stream()
									.collect(Collectors.toMap(
											getAnswerIdentification,
											ignored -> random.genRandomBase16String(Constants.BASE16_ID_LENGTH)));
							return new VoteTypeAttributeIds(attributeQuestionId, answerTypeAttributeId);
						}));
	}

	private static Map<String, ElectionTypeAttributeIds> generateElectionTypeAttributeIds(final Configuration configuration) {
		return configuration.getContest().getElectionGroupBallot().stream().parallel()
				.map(ElectionGroupBallotType::getElectionInformation)
				.flatMap(Collection::stream)
				.collect(Collectors.toConcurrentMap(
						electionInformationType -> electionInformationType.getElection().getElectionIdentification(),
						electionInformationType -> {
							// generate an attribute id for the attribute candidates
							final String candidatesAttributeId = random.genRandomBase16String(Constants.BASE16_ID_LENGTH);

							// generate an attribute id for all candidates
							final Map<String, String> candidateAttributeIdsMap = generateCandidateAttributeIdsMap(electionInformationType);

							// generate an attribute id for the attribute lists if there is at least one non-empty list
							final boolean electionWithLists = electionInformationType.getList().stream().parallel()
									.anyMatch(not(ListType::isListEmpty));
							final String listsAttributeId = electionWithLists ? random.genRandomBase16String(Constants.BASE16_ID_LENGTH) : null;

							final Map<String, String> listAttributeIdsMap = electionWithLists
									// generate an attribute id for all lists if there is at least one non-empty list
									? electionInformationType.getList().stream().parallel().collect(Collectors.toMap(
									ListType::getListIdentification,
									ignored -> random.genRandomBase16String(Constants.BASE16_ID_LENGTH)))
									// generate an attribute for all candidate's dummy lists otherwise
									: electionInformationType.getCandidate().stream().parallel().collect(Collectors.toMap(
									CandidateType::getCandidateIdentification,
									ignored -> random.genRandomBase16String(Constants.BASE16_ID_LENGTH))
							);

							return new ElectionTypeAttributeIds(candidatesAttributeId, candidateAttributeIdsMap, listsAttributeId,
									listAttributeIdsMap);
						}));
	}

	private static Map<String, String> generateCandidateAttributeIdsMap(final ElectionInformationType electionInformationType) {
		// generate an attribute id for all candidate identifications
		final Map<String, String> candidateAttributeIdsMap = electionInformationType.getCandidate().stream()
				.collect(Collectors.toMap(CandidateType::getCandidateIdentification,
						ignored -> random.genRandomBase16String(Constants.BASE16_ID_LENGTH)));

		// generate an attribute id for all blank candidates, if write-ins allowed generate also for all write-in candidates
		final String blankCandidateAttributeId = random.genRandomBase16String(Constants.BASE16_ID_LENGTH);
		final String writeInCandidateAttributeId = random.genRandomBase16String(Constants.BASE16_ID_LENGTH);
		candidateAttributeIdsMap.putAll(electionInformationType.getList().stream()
				.filter(ListType::isListEmpty)
				.map(ListType::getCandidatePosition)
				.flatMap(List::stream)
				.map(CandidatePositionType::getCandidateListIdentification)
				.distinct()
				.map(candidateListIdentification -> {
					final Map<String, String> candidateAttributeIds = new HashMap<>(Map.of(candidateListIdentification, blankCandidateAttributeId));
					if (electionInformationType.getElection().isWriteInsAllowed()) {
						candidateAttributeIds.put(ALIAS_PREFIX_WRITE_IN + candidateListIdentification, writeInCandidateAttributeId);
					}
					return candidateAttributeIds;
				})
				.flatMap(map -> map.entrySet().stream())
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)));

		return candidateAttributeIdsMap;
	}

	private static ArrayNode createElectionEvents(final Configuration configuration, final ObjectNode electionEventId,
			final ObjectNode administrationBoardId) {
		final Identifier administrationBoard = new Identifier(
				administrationBoardId.get(JsonConstants.ADMINISTRATION_BOARD).get(JsonConstants.ID).asText());
		final ContestType contestType = configuration.getContest();
		final String contestDescription = contestType.getContestDescription().getContestDescriptionInfo().stream()
				.filter(contestDescriptionInfo -> contestDescriptionInfo.getLanguage().equals(LanguageType.DE))
				.map(ContestDescriptionInformationType.ContestDescriptionInfo::getContestDescription)
				.collect(MoreCollectors.onlyElement());
		final int gracePeriod = configuration.getAuthorizations().getAuthorization().get(0).getAuthorizationGracePeriod();

		final ElectionEvent electionEvent = new ElectionEvent.ElectionEventBuilder()
				.setId(electionEventId.get(JsonConstants.ELECTION_EVENT).get(JsonConstants.ID).asText())
				.setDefaultTitle(contestDescription)
				.setDefaultDescription(contestDescription)
				.setAlias(contestType.getContestIdentification())
				.setDateFrom(toLocalDateTime(contestType.getEvotingFromDate()))
				.setDateTo(toLocalDateTime(contestType.getEvotingToDate()))
				.setGracePeriod(gracePeriod)
				.setAdministrationBoard(administrationBoard)
				.build();

		final JsonNode electionEventsNode;
		try {
			electionEventsNode = objectMapper.readTree(objectMapper.writeValueAsBytes(List.of(electionEvent)));
		} catch (final IOException e) {
			throw new UncheckedIOException("Cannot serialize the ElectionEvent object to a JsonNode.", e);
		}

		return (ArrayNode) electionEventsNode;
	}

	private static ArrayNode createSettings(final ObjectNode electionEventId) {
		return objectMapper.createArrayNode()
				.add(objectMapper.createObjectNode()
						.setAll(electionEventId));
	}

	private static ArrayNode createBallots(final List<AuthorizationType> authorizationTypes, final ObjectNode electionEventId,
			final Map<String, AuthorizationTypeIds> authorizationTypeIdsMap,
			final Map<String, List<ContestWithPosition>> domainOfInfluenceToContests) {
		return IntStream.range(0, authorizationTypes.size())
				.mapToObj(index -> {
					final AuthorizationType authorizationType = authorizationTypes.get(index);
					final AuthorizationTypeIds authorizationTypeIds = authorizationTypeIdsMap.get(authorizationType.getAuthorizationIdentification());
					final ObjectNode ballotId = authorizationTypeIds.ballotId();
					return createBallot(authorizationType, electionEventId, ballotId, index + 1, domainOfInfluenceToContests);
				})
				.reduce(objectMapper.createArrayNode(), ArrayNode::add, ArrayNode::addAll);
	}

	private static JsonNode createBallot(final AuthorizationType authorizationType, final ObjectNode electionEventId, final ObjectNode ballotId,
			final int index, final Map<String, List<ContestWithPosition>> domainOfInfluenceToContests) {
		final Identifier electionEvent = new Identifier(electionEventId.get(JsonConstants.ELECTION_EVENT).get(JsonConstants.ID).asText());
		final List<Contest> contests = authorizationType.getAuthorizationObject().stream()
				.map(AuthorizationObjectType::getDomainOfInfluence)
				.map(DomainOfInfluenceType::getDomainOfInfluenceIdentification)
				.flatMap(domainOfInfluenceType -> checkNotNull(domainOfInfluenceToContests.get(domainOfInfluenceType),
						"The domain of influence id of the authorization object does not exist.").stream())
				.sorted(Comparator.comparingInt(ContestWithPosition::position)
						.thenComparing(contestWithPosition -> contestWithPosition.contest().template()))
				.map(ContestWithPosition::contest)
				.toList();

		final Ballot ballot = new Ballot.BallotBuilder()
				.setId(ballotId.get(JsonConstants.BALLOT).get(JsonConstants.ID).asText())
				.setDefaultTitle(Ballot.PREFIX + authorizationType.getAuthorizationName())
				.setDefaultDescription(Ballot.PREFIX + authorizationType.getAuthorizationAlias())
				.setAlias(Ballot.PREFIX + index)
				.setStatus(STATUS_LOCKED)
				.setElectionEvent(electionEvent)
				.setContests(contests)
				.build();

		final JsonNode ballotNode;
		try {
			ballotNode = objectMapper.readTree(objectMapper.writeValueAsBytes(ballot));
		} catch (final IOException e) {
			throw new UncheckedIOException("Cannot serialize the Ballot object to a JsonNode.", e);
		}

		return ballotNode;
	}

	private static ArrayNode createBallotBoxes(final List<AuthorizationType> authorizationTypes, final ObjectNode electionEventId,
			final Map<String, AuthorizationTypeIds> authorizationTypeIdsMap, final ObjectNode electoralBoardId) {
		return authorizationTypes.stream()
				.map(authorizationType -> {
					final AuthorizationTypeIds authorizationTypeIds = authorizationTypeIdsMap.get(authorizationType.getAuthorizationIdentification());
					final ObjectNode ballotId = authorizationTypeIds.ballotId();
					final ObjectNode ballotBoxId = authorizationTypeIds.ballotBoxId();
					return createBallotBox(authorizationType, electionEventId, ballotId, ballotBoxId, electoralBoardId);
				})
				.reduce(objectMapper.createArrayNode(), ArrayNode::add, ArrayNode::addAll);
	}

	private static JsonNode createBallotBox(final AuthorizationType authorizationType, final ObjectNode electionEventId,
			final ObjectNode ballotId, final ObjectNode ballotBoxId, final ObjectNode electoralBoardId) {
		final Identifier electionEvent = new Identifier(electionEventId.get(JsonConstants.ELECTION_EVENT).get(JsonConstants.ID).asText());
		final Identifier ballot = new Identifier(ballotId.get(JsonConstants.BALLOT).get(JsonConstants.ID).asText());
		final Identifier electoralBoard = new Identifier(electoralBoardId.get(JsonConstants.ELECTORAL_BOARD).get(JsonConstants.ID).asText());

		final BallotBox ballotBox = new BallotBox.BallotBoxBuilder()
				.setId(ballotBoxId.get(JsonConstants.BALLOT_BOX).get(JsonConstants.ID).asText())
				.setDefaultTitle(authorizationType.getAuthorizationName())
				.setDefaultDescription(authorizationType.getAuthorizationAlias())
				.setAlias(authorizationType.getAuthorizationIdentification())
				.setDateFrom(toLocalDateTime(authorizationType.getAuthorizationFromDate()))
				.setDateTo(toLocalDateTime(authorizationType.getAuthorizationToDate()))
				.setTest(authorizationType.isAuthorizationTest())
				.setStatus(STATUS_LOCKED)
				.setGracePeriod(authorizationType.getAuthorizationGracePeriod())
				.setElectionEvent(electionEvent)
				.setBallot(ballot)
				.setElectoralBoard(electoralBoard)
				.build();

		final JsonNode ballotBoxNode;
		try {
			ballotBoxNode = objectMapper.readTree(objectMapper.writeValueAsBytes(ballotBox));
		} catch (final IOException e) {
			throw new UncheckedIOException("Cannot serialize the BallotBox object to a JsonNode.", e);
		}

		return ballotBoxNode;
	}

	private static LocalDateTime toLocalDateTime(final XMLGregorianCalendar xmlGregorianCalendar) {
		return OffsetDateTime.parse(xmlGregorianCalendar.toXMLFormat(), DateTimeFormatter.ISO_DATE_TIME)
				.atZoneSameInstant(ZoneId.systemDefault())
				.toLocalDateTime();
	}

	private static ArrayNode createVotingCardSets(final Configuration configuration, final ObjectNode electionEventId,
			final Map<String, AuthorizationTypeIds> authorizationTypeIdsMap) {
		return configuration.getAuthorizations().getAuthorization().stream()
				.map(authorizationType -> {
					final AuthorizationTypeIds authorizationTypeIds = authorizationTypeIdsMap.get(authorizationType.getAuthorizationIdentification());
					final ObjectNode ballotBoxId = authorizationTypeIds.ballotBoxId();
					final String votingCardSetId = authorizationTypeIds.votingCardSetId();

					return createVotingCardSet(configuration, authorizationType, electionEventId, ballotBoxId, votingCardSetId);
				})
				.reduce(objectMapper.createArrayNode(), ArrayNode::add, ArrayNode::addAll);
	}

	private static JsonNode createVotingCardSet(final Configuration configuration, final AuthorizationType authorizationType,
			final ObjectNode electionEventId, final ObjectNode ballotBoxId, final String votingCardSetId) {
		final int numberOfVotingCardsToGenerate = (int) configuration.getRegister().getVoter().stream()
				.filter(voterType -> voterType.getAuthorization().equals(authorizationType.getAuthorizationIdentification()))
				.count();
		final Identifier electionEvent = new Identifier(electionEventId.get(JsonConstants.ELECTION_EVENT).get(JsonConstants.ID).asText());
		final Identifier ballotBox = new Identifier(ballotBoxId.get(JsonConstants.BALLOT_BOX).get(JsonConstants.ID).asText());

		final VotingCardSet votingCardSet = new VotingCardSet.VotingCardSetBuilder()
				.setId(votingCardSetId)
				.setDefaultTitle(VotingCardSet.PREFIX + authorizationType.getAuthorizationName())
				.setDefaultDescription(VotingCardSet.PREFIX + authorizationType.getAuthorizationAlias())
				.setAlias(VotingCardSet.PREFIX + authorizationType.getAuthorizationIdentification())
				.setNumberOfVotingCardsToGenerate(numberOfVotingCardsToGenerate)
				.setVerificationCardSetId(random.genRandomBase16String(Constants.BASE16_ID_LENGTH))
				.setStatus(STATUS_LOCKED)
				.setElectionEvent(electionEvent)
				.setBallotBox(ballotBox)
				.build();

		final JsonNode votingCardSetNode;
		try {
			votingCardSetNode = objectMapper.readTree(objectMapper.writeValueAsBytes(votingCardSet));
		} catch (final IOException e) {
			throw new UncheckedIOException("Cannot serialize the VotingCardSet object to a JsonNode.", e);
		}

		return votingCardSetNode;
	}

	private static ArrayNode createElectoralBoards(final Configuration configuration, final ObjectNode electionEventId,
			final ObjectNode electoralBoardId) {
		final ElectoralBoardType electoralBoardType = configuration.getContest().getElectoralBoard();
		final List<String> boardMembers = electoralBoardType.getElectoralBoardMembers().getElectoralBoardMemberName();
		final Identifier electionEvent = new Identifier(electionEventId.get(JsonConstants.ELECTION_EVENT).get(JsonConstants.ID).asText());

		final ElectoralBoard electoralBoard = new ElectoralBoard.ElectoralBoardBuilder()
				.setId(electoralBoardId.get(JsonConstants.ELECTORAL_BOARD).get(JsonConstants.ID).asText())
				.setDefaultTitle(electoralBoardType.getElectoralBoardName())
				.setDefaultDescription(electoralBoardType.getElectoralBoardDescription())
				.setAlias(electoralBoardType.getElectoralBoardIdentification())
				.setStatus(STATUS_LOCKED)
				.setBoardMembers(boardMembers)
				.setElectionEvent(electionEvent)
				.build();

		final JsonNode electoralBoardNode;
		try {
			electoralBoardNode = objectMapper.readTree(objectMapper.writeValueAsBytes(List.of(electoralBoard)));
		} catch (final IOException e) {
			throw new UncheckedIOException("Cannot serialize the ElectoralBoard object to a JsonNode.", e);
		}

		return (ArrayNode) electoralBoardNode;
	}

	private static ArrayNode createAdministrationBoards(final Configuration configuration, final ObjectNode administrationBoardId) {
		final AdminBoardType adminBoardType = configuration.getContest().getAdminBoard();
		final String defaultTitle = adminBoardType.getAdminBoardName();
		final String defaultDescription = adminBoardType.getAdminBoardDescription();
		final String alias = adminBoardType.getAdminBoardIdentification();
		final int minimumThreshold = adminBoardType.getAdminBoardThresholdValue();
		final List<String> boardMembers = adminBoardType.getAdminBoardMembers().getAdminBoardMemberName();

		final AdministrationBoard administrationBoard = new AdministrationBoard.AdministrationBoardBuilder()
				.setId(administrationBoardId.get(JsonConstants.ADMINISTRATION_BOARD).get(JsonConstants.ID).asText())
				.setDefaultTitle(defaultTitle)
				.setDefaultDescription(defaultDescription)
				.setAlias(alias)
				.setMinimumThreshold(minimumThreshold)
				.setStatus(STATUS_LOCKED)
				.setBoardMembers(boardMembers)
				.build();

		final JsonNode administrationBoardsNode;
		try {
			administrationBoardsNode = objectMapper.readTree(objectMapper.writeValueAsBytes(List.of(administrationBoard)));
		} catch (final IOException e) {
			throw new UncheckedIOException("Cannot serialize the AdministrationBoard object to a JsonNode.", e);
		}

		return (ArrayNode) administrationBoardsNode;
	}

	private static ArrayNode createTranslations(final List<AuthorizationType> authorizationTypes,
			final Map<String, AuthorizationTypeIds> authorizationTypeIdsMap, final Map<String, List<Translation>> domainOfInfluenceToTranslations) {
		return authorizationTypes.stream()
				.map(authorizationType -> {
					final AuthorizationTypeIds authorizationTypeIds = authorizationTypeIdsMap.get(authorizationType.getAuthorizationIdentification());
					final ObjectNode ballotId = authorizationTypeIds.ballotId();
					return addBallotIdToTranslations(authorizationType, ballotId, domainOfInfluenceToTranslations);
				})
				.reduce(objectMapper.createArrayNode(), ArrayNode::addAll);
	}

	private static ArrayNode addBallotIdToTranslations(final AuthorizationType authorizationType, final ObjectNode ballotId,
			final Map<String, List<Translation>> domainOfInfluenceToTranslations) {
		// group by locale the translations belonging to the same domain of influence
		final Map<String, List<Translation>> localeToTranslations = authorizationType.getAuthorizationObject().stream()
				.map(AuthorizationObjectType::getDomainOfInfluence)
				.map(DomainOfInfluenceType::getDomainOfInfluenceIdentification)
				.flatMap(domainOfInfluenceType -> checkNotNull(domainOfInfluenceToTranslations.get(domainOfInfluenceType),
						"The domain of influence id of the authorization object does not exist.").stream())
				.collect(Collectors.groupingBy(Translation::locale));

		// merge the texts of the translations belonging to the same domain of influence and locale
		return localeToTranslations.keySet().stream()
				.map(locale -> new Translation(locale, localeToTranslations.get(locale).stream()
						.map(Translation::texts)
						.reduce(objectMapper.createObjectNode(), ObjectNode::setAll)))
				.map(translation -> createGenericTranslation(translation.texts(), translation.locale(), ballotId))
				.reduce(objectMapper.createArrayNode(), ArrayNode::add, ArrayNode::addAll);
	}

	private static ObjectNode createGenericTranslation(final ObjectNode texts, final String locale, final ObjectNode ballotId) {
		return ((ObjectNode) objectMapper.createObjectNode()
				.put(JsonConstants.LOCALE, locale)
				.put(JsonConstants.STATUS, STATUS_LOCKED)
				.set(JsonConstants.TEXTS, texts))
				.setAll(ballotId);
	}

	private static ObjectNode createGenericElectionsConfig(final ArrayNode electionEvents, final ArrayNode settings, final ArrayNode ballots,
			final ArrayNode ballotBoxes, final ArrayNode votingCardSets, final ArrayNode electoralBoards, final ArrayNode administrationBoards,
			final ArrayNode translations) {

		final ObjectNode electionsConfig = objectMapper.createObjectNode();
		electionsConfig.put(JsonConstants.RESOURCE_TYPE, RESOURCE_TYPE_CONFIGURATION);
		electionsConfig.set(JsonConstants.ELECTION_EVENTS, electionEvents);
		electionsConfig.set(JsonConstants.SETTINGS, settings);
		electionsConfig.set(JsonConstants.BALLOTS, ballots);
		electionsConfig.set(JsonConstants.BALLOT_BOXES, ballotBoxes);
		electionsConfig.set(JsonConstants.VOTING_CARD_SETS, votingCardSets);
		electionsConfig.set(JsonConstants.ELECTORAL_BOARDS, electoralBoards);
		electionsConfig.set(JsonConstants.ADMINISTRATION_BOARDS, administrationBoards);
		electionsConfig.set(JsonConstants.TRANSLATIONS, translations);

		return electionsConfig;
	}

	public record AuthorizationTypeIds(ObjectNode ballotId, ObjectNode ballotBoxId, String votingCardSetId) {

	}

	public record VoteTypeAttributeIds(String attributeQuestionId, Map<String, String> attributeAnswerId) {

	}

	/**
	 * @param listsAttributeId    null if there are no non-empty lists in the election.
	 * @param listAttributeIdsMap null if there are no non-empty lists in the election.
	 */
	public record ElectionTypeAttributeIds(String candidatesAttributeId, Map<String, String> candidateAttributeIdsMap, String listsAttributeId,
										   Map<String, String> listAttributeIdsMap) {

	}
}
