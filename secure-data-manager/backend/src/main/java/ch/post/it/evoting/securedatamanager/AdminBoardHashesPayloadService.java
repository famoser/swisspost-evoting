/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.domain.configuration.AdminBoardHashesPayload;

/**
 * Allows saving, retrieving and finding existing admin board hashes payloads.
 */
@Service
public class AdminBoardHashesPayloadService {

	private static final Logger LOGGER = LoggerFactory.getLogger(AdminBoardHashesPayloadService.class);

	private final AdminBoardHashesPayloadFileRepository adminBoardHashesPayloadFileRepository;

	public AdminBoardHashesPayloadService(
			final AdminBoardHashesPayloadFileRepository adminBoardHashesPayloadFileRepository) {
		this.adminBoardHashesPayloadFileRepository = adminBoardHashesPayloadFileRepository;
	}

	/**
	 * Saves an admin board hashes payload in the corresponding admin board folder.
	 *
	 * @param adminBoardHashesPayload the admin board hashes payload to save. Must be non-null.
	 * @throws NullPointerException if the payload is null.
	 */
	public void save(final AdminBoardHashesPayload adminBoardHashesPayload) {
		checkNotNull(adminBoardHashesPayload);

		adminBoardHashesPayloadFileRepository.save(adminBoardHashesPayload);
		LOGGER.info("Signed and saved admin board hashes payload. [adminBoardId: {}]", adminBoardHashesPayload.getAdminBoardId());
	}

	/**
	 * Checks if the admin board hashes payload is present for the given admin board id.
	 *
	 * @param adminBoardId the admin board id to check. Must be non-null and a valid UUID.
	 * @return {@code true} if the admin board hashes payload is present, {@code false} otherwise.
	 * @throws FailedValidationException if {@code adminBoardId} is invalid.
	 */
	public boolean exist(final String adminBoardId) {
		validateUUID(adminBoardId);

		return adminBoardHashesPayloadFileRepository.existsById(adminBoardId);
	}

	/**
	 * Loads the admin board hashes for the given {@code adminBoardId}. The result of this method is cached.
	 *
	 * @param adminBoardId the admin board id. Must be non-null and a valid UUID.
	 * @return the admin board key hashes for this {@code adminBoardId}.
	 * @throws NullPointerException      if any input is null.
	 * @throws FailedValidationException if {@code adminBoardId} is an invalid UUID.
	 * @throws IllegalStateException     if the requested admin board hashes payload is not present. </li>
	 */
	@Cacheable("adminBoardHashesPayloads")
	public AdminBoardHashesPayload load(final String adminBoardId) {
		validateUUID(adminBoardId);

		final AdminBoardHashesPayload adminBoardHashesPayload = adminBoardHashesPayloadFileRepository.findById(adminBoardId)
				.orElseThrow(() -> new IllegalStateException(
						String.format("Requested admin board hashes payload is not present. [adminBoardId: %s]", adminBoardId)));

		LOGGER.info("Loaded admin board hashes payload. [adminBoardId: {}]", adminBoardId);

		return adminBoardHashesPayload;
	}

}
