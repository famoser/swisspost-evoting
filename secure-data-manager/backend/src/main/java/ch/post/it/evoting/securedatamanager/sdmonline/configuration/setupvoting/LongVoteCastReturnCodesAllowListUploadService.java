/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.sdmonline.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.domain.configuration.setupvoting.SetupComponentLVCCAllowListPayload;
import ch.post.it.evoting.securedatamanager.commons.RemoteService;
import ch.post.it.evoting.securedatamanager.configuration.setupvoting.SetupComponentLVCCAllowListPayloadService;
import ch.post.it.evoting.securedatamanager.services.infrastructure.clients.MessageBrokerOrchestratorClient;

@Service
public class LongVoteCastReturnCodesAllowListUploadService {

	private static final Logger LOGGER = LoggerFactory.getLogger(LongVoteCastReturnCodesAllowListUploadService.class);

	private final SetupComponentLVCCAllowListPayloadService setupComponentLVCCAllowListPayloadService;
	private final MessageBrokerOrchestratorClient messageBrokerOrchestratorClient;
	private final boolean isVotingPortalEnabled;

	public LongVoteCastReturnCodesAllowListUploadService(
			final SetupComponentLVCCAllowListPayloadService setupComponentLVCCAllowListPayloadService,
			final MessageBrokerOrchestratorClient messageBrokerOrchestratorClient,
			@Value("${voting.portal.enabled}")
			final boolean isVotingPortalEnabled) {
		this.setupComponentLVCCAllowListPayloadService = setupComponentLVCCAllowListPayloadService;
		this.messageBrokerOrchestratorClient = messageBrokerOrchestratorClient;
		this.isVotingPortalEnabled = isVotingPortalEnabled;
	}

	/**
	 * Uploads the setup component LVCC allow list payload corresponding to the given election event id and verification card set id to the control
	 * components through the message broker orchestrator. If the election event id is empty, the upload is not done.
	 *
	 * @param electionEventId       the election event id. Must be non-null. If the election event id is not empty, it must be a valid UUID.
	 * @param verificationCardSetId the verification card set id. Must be non-null and a valid UUID.
	 * @throws NullPointerException      if {@code electionEventId} or {@code verificationCardSetId} is null.
	 * @throws FailedValidationException if {@code electionEventId} is not empty and not a valid UUID or if {@code verificationCardSetId} is not a
	 *                                   valid UUID.
	 */
	public void upload(final String electionEventId, final String verificationCardSetId) {
		checkNotNull(electionEventId);
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		checkState(isVotingPortalEnabled, "The voting-portal connection is not enabled.");

		LOGGER.debug("Uploading setup component LVCC allow list payload... [electionEventId: {}, verificationCardSetId: {}]", electionEventId,
				verificationCardSetId);

		final SetupComponentLVCCAllowListPayload setupComponentLVCCAllowListPayload = setupComponentLVCCAllowListPayloadService.load(electionEventId,
				verificationCardSetId);

		RemoteService.call(messageBrokerOrchestratorClient.uploadLongVoteCastReturnCodesAllowList(electionEventId, verificationCardSetId,
						setupComponentLVCCAllowListPayload))
				.networkErrorMessage("Failed to connect to orchestrator")
				.unsuccessfulResponseErrorMessage(
						"Request for uploading long vote cast return codes allow list failed. [electionEventId: %s, verificationCardSetId: %s]",
						electionEventId, verificationCardSetId)
				.execute();

		LOGGER.info("Successfully uploaded setup component LVCC allow list payload. [electionEventId: {}, verificationCardSetId: {}]",
				electionEventId, verificationCardSetId);
	}
}
