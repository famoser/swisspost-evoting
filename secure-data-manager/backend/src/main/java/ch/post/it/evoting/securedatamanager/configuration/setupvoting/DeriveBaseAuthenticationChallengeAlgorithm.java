/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateBase32NoPadAlphabet;
import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.cryptoprimitives.internal.utils.ByteArrays.cutToBitLength;
import static ch.post.it.evoting.cryptoprimitives.utils.Conversions.stringToByteArray;
import static com.google.common.base.Preconditions.checkArgument;

import java.util.function.Predicate;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.google.common.primitives.Bytes;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.hashing.Argon2;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableString;
import ch.post.it.evoting.cryptoprimitives.math.Base64;
import ch.post.it.evoting.securedatamanager.commons.Constants;

@Service
public class DeriveBaseAuthenticationChallengeAlgorithm {

	private final Hash hash;
	private final Argon2 argon2;
	private final Base64 base64;

	public DeriveBaseAuthenticationChallengeAlgorithm(final Hash hash,
			@Qualifier("argon2LessMemory")
			final Argon2 argon2,
			final Base64 base64) {
		this.hash = hash;
		this.argon2 = argon2;
		this.base64 = base64;
	}

	/**
	 * Derives the base authentication challenge for the given election event from the given start voting key and extended authentication factor
	 *
	 * @param electionEventId              ee, the identifier of the election event. Must be a valid UUID.
	 * @param startVotingKey               SVK<sub>id</sub>, a start voting key. Must be a valid Base32 string without padding of length
	 *                                     l<sub>SVK</sub>.
	 * @param extendedAuthenticationFactor EA<sub>id</sub>, an extended authentication factor. Must be a valid Base10 string of length
	 *                                     l<sub>EA</sub>.
	 * @return the base authentication challenge as a string.
	 * @throws NullPointerException      if any of the inputs is null.
	 * @throws FailedValidationException if
	 *                                   <ul>
	 *                                       <li>the election event id is not a valid UUID</li>
	 *                                       <li>the start voting key is not a valid Base32 string</li>
	 *                                   </ul>
	 * @throws IllegalArgumentException  if
	 *                                   <ul>
	 *                                       <li>the start voting key is not of size l<sub>SVK</sub></li>
	 *                                       <li>the extended authentication factor is not a valid Base10 string</li>
	 *                                   </ul>
	 */
	@SuppressWarnings("java:S117")
	public String deriveBaseAuthenticationChallenge(final String electionEventId, final String startVotingKey,
			final String extendedAuthenticationFactor) {
		final String ee = validateUUID(electionEventId);
		final String SVK_id = validateBase32NoPadAlphabet(startVotingKey, Constants.SVK_LENGTH);
		final Predicate<String> isDigitOfCorrectSize = ea -> Constants.EXTENDED_AUTHENTICATION_FACTOR_LENGTHS.stream()
				.parallel()
				.anyMatch(extendedAuthenticationFactorLength -> ea.matches(String.format("^\\d{%s}$", extendedAuthenticationFactorLength)));
		checkArgument(isDigitOfCorrectSize.test(extendedAuthenticationFactor),
				"The extended authentication factor must be a digit of correct size.");
		final String EA_id = extendedAuthenticationFactor;

		final byte[] salt_auth = cutToBitLength(hash.recursiveHash(HashableString.from(ee), HashableString.from("hAuth")), 128);
		final byte[] k = Bytes.concat(stringToByteArray(EA_id), stringToByteArray(SVK_id));
		final byte[] bhAuth_id = argon2.getArgon2id(k, salt_auth);
		return base64.base64Encode(bhAuth_id);
	}
}
