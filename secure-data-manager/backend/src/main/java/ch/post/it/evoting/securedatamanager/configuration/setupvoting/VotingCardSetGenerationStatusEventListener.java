/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static com.google.common.base.Preconditions.checkNotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import ch.post.it.evoting.securedatamanager.services.domain.model.status.Status;
import ch.post.it.evoting.securedatamanager.services.infrastructure.service.ConfigurationEntityStatusService;
import ch.post.it.evoting.securedatamanager.services.infrastructure.votingcardset.VotingCardSetRepository;

@Component
public class VotingCardSetGenerationStatusEventListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(VotingCardSetGenerationStatusEventListener.class);
	private final VotingCardSetRepository votingCardSetRepository;
	private final ConfigurationEntityStatusService configurationEntityStatusService;

	public VotingCardSetGenerationStatusEventListener(
			final VotingCardSetRepository votingCardSetRepository,
			final ConfigurationEntityStatusService configurationEntityStatusService) {
		this.votingCardSetRepository = votingCardSetRepository;
		this.configurationEntityStatusService = configurationEntityStatusService;
	}

	@EventListener()
	public void listen(final VotingCardSetGenerationStatusEvent event) {
		checkNotNull(event);
		final String electionEventId = event.getElectionEventId();
		final String votingCardSetId = event.getVotingCardSetId();
		LOGGER.debug("Received spring generation event. [electionEventId: {}, votingCardSetId: {}]", electionEventId, votingCardSetId);

		configurationEntityStatusService.update(Status.GENERATED.name(), votingCardSetId, votingCardSetRepository);
		LOGGER.info("Voting card set is generated. [electionEventId: {}, votingCardSetId: {}]", electionEventId, votingCardSetId);
	}

}

