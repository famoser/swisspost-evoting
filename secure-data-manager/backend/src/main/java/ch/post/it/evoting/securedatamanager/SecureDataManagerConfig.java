/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.integration.util.CallerBlocksPolicy;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamal;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalFactory;
import ch.post.it.evoting.cryptoprimitives.hashing.Argon2;
import ch.post.it.evoting.cryptoprimitives.hashing.Argon2Factory;
import ch.post.it.evoting.cryptoprimitives.hashing.Argon2Profile;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashFactory;
import ch.post.it.evoting.cryptoprimitives.math.Base16;
import ch.post.it.evoting.cryptoprimitives.math.Base64;
import ch.post.it.evoting.cryptoprimitives.math.BaseEncodingFactory;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.mixnet.Mixnet;
import ch.post.it.evoting.cryptoprimitives.mixnet.MixnetFactory;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystoreFactory;
import ch.post.it.evoting.cryptoprimitives.symmetric.Symmetric;
import ch.post.it.evoting.cryptoprimitives.symmetric.SymmetricFactory;
import ch.post.it.evoting.cryptoprimitives.utils.KeyDerivation;
import ch.post.it.evoting.cryptoprimitives.utils.KeyDerivationFactory;
import ch.post.it.evoting.cryptoprimitives.utils.VerificationResult;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ZeroKnowledgeProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ZeroKnowledgeProofFactory;
import ch.post.it.evoting.evotinglibraries.direct.trust.KeystoreValidator;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.votingoptions.FactorizeAlgorithm;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.votingoptions.GetActualVotingOptionsAlgorithm;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.votingoptions.GetEncodedVotingOptionsAlgorithm;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.votingoptions.GetSemanticInformationAlgorithm;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.writeins.DecodeWriteInsAlgorithm;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.writeins.IntegerToWriteInAlgorithm;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.writeins.IsWriteInOptionAlgorithm;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.writeins.QuadraticResidueToWriteInAlgorithm;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.tally.mixoffline.VerifyMixDecOfflineAlgorithm;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.tally.mixoffline.VerifyVotingClientProofsAlgorithm;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.tally.mixonline.GetMixnetInitialCiphertextsAlgorithm;
import ch.post.it.evoting.evotinglibraries.xml.XmlNormalizer;
import ch.post.it.evoting.securedatamanager.services.infrastructure.DatabaseManager;
import ch.post.it.evoting.securedatamanager.services.infrastructure.DatabaseManagerFactory;
import ch.post.it.evoting.securedatamanager.services.infrastructure.RestClientService;
import ch.post.it.evoting.securedatamanager.services.infrastructure.clients.MessageBrokerOrchestratorClient;
import ch.post.it.evoting.securedatamanager.services.infrastructure.clients.VoteVerificationClient;

@Configuration
public class SecureDataManagerConfig {
	private static final Logger LOGGER = LoggerFactory.getLogger(SecureDataManagerConfig.class);

	@Bean(initMethod = "createDatabase")
	public DatabaseManager databaseManager(final DatabaseManagerFactory databaseManagerFactory,
			@Value("${database.name}")
			final String databaseName) {
		return databaseManagerFactory.newDatabaseManager(databaseName);
	}

	@Bean
	public ObjectMapper objectMapper() {
		return DomainObjectMapper.getNewInstance();
	}

	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	@Bean
	public ObjectReader readerForDeserialization() {
		final ObjectMapper mapper = mapperForDeserialization();
		return mapper.reader();
	}

	private ObjectMapper mapperForDeserialization() {
		final ObjectMapper mapper = new ObjectMapper();
		mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		mapper.disable(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES);
		mapper.findAndRegisterModules();
		return mapper;
	}

	@Bean
	public Random random() {
		return RandomFactory.createRandom();
	}

	@Bean
	public Mixnet mixnet() {
		return MixnetFactory.createMixnet();
	}

	@Bean
	public ZeroKnowledgeProof zeroKnowledgeProof() {
		return ZeroKnowledgeProofFactory.createZeroKnowledgeProof();
	}

	@Bean
	public Hash cryptoPrimitivesHash() {
		return HashFactory.createHash();
	}

	@Bean
	ElGamal elGamal() {
		return ElGamalFactory.createElGamal();
	}

	@Bean
	Base64 base64() {
		return BaseEncodingFactory.createBase64();
	}

	@Bean
	Base16 base16() {
		return BaseEncodingFactory.createBase16();
	}

	@Bean
	KeyDerivation keyDerivation() {
		return KeyDerivationFactory.createKeyDerivation();
	}

	@Bean
	Symmetric symmetric() {
		return SymmetricFactory.createSymmetric();
	}

	@Bean
	GetEncodedVotingOptionsAlgorithm getEncodedVotingOptionsAlgorithm() {
		return new GetEncodedVotingOptionsAlgorithm();
	}

	@Bean
	GetActualVotingOptionsAlgorithm getActualVotingOptionsAlgorithm() {
		return new GetActualVotingOptionsAlgorithm();
	}

	@Bean
	GetSemanticInformationAlgorithm getSemanticInformationAlgorithm() {
		return new GetSemanticInformationAlgorithm();
	}

	@Bean
	FactorizeAlgorithm factorizeAlgorithm() {
		return new FactorizeAlgorithm();
	}

	@Bean
	IsWriteInOptionAlgorithm isWriteInOptionAlgorithm() {
		return new IsWriteInOptionAlgorithm();
	}

	@Bean
	IntegerToWriteInAlgorithm integerToWriteInAlgorithm() {
		return new IntegerToWriteInAlgorithm();
	}

	@Bean
	QuadraticResidueToWriteInAlgorithm quadraticResidueToWriteInAlgorithm(final IntegerToWriteInAlgorithm integerToWriteInAlgorithm) {
		return new QuadraticResidueToWriteInAlgorithm(integerToWriteInAlgorithm);
	}

	@Bean
	DecodeWriteInsAlgorithm decodeWriteInsAlgorithm(final IsWriteInOptionAlgorithm isWriteInOptionAlgorithm,
			final QuadraticResidueToWriteInAlgorithm quadraticResidueToWriteInAlgorithm) {
		return new DecodeWriteInsAlgorithm(isWriteInOptionAlgorithm, quadraticResidueToWriteInAlgorithm);
	}

	@Bean
	GetMixnetInitialCiphertextsAlgorithm getMixnetInitialCiphertextsAlgorithm(final ElGamal elGamal) {
		return new GetMixnetInitialCiphertextsAlgorithm(elGamal);
	}

	@Bean
	VerifyMixDecOfflineAlgorithm verifyMixDecOfflineAlgorithm(final ElGamal elGamal,
			final Mixnet mixnet,
			final ZeroKnowledgeProof zeroKnowledgeProof) {
		return new VerifyMixDecOfflineAlgorithm(elGamal, mixnet, zeroKnowledgeProof);
	}

	@Bean
	VerifyVotingClientProofsAlgorithm verifyVotingClientProofsAlgorithm(final ZeroKnowledgeProof zeroKnowledgeProof,
			final GetEncodedVotingOptionsAlgorithm getEncodedVotingOptionsAlgorithm,
			final GetActualVotingOptionsAlgorithm getActualVotingOptionsAlgorithm,
			final GetSemanticInformationAlgorithm getSemanticInformationAlgorithm) {
		return new VerifyVotingClientProofsAlgorithm(zeroKnowledgeProof, getEncodedVotingOptionsAlgorithm, getActualVotingOptionsAlgorithm,
				getSemanticInformationAlgorithm);
	}

	@Bean
	ExecutorService executorService() {
		final CallerBlocksPolicy policy = new CallerBlocksPolicy(1000000000);
		final int numberOfThreads = 20;
		return new ThreadPoolExecutor(numberOfThreads, numberOfThreads, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>(), policy);
	}

	@Bean(name = "keystoreServiceSdmConfig")
	@ConditionalOnProperty("role.isSetup")
	SignatureKeystore<Alias> keystoreServiceSdmConfig(final KeystoreRepository repository) throws IOException {
		LOGGER.info("Create a sdm config keystore service.");

		final Alias sdmConfig = Alias.SDM_CONFIG;
		return SignatureKeystoreFactory.createSignatureKeystore(repository.getConfigKeyStore(), "PKCS12", repository.getConfigKeystorePassword(),
				keystore -> {
					final VerificationResult result = KeystoreValidator.validateKeystore(keystore, sdmConfig);
					if (!result.isVerified()) {
						LOGGER.error("Keystore validation failed. [alias: {}]", sdmConfig.get());
						result.getErrorMessages().descendingIterator().forEachRemaining(LOGGER::error);
					}
					return result.isVerified();
				}, sdmConfig);
	}

	@Bean(name = "keystoreServiceSdmTally")
	@ConditionalOnProperty("role.isTally")
	SignatureKeystore<Alias> keystoreServiceSdmTally(final KeystoreRepository repository) throws IOException {
		LOGGER.info("Create a sdm tally keystore service.");

		final Alias sdmTally = Alias.SDM_TALLY;
		return SignatureKeystoreFactory.createSignatureKeystore(repository.getTallyKeyStore(), "PKCS12", repository.getTallyKeystorePassword(),
				keystore -> {
					final VerificationResult result = KeystoreValidator.validateKeystore(keystore, sdmTally);
					if (!result.isVerified()) {
						LOGGER.error("Keystore validation failed. [alias: {}]", sdmTally.get());
						result.getErrorMessages().descendingIterator().forEachRemaining(LOGGER::error);
					}
					return result.isVerified();
				}, sdmTally);
	}

	@Bean
	MessageBrokerOrchestratorClient messageBrokerOrchestratorClient(
			final RestClientService restClientService,
			@Value("${voting-server.url}")
			final String votingServerUrl,
			@Value("${voting-server.connection.timeout}")
			final long connectionTimeout) {
		return restClientService
				.getRestClientWithJacksonConverter(votingServerUrl, connectionTimeout)
				.create(MessageBrokerOrchestratorClient.class);
	}

	@Bean
	VoteVerificationClient voteVerificationClient(
			final RestClientService restClientService,
			@Value("${voting-server.url}")
			final String votingServerUrl,
			@Value("${voting-server.connection.timeout}")
			final long connectionTimeout) {
		return restClientService
				.getRestClientWithJacksonConverter(votingServerUrl, connectionTimeout)
				.create(VoteVerificationClient.class);
	}

	@Bean
	Argon2 argon2Standard() {
		return Argon2Factory.createArgon2(Argon2Profile.STANDARD.getContext());
	}

	@Bean
	Argon2 argon2LessMemory() {
		return Argon2Factory.createArgon2(Argon2Profile.LESS_MEMORY.getContext());
	}

	@Bean
	@ConditionalOnProperty("role.isTally")
	XmlNormalizer xmlNormalizer() {
		return new XmlNormalizer();
	}
}
