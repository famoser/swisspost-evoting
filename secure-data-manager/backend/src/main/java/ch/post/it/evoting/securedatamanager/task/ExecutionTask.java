/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.task;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "id", "description", "status" })
public class ExecutionTask {

	private final String id;
	private final String description;

	private ExecutionTaskStatus status;

	@JsonCreator
	private ExecutionTask(
			@JsonProperty("id")
			final String id,
			@JsonProperty("description")
			final String description,
			@JsonProperty("status")
			final ExecutionTaskStatus status) {
		this.id = checkNotNull(id);
		this.description = checkNotNull(description);
		this.status = checkNotNull(status);
	}

	private ExecutionTask(final ExecutionTaskType type, final String businessId, final String description, final ExecutionTaskStatus status) {
		this(String.format("%s-%s", type.toString(), businessId), description, status);
	}

	public String getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}

	public ExecutionTaskStatus getStatus() {
		return status;
	}

	public void setStatus(final ExecutionTaskStatus status) {
		this.status = status;
	}

	public static class Builder {
		private ExecutionTaskType type;
		private String businessId;
		private String description;
		private ExecutionTaskStatus status;

		public Builder setType(final ExecutionTaskType type) {
			this.type = checkNotNull(type);
			return this;
		}

		public Builder setBusinessId(final String businessId) {
			this.businessId = checkNotNull(businessId);
			return this;
		}

		public Builder setDescription(final String description) {
			this.description = checkNotNull(description);
			return this;
		}

		public Builder setStatus(final ExecutionTaskStatus status) {
			this.status = checkNotNull(status);
			return this;
		}

		public ExecutionTask build() {
			return new ExecutionTask(type, businessId, description, status);
		}
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final ExecutionTask that = (ExecutionTask) o;
		return id.equals(that.id) &&
				description.equals(that.description) &&
				status == that.status;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, description, status);
	}
}
