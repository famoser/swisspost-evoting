/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.commons;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Verification card set containing the election event identifier, the ballot box identifier, the voting card set identifier, the verification card
 * set identifier and the administration board identifier
 */
public record VerificationCardSet(String electionEventId, String ballotBoxId, String votingCardSetId, String verificationCardSetId,
								  String adminBoardId) {

	public VerificationCardSet {
		validateUUID(electionEventId);
		validateUUID(ballotBoxId);
		validateUUID(votingCardSetId);
		validateUUID(verificationCardSetId);
		checkNotNull(adminBoardId);
	}
}
