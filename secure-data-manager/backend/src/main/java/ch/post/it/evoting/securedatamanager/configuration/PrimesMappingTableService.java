/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTable;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;

/**
 * Allows saving and loading existing {@link PrimesMappingTable}s.
 */
@Service
public class PrimesMappingTableService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PrimesMappingTableService.class);

	private final PrimesMappingTableFileRepository primesMappingTableFileRepository;

	public PrimesMappingTableService(final PrimesMappingTableFileRepository primesMappingTableFileRepository) {
		this.primesMappingTableFileRepository = primesMappingTableFileRepository;
	}

	/**
	 * Saves a primes mapping table with its corresponding election event and verification card set ids.
	 *
	 * @param electionEventId       the election event id.
	 * @param verificationCardSetId the verification card set id.
	 * @param primesMappingTable    the primes mapping table to save.
	 * @throws NullPointerException      if {@code primesMappingTable} is null.
	 * @throws FailedValidationException if {@code electionEventId} or {@code verificationCardSetId} is invalid.
	 */
	public void save(final String electionEventId, final String verificationCardSetId, final PrimesMappingTable primesMappingTable) {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		checkNotNull(primesMappingTable);

		final GqGroup encryptionGroup = primesMappingTable.getPTable().getGroup();

		final PrimesMappingTableFileEntity primesMappingTableFileEntity = new PrimesMappingTableFileEntity(encryptionGroup, electionEventId,
				verificationCardSetId, primesMappingTable);

		primesMappingTableFileRepository.save(primesMappingTableFileEntity);
		LOGGER.debug("Saved primes mapping table. [electionEventId: {}, verificationCardSetId: {}]", electionEventId, verificationCardSetId);
	}

	/**
	 * Loads a primes mapping table corresponding to the given election event id and verification card set id.
	 *
	 * @param electionEventId       the corresponding election event id.
	 * @param verificationCardSetId the corresponding verification card set id.
	 * @return the primes mapping table.
	 * @throws FailedValidationException if {@code electionEventId} or {@code verificationCardSetId} is invalid.
	 * @throws IllegalStateException     if no primes mapping table is found for the given ids.
	 */
	@Cacheable("PrimesMappingTables")
	public PrimesMappingTable load(final String electionEventId, final String verificationCardSetId) {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);

		final PrimesMappingTable primesMappingTable = primesMappingTableFileRepository.findByVerificationCardSetId(electionEventId,
						verificationCardSetId)
				.orElseThrow(() -> new IllegalStateException(
						String.format("Primes mapping table not found. [electionEventId: %s, verificationCardSetId: %s]", electionEventId,
								verificationCardSetId)))
				.primesMappingTable();
		LOGGER.debug("Loaded primes mapping table. [electionEventId: {}, verificationCardSetId: {}]", electionEventId, verificationCardSetId);

		return primesMappingTable;
	}

	/**
	 * Loads a map of verificationCardSetId and primes mapping tables corresponding to the given election event id.
	 *
	 * @param electionEventId the corresponding election event id.
	 * @return the map of verificationCardSetIds and primes mapping tables.
	 * @throws FailedValidationException if {@code electionEventId} is invalid.
	 * @throws IllegalStateException     if no map of verificationCardSetIds and primes mapping tables is found for the given id.
	 */
	@Cacheable("AllPrimesMappingTables")
	public Map<String, PrimesMappingTable> loadAll(final String electionEventId) {
		validateUUID(electionEventId);

		final List<PrimesMappingTableFileEntity> primesMappingTableFileEntities = primesMappingTableFileRepository.findAll(electionEventId);

		if (primesMappingTableFileEntities.isEmpty()) {
			throw new IllegalStateException(
					String.format("Map of verificationCardSetIds and primes mapping tables not found. [electionEventId: %s]", electionEventId));
		}

		final Map<String, PrimesMappingTable> primesMappingTableMap = primesMappingTableFileEntities.stream().parallel()
				.collect(Collectors.toMap(
						PrimesMappingTableFileEntity::verificationCardSetId,
						PrimesMappingTableFileEntity::primesMappingTable
				));

		LOGGER.debug("Loaded the map of primes mapping table. [electionEventId: {}]", electionEventId);

		return primesMappingTableMap;
	}

}
