/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.sdmtally.tally.mixoffline;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import ch.post.it.evoting.cryptoprimitives.domain.mixnet.VerifiablePlaintextDecryption;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientMessage;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.mixnet.VerifiableShuffle;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.DecryptionProof;

/**
 * Value class containing the result of a re-encrypting shuffle and decryption.
 * <p>
 * It is composed of:
 * <ul>
 * 		<li>a verifiable shuffle.</li>
 * 		<li>a verifiable plaintext decryption.</li>
 * </ul>
 * </p>
 */
public class MixDecOfflineOutput {

	private final VerifiableShuffle verifiableShuffle;
	private final VerifiablePlaintextDecryption verifiablePlaintextDecryption;

	MixDecOfflineOutput(final VerifiableShuffle verifiableShuffle, final GroupVector<ElGamalMultiRecipientMessage, GqGroup> decryptedVotes,
			final GroupVector<DecryptionProof, ZqGroup> decryptionProofs) {
		checkNotNull(verifiableShuffle);
		checkNotNull(decryptedVotes);
		checkNotNull(decryptionProofs);

		// Cross-group checks
		final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> shuffledCiphertexts = verifiableShuffle.shuffledCiphertexts();
		checkArgument(shuffledCiphertexts.getGroup().equals(decryptedVotes.getGroup()),
				"The shuffled votes must have the same group as the decrypted votes.");
		checkArgument(decryptedVotes.getGroup().hasSameOrderAs(decryptionProofs.getGroup()),
				"The decrypted votes and the decryption proofs must have the same group order.");

		// Cross-dimension check
		checkArgument(shuffledCiphertexts.size() == decryptedVotes.size(),
				"The shuffled votes and the decrypted votes must have the same vector size.");
		checkArgument(shuffledCiphertexts.getElementSize() == decryptedVotes.getElementSize(),
				"The shuffled votes and the decrypted votes must have the same element size.");
		checkArgument(decryptedVotes.size() == decryptionProofs.size(),
				"The decrypted votes and the decryption proofs must have the same vector size.");

		this.verifiableShuffle = verifiableShuffle;
		this.verifiablePlaintextDecryption = new VerifiablePlaintextDecryption(decryptedVotes, decryptionProofs);
	}

	public VerifiableShuffle getVerifiableShuffle() {
		return verifiableShuffle;
	}

	public VerifiablePlaintextDecryption getVerifiablePlaintextDecryption() {
		return verifiablePlaintextDecryption;
	}
}
