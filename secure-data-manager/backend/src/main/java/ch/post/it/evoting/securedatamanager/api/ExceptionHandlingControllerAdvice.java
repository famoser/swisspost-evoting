/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.api;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ExceptionHandlingControllerAdvice extends ResponseEntityExceptionHandler {
	private static final Logger LOG = LoggerFactory.getLogger(ExceptionHandlingControllerAdvice.class);

	@ExceptionHandler(RuntimeException.class)
	public ResponseEntity<Void> error(final HttpServletRequest httpServletRequest, final RuntimeException runtimeException) {
		LOG.error("Failed to process request. [request: {}]", httpServletRequest.getRequestURI(), runtimeException);
		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
