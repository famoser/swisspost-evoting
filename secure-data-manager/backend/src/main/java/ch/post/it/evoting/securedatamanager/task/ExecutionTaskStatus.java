/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.task;

public enum ExecutionTaskStatus {
	STARTED,
	COMPLETED,
	FAILED,
	UNKNOWN
}
