/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

@Repository
public class KeystoreRepository {

	private final String keystoreLocationConfig;
	private final String keystorePasswordLocationConfig;
	private final String keystoreLocationTally;
	private final String keystorePasswordLocationTally;

	public KeystoreRepository(
			@Value("${direct.trust.keystore.location.config}")
			final String keystoreLocationConfig,
			@Value("${direct.trust.keystore.password.location.config}")
			final String keystorePasswordLocationConfig,
			@Value("${direct.trust.keystore.location.tally}")
			final String keystoreLocationTally,
			@Value("${direct.trust.keystore.password.location.tally}")
			final String keystorePasswordLocationTally) {
		this.keystoreLocationConfig = keystoreLocationConfig;
		this.keystorePasswordLocationConfig = keystorePasswordLocationConfig;
		this.keystoreLocationTally = keystoreLocationTally;
		this.keystorePasswordLocationTally = keystorePasswordLocationTally;
	}

	public InputStream getConfigKeyStore() throws IOException {
		return Files.newInputStream(Paths.get(keystoreLocationConfig));
	}

	public char[] getConfigKeystorePassword() throws IOException {
		return Files.readString(Paths.get(keystorePasswordLocationConfig)).toCharArray();
	}

	public InputStream getTallyKeyStore() throws IOException {
		return Files.newInputStream(Paths.get(keystoreLocationTally));
	}

	public char[] getTallyKeystorePassword() throws IOException {
		return Files.readString(Paths.get(keystorePasswordLocationTally)).toCharArray();
	}
}
