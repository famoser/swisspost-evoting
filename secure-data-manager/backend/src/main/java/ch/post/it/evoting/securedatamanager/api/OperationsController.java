/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.api;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkState;

import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidParameterException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import ch.post.it.evoting.securedatamanager.services.application.service.ElectionEventService;
import ch.post.it.evoting.securedatamanager.services.domain.model.operation.OperationResult;
import ch.post.it.evoting.securedatamanager.services.domain.model.operation.OperationsOutputCode;
import ch.post.it.evoting.securedatamanager.services.infrastructure.importexport.ImportExportService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/sdm-backend/operation")
@Api(value = "SDM Operations REST API")
public class OperationsController {

	private static final Logger LOGGER = LoggerFactory.getLogger(OperationsController.class);
	private static final String REQUEST_CAN_NOT_BE_PERFORMED = "The request can not be performed for the current resource";
	private final ElectionEventService electionEventService;
	private final ImportExportService importExportService;

	public OperationsController(
			final ElectionEventService electionEventService,
			final ImportExportService importExportService) {
		this.electionEventService = electionEventService;
		this.importExportService = importExportService;
	}

	@GetMapping(value = "/export/{electionEventId}")
	public ResponseEntity<StreamingResponseBody> exportOperation(
			@ApiParam(value = "String", required = true)
			@PathVariable
			final String electionEventId) {

		validateUUID(electionEventId);

		final String electionEventAlias = getElectionEventAlias(electionEventId);

		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_TYPE, "application/octet-stream")
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + String.format("export-%s.sdm", electionEventAlias))
				.body(outputStream -> importExportService.exportSdmData(outputStream, electionEventId));
	}

	private String getElectionEventAlias(final String electionEventId) {

		final String electionEventAlias = electionEventService.getElectionEventAlias(electionEventId);
		checkState(!StringUtils.isBlank(electionEventAlias), "The election event alias is blank. [electionEventId: %s]", electionEventId);

		return electionEventAlias;
	}

	@PostMapping(value = "/import")
	@ApiOperation(value = "Import operation service")
	@ApiResponses(value = { @ApiResponse(code = 404, message = "Not Found"), @ApiResponse(code = 403, message = "Forbidden"),
			@ApiResponse(code = 500, message = "Internal Server Error") })
	public ResponseEntity<OperationResult> importOperation(
			@ApiParam(value = "file", required = true)
			@RequestParam("file")
			final MultipartFile zip) {

		LOGGER.debug("Importing zip file...");

		try (final InputStream inputStream = zip.getInputStream()) {
			importExportService.importSdmData(inputStream);
		} catch (final IOException e) {
			final OperationsOutputCode code = OperationsOutputCode.ERROR_IO_OPERATIONS;
			return handleException(e, code.value());
		} catch (final InvalidParameterException e) {
			final OperationsOutputCode code = OperationsOutputCode.MISSING_PARAMETER;
			return handleInvalidParamException(e, code.value());
		} catch (final Exception e) {
			final OperationsOutputCode code = OperationsOutputCode.GENERAL_ERROR;
			return handleException(e, code.value());
		}

		LOGGER.info("Zip file successfully imported.");
		return new ResponseEntity<>(HttpStatus.OK);
	}

	private ResponseEntity<OperationResult> handleException(final Exception e, final int errorCode) {
		LOGGER.error("{}{}", REQUEST_CAN_NOT_BE_PERFORMED, errorCode, e);
		final OperationResult output = new OperationResult();
		output.setError(errorCode);
		output.setException(e.getClass().getName());
		output.setMessage(e.getMessage());
		return new ResponseEntity<>(output, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	private ResponseEntity<OperationResult> handleInvalidParamException(final Exception e, final int errorCode) {
		LOGGER.error("{}{}", REQUEST_CAN_NOT_BE_PERFORMED, errorCode, e);
		final OperationResult output = new OperationResult();
		output.setError(errorCode);
		output.setException(e.getClass().getName());
		output.setMessage(e.getMessage());
		return new ResponseEntity<>(output, HttpStatus.NOT_FOUND);
	}
}
