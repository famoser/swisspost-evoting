/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.api.setup;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.post.it.evoting.securedatamanager.services.application.service.IdleStatusService;
import ch.post.it.evoting.securedatamanager.services.application.service.VotingCardSetPrecomputationService;
import ch.post.it.evoting.securedatamanager.services.application.service.VotingCardSetSignService;
import ch.post.it.evoting.securedatamanager.services.domain.model.votingcardset.VotingCardSetsData;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * The voting card set end-point for the offline SDM.
 */
@RestController
@RequestMapping("/sdm-backend/setup/votingcardsets")
@Api(value = "Voting card set REST API")
@ConditionalOnProperty("role.isSetup")
public class VotingCardSetSetupController {

	private static final Logger LOGGER = LoggerFactory.getLogger(VotingCardSetSetupController.class);

	private final IdleStatusService idleStatusService;
	private final VotingCardSetSignService votingCardSetSignService;
	private final VotingCardSetPrecomputationService votingCardSetPrecomputationService;

	public VotingCardSetSetupController(
			final IdleStatusService idleStatusService,
			final VotingCardSetSignService votingCardSetSignService,
			final VotingCardSetPrecomputationService votingCardSetPrecomputationService) {
		this.idleStatusService = idleStatusService;
		this.votingCardSetSignService = votingCardSetSignService;
		this.votingCardSetPrecomputationService = votingCardSetPrecomputationService;
	}

	/**
	 * Pre-computes the voting card sets.
	 *
	 * @param electionEventId the election event id.
	 */
	@PutMapping(value = "/electionevent/{electionEventId}/precompute", consumes = "application/json")
	@ApiOperation(value = "Change the status of voting card sets to precomputed",
			notes = "Change the status of voting card sets to precomputed, performing the prepare and precompute operations")
	@ApiResponses(value = { @ApiResponse(code = 404, message = "Not Found") })
	public void setVotingCardSetStatusPrecomputed(
			@ApiParam(value = "String", required = true)
			@PathVariable
			final String electionEventId,
			@ApiParam(required = true)
			@RequestBody
			final VotingCardSetsData votingCardSetsData) {

		validateUUID(electionEventId);
		checkNotNull(votingCardSetsData);

		LOGGER.debug("Received request to pre-compute the voting card sets. [electionEventId: {}]", electionEventId);

		final String lockId = electionEventId + "-precompute";
		if (!idleStatusService.getIdLock(lockId)) {
			LOGGER.info("The voting card sets pre-computation is already processing, doing nothing. [electionEventId: {}]", electionEventId);
			return;
		}

		try {
			votingCardSetPrecomputationService.precomputeVotingCardSets(electionEventId, votingCardSetsData.adminBoardId());
		} finally {
			idleStatusService.freeIdLock(lockId);
		}

		LOGGER.info("Successfully pre-computed the voting card sets. [electionEventId: {}]", electionEventId);
	}

	/**
	 * Changes the status of a list of voting card sets by performing the sign operation. The HTTP call uses a PUT request to the election event
	 * endpoint. If the requested status cannot be transitioned to computing from the current one, the call will fail.
	 *
	 * @param electionEventId the election event id.
	 * @return a list of ids of the created voting card sets.
	 */
	@PutMapping(value = "/electionevent/{electionEventId}/sign", produces = "application/json")
	@ApiOperation(value = "Change the status of voting card sets to signed",
			notes = "Change the status of a voting card sets to signed, performing the sign operation")
	@ApiResponses(value = { @ApiResponse(code = 404, message = "Not Found") })
	public void setVotingCardSetsStatusSigned(
			@ApiParam(value = "String", required = true)
			@PathVariable
			final String electionEventId) {
		validateUUID(electionEventId);

		LOGGER.debug("Received request to sign the voting card sets. [electionEventId: {}]", electionEventId);

		final String lockId = electionEventId + "-sign";
		if (!idleStatusService.getIdLock(lockId)) {
			LOGGER.info("The voting card sets signing is already processing, doing nothing. [electionEventId: {}]", electionEventId);
			return;
		}

		try {
			votingCardSetSignService.sign(electionEventId);
		} finally {
			idleStatusService.freeIdLock(lockId);
		}

		LOGGER.info("Successfully signed the voting card sets. [electionEventId: {}]", electionEventId);
	}
}
