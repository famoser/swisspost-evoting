/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;
import java.util.Objects;

import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTable;
import ch.post.it.evoting.cryptoprimitives.domain.validations.Validations;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;

public final class GenCredDatContext {
	private final String electionEventId;
	private final String verificationCardSetId;
	private final ElGamalMultiRecipientPublicKey electionPublicKey;
	private final ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey;
	private final PrimesMappingTable primesMappingTable;
	private final List<String> correctnessInformationSelections;
	private final List<String> correctnessInformationVotingOptions;
	private final GqGroup encryptionGroup;

	private GenCredDatContext(
			final String electionEventId,
			final String verificationCardSetId,
			final ElGamalMultiRecipientPublicKey electionPublicKey,
			final ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey,
			final PrimesMappingTable primesMappingTable,
			final List<String> correctnessInformationSelections,
			final List<String> correctnessInformationVotingOptions,
			final GqGroup encryptionGroup) {
		this.electionEventId = electionEventId;
		this.verificationCardSetId = verificationCardSetId;
		this.electionPublicKey = electionPublicKey;
		this.choiceReturnCodesEncryptionPublicKey = choiceReturnCodesEncryptionPublicKey;
		this.primesMappingTable = primesMappingTable;
		this.correctnessInformationSelections = correctnessInformationSelections;
		this.correctnessInformationVotingOptions = correctnessInformationVotingOptions;
		this.encryptionGroup = encryptionGroup;
	}

	public String electionEventId() {
		return electionEventId;
	}

	public String verificationCardSetId() {
		return verificationCardSetId;
	}

	public ElGamalMultiRecipientPublicKey electionPublicKey() {
		return electionPublicKey;
	}

	public ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey() {
		return choiceReturnCodesEncryptionPublicKey;
	}

	public PrimesMappingTable primesMappingTable() {
		return primesMappingTable;
	}

	public List<String> correctnessInformationSelections() {
		return correctnessInformationSelections;
	}

	public List<String> correctnessInformationVotingOptions() {
		return correctnessInformationVotingOptions;
	}

	public GqGroup encryptionGroup() {
		return encryptionGroup;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final GenCredDatContext that = (GenCredDatContext) o;
		return Objects.equals(electionEventId, that.electionEventId) && Objects.equals(verificationCardSetId,
				that.verificationCardSetId) && Objects.equals(electionPublicKey, that.electionPublicKey) && Objects.equals(
				choiceReturnCodesEncryptionPublicKey, that.choiceReturnCodesEncryptionPublicKey) && Objects.equals(primesMappingTable,
				that.primesMappingTable) && Objects.equals(correctnessInformationSelections, that.correctnessInformationSelections)
				&& Objects.equals(correctnessInformationVotingOptions, that.correctnessInformationVotingOptions)
				&& Objects.equals(encryptionGroup, that.encryptionGroup);
	}

	@Override
	public int hashCode() {
		return Objects.hash(electionEventId, verificationCardSetId, electionPublicKey, choiceReturnCodesEncryptionPublicKey, primesMappingTable,
				correctnessInformationSelections, correctnessInformationVotingOptions, encryptionGroup);
	}

	@Override
	public String toString() {
		return "GenCredDatContext{" +
				"electionEventId='" + electionEventId + '\'' +
				", verificationCardSetId='" + verificationCardSetId + '\'' +
				", electionPublicKey=" + electionPublicKey +
				", choiceReturnCodesEncryptionPublicKey=" + choiceReturnCodesEncryptionPublicKey +
				", primesMappingTable=" + primesMappingTable +
				", correctnessInformationSelections=" + correctnessInformationSelections +
				", correctnessInformationVotingOptions=" + correctnessInformationVotingOptions +
				", encryptionGroup=" + encryptionGroup +
				'}';
	}

	public static class Builder {

		private String electionEventId;
		private String verificationCardSetId;
		private ElGamalMultiRecipientPublicKey electionPublicKey;
		private ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey;
		private PrimesMappingTable primesMappingTable;
		private List<String> correctnessInformationSelections;
		private List<String> correctnessInformationVotingOptions;

		public GenCredDatContext build() {

			validateUUID(electionEventId);
			validateUUID(verificationCardSetId);
			checkNotNull(electionPublicKey);
			checkNotNull(choiceReturnCodesEncryptionPublicKey);
			checkNotNull(primesMappingTable);
			checkNotNull(correctnessInformationSelections);
			checkNotNull(correctnessInformationVotingOptions);

			final GqGroup gqGroup = electionPublicKey.getGroup();

			checkArgument(choiceReturnCodesEncryptionPublicKey.getGroup().equals(gqGroup),
					"The choiceReturnCodesEncryptionPublicKey and electionPublicKey must have the same group.");
			checkArgument(primesMappingTable.getPTable().getGroup().equals(gqGroup),
					"The primesMappingTable and electionPublicKey must have the same group.");

			// The constructor of the PrimesMappingTable ensures the pTable is not empty, and the list of actualVotingOptions, encodedVotingOptions
			// and semanticInformation have the same size

			// The constructor of the PrimesMappingTableEntries ensures the actualVotingOptions, encodedVotingOptions and semanticInformation does
			// not contain null elements.

			checkArgument(correctnessInformationSelections.stream().allMatch(Objects::nonNull),
					"The correctness information selections must contains only non-null elements.");
			checkArgument(correctnessInformationVotingOptions.stream().allMatch(Objects::nonNull),
					"The correctness information voting options must contains only non-null elements.");

			correctnessInformationSelections.forEach(Validations::validateUUID);
			correctnessInformationVotingOptions.forEach(Validations::validateUUID);

			return new GenCredDatContext(electionEventId, verificationCardSetId, electionPublicKey, choiceReturnCodesEncryptionPublicKey,
					primesMappingTable, correctnessInformationSelections, correctnessInformationVotingOptions, gqGroup);
		}

		public Builder setElectionEventId(final String electionEventId) {
			this.electionEventId = electionEventId;
			return this;
		}

		public Builder setVerificationCardSetId(final String verificationCardSetId) {
			this.verificationCardSetId = verificationCardSetId;
			return this;
		}

		public Builder setElectionPublicKey(final ElGamalMultiRecipientPublicKey electionPublicKey) {
			this.electionPublicKey = electionPublicKey;
			return this;
		}

		public Builder setChoiceReturnCodesEncryptionPublicKey(final ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey) {
			this.choiceReturnCodesEncryptionPublicKey = choiceReturnCodesEncryptionPublicKey;
			return this;
		}

		public Builder setPrimesMappingTable(final PrimesMappingTable primesMappingTable) {
			this.primesMappingTable = primesMappingTable;
			return this;
		}

		public Builder setCorrectnessInformationSelections(final List<String> correctnessInformationSelections) {
			this.correctnessInformationSelections = List.copyOf(correctnessInformationSelections);
			return this;
		}

		public Builder setCorrectnessInformationVotingOptions(final List<String> correctnessInformationVotingOptions) {
			this.correctnessInformationVotingOptions = List.copyOf(correctnessInformationVotingOptions);
			return this;
		}

	}

}
