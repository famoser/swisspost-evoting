/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.sdmtally.tally.mixoffline;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.google.common.base.Preconditions;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.evotinglibraries.domain.tally.TallyComponentVotesPayload;
import ch.post.it.evoting.evotinglibraries.xml.mapper.ResultDeliveryMapper;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.AuthorizationType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.Configuration;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingdecrypt.Results;

@Service
@ConditionalOnProperty("role.isTally")
public class TallyComponentDecryptService {

	private static final Logger LOGGER = LoggerFactory.getLogger(TallyComponentDecryptService.class);

	private final CantonConfigTallyService cantonConfigTallyService;
	private final TallyComponentDecryptFileRepository tallyComponentDecryptFileRepository;

	public TallyComponentDecryptService(
			final CantonConfigTallyService cantonConfigTallyService,
			final TallyComponentDecryptFileRepository tallyComponentDecryptFileRepository) {
		this.cantonConfigTallyService = cantonConfigTallyService;
		this.tallyComponentDecryptFileRepository = tallyComponentDecryptFileRepository;
	}

	/**
	 * Generates and persists the tally component decrypt file.
	 *
	 * @param electionEventId                                   the election event id. Must be non-null and a valid UUID.
	 * @param authorizationToTallyComponentVotesPayloadInputMap the map of configuration authorizations to corresponding tally component votes payload
	 *                                                          to be included in the decrypt file. Must be non-null and not contain any null entry.
	 * @throws NullPointerException      if any input is null or if {@code authorizationToTallyComponentVotesPayloadInputMap} contains a null entry,
	 *                                   key or value.
	 * @throws FailedValidationException if the election event id is not a valid UUID.
	 */
	public void generate(final String electionEventId,
			final Map<AuthorizationType, TallyComponentVotesPayload> authorizationToTallyComponentVotesPayloadInputMap) {
		validateUUID(electionEventId);
		checkNotNull(authorizationToTallyComponentVotesPayloadInputMap);
		authorizationToTallyComponentVotesPayloadInputMap.entrySet().stream().parallel().forEach(Preconditions::checkNotNull);
		authorizationToTallyComponentVotesPayloadInputMap.keySet().stream().parallel().forEach(Preconditions::checkNotNull);
		authorizationToTallyComponentVotesPayloadInputMap.values().stream().parallel().forEach(Preconditions::checkNotNull);
		final Map<AuthorizationType, TallyComponentVotesPayload> authorizationToTallyComponentVotesPayloadMap = Map.copyOf(
				authorizationToTallyComponentVotesPayloadInputMap);

		LOGGER.debug("Generating tally component decrypt file... [electionEventId: {}]", electionEventId);

		final Configuration configuration = cantonConfigTallyService.load(electionEventId);

		final Map<String, TallyComponentVotesPayload> authorizationNameToTallyComponentVotesPayloadMap = authorizationToTallyComponentVotesPayloadMap
				.entrySet()
				.stream().parallel()
				.collect(Collectors.toMap(entry -> entry.getKey().getAuthorizationName(), Map.Entry::getValue));

		final Results results = ResultDeliveryMapper.toResults(configuration, authorizationNameToTallyComponentVotesPayloadMap);

		tallyComponentDecryptFileRepository.save(electionEventId, results);

		LOGGER.info("Tally component decrypt file successfully generated. [electionEventId: {}]", electionEventId);
	}

	/**
	 * Loads the tally component decrypt for the given election event id and validates its signature.
	 * <p>
	 * The result of this method is cached.
	 *
	 * @param electionEventId       the election event id. Must be non-null and a valid UUID.
	 * @param contestIdentification the contest identification. Must be non-null and non-blank.
	 * @return the tally component decrypt as {@link Results}.
	 * @throws NullPointerException      if the election event id or the contest identification is null.
	 * @throws IllegalArgumentException  if the contest identification is blank.
	 * @throws FailedValidationException if the election event id is not a valid UUID.
	 */
	@Cacheable("tallyComponentDecrypts")
	public Results load(final String electionEventId, final String contestIdentification) {
		validateUUID(electionEventId);
		checkNotNull(contestIdentification);
		checkArgument(!contestIdentification.isBlank(), "The contest identification cannot be blank.");

		return tallyComponentDecryptFileRepository.load(electionEventId, contestIdentification)
				.orElseThrow(() -> new IllegalStateException(
						String.format("Could not find the requested tally component decrypt file. [electionEventId: %s, contestIdentification: %s]",
								electionEventId, contestIdentification)));
	}

}
