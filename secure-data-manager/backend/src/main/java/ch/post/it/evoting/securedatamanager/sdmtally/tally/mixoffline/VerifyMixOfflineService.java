/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.sdmtally.tally.mixoffline;

import static ch.post.it.evoting.cryptoprimitives.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.cryptoprimitives.domain.VotingOptionsConstants.MAXIMUM_NUMBER_OF_SELECTABLE_VOTING_OPTIONS;
import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.cryptoprimitives.utils.Validations.allEqual;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.security.SignatureException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import com.google.common.collect.Iterables;
import com.google.common.collect.MoreCollectors;

import ch.post.it.evoting.cryptoprimitives.domain.election.ControlComponentPublicKeys;
import ch.post.it.evoting.cryptoprimitives.domain.election.ElectionEventContext;
import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTable;
import ch.post.it.evoting.cryptoprimitives.domain.election.SetupComponentPublicKeys;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ControlComponentShufflePayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.SignedPayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.mixnet.VerifiableShuffle;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.VerifiableDecryptions;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.evotinglibraries.domain.common.EncryptedVerifiableVote;
import ch.post.it.evoting.evotinglibraries.domain.configuration.SetupComponentTallyDataPayload;
import ch.post.it.evoting.evotinglibraries.domain.tally.ControlComponentBallotBoxPayload;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.tally.mixoffline.VerifyMixDecInput;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.tally.mixoffline.VerifyMixDecOfflineAlgorithm;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.tally.mixoffline.VerifyMixDecOfflineContext;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.tally.mixoffline.VerifyVotingClientProofsAlgorithm;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.tally.mixoffline.VerifyVotingClientProofsContext;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.tally.mixoffline.VerifyVotingClientProofsInput;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.tally.mixonline.GetMixnetInitialCiphertextsAlgorithm;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.tally.mixonline.GetMixnetInitialCiphertextsContext;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.tally.mixonline.GetMixnetInitialCiphertextsInput;
import ch.post.it.evoting.securedatamanager.sdmcommon.tally.ControlComponentBallotBoxPayloadFileRepository;
import ch.post.it.evoting.securedatamanager.sdmcommon.tally.ControlComponentShufflePayloadFileRepository;
import ch.post.it.evoting.securedatamanager.services.application.service.IdentifierValidationService;

/**
 * Handles the verifying steps of the offline mixing: verifying voting client proofs and verifying mixing and decryption.
 */
@Service
@ConditionalOnProperty("role.isTally")
public class VerifyMixOfflineService {

	private static final Logger LOGGER = LoggerFactory.getLogger(VerifyMixOfflineService.class);

	private final ControlComponentBallotBoxPayloadFileRepository controlComponentBallotBoxPayloadFileRepository;
	private final ControlComponentShufflePayloadFileRepository controlComponentShufflePayloadFileRepository;
	private final GetMixnetInitialCiphertextsAlgorithm getMixnetInitialCiphertextsAlgorithm;
	private final SignatureKeystore<Alias> signatureKeystore;
	private final VerifyMixDecOfflineAlgorithm verifyMixDecOfflineAlgorithm;
	private final VerifyVotingClientProofsAlgorithm verifyVotingClientProofsAlgorithm;
	private final IdentifierValidationService identifierValidationService;
	private final EncryptionParametersTallyService encryptionParametersTallyService;

	VerifyMixOfflineService(
			final ControlComponentBallotBoxPayloadFileRepository controlComponentBallotBoxPayloadFileRepository,
			final ControlComponentShufflePayloadFileRepository controlComponentShufflePayloadFileRepository,
			final GetMixnetInitialCiphertextsAlgorithm getMixnetInitialCiphertextsAlgorithm,
			@Qualifier("keystoreServiceSdmTally")
			final SignatureKeystore<Alias> signatureKeystore,
			final VerifyMixDecOfflineAlgorithm verifyMixDecOfflineAlgorithm,
			final VerifyVotingClientProofsAlgorithm verifyVotingClientProofsAlgorithm,
			final IdentifierValidationService identifierValidationService,
			final EncryptionParametersTallyService encryptionParametersTallyService) {
		this.controlComponentBallotBoxPayloadFileRepository = controlComponentBallotBoxPayloadFileRepository;
		this.controlComponentShufflePayloadFileRepository = controlComponentShufflePayloadFileRepository;
		this.getMixnetInitialCiphertextsAlgorithm = getMixnetInitialCiphertextsAlgorithm;
		this.signatureKeystore = signatureKeystore;
		this.verifyMixDecOfflineAlgorithm = verifyMixDecOfflineAlgorithm;
		this.verifyVotingClientProofsAlgorithm = verifyVotingClientProofsAlgorithm;
		this.identifierValidationService = identifierValidationService;
		this.encryptionParametersTallyService = encryptionParametersTallyService;
	}

	/**
	 * Verifies the control-component's mixing and decryption proofs.
	 *
	 * @param electionEventId                 the identifier of the election. Must be non-null and a valid UUID.
	 * @param ballotId                        the identifier of the ballot. Must be non-null and a valid UUID.
	 * @param ballotBoxId                     the identifier of the ballot box. Must be non-null and a valid UUID.
	 * @param numberOfSelectableVotingOptions the number of selectable voting options. Must be between 1 and 120 included.
	 * @param numberOfAllowedWriteInsPlusOne  the number of write-ins + 1. Must be greater or equal to 1.
	 * @param electionEventContext            the election event context. Must be non-null.
	 * @param setupComponentTallyDataPayload  the setup component tally data payload. Must be non-null.
	 * @return the control component shuffle payload of the last node.
	 * @throws NullPointerException      if any input is null.
	 * @throws FailedValidationException if any of the IDs is not a valid UUID.
	 * @throws IllegalArgumentException  if number of write-ins + 1 is strictly smaller than 1  or the number of selectable voting options is not in
	 *                                   range [1, 120].
	 */
	public ControlComponentShufflePayload verifyMixDecrypt(final String electionEventId, final String ballotId, final String ballotBoxId,
			final int numberOfSelectableVotingOptions, final int numberOfAllowedWriteInsPlusOne, final ElectionEventContext electionEventContext,
			final SetupComponentPublicKeys setupComponentPublicKeys, final SetupComponentTallyDataPayload setupComponentTallyDataPayload) {
		validateUUID(electionEventId);
		validateUUID(ballotId);
		validateUUID(ballotBoxId);
		identifierValidationService.validateBallotBoxRelatedIds(electionEventId, ballotBoxId);
		identifierValidationService.validateBallotId(ballotId, ballotBoxId);
		checkNotNull(electionEventContext);
		checkNotNull(setupComponentPublicKeys);
		checkNotNull(setupComponentTallyDataPayload);
		checkArgument(numberOfSelectableVotingOptions >= 1,
				"The number of selectable voting options must be greater or equal to 1. [numberOfSelectableVotingOptions: %s]",
				numberOfSelectableVotingOptions);
		checkArgument(numberOfSelectableVotingOptions <= MAXIMUM_NUMBER_OF_SELECTABLE_VOTING_OPTIONS,
				"The number of selectable voting options must be smaller or equal to %s. [numberOfSelectableVotingOptions: %s]",
				MAXIMUM_NUMBER_OF_SELECTABLE_VOTING_OPTIONS, numberOfSelectableVotingOptions);
		checkArgument(numberOfAllowedWriteInsPlusOne >= 1, "The number of allowed write-ins + 1 must be at least 1. [numberOfWriteInsPlusOne: %s]",
				numberOfAllowedWriteInsPlusOne);

		// Read mixnet payloads.
		final List<ControlComponentBallotBoxPayload> controlComponentBallotBoxPayloads = NODE_IDS.stream()
				.map(nodeId -> controlComponentBallotBoxPayloadFileRepository.getPayload(electionEventId, ballotId, ballotBoxId, nodeId))
				.toList();

		final List<ControlComponentShufflePayload> controlComponentShufflePayloads = NODE_IDS.stream()
				.map(nodeId -> controlComponentShufflePayloadFileRepository.getPayload(electionEventId, ballotId, ballotBoxId, nodeId))
				.toList();

		// Verify mixnet payloads signatures.
		controlComponentBallotBoxPayloads.forEach(payload ->
				verifySignature(Alias.getControlComponentByNodeId(payload.getNodeId()), payload,
						ChannelSecurityContextData.controlComponentBallotBox(payload.getNodeId(), electionEventId, ballotBoxId),
						String.format("[nodeId: %s, electionEventId: %s, ballotId: %s, ballotBoxId: %s]", payload.getNodeId(), electionEventId,
								ballotId, ballotBoxId)));

		controlComponentShufflePayloads.forEach(payload ->
				verifySignature(Alias.getControlComponentByNodeId(payload.getNodeId()), payload,
						ChannelSecurityContextData.controlComponentShuffle(payload.getNodeId(), electionEventId, ballotBoxId),
						String.format("[nodeId: %s, electionEventId: %s, ballotId: %s, ballotBoxId: %s]", payload.getNodeId(), electionEventId,
								ballotId, ballotBoxId)));

		// Verify mixnet payloads consistency.
		final GqGroup encryptionGroup = encryptionParametersTallyService.loadEncryptionGroup(electionEventId);
		verifyConsistency(encryptionGroup, controlComponentBallotBoxPayloads, controlComponentShufflePayloads);

		// After consistency checks, we know all payloads have the same votes, so we can pick one (first here).
		final List<EncryptedVerifiableVote> confirmedEncryptedVotes = controlComponentBallotBoxPayloads.get(0).getConfirmedEncryptedVotes();

		// The algorithm VerifyVotingClientProofs runs with at least one confirmed vote
		if (!confirmedEncryptedVotes.isEmpty()) {
			verifyVotingClientProofs(ballotId, ballotBoxId, confirmedEncryptedVotes, setupComponentTallyDataPayload, electionEventContext,
					setupComponentPublicKeys, numberOfSelectableVotingOptions, numberOfAllowedWriteInsPlusOne);
			LOGGER.info("The voting client's zero-knowledge proofs are valid. [electionEventId: {}, ballotId: {}, ballotBoxId: {}]",
					electionEventId, ballotId, ballotBoxId);
		}

		verifyMixDecOffline(encryptionGroup, ballotId, ballotBoxId, controlComponentBallotBoxPayloads, controlComponentShufflePayloads,
				electionEventContext, setupComponentPublicKeys, numberOfAllowedWriteInsPlusOne);

		LOGGER.info("The online control-component's mixing and decryption proofs are valid. [electionEventId: {}, ballotId: {}, ballotBoxId: {}]",
				electionEventId, ballotId, ballotBoxId);

		return Iterables.getLast(controlComponentShufflePayloads);
	}

	private void verifyConsistency(final GqGroup encryptionGroup, final List<ControlComponentBallotBoxPayload> controlComponentBallotBoxPayloads,
			final List<ControlComponentShufflePayload> controlComponentShufflePayloads) {

		checkState(allEqual(controlComponentBallotBoxPayloads.stream(), ControlComponentBallotBoxPayload::getEncryptionGroup),
				"All control component ballot box payloads must have the same group.");
		checkState(allEqual(controlComponentBallotBoxPayloads.stream(), ControlComponentBallotBoxPayload::getElectionEventId),
				"All control component ballot box payloads must have the same election event id.");
		checkState(allEqual(controlComponentBallotBoxPayloads.stream(), ControlComponentBallotBoxPayload::getBallotBoxId),
				"All control component ballot box payloads must have the same ballot box id.");
		checkState(allEqual(controlComponentBallotBoxPayloads.stream(), ControlComponentBallotBoxPayload::getConfirmedEncryptedVotes),
				"All control component ballot box payloads must have the same confirmed encrypted votes.");

		final List<Integer> ballotBoxPayloadsNodeIds = controlComponentBallotBoxPayloads.stream()
				.map(ControlComponentBallotBoxPayload::getNodeId)
				.toList();
		checkState(NODE_IDS.size() == ballotBoxPayloadsNodeIds.size() && NODE_IDS.equals(new HashSet<>(ballotBoxPayloadsNodeIds)),
				"Wrong number of control component ballot box payloads.");

		checkState(allEqual(controlComponentShufflePayloads.stream(), ControlComponentShufflePayload::getEncryptionGroup),
				"All control component shuffle payloads must have the same group.");
		checkState(allEqual(controlComponentShufflePayloads.stream(), ControlComponentShufflePayload::getElectionEventId),
				"All control component shuffle payloads must have the same election event id.");
		checkState(allEqual(controlComponentShufflePayloads.stream(), ControlComponentShufflePayload::getBallotBoxId),
				"All control component shuffle payloads must have the same ballot box id.");

		final List<Integer> shufflePayloadsNodeIds = controlComponentShufflePayloads.stream()
				.map(ControlComponentShufflePayload::getNodeId)
				.toList();
		checkState(NODE_IDS.size() == shufflePayloadsNodeIds.size() && NODE_IDS.equals(new HashSet<>(shufflePayloadsNodeIds)),
				"Wrong number of control component shuffle payloads.");

		// Cross-checks.
		checkState(controlComponentBallotBoxPayloads.get(0).getEncryptionGroup().equals(controlComponentShufflePayloads.get(0).getEncryptionGroup()),
				"The control component ballot box and shuffle payloads must have the same group.");
		checkState(controlComponentBallotBoxPayloads.get(0).getEncryptionGroup().equals(encryptionGroup),
				"The control component ballot box and shuffle payloads must have the expected group.");
		checkState(controlComponentBallotBoxPayloads.get(0).getElectionEventId().equals(controlComponentShufflePayloads.get(0).getElectionEventId()),
				"The control component ballot box and shuffle payloads must have the same election event id.");
		checkState(controlComponentBallotBoxPayloads.get(0).getBallotBoxId().equals(controlComponentShufflePayloads.get(0).getBallotBoxId()),
				"The control component ballot box and shuffle payloads must have the same ballot box id.");

	}

	private void verifyVotingClientProofs(final String ballotId, final String ballotBoxId, List<EncryptedVerifiableVote> confirmedEncryptedVotes,
			final SetupComponentTallyDataPayload setupComponentTallyDataPayload, final ElectionEventContext electionEventContext,
			final SetupComponentPublicKeys setupComponentPublicKeys, final int numberOfSelectableVotingOptions,
			final int numberOfAllowedWriteInsPlusOne) {

		final String electionEventId = electionEventContext.electionEventId();
		final List<String> verificationCardIds = setupComponentTallyDataPayload.getVerificationCardIds();
		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> verificationCardPublicKeys = setupComponentTallyDataPayload.getVerificationCardPublicKeys();
		final Map<String, ElGamalMultiRecipientPublicKey> verificationCardPublicKeysMap = IntStream.range(0, verificationCardIds.size()).boxed()
				.collect(Collectors.toMap(verificationCardIds::get, verificationCardPublicKeys::get));
		final PrimesMappingTable primesMappingTable = electionEventContext.verificationCardSetContexts().stream()
				.filter(vcsContext -> vcsContext.ballotBoxId().equals(ballotBoxId))
				.collect(MoreCollectors.onlyElement())
				.primesMappingTable();

		final VerifyVotingClientProofsContext verifyVotingClientProofsContext = new VerifyVotingClientProofsContext.Builder()
				.setEncryptionGroup(encryptionParametersTallyService.loadEncryptionGroup(electionEventId))
				.setElectionEventId(electionEventId)
				.setNumberOfSelectableVotingOptions(numberOfSelectableVotingOptions)
				.setNumberOfAllowedWriteInsPlusOne(numberOfAllowedWriteInsPlusOne)
				.setPrimesMappingTable(primesMappingTable)
				.build();
		final VerifyVotingClientProofsInput verifyVotingClientProofsInput = new VerifyVotingClientProofsInput.Builder()
				.setEncryptedVerifiableVotes(confirmedEncryptedVotes)
				.setVerificationCardPublicKeys(verificationCardPublicKeysMap)
				.setElectionPublicKey(setupComponentPublicKeys.electionPublicKey())
				.setChoiceReturnCodesEncryptionPublicKey(setupComponentPublicKeys.choiceReturnCodesEncryptionPublicKey())
				.build();

		checkState(verifyVotingClientProofsAlgorithm.verifyVotingClientProofs(verifyVotingClientProofsContext, verifyVotingClientProofsInput),
				"The voting client's zero-knowledge proofs are invalid. [electionEventId: %s, ballotId: %s, ballotBoxId: %s]",
				electionEventId, ballotId, ballotBoxId);
	}

	private void verifyMixDecOffline(final GqGroup encryptionGroup, final String ballotId, final String ballotBoxId,
			final List<ControlComponentBallotBoxPayload> controlComponentBallotBoxPayloads,
			final List<ControlComponentShufflePayload> controlComponentShufflePayloads, final ElectionEventContext electionEventContext,
			final SetupComponentPublicKeys setupComponentPublicKeys, final int numberOfAllowedWriteInsPlusOne) {
		final String electionEventId = electionEventContext.electionEventId();
		final List<VerifiableShuffle> precedingVerifiableShuffledVotes = controlComponentShufflePayloads.stream()
				.map(ControlComponentShufflePayload::getVerifiableShuffle)
				.toList();
		final List<VerifiableDecryptions> precedingVerifiableDecryptedVotes = controlComponentShufflePayloads.stream()
				.map(ControlComponentShufflePayload::getVerifiableDecryptions)
				.toList();

		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> ccmjElectionPublicKey = setupComponentPublicKeys.combinedControlComponentPublicKeys()
				.stream()
				.map(ControlComponentPublicKeys::ccmjElectionPublicKey)
				.collect(GroupVector.toGroupVector());
		final ElGamalMultiRecipientPublicKey electoralBoardPublicKey = setupComponentPublicKeys.electoralBoardPublicKey();
		final ElGamalMultiRecipientPublicKey electionPublicKey = setupComponentPublicKeys.electionPublicKey();

		final Map<String, ElGamalMultiRecipientCiphertext> confirmedEncryptedVotesMap = controlComponentBallotBoxPayloads.get(0)
				.getConfirmedEncryptedVotes()
				.stream()
				.collect(Collectors.toMap(encryptedVerifiableVote -> encryptedVerifiableVote.contextIds().verificationCardId(),
						EncryptedVerifiableVote::encryptedVote));

		final GetMixnetInitialCiphertextsContext getMixnetInitialCiphertextsContext = new GetMixnetInitialCiphertextsContext(encryptionGroup,
				electionEventId, ballotBoxId);
		final GetMixnetInitialCiphertextsInput getMixnetInitialCiphertextsInput = new GetMixnetInitialCiphertextsInput(numberOfAllowedWriteInsPlusOne,
				confirmedEncryptedVotesMap, electionPublicKey);

		final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> initialCiphertexts = getMixnetInitialCiphertextsAlgorithm.getMixnetInitialCiphertexts(
				getMixnetInitialCiphertextsContext, getMixnetInitialCiphertextsInput);

		final VerifyMixDecOfflineContext verifyMixDecOfflineContext = new VerifyMixDecOfflineContext(encryptionGroup, electionEventId, ballotBoxId,
				numberOfAllowedWriteInsPlusOne);
		final VerifyMixDecInput verifyMixDecInput = new VerifyMixDecInput(initialCiphertexts, precedingVerifiableShuffledVotes,
				precedingVerifiableDecryptedVotes, electionPublicKey, ccmjElectionPublicKey, electoralBoardPublicKey);

		checkState(verifyMixDecOfflineAlgorithm.verifyMixDecOffline(verifyMixDecOfflineContext, verifyMixDecInput),
				"The online control-component's mixing and decryption proofs are invalid. [electionEventId: %s, ballotId: %s, ballotBoxId: %s]",
				electionEventId, ballotId, ballotBoxId);
	}

	private void verifySignature(final Alias alias, final SignedPayload payload, final Hashable additionalContext, final String contextMessage) {

		final CryptoPrimitivesSignature signature = payload.getSignature();

		checkState(signature != null, "The signature of the mixnet payload is null. %s", contextMessage);

		final boolean isSignatureValid;
		try {
			isSignatureValid = signatureKeystore.verifySignature(alias, payload, additionalContext, signature.signatureContents());
		} catch (final SignatureException e) {
			throw new IllegalStateException(
					String.format("Could not verify the signature of payload. [name: %s, context: %s]", payload.getClass().getSimpleName(),
							contextMessage));
		}
		if (!isSignatureValid) {
			throw new InvalidPayloadSignatureException(payload.getClass(), contextMessage);
		}
	}

}
