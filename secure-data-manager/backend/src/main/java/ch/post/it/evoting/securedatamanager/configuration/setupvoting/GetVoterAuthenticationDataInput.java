/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateBase32NoPadAlphabet;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import com.google.common.base.Preconditions;

import ch.post.it.evoting.securedatamanager.commons.Constants;

/**
 * @param startVotingKeys               SVK, the vector of start voting keys. Must be non-null and must contain N<sub>E</sub> valid Base32 strings of
 *                                      size l<sub>SVK</sub>.
 * @param extendedAuthenticationFactors EA, the vector of extended authentication factors. Must be non-null and must contain N<sub>E</sub> digit
 *                                      sequences of size l<sub>EA</sub>.
 */
public record GetVoterAuthenticationDataInput(List<String> startVotingKeys, List<String> extendedAuthenticationFactors) {

	public GetVoterAuthenticationDataInput(final List<String> startVotingKeys, final List<String> extendedAuthenticationFactors) {
		checkNotNull(startVotingKeys).forEach(Preconditions::checkNotNull);
		final List<String> startVotingKeysCopy = List.copyOf(startVotingKeys);
		checkArgument(!startVotingKeysCopy.isEmpty(), "The start voting keys must not be empty.");
		startVotingKeysCopy.forEach(startVotingKey -> validateBase32NoPadAlphabet(startVotingKey, Constants.SVK_LENGTH));

		checkNotNull(extendedAuthenticationFactors).forEach(Preconditions::checkNotNull);
		final List<String> extendedAuthenticationFactorsCopy = List.copyOf(extendedAuthenticationFactors);
		checkArgument(!extendedAuthenticationFactorsCopy.isEmpty(), "The extended authentication factors must not be empty.");

		checkArgument(extendedAuthenticationFactorsCopy.stream().allMatch(ea -> Constants.EXTENDED_AUTHENTICATION_FACTOR_LENGTHS.stream()
						.anyMatch(extendedAuthenticationFactorLength -> ea.matches(String.format("^\\d{%s}$", extendedAuthenticationFactorLength)))),
				"The elements of the extended authentication factors must be digit sequences of correct size.");

		checkArgument(startVotingKeysCopy.size() == extendedAuthenticationFactorsCopy.size(),
				"There must be as many extended authentication factors as start voting keys.");

		this.startVotingKeys = startVotingKeysCopy;
		this.extendedAuthenticationFactors = extendedAuthenticationFactorsCopy;
	}

	@Override
	public List<String> extendedAuthenticationFactors() {
		return List.copyOf(extendedAuthenticationFactors);
	}

	@Override
	public List<String> startVotingKeys() {
		return List.copyOf(startVotingKeys);
	}
}
