/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.infrastructure.importexport;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

/**
 * Define regexp patterns that will match the files needed to import/export the SDM files.
 * </p>
 * The importation whitelist will match all the elections present, while the exportation whitelist will match only the given elections ID.
 */
@Component
public class WhiteList {

	// enough to reach the end of SDM structure while preventing wasting time in incorrect deep tree.
	public static final int MAX_DEPTH = 10;

	private static final Logger LOGGER = LoggerFactory.getLogger(WhiteList.class);
	/**
	 * To allow us to optimize the size of the export ZIP file, we handle only the relevant files for the specific export step. However, for testing
	 * purposes, we sometimes desire to export all files.
	 */
	private final boolean fullExport;
	private final PathResolver pathResolver;

	public WhiteList(final PathResolver pathResolver,
			@Value("${export.forceFull}")
			final boolean fullExport) {
		this.pathResolver = pathResolver;
		this.fullExport = fullExport;
	}

	public List<Pattern> getImportList() {
		return getList("[a-zA-Z0-9]{32}").stream()
				.map(ImmutablePair::getLeft)
				.toList();
	}

	public List<Pattern> getExportList(final String electionEventId) {
		final SyncExportPhase actualPhase = determineActualExportPhase(electionEventId);
		return getList(electionEventId).stream()
				.filter(pair -> fullExport || pair.getRight().isEmpty() || pair.getRight().stream().anyMatch(phase -> phase == actualPhase))
				.map(ImmutablePair::getLeft)
				.toList();
	}

	private List<ImmutablePair<Pattern, List<SyncExportPhase>>> getList(final String electionEventId) {

		final String config = "config/";
		final String adminBoardsDirectory = config + "admin-boards/";
		final String electionEventDirectory = String.format("%s%s/", config, electionEventId);
		final String customer = electionEventDirectory + "CUSTOMER/";
		final String online = electionEventDirectory + "ONLINE/";
		final String offline = electionEventDirectory + "OFFLINE/";

		return List.of(
				/*sdmConfig root*/
				createEntry("sdmConfig/elections_config\\.json", SyncExportPhase.AFTER_PRE_CONFIGURATION, SyncExportPhase.AFTER_MIX_DOWNLOAD),

				/* admin-boards directory*/
				createEntry(adminBoardsDirectory + "[a-zA-Z0-9]{32}/setupComponentAdminBoardHashesPayload\\.json",
						SyncExportPhase.AFTER_PRE_CONFIGURATION,
						SyncExportPhase.AFTER_PRE_COMPUTE, SyncExportPhase.AFTER_MIX_DOWNLOAD),

				/*election root*/
				createEntry(electionEventDirectory + "controlComponentPublicKeysPayload\\.[1-4]{1}\\.json", SyncExportPhase.AFTER_CC_KEYS),
				createEntry(electionEventDirectory + "electionEventContextPayload\\.json", SyncExportPhase.AFTER_GENERATE_SIGN,
						SyncExportPhase.AFTER_ELECTORAL_BOARD_SIGN,
						SyncExportPhase.AFTER_MIX_DOWNLOAD),
				createEntry(electionEventDirectory + "setupComponentPublicKeysPayload\\.json", SyncExportPhase.AFTER_ELECTORAL_BOARD_SIGN,
						SyncExportPhase.AFTER_MIX_DOWNLOAD),

				/*customer directory*/
				createEntry(customer + "input/configuration-anonymized\\.xml", SyncExportPhase.AFTER_PRE_CONFIGURATION,
						SyncExportPhase.AFTER_MIX_DOWNLOAD),
				createEntry(customer + "input/encryptionParametersPayload\\.json", SyncExportPhase.AFTER_PRE_CONFIGURATION,
						SyncExportPhase.AFTER_MIX_DOWNLOAD),

				/*offline directory*/
				createEntry(offline + "setupComponentElectoralBoardHashesPayload\\.json", SyncExportPhase.AFTER_ELECTORAL_BOARD_SIGN,
						SyncExportPhase.AFTER_MIX_DOWNLOAD),

				/*online directory*/
				createEntry(online + "electionInformation/ballots/[a-zA-Z0-9]{32}/ballot\\.json", SyncExportPhase.AFTER_GENERATE_SIGN,
						SyncExportPhase.AFTER_ELECTORAL_BOARD_SIGN,
						SyncExportPhase.AFTER_MIX_DOWNLOAD),
				createEntry(
						online
								+ "electionInformation/ballots/[a-zA-Z0-9]{32}/ballotBoxes/[a-zA-Z0-9]{32}/controlComponentBallotBoxPayload_[1-4]{1}\\.json",
						SyncExportPhase.AFTER_MIX_DOWNLOAD),
				createEntry(
						online
								+ "electionInformation/ballots/[a-zA-Z0-9]{32}/ballotBoxes/[a-zA-Z0-9]{32}/controlComponentShufflePayload_[1-4]{1}\\.json",
						SyncExportPhase.AFTER_MIX_DOWNLOAD),
				createEntry(online + "voteVerification/[a-zA-Z0-9]{32}/controlComponentCodeSharesPayload\\.[0-9]+\\.json",
						SyncExportPhase.AFTER_COMPUTE_DOWNLOAD),
				createEntry(online + "voteVerification/[a-zA-Z0-9]{32}/setupComponentCMTablePayload\\.[0-9]+\\.json",
						SyncExportPhase.AFTER_GENERATE_SIGN, SyncExportPhase.AFTER_ELECTORAL_BOARD_SIGN),
				createEntry(online + "voteVerification/[a-zA-Z0-9]{32}/setupComponentLVCCAllowListPayload\\.json",
						SyncExportPhase.AFTER_GENERATE_SIGN, SyncExportPhase.AFTER_ELECTORAL_BOARD_SIGN),
				createEntry(online + "voteVerification/[a-zA-Z0-9]{32}/setupComponentTallyDataPayload\\.json", SyncExportPhase.AFTER_PRE_COMPUTE,
						SyncExportPhase.AFTER_MIX_DOWNLOAD),
				createEntry(online + "voteVerification/[a-zA-Z0-9]{32}/setupComponentVerificationCardKeystoresPayload\\.json",
						SyncExportPhase.AFTER_ELECTORAL_BOARD_SIGN),
				createEntry(online + "voteVerification/[a-zA-Z0-9]{32}/setupComponentVerificationDataPayload\\.[0-9]+\\.json",
						SyncExportPhase.AFTER_PRE_COMPUTE),
				createEntry(online + "voteVerification/[a-zA-Z0-9]{32}/setupComponentVoterAuthenticationDataPayload.json",
						SyncExportPhase.AFTER_PRE_COMPUTE)
		);
	}

	private ImmutablePair<Pattern, List<SyncExportPhase>> createEntry(final String regex, final SyncExportPhase... phases) {
		return ImmutablePair.of(Pattern.compile(regex), Arrays.asList(phases));
	}

	private SyncExportPhase determineActualExportPhase(final String electionEventId) {
		final Optional<SyncExportPhase> actualPhase = Arrays.stream(SyncExportPhase.values())
				.sorted(Comparator.reverseOrder())
				.filter(phase -> fileExists(phase.getExistingFileRegex(), electionEventId))
				.findFirst();

		actualPhase.ifPresent(phase -> LOGGER.info("actual export phase is determined. [actualExportPhase: {}]", phase));
		return actualPhase.orElseThrow(() -> new IllegalStateException("Unable to determine the actual export phase"));
	}

	private boolean fileExists(final String existingFileRegex, final String electionEventId) {
		final Pattern pattern = Pattern.compile(existingFileRegex);
		try (final Stream<Path> walkStream = Files.walk(pathResolver.resolveElectionEventPath(electionEventId)).parallel()) {
			return walkStream
					.filter(Files::exists)
					.anyMatch(path -> pattern.matcher(path.getFileName().toString()).matches());
		} catch (final IOException e) {
			throw new UncheckedIOException(
					String.format("unable to check if the file exists in electionEventId directory. [regex: %s, electionEventId: %s]",
							existingFileRegex, electionEventId), e);
		}
	}

	private enum SyncExportPhase {
		AFTER_PRE_CONFIGURATION("configuration-anonymized\\.xml"),
		AFTER_CC_KEYS("controlComponentPublicKeysPayload\\.[1-4]{1}\\.json"),
		AFTER_PRE_COMPUTE("setupComponentVoterAuthenticationDataPayload\\.json"),
		AFTER_COMPUTE_DOWNLOAD("controlComponentCodeSharesPayload\\.[0-9]+\\.json"),
		AFTER_GENERATE_SIGN("setupComponentCMTablePayload\\.[0-9]+\\.json"),
		AFTER_ELECTORAL_BOARD_SIGN("setupComponentElectoralBoardHashesPayload\\.json"),
		AFTER_MIX_DOWNLOAD("controlComponentBallotBoxPayload_[1-4]{1}\\.json");

		private final String existingFileRegex;

		SyncExportPhase(final String existingFileRegex) {
			this.existingFileRegex = existingFileRegex;
		}

		public String getExistingFileRegex() {
			return existingFileRegex;
		}
	}
}
