/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.application.service;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.securedatamanager.commons.JsonUtils;
import ch.post.it.evoting.securedatamanager.services.domain.model.status.Status;
import ch.post.it.evoting.securedatamanager.services.domain.service.BallotDataGeneratorService;
import ch.post.it.evoting.securedatamanager.services.infrastructure.JsonConstants;
import ch.post.it.evoting.securedatamanager.services.infrastructure.ballotbox.BallotBoxRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.service.ConfigurationEntityStatusService;

/**
 * This is an application service that manages voting card sets.
 */
@Service
@ConditionalOnProperty("role.isSetup")
public class VotingCardSetGenerateBallotService extends BaseVotingCardSetService {

	private static final Logger LOGGER = LoggerFactory.getLogger(VotingCardSetGenerateBallotService.class);

	private final BallotBoxRepository ballotBoxRepository;
	private final BallotDataGeneratorService ballotDataGeneratorService;
	private final ConfigurationEntityStatusService configurationEntityStatusService;

	public VotingCardSetGenerateBallotService(
			final BallotBoxRepository ballotBoxRepository,
			final BallotDataGeneratorService ballotDataGeneratorService,
			final ConfigurationEntityStatusService configurationEntityStatusService) {
		this.ballotBoxRepository = ballotBoxRepository;
		this.ballotDataGeneratorService = ballotDataGeneratorService;
		this.configurationEntityStatusService = configurationEntityStatusService;
	}

	/**
	 * Generates the needed ballot data.
	 *
	 * @param electionEventId The id of the election event.
	 * @param votingCardSetId The id of the voting card set for which the data is generated.
	 * @return a boolean about the result of the generation.
	 */
	public boolean generate(final String electionEventId, final String votingCardSetId) {

		LOGGER.info("Generating the ballot box and the ballot. [electionEventId: {}, votingCardSetId: {}]", electionEventId, votingCardSetId);

		validateUUID(electionEventId);
		validateUUID(votingCardSetId);

		// get ballot box from voting card set repository
		final String ballotBoxId = votingCardSetRepository.getBallotBoxId(votingCardSetId);
		if (StringUtils.isEmpty(ballotBoxId)) {
			return false;
		}

		// get ballot from ballot box repository
		final String ballotId = ballotBoxRepository.getBallotId(ballotBoxId);
		if (StringUtils.isEmpty(ballotId)) {
			return false;
		}

		// generate ballot data
		if (!ballotDataGeneratorService.generate(ballotId, electionEventId)) {
			return false;
		}

		// set ballot status to ready
		final String ballotBoxAsJson = ballotBoxRepository.find(ballotBoxId);
		final String ballotBoxStatus = JsonUtils.getJsonObject(ballotBoxAsJson).getString(JsonConstants.STATUS);
		if (Status.LOCKED.name().equals(ballotBoxStatus)) {
			configurationEntityStatusService.update(Status.READY.name(), ballotBoxId, ballotBoxRepository);
		}

		LOGGER.info("Ballot generated successfully. [electionEventId: {}, votingCardSetId: {}]", electionEventId, votingCardSetId);
		return true;
	}
}
