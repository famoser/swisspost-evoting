/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;

public class VotingCardSetGenerationStatusEvent {

	private final String electionEventId;
	private final String votingCardSetId;

	public VotingCardSetGenerationStatusEvent(final String electionEventId, final String votingCardSetId) {
		this.electionEventId = validateUUID(electionEventId);
		this.votingCardSetId = validateUUID(votingCardSetId);
	}

	public String getElectionEventId() {
		return electionEventId;
	}

	public String getVotingCardSetId() {
		return votingCardSetId;
	}

	public static class Builder {
		private String electionEventId;
		private String votingCardSetId;

		public Builder setElectionEventId(final String electionEventId) {
			this.electionEventId = electionEventId;
			return this;
		}

		public Builder setVotingCardSetId(final String votingCardSetId) {
			this.votingCardSetId = votingCardSetId;
			return this;
		}

		public VotingCardSetGenerationStatusEvent build() {
			return new VotingCardSetGenerationStatusEvent(electionEventId, votingCardSetId);
		}
	}
}

