/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.infrastructure.clients;

import javax.ws.rs.core.MediaType;

import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.SetupComponentPublicKeysPayload;
import ch.post.it.evoting.securedatamanager.commons.Constants;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface VoteVerificationClient {

	@POST("api/v1/processor/configuration/electioncontext/electionevent/{electionEventId}")
	@Headers("Accept:" + MediaType.APPLICATION_JSON)
	Call<Void> uploadElectionEventContext(
			@Path(Constants.ELECTION_EVENT_ID)
			final String electionEventId,
			@Body
			final ElectionEventContextPayload electionEventContextPayload);

	@POST("api/v1/processor/configuration/setupkeys/electionevent/{electionEventId}")
	@Headers("Accept:" + MediaType.APPLICATION_JSON)
	Call<Void> uploadSetupComponentPublicKeys(
			@Path(Constants.ELECTION_EVENT_ID)
			final String electionEventId,
			@Body
			final SetupComponentPublicKeysPayload setupComponentPublicKeysPayload);

	@POST("api/v1/processor/configuration/returncodesmappingtable/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}/chunk/{chunkId}")
	@Headers({ "Accept:" + MediaType.APPLICATION_JSON, "Transfer-Encoding:chunked" })
	Call<ResponseBody> saveReturnCodeMappingTable(
			@Path(Constants.ELECTION_EVENT_ID)
			final String electionEventId,
			@Path(Constants.VERIFICATION_CARD_SET_ID_PARAM)
			final String verificationCardSetId,
			@Path(Constants.CHUNK_ID_PARAM)
			final int chunkId,
			@Body
			final RequestBody setupComponentCMTablePayload);

	@POST("api/v1/processor/configuration/setupvoting/voterauthenticationdata/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}")
	@Headers({ "Accept:" + MediaType.APPLICATION_JSON, "Transfer-Encoding:chunked" })
	Call<ResponseBody> uploadVoterAuthenticationData(
			@Path(Constants.ELECTION_EVENT_ID)
			final String electionEventId,
			@Path(Constants.VERIFICATION_CARD_SET_ID_PARAM)
			final String verificationCardSetId,
			@Body
			final RequestBody setupComponentVoterAuthenticationDataPayload);

	@POST("api/v1/processor/configuration/setupcomponentverificationcardkeystores/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}")
	@Headers({ "Accept:" + MediaType.APPLICATION_JSON, "Transfer-Encoding:chunked" })
	Call<ResponseBody> uploadSetupComponentVerificationCardKeystores(
			@Path(Constants.ELECTION_EVENT_ID)
			final String electionEventId,
			@Path(Constants.VERIFICATION_CARD_SET_ID_PARAM)
			final String verificationCardSetId,
			@Body
			final RequestBody setupComponentVerificationCardKeystoresPayload);

	@POST("api/v1/processor/configuration/setupvoting/ballotdata/electionevent/{electionEventId}/ballot/{ballotId}")
	@Headers({ "Accept:" + MediaType.APPLICATION_JSON, "Transfer-Encoding:chunked" })
	Call<ResponseBody> uploadBallotData(
			@Path(Constants.ELECTION_EVENT_ID)
			final String electionEventId,
			@Path(Constants.BALLOT_ID)
			final String ballotId,
			@Body
			final RequestBody ballotDataPayload);
}
