/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.infrastructure;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ch.post.it.evoting.securedatamanager.services.domain.model.ElectionEventContextEntity;

@Repository
public class ElectionEventContextRepository extends AbstractEntityRepository {

	@Autowired
	public ElectionEventContextRepository(final DatabaseManager databaseManager) {
		super(databaseManager);
	}

	@PostConstruct
	@Override
	public void initialize() {
		super.initialize();
	}

	@Override
	protected String entityName() {
		return ElectionEventContextEntity.class.getSimpleName();
	}

}
