/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.securedatamanager.configuration.PrimesMappingTableFileEntity.PrimesMappingTableFileEntityDeserializer;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTable;
import ch.post.it.evoting.cryptoprimitives.domain.mapper.EncryptionGroupUtils;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;

/**
 * Contains additional ids together with the {@link PrimesMappingTable} for file system persistence.
 *
 * @param encryptionGroup       the encryption group of the primes mapping table.
 * @param electionEventId       the election event id of the primes mapping table.
 * @param verificationCardSetId the verification card set id of the primes mapping table.
 * @param primesMappingTable    the primes mapping table.
 */
@JsonDeserialize(using = PrimesMappingTableFileEntityDeserializer.class)
record PrimesMappingTableFileEntity(GqGroup encryptionGroup, String electionEventId, String verificationCardSetId,
									PrimesMappingTable primesMappingTable) {

	/**
	 * The given encryption group must be corresponding to the given primes mapping table.
	 *
	 * @throws NullPointerException      if {@code encryptionGroup} or {@code primesMappingTable} is null.
	 * @throws FailedValidationException if {@code electionEventId} or {@code verificationCardSetId} is invalid.
	 * @throws IllegalArgumentException  if {@code encryptionGroup} is not the one of the {@code primesMappingTable}.
	 */
	PrimesMappingTableFileEntity {
		checkNotNull(encryptionGroup);
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		checkNotNull(primesMappingTable);

		checkArgument(encryptionGroup.equals(primesMappingTable.getPTable().getGroup()),
				"The encryption group does not correspond to the one of the primes mapping table.");
	}

	static class PrimesMappingTableFileEntityDeserializer extends JsonDeserializer<PrimesMappingTableFileEntity> {

		@Override
		public PrimesMappingTableFileEntity deserialize(final JsonParser parser, final DeserializationContext context) throws IOException {
			final ObjectMapper objectMapper = (ObjectMapper) parser.getCodec();

			final JsonNode node = objectMapper.readTree(parser);

			final String electionEventId = objectMapper.readValue(node.get("electionEventId").toString(), String.class);
			final String verificationCardSetId = objectMapper.readValue(node.get("verificationCardSetId").toString(), String.class);

			final JsonNode encryptionGroupNode = node.get("encryptionGroup");
			final GqGroup encryptionGroup = EncryptionGroupUtils.getEncryptionGroup(objectMapper, encryptionGroupNode);

			final PrimesMappingTable primesMappingTable = objectMapper.reader()
					.withAttribute("group", encryptionGroup)
					.readValue(node.get("primesMappingTable"), PrimesMappingTable.class);

			return new PrimesMappingTableFileEntity(encryptionGroup, electionEventId, verificationCardSetId, primesMappingTable);
		}

	}

}
