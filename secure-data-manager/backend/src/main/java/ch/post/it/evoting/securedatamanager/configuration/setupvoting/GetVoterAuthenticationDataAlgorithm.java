/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;
import java.util.stream.IntStream;

import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.securedatamanager.commons.Constants;

/**
 * Implements the GetVoterAuthenticationData algorithm.
 */
@Service
public class GetVoterAuthenticationDataAlgorithm {

	private final DeriveCredentialIdAlgorithm deriveCredentialIdAlgorithm;
	private final DeriveBaseAuthenticationChallengeAlgorithm deriveBaseAuthenticationChallengeAlgorithm;

	public GetVoterAuthenticationDataAlgorithm(final DeriveCredentialIdAlgorithm deriveCredentialIdAlgorithm,
			final DeriveBaseAuthenticationChallengeAlgorithm deriveBaseAuthenticationChallengeAlgorithm) {
		this.deriveCredentialIdAlgorithm = deriveCredentialIdAlgorithm;
		this.deriveBaseAuthenticationChallengeAlgorithm = deriveBaseAuthenticationChallengeAlgorithm;
	}

	/**
	 * Derives the credentialID and hAuth from the start voting keys and extended authentication factors.
	 *
	 * @param electionEventId ee, the election event id. Must be non-null and a valid UUID.
	 * @param input           the {@link GetVoterAuthenticationDataInput}. Must be non-null.
	 * @return the derived credentialID and hAuth.
	 * @throws NullPointerException      if any parameter is null.
	 * @throws IllegalArgumentException  if the Start Voting Key's size is not l<sub>SVK</sub>={@value Constants#SVK_LENGTH}.
	 * @throws FailedValidationException if the election event id is not a valid UUID or the start voting key is not a valid Base32 string.
	 */
	@SuppressWarnings("java:S117")
	public GetVoterAuthenticationDataOutput getVoterAuthenticationData(final String electionEventId, final GetVoterAuthenticationDataInput input) {
		checkNotNull(input);

		// Context.
		final String ee = validateUUID(electionEventId);

		// Input.
		final List<String> SVK = input.startVotingKeys();
		final List<String> EA = input.extendedAuthenticationFactors();
		final int N_E = SVK.size();

		// Operation.
		final List<VoterAuthenticationData> voterAuthenticationData = IntStream.range(0, N_E)
				.parallel()
				.mapToObj(id -> {
					final String SVK_id = SVK.get(id);
					final String credentialID_id = deriveCredentialIdAlgorithm.deriveCredentialId(ee, SVK_id);

					final String EA_id = EA.get(id);
					final String hAuth_id = deriveBaseAuthenticationChallengeAlgorithm.deriveBaseAuthenticationChallenge(ee, SVK_id, EA_id);

					return new VoterAuthenticationData(credentialID_id, hAuth_id);
				}).toList();

		final List<String> credentialID = voterAuthenticationData.stream()
				.parallel()
				.map(VoterAuthenticationData::credentialID_id)
				.toList();

		final List<String> hAuth = voterAuthenticationData.stream()
				.parallel()
				.map(VoterAuthenticationData::hAuth_id)
				.toList();

		return new GetVoterAuthenticationDataOutput(credentialID, hAuth);
	}

	private record VoterAuthenticationData(String credentialID_id, String hAuth_id) {
	}

}
