/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.HashSet;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.domain.validations.Validations;

/**
 * Encapsulates the flattened (combining all chunks) control component node contributions.
 * <p>
 * All control components generate encrypted long return code shares during the configuration phase. The encrypted long return code shares contain
 * both the exponentiated encrypted partial choice return codes and the exponentiated encrypted confirmation keys.
 */
public class EncryptedNodeLongReturnCodeSharesChunk {

	private final String electionEventId;
	private final String verificationCardSetId;
	private final List<String> verificationCardIds;

	private final int chunkId;

	private final List<EncryptedSingleNodeLongReturnCodeSharesChunk> nodeReturnCodesValues;

	private EncryptedNodeLongReturnCodeSharesChunk(final String electionEventId, final String verificationCardSetId,
			final List<String> verificationCardIds, final int chunkId,
			final List<EncryptedSingleNodeLongReturnCodeSharesChunk> nodeReturnCodesValues) {
		this.electionEventId = electionEventId;
		this.verificationCardSetId = verificationCardSetId;
		this.verificationCardIds = verificationCardIds;
		this.chunkId = chunkId;
		this.nodeReturnCodesValues = nodeReturnCodesValues;
	}

	public String getElectionEventId() {
		return electionEventId;
	}

	public String getVerificationCardSetId() {
		return verificationCardSetId;
	}

	public List<String> getVerificationCardIds() {
		return verificationCardIds;
	}

	public int getChunkId() {
		return chunkId;
	}

	public List<EncryptedSingleNodeLongReturnCodeSharesChunk> getNodeReturnCodesValues() {
		return nodeReturnCodesValues;
	}

	public static class Builder {

		private String electionEventId;
		private String verificationCardSetId;
		private List<String> verificationCardIds;

		private int chunkId;
		private List<EncryptedSingleNodeLongReturnCodeSharesChunk> nodeReturnCodesValues;

		public Builder setElectionEventId(final String electionEventId) {
			this.electionEventId = electionEventId;
			return this;
		}

		public Builder setVerificationCardSetId(final String verificationCardSetId) {
			this.verificationCardSetId = verificationCardSetId;
			return this;
		}

		public Builder setVerificationCardIds(final List<String> verificationCardIds) {
			this.verificationCardIds = verificationCardIds;
			return this;
		}

		public Builder setNodeReturnCodesValues(final List<EncryptedSingleNodeLongReturnCodeSharesChunk> nodeReturnCodesValues) {
			this.nodeReturnCodesValues = nodeReturnCodesValues;
			return this;
		}

		public Builder setChunkId(final int chunkId) {
			this.chunkId = chunkId;
			return this;
		}

		/**
		 * Creates the EncryptedNodeLongReturnCodeSharesChunk. All fields must have been set and be non-null.
		 *
		 * @return a new EncryptedNodeLongReturnCodeSharesChunk.
		 * @throws NullPointerException      if any of the fields is null.
		 * @throws IllegalArgumentException  <ul>
		 *                                   <li>if the return codes contributions node ids are invalid (size and values).</li>
		 *                                   <li>the return codes contributions chunk ids are not positive.</li>
		 *                                   <li>if the nodes does not return the same verification card set ids (size and values).</li>
		 *                                   <li>if the verification card set ids does not correspond to node's ones (size and values).</li>
		 *                                   </ul>
		 * @throws FailedValidationException if
		 *                                   <ul>
		 *                                       <li>{@code electionEventId} has an invalid UUID format.</li>
		 *                                       <li>{@code verificationCardSetId} has an invalid UUID format.</li>
		 *                                       <li>{@code verificationCardIds} contains an id with an invalid UUID format.</li>
		 *                                   </ul>
		 */
		public EncryptedNodeLongReturnCodeSharesChunk build() {
			validateUUID(electionEventId);
			validateUUID(verificationCardSetId);
			checkArgument(chunkId >= 0, String.format("Control component chunk id must be positive. [chunkId: %s]", chunkId));
			verificationCardIds.forEach(Validations::validateUUID);
			checkNotNull(nodeReturnCodesValues);

			final List<Integer> nodeReturnCodesValuesNodeIds = nodeReturnCodesValues.stream()
					.map(EncryptedSingleNodeLongReturnCodeSharesChunk::getNodeId).toList();
			checkArgument(NODE_IDS.size() == nodeReturnCodesValuesNodeIds.size() && new HashSet<>(nodeReturnCodesValuesNodeIds).containsAll(NODE_IDS),
					"Wrong number or invalid values of return codes contributions node ids. [required node ids: %s, found: %s]",
					NODE_IDS, nodeReturnCodesValuesNodeIds);

			checkArgument(nodeReturnCodesValues.stream().map(nrc -> nrc.getVerificationCardIds().size()).distinct().count() == 1,
					"All nodes must return the same number of verificationCardIds. [electionEventId: %s, verificationCardSetId: %s]", electionEventId,
					verificationCardSetId);

			checkArgument(nodeReturnCodesValues.stream().map(EncryptedSingleNodeLongReturnCodeSharesChunk::getVerificationCardIds)
							.collect(Collectors.groupingBy(Function.identity(), Collectors.counting())).values().stream()
							.allMatch(i -> i == NODE_IDS.size()),
					"All nodes must return the same verificationCardIds. [electionEventId: %s, verificationCardSetId: %s]", electionEventId,
					verificationCardSetId);

			final List<String> nodeVerificationCardIds = nodeReturnCodesValues.get(0).getVerificationCardIds();
			checkArgument(verificationCardIds.size() == nodeVerificationCardIds.size() && new HashSet<>(verificationCardIds).containsAll(
							nodeVerificationCardIds),
					"The verificationCardIds must match the verificationCardIds in the nodeReturnCodesValues. [electionEventId: %s, verificationCardSetId: %s]",
					electionEventId, verificationCardSetId);

			return new EncryptedNodeLongReturnCodeSharesChunk(electionEventId, verificationCardSetId, verificationCardIds, chunkId,
					nodeReturnCodesValues);
		}
	}
}
