/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.securedatamanager.services.infrastructure.JsonConstants.EMPTY_OBJECT;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.json.JsonObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.election.Ballot;
import ch.post.it.evoting.cryptoprimitives.domain.election.CombinedCorrectnessInformation;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPrivateKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupMatrix;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.securedatamanager.SetupKeyPairService;
import ch.post.it.evoting.securedatamanager.commons.JsonUtils;
import ch.post.it.evoting.securedatamanager.configuration.EncryptionParametersConfigService;
import ch.post.it.evoting.securedatamanager.configuration.setupvoting.NodeContributionsResponsesService.NodeContributionsChunk;
import ch.post.it.evoting.securedatamanager.services.application.exception.ResourceNotFoundException;
import ch.post.it.evoting.securedatamanager.services.application.service.ElectionEventService;
import ch.post.it.evoting.securedatamanager.services.infrastructure.JsonConstants;
import ch.post.it.evoting.securedatamanager.services.infrastructure.ballotbox.BallotBoxRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.cc.SetupComponentVerificationDataPayloadFileRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.votingcardset.VotingCardSetRepository;

@Service
@ConditionalOnProperty("role.isSetup")
public class VotingCardSetDataGenerationService {

	private static final Logger LOGGER = LoggerFactory.getLogger(VotingCardSetDataGenerationService.class);

	private final GenCMTableAlgorithm genCMTableAlgorithm;
	private final ElectionEventService electionEventService;
	private final BallotConfigService ballotConfigService;
	private final BallotBoxRepository ballotBoxRepository;
	private final SetupKeyPairService setupKeyPairService;
	private final VotingCardSetRepository votingCardSetRepository;

	private final EncryptionParametersConfigService encryptionParametersConfigService;
	private final CombineEncLongCodeSharesAlgorithm combineEncLongCodeSharesAlgorithm;
	private final NodeContributionsResponsesService nodeContributionsResponsesService;
	private final EncryptedNodeLongReturnCodeSharesService encryptedNodeLongReturnCodeSharesService;

	private final SetupComponentVerificationDataPayloadFileRepository setupComponentVerificationDataPayloadFileRepository;

	public VotingCardSetDataGenerationService(
			final GenCMTableAlgorithm genCMTableAlgorithm,
			final ElectionEventService electionEventService,
			final BallotConfigService ballotConfigService,
			final BallotBoxRepository ballotBoxRepository,
			final SetupKeyPairService setupKeyPairService,
			final VotingCardSetRepository votingCardSetRepository,
			final EncryptionParametersConfigService encryptionParametersConfigService,
			final CombineEncLongCodeSharesAlgorithm combineEncLongCodeSharesAlgorithm,
			final NodeContributionsResponsesService nodeContributionsResponsesService,
			final EncryptedNodeLongReturnCodeSharesService encryptedNodeLongReturnCodeSharesService,
			final SetupComponentVerificationDataPayloadFileRepository setupComponentVerificationDataPayloadFileRepository) {
		this.genCMTableAlgorithm = genCMTableAlgorithm;
		this.electionEventService = electionEventService;
		this.ballotConfigService = ballotConfigService;
		this.ballotBoxRepository = ballotBoxRepository;
		this.setupKeyPairService = setupKeyPairService;
		this.votingCardSetRepository = votingCardSetRepository;
		this.encryptionParametersConfigService = encryptionParametersConfigService;
		this.combineEncLongCodeSharesAlgorithm = combineEncLongCodeSharesAlgorithm;
		this.nodeContributionsResponsesService = nodeContributionsResponsesService;
		this.encryptedNodeLongReturnCodeSharesService = encryptedNodeLongReturnCodeSharesService;
		this.setupComponentVerificationDataPayloadFileRepository = setupComponentVerificationDataPayloadFileRepository;
	}

	/**
	 * Calls the CombineEncLongCodeShares and GenCMTable algorithms.
	 *
	 * @param electionEventId the election event id. Must be non-null and a valid UUID.
	 * @param votingCardSetId the voting card set id. Must be non-null and a valid UUID.
	 * @return a {@link ReturnCodesGenerationOutput} containing outputs of the CombineEncLongCodeShares and GenCMTable algorithms.
	 * @throws NullPointerException      if {@code electionEventId} or {@code votingCardSetId} is null.
	 * @throws FailedValidationException if {@code electionEventId} or {@code votingCardSetId} is invalid.
	 */
	public ReturnCodesGenerationOutput generate(final String electionEventId, final String votingCardSetId) {
		validateUUID(electionEventId);
		validateUUID(votingCardSetId);
		checkArgument(electionEventService.exists(electionEventId), "The given election event ID does not exist. [electionEventId: %s]",
				electionEventId);
		checkArgument(votingCardSetRepository.exists(electionEventId, votingCardSetId),
				"The given voting card set ID does not belong to the given election event ID. [votingCardSetId: %s, electionEventId: %s]",
				votingCardSetId, electionEventId);

		final GqGroup encryptionGroup = encryptionParametersConfigService.loadEncryptionGroup(electionEventId);
		final String verificationCardSetId = getVerificationCardSetId(electionEventId, votingCardSetId);
		final String ballotId = getBallotId(votingCardSetId);
		final int numberOfVotingOptions = getNumberOfVotingOptions(electionEventId, ballotId);

		// Load Setup secret key
		final ElGamalMultiRecipientPrivateKey setupSecretKey = setupKeyPairService.load(electionEventId).getPrivateKey();

		// Load node contributions chunk paths
		final List<Path> nodeContributionsChunkPaths = nodeContributionsResponsesService.loadAllChunkPaths(electionEventId, verificationCardSetId);
		verifyChunkPaths(electionEventId, verificationCardSetId, nodeContributionsChunkPaths);

		LOGGER.info("Node contributions chunk paths successfully loaded."
						+ " [electionEventId: {}, votingCardSetId: {}, verificationCardSetId: {}, chunks: {}]", electionEventId, votingCardSetId,
				verificationCardSetId, nodeContributionsChunkPaths.size());

		final List<ReturnCodesGenerationOutputChunk> returnCodesGenerationOutputChunks = nodeContributionsChunkPaths.stream()
				.parallel()
				.map(nodeContributionsPath -> {
					// Load node contributions chunk and convert it
					final NodeContributionsChunk nodeContributionsChunk = nodeContributionsResponsesService.loadNodeContributionsChunk(
							nodeContributionsPath);
					final EncryptedNodeLongReturnCodeSharesChunk encryptedNodeLongReturnCodeSharesChunk = encryptedNodeLongReturnCodeSharesService.convertNodeContributionsChunk(
							electionEventId, verificationCardSetId, nodeContributionsChunk);

					final int chunkId = encryptedNodeLongReturnCodeSharesChunk.getChunkId();

					// Prepare and call CombineEncLongCodeShares algorithm
					final CombineEncLongCodeSharesContext combineEncLongCodeSharesContext = new CombineEncLongCodeSharesContext.Builder()
							.setEncryptionGroup(encryptionGroup)
							.setElectionEventId(electionEventId)
							.setVerificationCardSetId(verificationCardSetId)
							.setSetupSecretKey(setupSecretKey)
							.setNumberOfVotingOptions(numberOfVotingOptions)
							.setNumberOfEligibleVoters(encryptedNodeLongReturnCodeSharesChunk.getVerificationCardIds().size())
							.build();
					final CombineEncLongCodeSharesInput combineEncLongCodeSharesInput = prepareCombineEncLongCodeSharesInput(
							encryptedNodeLongReturnCodeSharesChunk);
					final CombineEncLongCodeSharesOutput combineEncLongCodeSharesOutput = combineEncLongCodeSharesAlgorithm.combineEncLongCodeShares(
							combineEncLongCodeSharesContext, combineEncLongCodeSharesInput);
					LOGGER.info(
							"Encrypted long return code shares successfully combined. [electionEventId: {}, votingCardSetId: {}, verificationCardSetId: {}, chunkId: {}]",
							electionEventId, votingCardSetId, verificationCardSetId, chunkId);

					// Prepare and call genCMTable algorithm
					final GenCMTableContext genCMTableContext = new GenCMTableContext.Builder()
							.setEncryptionGroup(encryptionGroup)
							.setElectionEventId(electionEventId)
							.setBallotId(ballotId)
							.setVerificationCardSetId(verificationCardSetId)
							.setSetupSecretKey(setupSecretKey)
							.build();
					final GenCMTableInput genCMTableInput = new GenCMTableInput.Builder()
							.setVerificationCardIds(encryptedNodeLongReturnCodeSharesChunk.getVerificationCardIds())
							.setEncryptedPreChoiceReturnCodes(combineEncLongCodeSharesOutput.getEncryptedPreChoiceReturnCodesVector())
							.setPreVoteCastReturnCodes(GroupVector.from(combineEncLongCodeSharesOutput.getPreVoteCastReturnCodesVector()))
							.build();
					final GenCMTableOutput genCMTableOutput = genCMTableAlgorithm.genCMTable(genCMTableContext, genCMTableInput);
					LOGGER.info(
							"Return codes mapping table successfully generated. [electionEventId: {}, votingCardSetId: {}, verificationCardSetId: {}, chunkId: {}]",
							electionEventId, votingCardSetId, verificationCardSetId, chunkId);

					return new ReturnCodesGenerationOutputChunk(encryptedNodeLongReturnCodeSharesChunk.getVerificationCardIds(),
							combineEncLongCodeSharesOutput.getLongVoteCastReturnCodesAllowList(), genCMTableOutput);
				})
				.toList();

		final int numberOfEligibleVoters = getNumberOfEligibleVoters(electionEventId, votingCardSetId);
		final Set<String> treatedVerificationCardIds = returnCodesGenerationOutputChunks.stream()
				.flatMap(chunk -> chunk.verificationCardIds().stream())
				.collect(Collectors.toUnmodifiableSet());
		checkState(numberOfEligibleVoters == treatedVerificationCardIds.size());

		LOGGER.info("Return codes generation finished. [electionEventId: {}, votingCardSetId: {}, verificationCardSetId: {}]", electionEventId,
				votingCardSetId, verificationCardSetId);

		final List<String> verificationCardIds = new ArrayList<>();
		final List<String> longVoteCastReturnCodesAllowList = new ArrayList<>();
		final List<String> shortVoteCastReturnCodes = new ArrayList<>();
		final List<List<String>> shortChoiceReturnCodes = new ArrayList<>();
		final SortedMap<String, String> returnCodesMappingTable = new TreeMap<>();
		returnCodesGenerationOutputChunks.forEach(returnCodesGenerationOutputChunk -> {
			verificationCardIds.addAll(returnCodesGenerationOutputChunk.verificationCardIds());
			longVoteCastReturnCodesAllowList.addAll(returnCodesGenerationOutputChunk.longVoteCastReturnCodesAllowList());
			shortChoiceReturnCodes.addAll(returnCodesGenerationOutputChunk.genCMTableOutput().getShortChoiceReturnCodes());
			shortVoteCastReturnCodes.addAll(returnCodesGenerationOutputChunk.genCMTableOutput().getShortVoteCastReturnCodes());
			returnCodesMappingTable.putAll(returnCodesGenerationOutputChunk.genCMTableOutput().getReturnCodesMappingTable());
		});

		return new ReturnCodesGenerationOutput.Builder()
				.setElectionEventId(electionEventId)
				.setVerificationCardSetId(verificationCardSetId)
				.setVerificationCardIds(verificationCardIds)
				.setShortVoteCastReturnCodes(shortVoteCastReturnCodes)
				.setLongVoteCastReturnCodesAllowList(longVoteCastReturnCodesAllowList)
				.setReturnCodesMappingTable(returnCodesMappingTable)
				.setShortChoiceReturnCodes(shortChoiceReturnCodes)
				.build();
	}

	/**
	 * Verifies the consistency of the chunks' paths between the SetupComponentVerificationDataPayloads and ControlComponentCodeSharesPayloads.
	 */
	private void verifyChunkPaths(final String electionEventId, final String verificationCardSetId, final List<Path> nodeContributionsChunkPaths) {
		checkState(!nodeContributionsChunkPaths.isEmpty(), "No node contributions responses. [electionEventId: %s, verificationCardSetId: %s]",
				electionEventId, verificationCardSetId);

		final List<Path> verificationDataPayloadChunkPaths = setupComponentVerificationDataPayloadFileRepository.findAllPathsOrderByChunkId(
				electionEventId, verificationCardSetId);
		checkState(nodeContributionsChunkPaths.size() == verificationDataPayloadChunkPaths.size(),
				"The SetupComponentVerificationDataPayloads and ControlComponentCodeSharesPayloads have different chunk counts. "
						+ "[electionEventId: %s, verificationCardSetId: %s, setupComponentChunkCount: %s, , controlComponentChunkCount: %s]",
				electionEventId, verificationCardSetId, verificationDataPayloadChunkPaths.size(), nodeContributionsChunkPaths.size());

		IntStream.range(0, nodeContributionsChunkPaths.size())
				.forEach(i -> {
					final Path nodeContributionChunkPath = nodeContributionsChunkPaths.get(i);
					final Path verificationDataPayloadChunkPath = verificationDataPayloadChunkPaths.get(i);
					final int nodeContributionsChunkId = nodeContributionsResponsesService.getChunkId(nodeContributionChunkPath);
					final int verificationDataPayloadChunkId = setupComponentVerificationDataPayloadFileRepository.getChunkId(
							verificationDataPayloadChunkPath);
					checkState(i == nodeContributionsChunkId && nodeContributionsChunkId == verificationDataPayloadChunkId,
							"The chunk ID sequence is interrupted or incomplete. [electionEventId: %s, verificationCardSetId: %s]", electionEventId,
							verificationCardSetId);
				});
	}

	private String getVerificationCardSetId(final String electionEventId, final String votingCardSetId) {
		final String votingCardSetAsJson = votingCardSetRepository.find(votingCardSetId);
		checkState(isNotEmpty(votingCardSetAsJson) && !EMPTY_OBJECT.equals(votingCardSetAsJson),
				"No voting card set found. [electionEventId: %s, votingCardSetId: %s]", electionEventId, votingCardSetId);

		final JsonObject votingCardSet = JsonUtils.getJsonObject(votingCardSetAsJson);
		return votingCardSet.getString(JsonConstants.VERIFICATION_CARD_SET_ID);
	}

	private String getBallotId(final String votingCardSetId) {
		final String ballotBoxId = votingCardSetRepository.getBallotBoxId(votingCardSetId);
		checkState(ballotBoxId != null, "No ballot box found for voting card set. [votingCardSetId: %s]", votingCardSetId);
		return ballotBoxRepository.getBallotId(ballotBoxId);
	}

	private int getNumberOfVotingOptions(final String electionEventId, final String ballotId) {
		final Ballot ballot = ballotConfigService.getBallot(electionEventId, ballotId);
		return new CombinedCorrectnessInformation(ballot).getTotalNumberOfVotingOptions();
	}

	private int getNumberOfEligibleVoters(final String electionEventId, final String votingCardSetId) {
		try {
			return votingCardSetRepository.getNumberOfVotingCards(electionEventId, votingCardSetId);
		} catch (final ResourceNotFoundException e) {
			throw new IllegalStateException(
					String.format("No voting card set found. [electionEventId: %s, votingCardSetId: %s]", electionEventId, votingCardSetId));
		}
	}

	private CombineEncLongCodeSharesInput prepareCombineEncLongCodeSharesInput(
			final EncryptedNodeLongReturnCodeSharesChunk encryptedNodeLongReturnCodeSharesChunk) {

		// Prepare the input matrix from the node return codes values
		final List<EncryptedSingleNodeLongReturnCodeSharesChunk> nodeReturnCodesValues = encryptedNodeLongReturnCodeSharesChunk.getNodeReturnCodesValues();

		final List<List<ElGamalMultiRecipientCiphertext>> partialChoiceReturnCodesColumns = nodeReturnCodesValues.stream()
				.parallel()
				.map(EncryptedSingleNodeLongReturnCodeSharesChunk::getExponentiatedEncryptedPartialChoiceReturnCodes)
				.toList();
		final GroupMatrix<ElGamalMultiRecipientCiphertext, GqGroup> partialChoiceReturnCodesMatrix = GroupMatrix.fromColumns(
				partialChoiceReturnCodesColumns);

		final List<List<ElGamalMultiRecipientCiphertext>> confirmationKeysColumns = nodeReturnCodesValues.stream()
				.parallel()
				.map(EncryptedSingleNodeLongReturnCodeSharesChunk::getExponentiatedEncryptedConfirmationKeys)
				.toList();
		final GroupMatrix<ElGamalMultiRecipientCiphertext, GqGroup> confirmationKeysMatrix = GroupMatrix.fromColumns(confirmationKeysColumns);

		return new CombineEncLongCodeSharesInput.Builder()
				.setExponentiatedEncryptedChoiceReturnCodesMatrix(partialChoiceReturnCodesMatrix)
				.setExponentiatedEncryptedConfirmationKeysMatrix(confirmationKeysMatrix)
				.setVerificationCardIds(encryptedNodeLongReturnCodeSharesChunk.getVerificationCardIds())
				.build();
	}

	record ReturnCodesGenerationOutputChunk(List<String> verificationCardIds, List<String> longVoteCastReturnCodesAllowList,
											GenCMTableOutput genCMTableOutput) {
	}

}
