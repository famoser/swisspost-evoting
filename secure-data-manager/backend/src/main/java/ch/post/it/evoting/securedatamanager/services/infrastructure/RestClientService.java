/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.infrastructure;

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;

import okhttp3.OkHttpClient;
import okhttp3.OkHttpClient.Builder;
import okhttp3.logging.HttpLoggingInterceptor;
import okhttp3.logging.HttpLoggingInterceptor.Level;
import retrofit2.Converter.Factory;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;

@Service
public class RestClientService {
	private static final Logger LOGGER = LoggerFactory.getLogger(RestClientService.class);

	public Retrofit getRestClientWithJacksonConverter(final String url, final long connectionTimeout) {
		final OkHttpClient restClient = new OkHttpClient();
		LOGGER.debug("A new RestClient has been created.");

		// Returns a client without SSL.
		final Builder newBuilder = restClient.newBuilder();
		final Retrofit restAdapter = createRetrofitClient(url, newBuilder, connectionTimeout);

		LOGGER.debug("A non SSL RestClient has been served.");
		return restAdapter;
	}

	private Retrofit createRetrofitClient(final String url, final Builder okHttpClientBuilder, final long connectionTimeout) {
		final HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
		logging.setLevel(Level.NONE);
		okHttpClientBuilder.addInterceptor(logging);
		okHttpClientBuilder.connectTimeout(connectionTimeout, TimeUnit.SECONDS);
		okHttpClientBuilder.readTimeout(connectionTimeout, TimeUnit.SECONDS);
		okHttpClientBuilder.writeTimeout(connectionTimeout, TimeUnit.SECONDS);

		String validUrl = url;
		final String retrofit2CompatibleUrlEnding = "/";
		if (!url.endsWith(retrofit2CompatibleUrlEnding)) {
			validUrl = url.concat(retrofit2CompatibleUrlEnding);
		}

		final Factory factory = JacksonConverterFactory.create(DomainObjectMapper.getNewInstance());

		return new Retrofit.Builder()
				.addCallAdapterFactory(RxJavaCallAdapterFactory.create())
				.addConverterFactory(factory)
				.baseUrl(validUrl)
				.client(okHttpClientBuilder.build())
				.build();
	}

}
