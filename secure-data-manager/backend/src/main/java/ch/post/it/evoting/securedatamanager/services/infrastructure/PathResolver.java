/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.infrastructure;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.securedatamanager.commons.Constants;

@Service
public class PathResolver {

	private final String workspace;

	public PathResolver(
			@Value("${sdm.workspace}")
			final String workspace) {
		this.workspace = workspace;
	}

	/**
	 * Provides the sdm directory path in the SDM workspace.
	 * <p>
	 * The path corresponds to the location {@value Constants#SDM_DIR_NAME}.
	 *
	 * @return the sdm directory path.
	 */
	public Path resolveSdmPath() {
		return Paths.get(workspace, Constants.SDM_DIR_NAME);
	}

	/**
	 * Provides the sdmConfig directory path in the SDM workspace.
	 * <p>
	 * The path corresponds to the location {@value Constants#SDM_CONFIG_DIR}.
	 *
	 * @return the sdmConfig directory path.
	 */
	public Path resolveSdmConfigPath() {
		final Path sdmConfigPath = Paths.get(workspace, Constants.SDM_CONFIG_DIR);

		createFolderIfNotExists(sdmConfigPath);

		return sdmConfigPath;
	}

	/**
	 * Provides the config directory path in the SDM workspace.
	 * <p>
	 * The path corresponds to the location {@value Constants#CONFIG_FILES_BASE_DIR}.
	 *
	 * @return the config directory path.
	 */
	public Path resolveConfigPath() {
		return Paths.get(workspace, Constants.CONFIG_FILES_BASE_DIR);
	}

	/**
	 * Provides the election event directory path in the SDM workspace for the given election event.
	 * <p>
	 * The path corresponds to the location {@value Constants#CONFIG_FILES_BASE_DIR}/{@code electionEventId}.
	 *
	 * @param electionEventId the election event id. Must be non-null and a valid UUID.
	 * @return the election event path in the SDM workspace.
	 * @throws NullPointerException      if {@code electionEventId} is null.
	 * @throws FailedValidationException if {@code electionEventId} is not valid.
	 */
	public Path resolveElectionEventPath(final String electionEventId) {
		validateUUID(electionEventId);

		return Paths.get(workspace, Constants.CONFIG_FILES_BASE_DIR, electionEventId);
	}

	/**
	 * Provides the election event directory path in the SDM workspace for the given election event.
	 * <p>
	 * The path corresponds to the location {@value Constants#CONFIG_FILES_BASE_DIR}/{@value Constants#ADMIN_BOARDS_DIR}/{@code adminBoardId}.
	 *
	 * @param adminBoardId the admin board id. Must be non-null and a valid UUID.
	 * @return the admin board path in the SDM workspace.
	 * @throws NullPointerException      if {@code adminBoardId} is null.
	 * @throws FailedValidationException if {@code adminBoardId} is not valid.
	 */
	public Path resolveAdminBoardPath(final String adminBoardId) {
		validateUUID(adminBoardId);

		return Paths.get(workspace, Constants.CONFIG_FILES_BASE_DIR, Constants.ADMIN_BOARDS_DIR, adminBoardId);
	}

	/**
	 * Provides the offline directory path in the SDM workspace for the given election event.
	 * <p>
	 * The path corresponds to the location
	 * {@value Constants#CONFIG_FILES_BASE_DIR}/{@code electionEventId}/{@value Constants#CONFIG_DIR_NAME_OFFLINE}.
	 *
	 * @param electionEventId the election event id for which to retrieve the election information directory. Must be non-null and a valid UUID.
	 * @return the offline path in the SDM workspace.
	 * @throws NullPointerException      if {@code electionEventId} is null.
	 * @throws FailedValidationException if {@code electionEventId} is not valid.
	 */
	public Path resolveOfflinePath(final String electionEventId) {
		validateUUID(electionEventId);

		return Paths.get(workspace, Constants.CONFIG_FILES_BASE_DIR, electionEventId, Constants.CONFIG_DIR_NAME_OFFLINE);
	}

	/**
	 * Provides the directory path in the SDM workspace to the primes mapping tables.
	 * <p>
	 * The path corresponds to the location
	 * {@value Constants#CONFIG_FILES_BASE_DIR}/{@code electionEventId}/{@value Constants#CONFIG_DIR_NAME_OFFLINE}/{@value
	 * Constants#CONFIG_DIR_NAME_PRIMES_MAPPING_TABLES}.
	 *
	 * @param electionEventId the election event id for which to retrieve the election information directory. Must be non-null and a valid UUID.
	 * @return the primes mapping tables path.
	 */
	public Path resolvePrimesMappingTablesPath(final String electionEventId) {
		validateUUID(electionEventId);

		final Path offlinePath = resolveOfflinePath(electionEventId);

		return offlinePath.resolve(Constants.CONFIG_DIR_NAME_PRIMES_MAPPING_TABLES);
	}

	/**
	 * Provides the directory path in the SDM workspace to the verification card secret keys.
	 * <p>
	 * The path corresponds to the location
	 * {@value Constants#CONFIG_FILES_BASE_DIR}/{@code electionEventId}/{@value Constants#CONFIG_DIR_NAME_OFFLINE}/{@value
	 * Constants#CONFIG_DIR_NAME_VERIFICATION_CARD_SECRET_KEYS}.
	 *
	 * @param electionEventId the election event id for which to retrieve the election information directory. Must be non-null and a valid UUID.
	 * @return the verification card secret keys path path.
	 */
	public Path resolveVerificationCardSecretKeys(final String electionEventId) {
		validateUUID(electionEventId);

		final Path offlinePath = resolveOfflinePath(electionEventId);

		return offlinePath.resolve(Constants.CONFIG_DIR_NAME_VERIFICATION_CARD_SECRET_KEYS);
	}

	/**
	 * Provides the ballot directory path in the SDM workspace for the given election event and ballot.
	 * <p>
	 * The path corresponds to the location
	 * {@value Constants#CONFIG_FILES_BASE_DIR}/{@code electionEventId}/{@value Constants#CONFIG_DIR_NAME_ONLINE}/{@value
	 * Constants#CONFIG_DIR_NAME_ELECTIONINFORMATION}/{@value Constants#CONFIG_DIR_NAME_BALLOTS}/{@code ballotId}.
	 *
	 * @param electionEventId the election event the ballot belongs to. Must be non-null and a valid UUID.
	 * @param ballotId        the ballot identification. Must be non-null and a valid UUID.
	 * @return the ballot path in the SDM workspace.
	 * @throws NullPointerException      if any of the inputs is null.
	 * @throws FailedValidationException if any of the inputs is not valid.
	 */
	public Path resolveBallotPath(final String electionEventId, final String ballotId) {
		validateUUID(electionEventId);
		validateUUID(ballotId);

		return Paths.get(workspace, Constants.CONFIG_FILES_BASE_DIR, electionEventId, Constants.CONFIG_DIR_NAME_ONLINE,
				Constants.CONFIG_DIR_NAME_ELECTIONINFORMATION, Constants.CONFIG_DIR_NAME_BALLOTS, ballotId);
	}

	/**
	 * Provides the ballot box directory path in the SDM workspace for the given election event, ballot and ballot box.
	 * <p>
	 * The path corresponds to the location
	 * {@value Constants#CONFIG_FILES_BASE_DIR}/{@code electionEventId}/{@value Constants#CONFIG_DIR_NAME_ONLINE}/{@value
	 * Constants#CONFIG_DIR_NAME_ELECTIONINFORMATION}/{@value Constants#CONFIG_DIR_NAME_BALLOTS}/
	 * {@code ballotId}/{@value Constants#CONFIG_DIR_NAME_BALLOTBOXES}/{@code ballotBoxId}.
	 *
	 * @param electionEventId the election event the ballot box belongs to. Must be non-null and a valid UUID.
	 * @param ballotId        the ballot belongs to. Must be non-null and a valid UUID.
	 * @param ballotBoxId     the expected ballot box. Must be non-null and a valid UUID.
	 * @return the ballot box path in the SDM workspace.
	 * @throws NullPointerException      if any of the inputs is null.
	 * @throws FailedValidationException if any of the inputs is not valid.
	 */
	public Path resolveBallotBoxPath(final String electionEventId, final String ballotId, final String ballotBoxId) {
		validateUUID(electionEventId);
		validateUUID(ballotId);
		validateUUID(ballotBoxId);

		return Paths.get(workspace, Constants.CONFIG_FILES_BASE_DIR, electionEventId, Constants.CONFIG_DIR_NAME_ONLINE,
				Constants.CONFIG_DIR_NAME_ELECTIONINFORMATION, Constants.CONFIG_DIR_NAME_BALLOTS, ballotId, Constants.CONFIG_DIR_NAME_BALLOTBOXES,
				ballotBoxId);
	}

	/**
	 * Provides the vote verification directory path in the SDM workspace for the given election event.
	 * <p>
	 * The path corresponds to the location
	 * {@value Constants#CONFIG_FILES_BASE_DIR}/{@code electionEventId}/{@value Constants#CONFIG_DIR_NAME_ONLINE}/{@value
	 * Constants#CONFIG_DIR_NAME_VOTEVERIFICATION}.
	 *
	 * @param electionEventId the election event the ballot box belongs to. Must be non-null and a valid UUID.
	 * @return the vote verification path in the SDM workspace.
	 * @throws NullPointerException      if any of the inputs is null.
	 * @throws FailedValidationException if any of the inputs is not valid.
	 */
	public Path resolveVoteVerificationPath(final String electionEventId) {
		validateUUID(electionEventId);

		return Paths.get(workspace, Constants.CONFIG_FILES_BASE_DIR, electionEventId, Constants.CONFIG_DIR_NAME_ONLINE,
				Constants.CONFIG_DIR_NAME_VOTEVERIFICATION);
	}

	/**
	 * Provides the verification card set directory path in the SDM workspace for the given election event and verification card set.
	 * <p>
	 * The path corresponds to the location
	 * {@value Constants#CONFIG_FILES_BASE_DIR}/{@code electionEventId}/{@value Constants#CONFIG_DIR_NAME_ONLINE}/{@value
	 * Constants#CONFIG_DIR_NAME_VOTEVERIFICATION}/{@code verificationCardSetId}.
	 *
	 * @param electionEventId       the election event the ballot box belongs to. Must be non-null and a valid UUID.
	 * @param verificationCardSetId the expected verification card set. Must be non-null and a valid UUID.
	 * @return the verification card set path in the SDM workspace.
	 * @throws NullPointerException      if any of the inputs is null.
	 * @throws FailedValidationException if any of the inputs is not valid.
	 */
	public Path resolveVerificationCardSetPath(final String electionEventId, final String verificationCardSetId) {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);

		return Paths.get(workspace, Constants.CONFIG_FILES_BASE_DIR, electionEventId, Constants.CONFIG_DIR_NAME_ONLINE,
				Constants.CONFIG_DIR_NAME_VOTEVERIFICATION, verificationCardSetId);
	}

	/**
	 * Provides the election event printing directory path in the SDM workspace for the given election event.
	 * <p>
	 * The path corresponds to the location
	 * {@value Constants#CONFIG_FILES_BASE_DIR}/{@code electionEventId}/{@value Constants#CONFIG_DIR_NAME_ONLINE}/{@value
	 * Constants#CONFIG_DIR_NAME_PRINTING}.
	 * <p>
	 * If the folder {@value Constants#CONFIG_DIR_NAME_PRINTING} does not exist, it will be created.
	 *
	 * @param electionEventId the election event the printing directory belongs to. Must be non-null and a valid UUID.
	 * @return the election event printing path in the SDM workspace.
	 * @throws NullPointerException      if {@code electionEventId} is null.
	 * @throws FailedValidationException if {@code electionEventId} is not valid.
	 */
	public Path resolvePrintingPath(final String electionEventId) {
		validateUUID(electionEventId);

		final Path printingPath = Paths.get(workspace, Constants.CONFIG_FILES_BASE_DIR, electionEventId, Constants.CONFIG_DIR_NAME_ONLINE,
				Constants.CONFIG_DIR_NAME_PRINTING);

		createFolderIfNotExists(printingPath);

		return printingPath;
	}

	/**
	 * Provides the election event customer output directory path in the SDM workspace for the given election event.
	 * <p>
	 * The path corresponds to the location
	 * {@value Constants#CONFIG_FILES_BASE_DIR}/{@code electionEventId}/{@value Constants#CONFIG_DIR_NAME_CUSTOMER}/{@value
	 * Constants#CONFIG_DIR_NAME_OUTPUT}.
	 * <p>
	 * If the folder {@value Constants#CONFIG_DIR_NAME_OUTPUT} does not exist, it will be created.
	 *
	 * @param electionEventId the election event the customer output directory belongs to. Must be non-null and a valid UUID.
	 * @return the election event customer output path in the SDM workspace.
	 * @throws NullPointerException      if {@code electionEventId} is null.
	 * @throws FailedValidationException if {@code electionEventId} is not valid.
	 */
	public Path resolveOutputPath(final String electionEventId) {
		validateUUID(electionEventId);

		final Path outputPath = Paths.get(workspace, Constants.CONFIG_FILES_BASE_DIR, electionEventId, Constants.CONFIG_DIR_NAME_CUSTOMER,
				Constants.CONFIG_DIR_NAME_OUTPUT);

		createFolderIfNotExists(outputPath);

		return outputPath;
	}

	/**
	 * Provides the election event customer input directory path in the SDM workspace for the given election event.
	 * <p>
	 * The path corresponds to the location
	 * {@value Constants#CONFIG_FILES_BASE_DIR}/{@code electionEventId}/{@value Constants#CONFIG_DIR_NAME_CUSTOMER}/{@value
	 * Constants#CONFIG_DIR_NAME_INPUT}.
	 * <p>
	 * If the folder {@value Constants#CONFIG_DIR_NAME_INPUT} does not exist, it will be created.
	 *
	 * @param electionEventId the election event the customer input directory belongs to. Must be non-null and a valid UUID.
	 * @return the election event customer input path in the SDM workspace.
	 * @throws NullPointerException      if {@code electionEventId} is null.
	 * @throws FailedValidationException if {@code electionEventId} is not valid.
	 */
	public Path resolveInputPath(final String electionEventId) {
		validateUUID(electionEventId);

		final Path inputPath = Paths.get(workspace, Constants.CONFIG_FILES_BASE_DIR, electionEventId, Constants.CONFIG_DIR_NAME_CUSTOMER,
				Constants.CONFIG_DIR_NAME_INPUT);

		createFolderIfNotExists(inputPath);

		return inputPath;
	}

	/**
	 * Provides the input directory path in the SDM workspace.
	 * <p>
	 * The path corresponds to the location {@value Constants#SDM_INPUT_FILES_BASE_DIR}.
	 *
	 * @return the input path in the SDM workspace.
	 */
	public Path resolveInputPath() {
		return Paths.get(workspace, Constants.SDM_INPUT_FILES_BASE_DIR);
	}

	private void createFolderIfNotExists(final Path path) {
		if (!Files.exists(path)) {
			try {
				Files.createDirectories(path);
			} catch (final IOException e) {
				throw new UncheckedIOException(String.format("An error occurred while creating the folder. [folder: %s]", path), e);
			}
		}
	}
}
