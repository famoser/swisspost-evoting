/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.infrastructure;

/**
 * Constants used for managing json formats.
 */
public final class JsonConstants {

	// ////////////////////////////////////
	//
	// elections_config
	//
	// ////////////////////////////////////

	public static final String RESOURCE_TYPE = "resourceType";

	public static final String ELECTION_EVENTS = "electionEvents";
	public static final String ID = "id";
	public static final String DEFAULT_TITLE = "defaultTitle";
	public static final String DEFAULT_DESCRIPTION = "defaultDescription";
	public static final String ALIAS = "alias";
	public static final String DATE_FROM = "dateFrom";
	public static final String DATE_TO = "dateTo";
	public static final String GRACE_PERIOD = "gracePeriod";
	public static final String ADMINISTRATION_BOARD = "administrationBoard";

	public static final String SETTINGS = "settings";
	public static final String ELECTION_EVENT = "electionEvent";

	public static final String BALLOTS = "ballots";
	public static final String CONTESTS = "contests";
	public static final String OPTIONS = "options";
	public static final String ATTRIBUTES = "attributes";
	public static final String STATUS = "status";

	public static final String BALLOT_BOXES = "ballotBoxes";
	public static final String BALLOT = "ballot";
	public static final String ELECTORAL_BOARD = "electoralBoard";
	public static final String TEST = "test";

	public static final String VOTING_CARD_SETS = "votingCardSets";
	public static final String NUMBER_OF_VOTING_CARDS_TO_GENERATE = "numberOfVotingCardsToGenerate";
	public static final String BALLOT_BOX = "ballotBox";
	public static final String VERIFICATION_CARD_SET_ID = "verificationCardSetId";

	public static final String ELECTORAL_BOARDS = "electoralBoards";

	public static final String ADMINISTRATION_BOARDS = "administrationBoards";
	public static final String MINIMUM_THRESHOLD = "minimumThreshold";
	public static final String BOARD_MEMBERS = "boardMembers";

	public static final String TRANSLATIONS = "translations";
	public static final String TEXTS = "texts";
	public static final String TEXT = "text";
	public static final String ATTRIBUTE_KEYS = "attributeKeys";
	public static final String ANSWER_TYPE_TEXT = "answerType_text";
	public static final String QUESTION_TYPE_TEXT = "questionType_text";
	public static final String LIST_TYPE_ATTRIBUTE_1 = "listType_attribute1";
	public static final String LIST_TYPE_APPARENTMENT = "listType_apparentment";
	public static final String LIST_TYPE_SOUS_APPARENTMENT = "listType_sousApparentment";
	public static final String CANDIDATE_TYPE_ATTRIBUTE = "candidateType_attribute";
	public static final String CANDIDATE_TYPE_ATTRIBUTE_1 = CANDIDATE_TYPE_ATTRIBUTE + 1;
	public static final String CANDIDATE_TYPE_ATTRIBUTE_2 = CANDIDATE_TYPE_ATTRIBUTE + 2;
	public static final String CANDIDATE_TYPE_ATTRIBUTE_3 = CANDIDATE_TYPE_ATTRIBUTE + 3;
	public static final String CANDIDATE_TYPE_INITIAL_ACCUMULATION = "candidateType_initialAccumulation";
	public static final String CANDIDATE_TYPE_POSITION_ON_LIST = "candidateType_positionOnList";
	public static final String TITLE = "title";
	public static final String DESCRIPTION = "description";
	public static final String HOW_TO_VOTE = "howToVote";
	public static final String LOCALE = "locale";

	// ////////////////////////////////////
	//
	// miscellaneous constants
	//
	// ////////////////////////////////////

	public static final String P = "p";

	public static final String Q = "q";

	public static final String G = "g";

	public static final String ENCRYPTION_PARAMETERS = "encryptionParameters";

	public static final String SERVER_MODE = "serverMode";

	public static final String RESULT = "result";

	public static final String DETAILS = "details";

	public static final String BALLOT_BOX_ALIAS = "ballotBoxAlias";

	public static final String ELECTION_EVENT_DOT_ID = "electionEvent.id";
	public static final String BALLOT_DOT_ID = "ballot.id";
	public static final String ELECTORAL_BOARD_DOT_ID = "electoralBoard.id";
	public static final String ELECTION_EVENT_CONTEXT = "electionEventContext";
	public static final String SYNCHRONIZED = "synchronized";

	public static final String UPLOADED = "uploaded";

	public static final String EMPTY_OBJECT = "{}";

	public static final String RESULT_EMPTY = "{\"result\":[]}";

	public static final String SIGNED_OBJECT = "signedObject";

	public static final java.lang.String BALLOT_ALIAS = "ballotAlias";

	private JsonConstants() {
		// Avoids instantiation.
	}
}
