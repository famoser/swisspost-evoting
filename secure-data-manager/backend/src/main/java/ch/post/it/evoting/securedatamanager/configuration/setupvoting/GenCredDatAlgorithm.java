/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.hashing.HashableString.from;
import static ch.post.it.evoting.cryptoprimitives.utils.Conversions.integerToByteArray;
import static ch.post.it.evoting.cryptoprimitives.utils.Conversions.stringToByteArray;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.google.common.collect.Streams;
import com.google.common.primitives.Bytes;

import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTable;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.hashing.Argon2;
import ch.post.it.evoting.cryptoprimitives.hashing.Argon2Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableBigInteger;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableByteArray;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableList;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableString;
import ch.post.it.evoting.cryptoprimitives.math.Base64;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.symmetric.Symmetric;
import ch.post.it.evoting.cryptoprimitives.symmetric.SymmetricCiphertext;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.votingoptions.GetActualVotingOptionsAlgorithm;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.votingoptions.GetEncodedVotingOptionsAlgorithm;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.votingoptions.GetSemanticInformationAlgorithm;

/**
 * Implements the GenCredDat algorithm described in the cryptographic protocol.
 */
@Service
public class GenCredDatAlgorithm {

	private final Hash hash;
	private final Symmetric symmetric;
	private final Base64 base64;
	private final Argon2 argon2;
	private final GetEncodedVotingOptionsAlgorithm getEncodedVotingOptionsAlgorithm;
	private final GetActualVotingOptionsAlgorithm getActualVotingOptionsAlgorithm;
	private final GetSemanticInformationAlgorithm getSemanticInformationAlgorithm;

	public GenCredDatAlgorithm(final Hash hash,
			final Symmetric symmetric,
			final Base64 base64,
			@Qualifier("argon2LessMemory")
			final Argon2 argon2,
			final GetEncodedVotingOptionsAlgorithm getEncodedVotingOptionsAlgorithm,
			final GetActualVotingOptionsAlgorithm getActualVotingOptionsAlgorithm,
			final GetSemanticInformationAlgorithm getSemanticInformationAlgorithm) {
		this.hash = hash;
		this.symmetric = symmetric;
		this.base64 = base64;
		this.argon2 = argon2;
		this.getEncodedVotingOptionsAlgorithm = getEncodedVotingOptionsAlgorithm;
		this.getActualVotingOptionsAlgorithm = getActualVotingOptionsAlgorithm;
		this.getSemanticInformationAlgorithm = getSemanticInformationAlgorithm;
	}

	/**
	 * Services for the generation of the voter’s credential data.
	 *
	 * @param context the context data.
	 * @param input   the input data.
	 * @return the generated verification card key stores data as a {@link GenVerDatOutput}.
	 * @throws NullPointerException if {@code GenCredDatContext} or {@code GenCredDatInput} is null.
	 */
	@SuppressWarnings("java:S117")
	public GenCredDatOutput genCredDat(final GenCredDatContext context, final GenCredDatInput input) {
		checkNotNull(context);
		checkNotNull(input);

		checkArgument(context.encryptionGroup().hasSameOrderAs(input.verificationCardSecretKeys().getGroup()),
				"The context and input must have the same GqGroup.");

		final GqGroup encryptionGroup = context.encryptionGroup();
		final BigInteger p = encryptionGroup.getP();
		final BigInteger q = encryptionGroup.getQ();
		final BigInteger g = encryptionGroup.getGenerator().getValue();
		final String ee = context.electionEventId();
		final String vcs = context.verificationCardSetId();
		final ElGamalMultiRecipientPublicKey EL_pk = context.electionPublicKey();
		final ElGamalMultiRecipientPublicKey pk_CCR = context.choiceReturnCodesEncryptionPublicKey();
		final PrimesMappingTable pTable = context.primesMappingTable();
		final List<String> ciSelections = context.correctnessInformationSelections();
		final List<String> ciVotingOptions = context.correctnessInformationVotingOptions();

		final List<String> vc = input.verificationCardIds();
		final GroupVector<ZqElement, ZqGroup> k = input.verificationCardSecretKeys();
		final List<String> SVK = input.startVotingKeys();

		final int N_E = input.verificationCardIds().size();

		// Algorithm.
		final HashableList h_aux = Streams.concat(
				Stream.of(from("Context"), HashableBigInteger.from(p), HashableBigInteger.from(q), HashableBigInteger.from(g), from(ee), from(vcs)),
				Stream.of(from("ELpk")), EL_pk.toHashableForm().stream(),
				Stream.of(from("pkCCR")), pk_CCR.toHashableForm().stream(),
				Stream.of(from("EncodedVotingOptions")),
				getEncodedVotingOptionsAlgorithm.getEncodedVotingOptions(pTable, List.of()).toHashableForm().stream(),
				Stream.of(from("ActualVotingOptions")),
				getActualVotingOptionsAlgorithm.getActualVotingOptions(pTable, GroupVector.of()).stream().map(HashableString::from),
				Stream.of(from("SemanticInformation")),
				getSemanticInformationAlgorithm.getSemanticInformation(pTable).stream().map(HashableString::from),
				Stream.of(from("ciSelections")), ciSelections.stream().map(HashableString::from),
				Stream.of(from("ciVotingOptions")), ciVotingOptions.stream().map(HashableString::from)
		).collect(HashableList.toHashableList());

		final String i_aux = base64.base64Encode(hash.recursiveHash(h_aux));

		final List<String> VCks = IntStream.range(0, N_E)
				.parallel()
				.boxed()
				.map(id -> {
					final Argon2Hash argon2Hash = argon2.genArgon2id(stringToByteArray(SVK.get(id)));
					final byte[] dSVK_id = argon2Hash.getTag();
					final byte[] VCks_id_salt = argon2Hash.getSalt();

					final byte[] KSkey_id = hash.recursiveHash(
							HashableList.of(from("VerificationCardKeystore"), from(ee), from(vcs), from(vc.get(id)),
									HashableByteArray.from(dSVK_id)));

					final SymmetricCiphertext VCks_id_ciphertextSymmetric = symmetric.genCiphertextSymmetric(KSkey_id,
							integerToByteArray(k.get(id).getValue()),
							List.of(i_aux));
					final byte[] VCks_id_ciphertext = VCks_id_ciphertextSymmetric.getCiphertext();
					final byte[] VCks_id_nonce = VCks_id_ciphertextSymmetric.getNonce();

					return base64.base64Encode(Bytes.concat(VCks_id_ciphertext, VCks_id_nonce, VCks_id_salt));
				})
				.toList();

		return new GenCredDatOutput(VCks);
	}
}
