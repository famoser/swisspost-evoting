/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.infrastructure.preconfiguration;

import static ch.post.it.evoting.cryptoprimitives.domain.election.ElectionAttributesAliasConstants.ALIAS_CANDIDATES;
import static ch.post.it.evoting.cryptoprimitives.domain.election.ElectionAttributesAliasConstants.ALIAS_JOIN_DELIMITER;
import static ch.post.it.evoting.cryptoprimitives.domain.election.ElectionAttributesAliasConstants.ALIAS_LISTS;
import static ch.post.it.evoting.cryptoprimitives.domain.election.ElectionAttributesAliasConstants.ALIAS_PREFIX_WRITE_IN;
import static ch.post.it.evoting.securedatamanager.services.infrastructure.preconfiguration.TranslationsFactory.getInLanguage;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static java.util.function.Predicate.not;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.MoreCollectors;

import ch.post.it.evoting.cryptoprimitives.domain.election.Contest;
import ch.post.it.evoting.cryptoprimitives.domain.election.ElectionAttributes;
import ch.post.it.evoting.cryptoprimitives.domain.election.ElectionOption;
import ch.post.it.evoting.cryptoprimitives.domain.election.Question;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.AnswerInformationType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.BallotQuestionType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.CandidatePositionType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.CandidateType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.Configuration;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ElectionDescriptionInformationType.ElectionDescriptionInfo;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ElectionGroupBallotType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ElectionInformationType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ElectionType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.LanguageType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ListDescriptionInformationType.ListDescriptionInfo;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ListType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.StandardAnswerType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.StandardBallotType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.StandardQuestionType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.TieBreakQuestionType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.TiebreakAnswerType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.VariantBallotType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.VoteDescriptionInformationType.VoteDescriptionInfo;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.VoteInformationType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.VoteType;
import ch.post.it.evoting.securedatamanager.commons.Constants;
import ch.post.it.evoting.securedatamanager.services.infrastructure.preconfiguration.ElectionsConfigFactory.ElectionTypeAttributeIds;
import ch.post.it.evoting.securedatamanager.services.infrastructure.preconfiguration.ElectionsConfigFactory.VoteTypeAttributeIds;

/**
 * Creates the ballot contest of the elections_config.json.
 */
public class BallotContestFactory {

	private static final Logger LOGGER = LoggerFactory.getLogger(BallotContestFactory.class);
	private static final String LEER = "Leer";
	private static final String EMPTY = "EMPTY";
	private static final String ALIAS_PREFIX_DUMMY_LIST = "list_for_";
	private static final String SEMANTICS_DELIMITER = "-";
	private static final String ELECTION_DUMMY_VALUE = "-";
	private static final String SEMANTICS_SUFFIX_WRITE_IN = "WRITE_IN_POSITION";
	private static final String SEMANTICS_SUFFIX_EMPTY_CANDIDATE = "EMPTY_CANDIDATE_POSITION";
	private static final Random random = RandomFactory.createRandom();

	private BallotContestFactory() {
		// static usage only.
	}

	/**
	 * Creates the ballot contest for all elections and votes.
	 *
	 * @param configuration the {@link Configuration} representing the configuration-anonymized. Must be non-null.
	 * @return the mapping of the domain of influence of the election or vote to the ballot contest with its position.
	 */
	public static Map<String, List<ContestWithPosition>> createContest(final Configuration configuration,
			final Map<String, VoteTypeAttributeIds> voteTypeAttributeIdsMap,
			final Map<String, ElectionTypeAttributeIds> electionTypeAttributeIdsMap) {
		checkNotNull(configuration);
		checkNotNull(voteTypeAttributeIdsMap);
		checkNotNull(electionTypeAttributeIdsMap);

		// Create ballot contest for all elections
		final Map<String, List<ContestWithPosition>> electionContest = configuration.getContest().getElectionGroupBallot().stream().parallel()
				.collect(Collectors.groupingByConcurrent(
						ElectionGroupBallotType::getDomainOfInfluence,
						Collectors.mapping(
								electionGroupBallotType -> {
									final ElectionInformationType electionInformationType = electionGroupBallotType.getElectionInformation().stream()
											.collect(MoreCollectors.onlyElement());
									final ElectionTypeAttributeIds electionTypeAttributeIds = checkNotNull(electionTypeAttributeIdsMap
											.get(electionInformationType.getElection().getElectionIdentification()));
									return new ContestWithPosition(createElectionContest(electionInformationType, electionTypeAttributeIds),
											electionGroupBallotType.getElectionGroupPosition());
								},
								Collectors.toList())
				));

		// Create ballot contest for all votes
		final Map<String, List<ContestWithPosition>> voteContest = configuration.getContest().getVoteInformation().stream()
				.map(VoteInformationType::getVote)
				.collect(Collectors.groupingBy(
						VoteType::getDomainOfInfluence,
						Collectors.mapping(
								voteType -> new ContestWithPosition(createVoteContest(voteType, voteTypeAttributeIdsMap),
										voteType.getVotePosition().intValue()),
								Collectors.toList())
				));

		final Map<String, List<ContestWithPosition>> domainOfInfluenceToContest = Stream.of(electionContest, voteContest)
				.flatMap(map -> map.entrySet().stream())
				.collect(Collectors.toMap(
						Map.Entry::getKey,
						entry -> new ArrayList<>(entry.getValue()),
						(contestsLeft, contestsRight) -> {
							contestsLeft.addAll(contestsRight);
							return contestsLeft;
						})
				);

		LOGGER.info("Successfully created the ballot contest.");

		return domainOfInfluenceToContest;
	}

	private static Contest createElectionContest(final ElectionInformationType electionInformationType,
			final ElectionTypeAttributeIds electionTypeAttributeIds) {
		final ListType emptyListType = electionInformationType.getList().stream()
				.filter(ListType::isListEmpty)
				.collect(MoreCollectors.onlyElement());
		checkArgument(electionInformationType.getElection().getNumberOfMandates() == emptyListType.getCandidatePosition().size());

		final boolean electionWithLists = electionInformationType.getList().stream().anyMatch(not(ListType::isListEmpty));

		final ElectionType electionType = electionInformationType.getElection();
		final String contestId = random.genRandomBase16String(Constants.BASE16_ID_LENGTH);
		final String defaultTitle = getInLanguage(LanguageType.DE, electionType.getElectionDescription().getElectionDescriptionInfo(),
				ElectionDescriptionInfo::getLanguage, ElectionDescriptionInfo::getElectionDescription);

		final List<ElectionOption> options = createElectionContestOptions(electionInformationType, electionTypeAttributeIds, emptyListType,
				electionWithLists);
		final List<ElectionAttributes> attributes = createElectionContestAttributes(electionInformationType, electionTypeAttributeIds, emptyListType,
				electionWithLists);
		final List<Question> questions = createElectionContestQuestions(electionInformationType, electionTypeAttributeIds, emptyListType,
				electionWithLists);

		return new Contest(contestId, defaultTitle, defaultTitle, electionType.getElectionIdentification(), Contest.ELECTIONS_TEMPLATE, true, options,
				attributes, questions);
	}

	private static List<ElectionOption> createElectionContestOptions(final ElectionInformationType electionInformationType,
			final ElectionTypeAttributeIds electionTypeAttributeIds, final ListType emptyListType, final boolean electionWithLists) {
		final ElectionType electionType = electionInformationType.getElection();
		final String electionDescription = getInLanguage(LanguageType.DE, electionType.getElectionDescription().getElectionDescriptionInfo(),
				ElectionDescriptionInfo::getLanguage, ElectionDescriptionInfo::getElectionDescription);
		final String emptySemantics = String.join(SEMANTICS_DELIMITER, electionDescription, SEMANTICS_SUFFIX_EMPTY_CANDIDATE);
		final String writeInSemantics = String.join(SEMANTICS_DELIMITER, electionDescription, SEMANTICS_SUFFIX_WRITE_IN);

		// create an option for all blank candidate positions
		final Map<String, String> candidateAttributeIdsMap = electionTypeAttributeIds.candidateAttributeIdsMap();
		final List<ElectionOption> options = new ArrayList<>(emptyListType.getCandidatePosition().stream()
				.map(CandidatePositionType::getCandidateListIdentification)
				.flatMap(candidateListIdentification -> {
					final List<ElectionOption> candidatePositionOptions = new ArrayList<>();
					candidatePositionOptions.add(createGenericOption(candidateAttributeIdsMap.get(candidateListIdentification), emptySemantics));

					if (electionType.isWriteInsAllowed()) {
						// create an additional option for all candidate positions if write-ins allowed
						candidatePositionOptions.add(
								createGenericOption(candidateAttributeIdsMap.get(ALIAS_PREFIX_WRITE_IN + candidateListIdentification),
										writeInSemantics));
					}
					return candidatePositionOptions.stream();
				}).toList());

		// create a candidate option as many times as there are accumulations
		final int accumulations = electionType.getCandidateAccumulation().intValue();
		final List<CandidateType> candidates = electionInformationType.getCandidate();
		if (candidates != null) {
			options.addAll(createGenericElectionContestOptionCandidate(candidates, accumulations, candidateAttributeIdsMap));
		}

		// create an option for all lists if there is at least one non-empty list
		if (electionWithLists) {
			options.addAll(electionInformationType.getList().stream()
					.map(listType -> {
						final String semantics = getInLanguage(LanguageType.DE, listType.getListDescription().getListDescriptionInfo(),
								ListDescriptionInfo::getLanguage, ListDescriptionInfo::getListDescription);
						return createGenericOption(electionTypeAttributeIds.listAttributeIdsMap().get(listType.getListIdentification()), semantics);
					})
					.toList());
		}

		return options;
	}

	private static List<ElectionOption> createGenericElectionContestOptionCandidate(final List<CandidateType> candidates, final int accumulations,
			final Map<String, String> candidateAttributeIdsMap) {
		return IntStream.range(0, candidates.size() * accumulations)
				.mapToObj(i -> candidates.get(i / accumulations))
				.map(candidate -> {
					final String semantics = String.join(SEMANTICS_DELIMITER, candidate.getFamilyName(), candidate.getFirstName(),
							candidate.getCallName(), candidate.getDateOfBirth().toXMLFormat());
					return createGenericOption(candidateAttributeIdsMap.get(candidate.getCandidateIdentification()), semantics);
				})
				.toList();
	}

	private static List<ElectionAttributes> createElectionContestAttributes(final ElectionInformationType electionInformationType,
			final ElectionTypeAttributeIds electionTypeAttributeIds, final ListType emptyListType, final boolean electionWithLists) {
		final List<ElectionAttributes> attributes = new ArrayList<>();

		// create an attribute for candidates
		final String candidatesAttributeId = electionTypeAttributeIds.candidatesAttributeId();
		attributes.add(new ElectionAttributes(candidatesAttributeId, ALIAS_CANDIDATES, List.of(), true));

		// create an attribute for all candidate identifications
		attributes.addAll(electionInformationType.getCandidate().stream()
				.map(candidateType -> createElectionContestAttributeCandidate(electionInformationType, candidateType, electionTypeAttributeIds,
						electionWithLists))
				.toList());

		// create an attribute for one blank candidate position
		final Map<String, String> candidateAttributeIdsMap = electionTypeAttributeIds.candidateAttributeIdsMap();
		final String blankCandidateListIdentification = emptyListType.getCandidatePosition().get(0).getCandidateListIdentification();
		attributes.add(new ElectionAttributes(candidateAttributeIdsMap.get(blankCandidateListIdentification), blankCandidateListIdentification,
				List.of(candidatesAttributeId), false));

		// create an attribute for one write-in candidate position if write-ins allowed
		if (electionInformationType.getElection().isWriteInsAllowed()) {
			final String writeInCandidateListIdentification = ALIAS_PREFIX_WRITE_IN + blankCandidateListIdentification;
			attributes.add(new ElectionAttributes(candidateAttributeIdsMap.get(writeInCandidateListIdentification),
					writeInCandidateListIdentification, List.of(candidatesAttributeId), false));
		}

		attributes.addAll(createElectionContestAttributesList(electionInformationType, electionTypeAttributeIds, electionWithLists));

		return attributes;
	}

	private static List<ElectionAttributes> createElectionContestAttributesList(final ElectionInformationType electionInformationType,
			final ElectionTypeAttributeIds electionTypeAttributeIds, final boolean electionWithLists) {
		final String electionIdentification = electionInformationType.getElection().getElectionIdentification();
		final Map<String, String> listAttributeIdsMap = electionTypeAttributeIds.listAttributeIdsMap();
		final String listsAttributeId = electionTypeAttributeIds.listsAttributeId();
		final List<ElectionAttributes> attributes = new ArrayList<>();

		// if at least one list is not empty
		if (electionWithLists) {
			//create an attribute for lists
			attributes.add(new ElectionAttributes(listsAttributeId, ALIAS_LISTS, List.of(), true));

			// create an attribute for all list identifications
			attributes.addAll(electionInformationType.getList().stream()
					.map(listType -> {
						final String listIdentification = listType.getListIdentification();
						final String alias = String.join(ALIAS_JOIN_DELIMITER, electionIdentification, listIdentification);
						return new ElectionAttributes(listAttributeIdsMap.get(listType.getListIdentification()), alias, List.of(listsAttributeId),
								false);
					})
					.toList());
		} else {
			// create an attribute for all candidate's dummy lists
			attributes.addAll(electionInformationType.getCandidate().stream()
					.map(candidateType -> new ElectionAttributes(listAttributeIdsMap.get(candidateType.getCandidateIdentification()),
							ALIAS_PREFIX_DUMMY_LIST + candidateType.getCandidateIdentification(), List.of(),
							false))
					.toList());
		}

		return attributes;
	}

	private static ElectionAttributes createElectionContestAttributeCandidate(final ElectionInformationType electionInformationType,
			final CandidateType candidateType, final ElectionTypeAttributeIds electionTypeAttributeIds, final boolean electionWithLists) {
		final String electionIdentification = electionInformationType.getElection().getElectionIdentification();
		final String candidateIdentification = candidateType.getCandidateIdentification();
		final String candidateAttributeId = electionTypeAttributeIds.candidateAttributeIdsMap().get(candidateIdentification);
		final Map<String, String> listAttributeIdsMap = electionTypeAttributeIds.listAttributeIdsMap();

		final String alias = String.join(ALIAS_JOIN_DELIMITER, electionIdentification, candidateIdentification);

		final List<String> related = new ArrayList<>(List.of(electionTypeAttributeIds.candidatesAttributeId()));
		// search all lists where the candidate identification appears at least once
		if (electionWithLists) {
			related.addAll(electionInformationType.getList().stream()
					.filter(not(ListType::isListEmpty))
					.filter(listType -> listType.getCandidatePosition().stream()
							.anyMatch(candidatePositionType -> candidatePositionType.getCandidateIdentification().equals(candidateIdentification)))
					.map(ListType::getListIdentification)
					.map(listAttributeIdsMap::get)
					.toList());
		} else {
			related.add(listAttributeIdsMap.get(candidateIdentification));
		}

		return new ElectionAttributes(candidateAttributeId, alias, related, false);
	}

	private static List<Question> createElectionContestQuestions(final ElectionInformationType electionInformationType,
			final ElectionTypeAttributeIds electionTypeAttributeIds, final ListType emptyListType, final boolean electionWithLists) {
		final List<Question> questions = new ArrayList<>();

		final Map<String, String> candidateAttributeIdsMap = electionTypeAttributeIds.candidateAttributeIdsMap();
		final String candidateListIdentification = emptyListType.getCandidatePosition().get(0).getCandidateListIdentification();

		final ElectionType electionType = electionInformationType.getElection();
		final boolean writeInsAllowed = electionType.isWriteInsAllowed();
		final String writeInAttributeId = writeInsAllowed ?
				candidateAttributeIdsMap.get(ALIAS_PREFIX_WRITE_IN + candidateListIdentification) :
				Question.WRITE_IN_ATTRIBUTE_NO_WRITE_IN;

		// create a candidate question
		questions.add(createElectionQuestion(random.genRandomBase16String(Constants.BASE16_ID_LENGTH),
				electionType.getNumberOfMandates(), electionType.getMinimalCandidateSelectionInList(),
				electionType.getCandidateAccumulation().intValue(), candidateAttributeIdsMap.get(candidateListIdentification), writeInAttributeId,
				electionTypeAttributeIds.candidatesAttributeId()));

		// create a list question if at least one list is not empty
		if (electionWithLists) {
			checkState(!writeInsAllowed, "There cannot be write-ins with multiple lists.");
			questions.add(createElectionQuestion(random.genRandomBase16String(Constants.BASE16_ID_LENGTH), 1, 0, 1,
					electionTypeAttributeIds.listAttributeIdsMap().get(emptyListType.getListIdentification()),
					Question.WRITE_IN_ATTRIBUTE_NO_WRITE_IN, electionTypeAttributeIds.listsAttributeId()));
		}

		return questions;
	}

	private static Question createElectionQuestion(final String id, final int max, final int min, final int accumulation, final String blankAttribute,
			final String writeInAttribute, final String attribute) {
		return createGenericQuestion(id, max, min, accumulation, blankAttribute, writeInAttribute, attribute, ELECTION_DUMMY_VALUE, false,
				ELECTION_DUMMY_VALUE);
	}

	private static Contest createVoteContest(final VoteType voteType, final Map<String, VoteTypeAttributeIds> voteTypeAttributeIdsMap) {
		final String contestId = random.genRandomBase16String(Constants.BASE16_ID_LENGTH);
		final String defaultTitle = getInLanguage(LanguageType.DE, voteType.getVoteDescription().getVoteDescriptionInfo(),
				VoteDescriptionInfo::getLanguage, VoteDescriptionInfo::getVoteDescription);

		final List<ElectionOption> options = createVoteContestOptions(voteType, voteTypeAttributeIdsMap);
		final List<ElectionAttributes> attributes = createVoteContestAttributes(voteType, voteTypeAttributeIdsMap);
		final List<Question> questions = createVoteContestQuestions(voteType, voteTypeAttributeIdsMap);

		return new Contest(contestId, defaultTitle, defaultTitle, voteType.getVoteIdentification(), Contest.VOTES_TEMPLATE, false, options,
				attributes, questions);
	}

	private static List<ElectionOption> createVoteContestOptions(final VoteType voteType,
			final Map<String, VoteTypeAttributeIds> voteTypeAttributeIdsMap) {
		return voteType.getBallot().stream()
				.flatMap(ballotType -> {
					final List<ElectionOption> options = new ArrayList<>();

					// create an option for all standard ballot answers
					final StandardBallotType standardBallotType = ballotType.getStandardBallot();
					if (standardBallotType != null) {
						options.addAll(createGenericVoteContestOptions(Stream.of(standardBallotType),
								StandardBallotType::getQuestionIdentification, StandardBallotType::getBallotQuestion,
								StandardBallotType::getAnswer, StandardAnswerType::getAnswerIdentification, StandardAnswerType::getStandardAnswerType,
								voteTypeAttributeIdsMap));
					}

					// create an option for all variant ballot standard answers
					final VariantBallotType variantBallot = ballotType.getVariantBallot();
					if (variantBallot != null) {
						options.addAll(createGenericVoteContestOptions(variantBallot.getStandardQuestion().stream(),
								StandardQuestionType::getQuestionIdentification, StandardQuestionType::getBallotQuestion,
								StandardQuestionType::getAnswer, StandardAnswerType::getAnswerIdentification,
								StandardAnswerType::getStandardAnswerType, voteTypeAttributeIdsMap));

						// create an option for all variant ballot tie break answers
						final Function<TiebreakAnswerType, String> getTieBreakAnswerSemantics = tiebreakAnswerType -> getInLanguage(LanguageType.DE,
								tiebreakAnswerType.getAnswerInfo(), AnswerInformationType::getLanguage, AnswerInformationType::getAnswer);
						final List<TieBreakQuestionType> tieBreakQuestion = variantBallot.getTieBreakQuestion();
						if (tieBreakQuestion != null) {
							options.addAll(createGenericVoteContestOptions(variantBallot.getTieBreakQuestion().stream(),
									TieBreakQuestionType::getQuestionIdentification, TieBreakQuestionType::getBallotQuestion,
									TieBreakQuestionType::getAnswer, TiebreakAnswerType::getAnswerIdentification, getTieBreakAnswerSemantics,
									voteTypeAttributeIdsMap));
						}
					}

					return options.stream();
				})
				.toList();
	}

	private static <S, T> List<ElectionOption> createGenericVoteContestOptions(final Stream<S> questions,
			final Function<S, String> getQuestionIdentification, final Function<S, BallotQuestionType> getBallotQuestion,
			final Function<S, List<T>> getAnswer, final Function<T, String> getAnswerIdentification, final Function<T, String> getAnswerSemantics,
			final Map<String, VoteTypeAttributeIds> voteTypeAttributeIdsMap) {
		return questions
				.flatMap(question -> {
					final VoteTypeAttributeIds attributeIds = voteTypeAttributeIdsMap.get(getQuestionIdentification.apply(question));
					final BallotQuestionType ballotQuestionType = getBallotQuestion.apply(question);
					final String questionSemantics = getInLanguage(LanguageType.DE, ballotQuestionType.getBallotQuestionInfo(),
							BallotQuestionType.BallotQuestionInfo::getLanguage, BallotQuestionType.BallotQuestionInfo::getBallotQuestion);
					return getAnswer.apply(question).stream()
							.map(answer -> {
								final String attributeAnswerId = attributeIds.attributeAnswerId().get(getAnswerIdentification.apply(answer));
								final String semantics = String.join(SEMANTICS_DELIMITER, questionSemantics, getAnswerSemantics.apply(answer));
								return createGenericOption(attributeAnswerId, semantics);
							});
				})
				.toList();
	}

	private static List<ElectionAttributes> createVoteContestAttributes(final VoteType voteType,
			final Map<String, VoteTypeAttributeIds> voteTypeAttributeIdsMap) {
		final String voteIdentification = voteType.getVoteIdentification();
		return voteType.getBallot().stream()
				.flatMap(ballotType -> {
					final List<ElectionAttributes> attributes = new ArrayList<>();

					// create an attribute for all standard ballot questions and answers
					final StandardBallotType standardBallotType = ballotType.getStandardBallot();
					if (standardBallotType != null) {
						attributes.addAll(createGenericVoteContestAttributes(Stream.of(standardBallotType),
								StandardBallotType::getQuestionIdentification, StandardBallotType::getAnswer,
								StandardAnswerType::getAnswerIdentification, voteIdentification, voteTypeAttributeIdsMap));
					}

					// create an attribute for all variant ballot standard questions and answers
					final VariantBallotType variantBallot = ballotType.getVariantBallot();
					if (variantBallot != null) {
						attributes.addAll(createGenericVoteContestAttributes(variantBallot.getStandardQuestion().stream(),
								StandardQuestionType::getQuestionIdentification, StandardQuestionType::getAnswer,
								StandardAnswerType::getAnswerIdentification, voteIdentification, voteTypeAttributeIdsMap));

						// create an attribute for all variant ballot tie break questions and answers
						final List<TieBreakQuestionType> tieBreakQuestion = variantBallot.getTieBreakQuestion();
						if (tieBreakQuestion != null) {
							attributes.addAll(createGenericVoteContestAttributes(variantBallot.getTieBreakQuestion().stream(),
									TieBreakQuestionType::getQuestionIdentification, TieBreakQuestionType::getAnswer,
									TiebreakAnswerType::getAnswerIdentification, voteIdentification, voteTypeAttributeIdsMap));
						}
					}

					return attributes.stream();
				}).toList();
	}

	private static <S, T> List<ElectionAttributes> createGenericVoteContestAttributes(final Stream<S> questions,
			final Function<S, String> getQuestionIdentification, final Function<S, List<T>> getAnswer,
			final Function<T, String> getAnswerIdentification, final String voteIdentification,
			final Map<String, VoteTypeAttributeIds> voteTypeAttributeIdsMap) {
		return questions
				.flatMap(question -> {
					final String questionIdentification = getQuestionIdentification.apply(question);
					final VoteTypeAttributeIds attributeIds = voteTypeAttributeIdsMap.get(questionIdentification);
					final String attributeQuestionId = attributeIds.attributeQuestionId();
					final List<ElectionAttributes> attributes = new ArrayList<>();

					// create an attribute for the question
					final ElectionAttributes attributeQuestion = new ElectionAttributes(attributeQuestionId, questionIdentification, List.of(), true);
					attributes.add(attributeQuestion);

					// create an attribute for all answers
					attributes.addAll(getAnswer.apply(question).stream()
							.map(answer -> {
								final String answerIdentification = getAnswerIdentification.apply(answer);
								final String alias = String.join(ALIAS_JOIN_DELIMITER, voteIdentification, answerIdentification);
								final String attributeAnswerId = attributeIds.attributeAnswerId().get(answerIdentification);
								return new ElectionAttributes(attributeAnswerId, alias, List.of(attributeQuestionId), false);
							})
							.toList());

					return attributes.stream();
				})
				.toList();
	}

	private static List<Question> createVoteContestQuestions(final VoteType voteType,
			final Map<String, VoteTypeAttributeIds> voteTypeAttributeIdsMap) {
		final Predicate<StandardAnswerType> isBlankStandardAnswer = standardAnswerType -> standardAnswerType.getStandardAnswerType().equals(EMPTY);
		final Predicate<TiebreakAnswerType> isBlankTieBreakAnswer = tiebreakAnswerType -> tiebreakAnswerType.getAnswerInfo().stream()
				.filter(answerInformationType -> answerInformationType.getLanguage().equals(LanguageType.DE))
				.map(AnswerInformationType::getAnswer)
				.allMatch(answer -> answer.equals(LEER));

		return voteType.getBallot().stream()
				.flatMap(ballotType -> {
					final String ballotIdentification = ballotType.getBallotIdentification();
					final List<Question> questions = new ArrayList<>();

					// create a question for all standard ballot questions
					final StandardBallotType standardBallot = ballotType.getStandardBallot();
					if (standardBallot != null) {
						questions.addAll(createGenericVoteContestQuestion(Stream.of(standardBallot),
								StandardBallotType::getQuestionIdentification, StandardBallotType::getAnswer, isBlankStandardAnswer,
								StandardAnswerType::getAnswerIdentification, voteTypeAttributeIdsMap, StandardBallotType::getQuestionNumber, false,
								ballotIdentification));
					}

					// create a question for all variant ballot standard questions
					final VariantBallotType variantBallot = ballotType.getVariantBallot();
					if (variantBallot != null) {
						questions.addAll(createGenericVoteContestQuestion(variantBallot.getStandardQuestion().stream(),
								StandardQuestionType::getQuestionIdentification, StandardQuestionType::getAnswer, isBlankStandardAnswer,
								StandardAnswerType::getAnswerIdentification, voteTypeAttributeIdsMap, StandardQuestionType::getQuestionNumber, true,
								ballotIdentification));

						// create a question for all variant ballot tie break questions
						final List<TieBreakQuestionType> tieBreakQuestion = variantBallot.getTieBreakQuestion();
						if (tieBreakQuestion != null) {
							questions.addAll(createGenericVoteContestQuestion(variantBallot.getTieBreakQuestion().stream(),
									TieBreakQuestionType::getQuestionIdentification, TieBreakQuestionType::getAnswer, isBlankTieBreakAnswer,
									TiebreakAnswerType::getAnswerIdentification, voteTypeAttributeIdsMap, TieBreakQuestionType::getQuestionNumber,
									true, ballotIdentification));
						}
					}

					return questions.stream();
				}).toList();
	}

	private static <S, T> List<Question> createGenericVoteContestQuestion(final Stream<S> questions,
			final Function<S, String> getQuestionIdentification, final Function<S, List<T>> getAnswer, final Predicate<T> isBlankAnswer,
			final Function<T, String> getAnswerIdentification, final Map<String, VoteTypeAttributeIds> voteTypeAttributeIdsMap,
			final Function<S, String> getQuestionNumber, final boolean variantBallot, final String ballotIdentification) {
		return questions
				.map(question -> {
					final String questionIdentification = getQuestionIdentification.apply(question);
					final VoteTypeAttributeIds attributeIds = voteTypeAttributeIdsMap.get(questionIdentification);
					final String attributeQuestionId = attributeIds.attributeQuestionId();

					final T blankAnswer = getAnswer.apply(question).stream()
							.filter(isBlankAnswer)
							.collect(MoreCollectors.onlyElement());
					final String attributeAnswerIdBlank = attributeIds.attributeAnswerId().get(getAnswerIdentification.apply(blankAnswer));
					final String questionNumber = getQuestionNumber.apply(question);
					return createGenericQuestion(random.genRandomBase16String(Constants.BASE16_ID_LENGTH), 1, 0, 1, attributeAnswerIdBlank,
							Question.WRITE_IN_ATTRIBUTE_NO_WRITE_IN, attributeQuestionId, questionNumber, variantBallot, ballotIdentification);
				})
				.toList();
	}

	private static ElectionOption createGenericOption(final String attribute, final String semantics) {
		final String optionId = random.genRandomBase16String(Constants.BASE16_ID_LENGTH);
		return new ElectionOption(optionId, attribute, semantics, "1");
	}

	private static Question createGenericQuestion(final String id, final int max, final int min, final int accumulation, final String blankAttribute,
			final String writeInAttribute, final String attribute, final String questionNumber, final boolean variantBallot,
			final String ballotIdentification) {
		final boolean writeIn = !writeInAttribute.equals(Question.WRITE_IN_ATTRIBUTE_NO_WRITE_IN);
		return new Question(id, max, min, accumulation, writeIn, blankAttribute, writeInAttribute, attribute, List.of(), questionNumber,
				variantBallot, ballotIdentification);
	}

	record ContestWithPosition(Contest contest, int position) {
	}
}
