/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.post.it.evoting.securedatamanager.services.application.service.AdminBoardService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * The admin board end-point.
 */
@RestController
@RequestMapping("/sdm-backend/adminboard-management")
@Api(value = "Administration Board REST API")
public class AdminBoardController {

	private final AdminBoardService adminBoardService;

	public AdminBoardController(final AdminBoardService adminBoardService) {
		this.adminBoardService = adminBoardService;
	}

	/**
	 * Lists all admin boards.
	 *
	 * @return The list of admin boards as String.
	 */
	@GetMapping(value = "adminboards", produces = "application/json")
	@ApiOperation(value = "List administration boards", response = String.class, notes = "Service to retrieve the list of administration boards.")
	public String getAdminBoards() {
		return adminBoardService.getAdminBoards();
	}

}
