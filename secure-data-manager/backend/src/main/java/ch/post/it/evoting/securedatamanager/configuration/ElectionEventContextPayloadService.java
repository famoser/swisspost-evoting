/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.io.UncheckedIOException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import javax.json.JsonObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.securedatamanager.commons.JsonUtils;
import ch.post.it.evoting.securedatamanager.commons.RemoteService;
import ch.post.it.evoting.securedatamanager.services.infrastructure.ElectionEventContextRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.JsonConstants;
import ch.post.it.evoting.securedatamanager.services.infrastructure.clients.VoteVerificationClient;

/**
 * Allows saving, retrieving and finding existing election event context payloads.
 */
@Service
public class ElectionEventContextPayloadService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ElectionEventContextPayloadService.class);
	private static final String VOTING_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE = "The voting-portal connection is not enabled.";

	private final ObjectMapper objectMapper;
	private final boolean isVotingPortalEnabled;
	private final VoteVerificationClient voteVerificationClient;
	private final ElectionEventContextRepository electionEventContextRepository;
	private final ElectionEventContextPayloadFileRepository electionEventContextPayloadFileRepository;

	public ElectionEventContextPayloadService(
			final ObjectMapper objectMapper,
			@Value("${voting.portal.enabled}")
			final boolean isVotingPortalEnabled,
			final VoteVerificationClient voteVerificationClient,
			final ElectionEventContextRepository electionEventContextRepository,
			final ElectionEventContextPayloadFileRepository electionEventContextPayloadFileRepository) {
		this.objectMapper = objectMapper;
		this.isVotingPortalEnabled = isVotingPortalEnabled;
		this.voteVerificationClient = voteVerificationClient;
		this.electionEventContextRepository = electionEventContextRepository;
		this.electionEventContextPayloadFileRepository = electionEventContextPayloadFileRepository;
	}

	/**
	 * Saves an election event context payload in the corresponding election event folder.
	 *
	 * @param electionEventContextPayload the election event context payload to save.
	 * @throws NullPointerException if {@code electionEventContext} is null.
	 */
	public void save(final ElectionEventContextPayload electionEventContextPayload) {
		checkNotNull(electionEventContextPayload);

		final String electionEventId = electionEventContextPayload.getElectionEventContext().electionEventId();

		electionEventContextPayloadFileRepository.save(electionEventContextPayload);
		LOGGER.info("Saved election event context payload. [electionEventId: {}]", electionEventId);

		final ObjectNode electionEventContextNode = objectMapper.createObjectNode();
		electionEventContextNode.put(JsonConstants.ID, electionEventId);
		electionEventContextNode.put(JsonConstants.UPLOADED, "false");
		electionEventContextNode.put(JsonConstants.STATUS, "READY");
		try {
			electionEventContextRepository.save(objectMapper.writeValueAsString(electionEventContextNode));
		} catch (JsonProcessingException e) {
			throw new UncheckedIOException(e);
		}
		LOGGER.info("Saved election event context entity. [electionEventId: {}]", electionEventId);
	}

	/**
	 * Checks if the election event context payload is present for the given election event id.
	 *
	 * @param electionEventId the election event id to check.
	 * @return {@code true} if the election event context payload is present, {@code false} otherwise.
	 * @throws FailedValidationException if {@code electionEventId} is invalid.
	 */
	public boolean exist(final String electionEventId) {
		validateUUID(electionEventId);

		return electionEventContextPayloadFileRepository.existsById(electionEventId);
	}

	/**
	 * Loads the election event context payload for the given {@code electionEventId}. The result of this method is cached.
	 *
	 * @param electionEventId the election event id. Must be non-null and a valid UUID.
	 * @return the election event context payload for this {@code electionEventId}.
	 * @throws FailedValidationException if {@code electionEventId} is invalid.
	 * @throws IllegalStateException     if the requested election event context is not present.
	 */
	@Cacheable("electionEventContextPayloads")
	public ElectionEventContextPayload load(final String electionEventId) {
		validateUUID(electionEventId);

		final ElectionEventContextPayload payload = electionEventContextPayloadFileRepository.findById(electionEventId)
				.orElseThrow(() -> new IllegalStateException(
						String.format("Requested election event context payload is not present. [electionEventId: %s]", electionEventId)));

		LOGGER.info("Loaded election event context payload. [electionEventId: {}]", electionEventId);

		return payload;
	}

	/**
	 * Uploads the election event context payload to the vote verification.
	 *
	 * @param electionEventId the election event id of the election to upload. Must be a valid uuid and non-null.
	 * @throws NullPointerException      if {@code electionEventId} is null.
	 * @throws FailedValidationException if {@code electionEventId} is not a valid uuid.
	 */
	public void uploadElectionEventContextData(final String electionEventId) {
		validateUUID(electionEventId);
		checkState(isVotingPortalEnabled, VOTING_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE);

		LOGGER.debug("Uploading election event context payload... [electionEventId: {}]", electionEventId);

		final ElectionEventContextPayload electionEventContextPayload = load(electionEventId);

		checkState(electionEventContextPayload.getElectionEventContext().finishTime().isAfter(LocalDateTime.now()),
				"The election event period should not be finished yet. [electionEventId: %s]", electionEventId);

		RemoteService.call(voteVerificationClient.uploadElectionEventContext(electionEventId, electionEventContextPayload))
				.networkErrorMessage("Failed to communicate with vote verification.")
				.unsuccessfulResponseErrorMessage("Request for uploading election event context failed. [electionEventId: %s]", electionEventId)
				.execute();

		// Update status
		final ObjectNode electionEventContextNode = objectMapper.createObjectNode();
		electionEventContextNode.put(JsonConstants.ID, electionEventId);
		electionEventContextNode.put(JsonConstants.UPLOADED, "true");
		electionEventContextNode.put(JsonConstants.STATUS, "UPDATED");
		try {
			electionEventContextRepository.update(objectMapper.writeValueAsString(electionEventContextNode));
		} catch (JsonProcessingException e) {
			throw new UncheckedIOException(e);
		}

		LOGGER.info("Successfully uploaded election event context payload. [electionEventId: {}]", electionEventId);
	}

	public boolean isElectionEventContextUploaded(final String electionEventId) {
		final JsonObject electionEventContext = getElectionEventContextEntity(electionEventId);

		return !electionEventContext.isEmpty() && "true".equals(electionEventContext.getString(JsonConstants.UPLOADED));
	}

	private JsonObject getElectionEventContextEntity(final String electionEventId) {
		final Map<String, Object> params = new HashMap<>();
		params.put(JsonConstants.ID, electionEventId);

		final String document = electionEventContextRepository.find(params);
		return JsonUtils.getJsonObject(document);
	}

}
