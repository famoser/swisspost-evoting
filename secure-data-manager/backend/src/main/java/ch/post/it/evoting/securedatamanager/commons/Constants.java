/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.commons;

import java.util.List;

public final class Constants {

	// ////////////////////////////////////
	//
	// file extensions
	//
	// ////////////////////////////////////

	public static final String JSON = ".json";

	// ////////////////////////////////////
	//
	// filenames
	//
	// ////////////////////////////////////

	public static final String CONFIG_FILE_NAME_BALLOT_JSON = "ballot.json";

	public static final String CONFIG_FILE_NAME_ELECTION_OPTION_REPRESENTATIONS = "electionOptionRepresentations.csv";

	public static final String CONFIG_FILE_NAME_ENCRYPTION_PARAMETERS_PAYLOAD_JSON = "encryptionParametersPayload.json";

	public static final String CONFIG_FILE_NAME_PREFIX_SETUP_COMPONENT_VERIFICATION_DATA_PAYLOAD = "setupComponentVerificationDataPayload.";

	public static final String CONFIG_FILE_NAME_PREFIX_SETUP_COMPONENT_CM_TABLE_PAYLOAD = "setupComponentCMTablePayload.";

	public static final String CONFIG_FILE_NAME_SETUP_COMPONENT_TALLY_DATA_PAYLOAD = "setupComponentTallyDataPayload.json";
	public static final String CONFIG_FILE_NAME_VOTER_INITIAL_CODES_PAYLOAD = "voterInitialCodesPayload.json";
	public static final String CONFIG_FILE_NAME_VOTER_RETURN_CODES_PAYLOAD = "voterReturnCodesPayload.json";

	public static final String CONFIG_FILE_NAME_SETUP_COMPONENT_VERIFICATION_CARD_KEYSTORES_PAYLOAD = "setupComponentVerificationCardKeystoresPayload.json";

	public static final String CONFIG_FILE_NAME_VERIFICATION_CARD_SECRET_KEY_PAYLOAD = "verificationCardSecretKeyPayload.json";

	public static final String CONFIG_FILE_NAME_ELECTION_EVENT_CONTEXT_PAYLOAD = "electionEventContextPayload.json";

	public static final String CONFIG_FILE_NAME_SETUP_COMPONENT_PUBLIC_KEYS_PAYLOAD = "setupComponentPublicKeysPayload.json";

	public static final String CONFIG_SETUP_COMPONENT_ELECTORAL_BOARD_HASHES_PAYLOAD = "setupComponentElectoralBoardHashesPayload.json";

	public static final String CONFIG_SETUP_COMPONENT_ADMIN_BOARD_HASHES_PAYLOAD = "setupComponentAdminBoardHashesPayload.json";

	public static final String CONFIG_FILE_NAME_CONFIGURATION_ANONYMIZED = "configuration-anonymized.xml";

	public static final String SDM_CONFIG_FILE_NAME_ELECTIONS_CONFIG = "elections_config.json";

	public static final String DBDUMP_FILE_NAME = "db_dump.json";

	public static final String SETUP_SECRET_KEY_FILE_NAME = "setupSecretKey.json";

	public static final String SETUP_KEY_PAIR_FILE_NAME = "setupKeyPair.json";

	public static final String SETUP_COMPONENT_EVOTING_PRINT_XML = "evoting-print_%s.xml";

	public static final String TALLY_COMPONENT_DECRYPT_XML = "evoting-decrypt_%s.xml";

	public static final String TALLY_COMPONENT_ECH_0110_XML = "eCH-0110_%s.xml";

	public static final String TALLY_COMPONENT_ECH_0222_XML = "eCH-0222_%s.xml";

	// ////////////////////////////////////
	//
	// directories
	//
	// ////////////////////////////////////

	public static final String CONFIG_DIR_NAME_BALLOTBOXES = "ballotBoxes";

	public static final String CONFIG_DIR_NAME_BALLOTS = "ballots";

	public static final String CONFIG_DIR_NAME_CUSTOMER = "CUSTOMER";

	public static final String CONFIG_DIR_NAME_ELECTIONINFORMATION = "electionInformation";

	public static final String CONFIG_DIR_NAME_OFFLINE = "OFFLINE";

	public static final String CONFIG_DIR_NAME_VERIFICATION_CARD_SECRET_KEYS = "verificationCardSecretKeys";

	public static final String CONFIG_DIR_NAME_PRIMES_MAPPING_TABLES = "primesMappingTables";

	public static final String CONFIG_DIR_NAME_ONLINE = "ONLINE";

	public static final String CONFIG_DIR_NAME_OUTPUT = "output";

	public static final String CONFIG_DIR_NAME_INPUT = "input";

	public static final String CONFIG_DIR_NAME_PRINTING = "printing";

	public static final String CONFIG_DIR_NAME_VOTEVERIFICATION = "voteVerification";

	public static final String SDM_DIR_NAME = "sdm";

	public static final String SDM_LANGS_DIR_NAME = "langs";

	public static final String SDM_CONFIG_DIR = SDM_DIR_NAME + "/sdmConfig";

	public static final String CONFIG_FILES_BASE_DIR = SDM_DIR_NAME + "/config";

	public static final String ADMIN_BOARDS_DIR = "admin-boards";

	public static final String SDM_INPUT_FILES_BASE_DIR = SDM_DIR_NAME + "/input";

	// ////////////////////////////////////
	//
	// lengths
	//
	// ////////////////////////////////////

	public static final int SVK_LENGTH = 24;
	public static final int BASE16_ID_LENGTH = 32;
	public static final int BASE64_ENCODED_HASH_OUTPUT_LENGTH = 44;
	private static final int BIRTH_YEAR_LENGTH = 4;
	private static final int BIRTH_DATE_LENGTH = 8;
	public static final List<Integer> EXTENDED_AUTHENTICATION_FACTOR_LENGTHS = List.of(BIRTH_YEAR_LENGTH, BIRTH_DATE_LENGTH);

	// ////////////////////////////////////
	//
	// tags and miscellaneous constants
	//
	// ////////////////////////////////////

	public static final String BALLOT_BOX_ID = "ballotBoxId";

	public static final String BALLOT_ID = "ballotId";

	public static final String CONFIG_FILE_CONTROL_COMPONENT_CODE_SHARES_PAYLOAD = "controlComponentCodeSharesPayload";

	public static final String CHUNK_ID_PARAM = "chunkId";

	public static final String CHUNK_COUNT_PARAM = "chunkCount";

	public static final String ELECTION_EVENT_ID = "electionEventId";

	public static final String VERIFICATION_CARD_SET_ID = "verificationCardSetID";

	public static final String VERIFICATION_CARD_SET_ID_PARAM = "verificationCardSetId";

	private Constants() {
	}

}
