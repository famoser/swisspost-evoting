/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.api.setup;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ch.post.it.evoting.securedatamanager.services.application.service.IdleStatusService;
import ch.post.it.evoting.securedatamanager.services.infrastructure.preconfiguration.PreconfigurationRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * The election event end-point.
 */
@RestController
@RequestMapping("/sdm-backend/preconfiguration")
@Api(value = "Pre-configuration REST API")
@ConditionalOnProperty("role.isSetup")
public class PreconfigurationController {

	private static final Logger LOGGER = LoggerFactory.getLogger(PreconfigurationController.class);

	private final PreconfigurationRepository preconfigurationRepository;
	private final IdleStatusService idleStatusService;

	public PreconfigurationController(
			final PreconfigurationRepository preconfigurationRepository,
			final IdleStatusService idleStatusService) {
		this.preconfigurationRepository = preconfigurationRepository;
		this.idleStatusService = idleStatusService;
	}

	@PostMapping(produces = "application/json")
	@ResponseStatus(value = HttpStatus.CREATED)
	@ApiOperation(value = "Get configuration", notes = "Service to create and save the elections_config.", response = String.class)
	public String createElectionEvent() throws IOException {

		final String lockId = "preconfiguration";
		if (!idleStatusService.getIdLock(lockId)) {
			LOGGER.warn("The election preconfiguration is already processing, doing nothing.");
			return "";
		}

		final String electionsConfig;
		try {
			final String electionEventId = preconfigurationRepository.createElectionsConfig();
			LOGGER.info("Successfully created the elections_config. [electionEventId: {}]", electionEventId);

			LOGGER.debug("Reading configuration from file...");
			electionsConfig = preconfigurationRepository.readFromFileAndSave();
		} finally {
			idleStatusService.freeIdLock(lockId);
		}

		return electionsConfig;
	}
}
