/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static java.lang.Integer.min;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.election.Ballot;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientKeyPair;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPrivateKey;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.BigIntegersOptimizations;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement.PrimeGqElementFactory;
import ch.post.it.evoting.securedatamanager.commons.Constants;
import ch.post.it.evoting.securedatamanager.commons.VerificationCardSet;
import ch.post.it.evoting.securedatamanager.configuration.EncryptionParametersConfigService;
import ch.post.it.evoting.securedatamanager.services.application.service.BallotBoxService;
import ch.post.it.evoting.securedatamanager.services.application.service.BaseVotingCardSetService;
import ch.post.it.evoting.securedatamanager.services.application.service.ElectionEventService;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

/**
 * Service that invokes the GenVerDat algorithm described in the cryptographic protocol.
 */
@Service
@ConditionalOnProperty("role.isSetup")
public class GenVerDatService extends BaseVotingCardSetService {

	private static final Logger LOGGER = LoggerFactory.getLogger(GenVerDatService.class);

	private final ObjectMapper objectMapper;
	private final PathResolver pathResolver;
	private final BallotBoxService ballotBoxService;
	private final GenVerDatAlgorithm genVerDatAlgorithm;
	private final BallotConfigService ballotConfigService;
	private final ElectionEventService electionEventService;
	private final EncryptionParametersConfigService encryptionParametersConfigService;

	@Value("${choiceCodeGenerationChunkSize:100}")
	private int chunkSize;

	@Value("${genVerDatCacheSize}")
	private int cacheSize;

	@Autowired
	public GenVerDatService(
			final ObjectMapper objectMapper,
			final PathResolver pathResolver,
			final BallotBoxService ballotBoxService,
			final GenVerDatAlgorithm genVerDatAlgorithm,
			final BallotConfigService ballotConfigService,
			final ElectionEventService electionEventService,
			final EncryptionParametersConfigService encryptionParametersConfigService) {
		this.objectMapper = objectMapper;
		this.pathResolver = pathResolver;
		this.ballotConfigService = ballotConfigService;
		this.genVerDatAlgorithm = genVerDatAlgorithm;
		this.electionEventService = electionEventService;
		this.ballotBoxService = ballotBoxService;
		this.encryptionParametersConfigService = encryptionParametersConfigService;
	}

	/**
	 * Invoke the GenVerDat algorithm.
	 */
	public List<GenVerDatOutput> genVerDat(final VerificationCardSet precomputeContext, final int numberOfVotingCardsToGenerate) {
		checkNotNull(precomputeContext);
		checkArgument(numberOfVotingCardsToGenerate >= 0, "The number of voting cards to generate must be positive.");
		checkArgument(chunkSize >= 0, "The chunk size must be positive.");

		final String electionEventId = precomputeContext.electionEventId();
		final String ballotBoxId = precomputeContext.ballotBoxId();
		final String verificationCardSetId = precomputeContext.verificationCardSetId();

		checkArgument(electionEventService.exists(electionEventId), "The election event id does not exist.");

		// Get the setup public key.
		final GqGroup encryptionGroup = encryptionParametersConfigService.loadEncryptionGroup(electionEventId);
		final ElGamalMultiRecipientPublicKey setupPublicKey = getSetupPublicKey(encryptionGroup, electionEventId);

		// Retrieve the encoded and actual voting options.
		final String ballotId = ballotBoxService.getBallotId(ballotBoxId);
		final Ballot ballot = ballotConfigService.getBallot(electionEventId, ballotId);
		final GroupVector<PrimeGqElement, GqGroup> encodedVotingOptions = ballot.getEncodedVotingOptions().stream()
				.map(value -> PrimeGqElementFactory.fromValue(value, encryptionGroup))
				.collect(GroupVector.toGroupVector());
		final List<String> actualVotingOptions = ballot.getActualVotingOptions();
		final List<String> semanticInformation = ballot.getSemanticInformation();

		// Add to the GMP cache the fixed-base exponentiation.
		prepareFixedBaseOptimizations(encryptionGroup, setupPublicKey, encodedVotingOptions);

		// Build full-sized chunks (i.e. with `chunkSize` elements)
		final int fullChunkCount = numberOfVotingCardsToGenerate / chunkSize;
		final List<GenVerDatOutput> genVerDatOutputs = IntStream.range(0, fullChunkCount)
				.mapToObj(chunkId -> {

					LOGGER.debug("Generating verification data... [electionEventId: {}, verificationCardSetId: {}, chunkId: {}]", electionEventId,
							verificationCardSetId, chunkId);

					// Generate verification data. Since we chunk the payloads, we are not directly working with the number of eligible voters (N_E),
					// but rather with the chunk size as input of the algorithm.
					final GenVerDatOutput genVerDatOutput =
							genVerDatAlgorithm.genVerDat(chunkSize, encodedVotingOptions, actualVotingOptions, semanticInformation, precomputeContext,
									setupPublicKey);

					LOGGER.info(
							"Successfully generated the verification data. [electionEventId: {}, verificationCardSetId: {}, chunkId: {}]",
							electionEventId, verificationCardSetId, chunkId);

					return genVerDatOutput;
				})
				.collect(Collectors.toCollection(ArrayList::new));

		// Build an eventual last chunk with the remaining elements.
		final int lastChunkSize = numberOfVotingCardsToGenerate % chunkSize;
		if (lastChunkSize > 0) {
			LOGGER.debug("Generating verification data... [electionEventId: {}, verificationCardSetId: {}, chunkId: {}]", electionEventId,
					verificationCardSetId, fullChunkCount);
			genVerDatOutputs.add(
					genVerDatAlgorithm.genVerDat(lastChunkSize, encodedVotingOptions, actualVotingOptions, semanticInformation, precomputeContext,
							setupPublicKey));
			LOGGER.info(
					"Successfully generated the verification data. [electionEventId: {}, verificationCardSetId: {}, chunkId: {}]", electionEventId,
					verificationCardSetId, fullChunkCount);
		}

		LOGGER.info(
				"Successfully executed the GenVerDat algorithm. [electionEventId: {}, verificationCardSetId: {}]", electionEventId,
				verificationCardSetId);

		return genVerDatOutputs;
	}

	/**
	 * Read the setup secret key from the file system and derive the setup public key from it.
	 *
	 * @return the setup public key.
	 */
	private ElGamalMultiRecipientPublicKey getSetupPublicKey(final GqGroup encryptionGroup, final String electionEventId) {
		try {
			// Read the setup secret key from file system.
			final ElGamalMultiRecipientPrivateKey setupSecretKey = objectMapper.reader()
					.withAttribute("group", encryptionGroup)
					.readValue(pathResolver.resolveOfflinePath(electionEventId).resolve(Constants.SETUP_SECRET_KEY_FILE_NAME).toFile(),
							ElGamalMultiRecipientPrivateKey.class);

			// Create key pair from the setup secret key and retrieve corresponding setup public key.
			return ElGamalMultiRecipientKeyPair.from(setupSecretKey, encryptionGroup.getGenerator()).getPublicKey();
		} catch (final IOException e) {
			throw new UncheckedIOException("Failed to deserialize setup secret key.", e);
		}
	}

	/**
	 * We limit the number of elements saved in the cache. We first save k=min(n, cacheSize) encoded voting options. Then, if the cache size allows
	 * it, we save at most k setup public key's elements.
	 */
	private void prepareFixedBaseOptimizations(final GqGroup encryptionGroup, final ElGamalMultiRecipientPublicKey setupPublicKey,
			final GroupVector<PrimeGqElement, GqGroup> encodedVotingOptions) {
		final int maximumEncodedVotingOptions = min(encodedVotingOptions.size(), cacheSize);
		final int maximumSetupPublicKeyElements = min(encodedVotingOptions.size(), cacheSize - maximumEncodedVotingOptions);
		checkState(maximumSetupPublicKeyElements <= setupPublicKey.size(),
				"The number of encoded voting options must be smaller than or equal to the setup public key length.");

		LOGGER.debug(
				"Preparing for fixed-base optimizations if supported. [cacheSize: {}, maximumEncodedVotingOptions: {}, maximumSetupPublicKeyElements: {}]",
				cacheSize, maximumEncodedVotingOptions, maximumSetupPublicKeyElements);

		IntStream.range(0, maximumEncodedVotingOptions)
				.parallel()
				.forEach(i -> BigIntegersOptimizations.prepareFixedBaseOptimizations(encodedVotingOptions.get(i).getValue(), encryptionGroup.getP()));
		IntStream.range(0, maximumSetupPublicKeyElements)
				.parallel()
				.forEach(i -> BigIntegersOptimizations.prepareFixedBaseOptimizations(setupPublicKey.get(i).getValue(), encryptionGroup.getP()));
	}

}

