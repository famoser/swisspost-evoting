/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain;

public class SharedQueue {
	public static final String CREATE_LVCC_SHARE_HASH_REQUEST_PATTERN = "voting.return-codes.CreateLVCCShareHashRequest.";
	public static final String CREATE_LVCC_SHARE_HASH_RESPONSE_PATTERN = "voting.return-codes.CreateLVCCShareHashResponse.";

	public static final String VERIFY_LVCC_SHARE_HASH_REQUEST_PATTERN = "voting.return-codes.VerifyLVCCShareHashRequest.";
	public static final String VERIFY_LVCC_SHARE_HASH_RESPONSE_PATTERN = "voting.return-codes.VerifyLVCCShareHashResponse.";

	public static final String CREATE_LCC_SHARE_REQUEST_PATTERN = "voting.return-codes.CreateLCCShareRequest.";
	public static final String CREATE_LCC_SHARE_RESPONSE_PATTERN = "voting.return-codes.CreateLCCShareResponse.";

	public static final String GEN_ENC_LONG_CODE_SHARES_REQUEST_PATTERN = "configuration.return-codes.GenEncLongCodeSharesRequest.";
	public static final String GEN_ENC_LONG_CODE_SHARES_RESPONSE_PATTERN = "configuration.return-codes.GenEncLongCodeSharesResponse.";

	public static final String GEN_KEYS_CCR_REQUEST_PATTERN = "configuration.return-codes.GenKeysCCRRequest.";
	public static final String GEN_KEYS_CCR_RESPONSE_PATTERN = "configuration.return-codes.GenKeysCCRResponse.";

	public static final String MIX_DEC_ONLINE_REQUEST_PATTERN = "tally.mixing.MixDecOnlineRequest.";
	public static final String MIX_DEC_ONLINE_RESPONSE_PATTERN = "tally.mixing.MixDecOnlineResponse.";

	public static final String PARTIAL_DECRYPT_PCC_REQUEST_PATTERN = "voting.return-codes.PartialDecryptPCCRequest.";
	public static final String PARTIAL_DECRYPT_PCC_RESPONSE_PATTERN = "voting.return-codes.PartialDecryptPCCResponse.";

	public static final String ELECTION_CONTEXT_REQUEST_PATTERN = "configuration.electioncontext.ElectionContextRequest.";
	public static final String ELECTION_CONTEXT_RESPONSE_PATTERN = "configuration.electioncontext.ElectionContextResponse.";

	public static final String SETUP_COMPONENT_PUBLIC_KEYS_REQUEST_PATTERN = "configuration.setupkeys.SetupComponentPublicKeysRequest.";
	public static final String SETUP_COMPONENT_PUBLIC_KEYS_RESPONSE_PATTERN = "configuration.setupkeys.SetupComponentPublicKeysResponse.";

	public static final String LONG_VOTE_CAST_RETURN_CODES_ALLOW_LIST_REQUEST_PATTERN = "configuration.setup-voting.longVoteCastReturnCodesAllowListRequest.";
	public static final String LONG_VOTE_CAST_RETURN_CODES_ALLOW_LIST_RESPONSE_PATTERN = "configuration.setup-voting.longVoteCastReturnCodesAllowListResponse.";

	private SharedQueue() {
		// static usage only.
	}

	public static String fromName(final String name) {
		return switch (name) {
			case "CREATE_LCC_SHARE_RESPONSE_PATTERN" -> CREATE_LCC_SHARE_RESPONSE_PATTERN;
			case "GEN_KEYS_CCR_RESPONSE_PATTERN" -> GEN_KEYS_CCR_RESPONSE_PATTERN;
			case "PARTIAL_DECRYPT_PCC_RESPONSE_PATTERN" -> PARTIAL_DECRYPT_PCC_RESPONSE_PATTERN;
			case "ELECTION_CONTEXT_RESPONSE_PATTERN" -> ELECTION_CONTEXT_RESPONSE_PATTERN;
			case "SETUP_COMPONENT_PUBLIC_KEYS_RESPONSE_PATTERN" -> SETUP_COMPONENT_PUBLIC_KEYS_RESPONSE_PATTERN;
			case "LONG_VOTE_CAST_RETURN_CODES_ALLOW_LIST_RESPONSE_PATTERN" -> LONG_VOTE_CAST_RETURN_CODES_ALLOW_LIST_RESPONSE_PATTERN;
			case "MIX_DEC_ONLINE_RESPONSE_PATTERN" -> MIX_DEC_ONLINE_RESPONSE_PATTERN;
			case "CREATE_LVCC_SHARE_HASH_RESPONSE_PATTERN" -> CREATE_LVCC_SHARE_HASH_RESPONSE_PATTERN;
			case "VERIFY_LVCC_SHARE_HASH_RESPONSE_PATTERN" -> VERIFY_LVCC_SHARE_HASH_RESPONSE_PATTERN;
			case "GEN_ENC_LONG_CODE_SHARES_RESPONSE_PATTERN" -> GEN_ENC_LONG_CODE_SHARES_RESPONSE_PATTERN;
			default -> throw new IllegalArgumentException(String.format("Unknown shared queue name provided. [provided name: %s]", name));
		};
	}
}
