/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.configuration;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.hasNoDuplicates;
import static ch.post.it.evoting.cryptoprimitives.utils.Validations.allEqual;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Comparator;
import java.util.List;

import com.google.common.base.Preconditions;

/**
 * Represents the list of {@link SetupComponentCMTablePayload} sorted by chunk id per election event id and verification card set id.
 *
 * @param payloads the list of {@link SetupComponentCMTablePayload}.
 *                 <li>It must not be null.</li>
 *                 <li>It must not contain null values.</li>
 *                 <li>The election event id must be consistent throughout the payloads.</li>
 *                 <li>The verification card set id must be consistent throughout the payloads.</li>
 *                 <li>The chunk ids must be consistent.</li>
 */
public record SetupComponentCMTablePayloadChunks(List<SetupComponentCMTablePayload> payloads) {

	public SetupComponentCMTablePayloadChunks(final List<SetupComponentCMTablePayload> payloads) {
		checkNotNull(payloads);
		payloads.stream().parallel().forEach(Preconditions::checkNotNull);

		final List<SetupComponentCMTablePayload> payloadsCopy = List.copyOf(payloads).stream()
				.parallel()
				.sorted(Comparator.comparingInt(SetupComponentCMTablePayload::getChunkId))
				.toList();
		checkArgument(!payloadsCopy.isEmpty(), "There must be at least one SetupComponentCMTablePayload.");

		checkArgument(allEqual(payloadsCopy.stream().parallel(), SetupComponentCMTablePayload::getElectionEventId),
				"All SetupComponentCMTablePayloads must have the same election event id.");
		checkArgument(allEqual(payloadsCopy.stream().parallel(), SetupComponentCMTablePayload::getVerificationCardSetId),
				"All SetupComponentCMTablePayloads must have the same verification card set id.");

		final List<Integer> chunkIds = payloadsCopy.stream().parallel().map(SetupComponentCMTablePayload::getChunkId).toList();
		checkArgument(hasNoDuplicates(chunkIds) && chunkIds.get(0) == 0 && chunkIds.get(chunkIds.size() - 1) == chunkIds.size() - 1,
				"The SetupComponentCMTablePayloads' chunk ids are not consistent.");

		this.payloads = payloadsCopy;
	}

	public List<SetupComponentCMTablePayload> payloads() {
		return List.copyOf(payloads);
	}

	public int getChunkCount() {
		return payloads.size();
	}

	public String getElectionEventId() {
		return payloads.get(0).getElectionEventId();
	}

	public String getVerificationCardSetId() {
		return payloads.get(0).getVerificationCardSetId();
	}

}
