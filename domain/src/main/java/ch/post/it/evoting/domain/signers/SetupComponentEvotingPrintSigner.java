/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.signers;

import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.evotinglibraries.xml.XsdConstants;
import ch.post.it.evoting.evotinglibraries.xml.hashable.HashableSetupComponentEvotingPrintFactory;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingprint.VotingCardList;

public class SetupComponentEvotingPrintSigner extends XmlSigner<VotingCardList> {
	public SetupComponentEvotingPrintSigner(final SignatureKeystore<Alias> signatureKeystore) {
		super(signatureKeystore,
				VotingCardList::setSignature,
				VotingCardList::getSignature,
				HashableSetupComponentEvotingPrintFactory::fromVotingCardList,
				print -> ChannelSecurityContextData.setupComponentEvotingPrint(),
				Alias.SDM_CONFIG,
				VotingCardList.class,
				SetupComponentEvotingPrintSigner.class.getResource(XsdConstants.SETUP_COMPONENT_EVOTING_PRINT_XSD));
	}
}
