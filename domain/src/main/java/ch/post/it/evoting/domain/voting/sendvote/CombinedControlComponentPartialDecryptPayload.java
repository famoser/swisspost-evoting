/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.voting.sendvote;

import static ch.post.it.evoting.cryptoprimitives.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.cryptoprimitives.utils.Validations.allEqual;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;
import java.util.stream.Collectors;

public record CombinedControlComponentPartialDecryptPayload(List<ControlComponentPartialDecryptPayload> controlComponentPartialDecryptPayloads) {

	public CombinedControlComponentPartialDecryptPayload {
		checkNotNull(controlComponentPartialDecryptPayloads);

		checkArgument(controlComponentPartialDecryptPayloads.size() == NODE_IDS.size(),
				"There must be the expected number of control component partial decrypt payloads.");

		checkArgument(NODE_IDS.equals(controlComponentPartialDecryptPayloads.stream()
				.map(controlComponentPartialDecryptPayload -> controlComponentPartialDecryptPayload.getPartiallyDecryptedEncryptedPCC().nodeId())
				.collect(Collectors.toSet())), "The control component partial decrypt payloads must contain the expected node ids.");

		checkArgument(allEqual(controlComponentPartialDecryptPayloads.stream(), ControlComponentPartialDecryptPayload::getEncryptionGroup),
				"All control component partial decrypt payloads must have the same group.");
		checkArgument(
				allEqual(controlComponentPartialDecryptPayloads.stream(), payload -> payload.getPartiallyDecryptedEncryptedPCC().contextIds()),
				"All control component partial decrypt payloads must have the same contextIds.");
	}

}
