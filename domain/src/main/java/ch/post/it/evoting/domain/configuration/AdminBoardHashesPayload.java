/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.configuration;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import ch.post.it.evoting.cryptoprimitives.domain.signature.SignedPayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableByteArray;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableList;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableString;

@JsonPropertyOrder({ "adminBoardId", "adminBoardHashes", "threshold", "signature" })
public final class AdminBoardHashesPayload implements SignedPayload {

	private final String adminBoardId;

	private final List<byte[]> adminBoardHashes;

	private final int threshold;

	private CryptoPrimitivesSignature signature;

	@JsonCreator
	public AdminBoardHashesPayload(
			@JsonProperty("adminBoardId")
			final String adminBoardId,
			@JsonProperty("adminBoardHashes")
			final List<byte[]> adminBoardHashes,
			@JsonProperty("threshold")
			final int threshold,
			@JsonProperty("signature")
			final CryptoPrimitivesSignature signature
	) {
		this(adminBoardId, adminBoardHashes, threshold);
		this.signature = checkNotNull(signature);
	}

	public AdminBoardHashesPayload(final String adminBoardId, final List<byte[]> adminBoardHashes, final int threshold) {
		this.adminBoardId = validateUUID(adminBoardId);
		this.adminBoardHashes = List.copyOf(checkNotNull(adminBoardHashes));
		checkArgument(this.adminBoardHashes.size() >= 2, "The AdminBoardHashesPayload must have at least two hashes.");
		checkArgument(threshold > 0 && threshold <= this.adminBoardHashes.size(),
				"The threshold must be greater than 0 and smaller than or equal to the number of hashes.");
		this.threshold = threshold;
	}

	public String getAdminBoardId() {
		return adminBoardId;
	}

	public List<byte[]> getAdminBoardHashes() {
		return List.copyOf(checkNotNull(adminBoardHashes));
	}

	public int getThreshold() {
		return threshold;
	}

	public CryptoPrimitivesSignature getSignature() {
		return signature;
	}

	public void setSignature(final CryptoPrimitivesSignature signature) {
		this.signature = checkNotNull(signature);
	}

	@Override
	public List<Hashable> toHashableForm() {
		return List.of(
				HashableString.from(adminBoardId),
				HashableList.from(adminBoardHashes.stream().map(HashableByteArray::from).toList()),
				HashableString.from(String.valueOf(threshold)));
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final AdminBoardHashesPayload that = (AdminBoardHashesPayload) o;
		return adminBoardId.equals(that.adminBoardId) &&
				adminBoardHashes.equals(that.adminBoardHashes) &&
				threshold == that.threshold &&
				Objects.equals(signature, that.signature);
	}

	@Override
	public int hashCode() {
		return Objects.hash(adminBoardId, adminBoardHashes, threshold, signature);
	}
}
