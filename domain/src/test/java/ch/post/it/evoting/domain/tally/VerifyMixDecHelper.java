/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.tally;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import com.google.common.collect.Streams;

import ch.post.it.evoting.cryptoprimitives.domain.election.ControlComponentPublicKeys;
import ch.post.it.evoting.cryptoprimitives.domain.election.SetupComponentPublicKeys;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamal;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalFactory;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.mixnet.ShuffleArgument;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.VerifiableShuffleGenerator;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.SchnorrProof;

public class VerifyMixDecHelper {

	private static final ElGamal elGamal = ElGamalFactory.createElGamal();

	private final GqGroup gqGroup;
	private final ElGamalGenerator elGamalGenerator;
	private final VerifiableShuffleGenerator verifiableShuffleGenerator;

	public VerifyMixDecHelper(final GqGroup gqGroup) {
		this.gqGroup = gqGroup;
		this.elGamalGenerator = new ElGamalGenerator(gqGroup);
		this.verifiableShuffleGenerator = new VerifiableShuffleGenerator(gqGroup);
	}

	public SetupComponentPublicKeys createSetupComponentPublicKeys(final int l) {
		final List<ControlComponentPublicKeys> combinedControlComponentPublicKeys = new ArrayList<>();

		IntStream.rangeClosed(1, 4)
				.forEach(nodeId -> combinedControlComponentPublicKeys.add(generateCombinedControlComponentPublicKeys(nodeId, l)));

		final ElGamalMultiRecipientPublicKey electoralBoardPublicKey = elGamalGenerator.genRandomPublicKey(l);

		final GroupVector<SchnorrProof, ZqGroup> schnorrProofs = generateSchnorrProofs(l);

		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> ccrChoiceReturnCodePublicKeys = combinedControlComponentPublicKeys.stream()
				.map(ControlComponentPublicKeys::ccrjChoiceReturnCodesEncryptionPublicKey).collect(GroupVector.toGroupVector());

		final ElGamalMultiRecipientPublicKey choiceReturnCodesPublicKey = elGamal.combinePublicKeys(ccrChoiceReturnCodePublicKeys);

		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> ccmElectionPublicKeys = Streams.concat(
				combinedControlComponentPublicKeys.stream()
						.map(ControlComponentPublicKeys::ccmjElectionPublicKey),
				Stream.of(electoralBoardPublicKey)).collect(GroupVector.toGroupVector());

		final ElGamalMultiRecipientPublicKey electionPublicKey = elGamal.combinePublicKeys(ccmElectionPublicKeys);

		return new SetupComponentPublicKeys(combinedControlComponentPublicKeys, electoralBoardPublicKey, schnorrProofs, electionPublicKey,
				choiceReturnCodesPublicKey);
	}

	public ShuffleArgument createShuffleArgument(final int N, final int l) {
		return verifiableShuffleGenerator.genVerifiableShuffle(N, l).shuffleArgument();
	}

	private ControlComponentPublicKeys generateCombinedControlComponentPublicKeys(final int nodeId, final int l) {
		final ElGamalMultiRecipientPublicKey ccrChoiceReturnCodesEncryptionPublicKey = elGamalGenerator.genRandomPublicKey(l);
		final ElGamalMultiRecipientPublicKey ccmElectionPublicKey = elGamalGenerator.genRandomPublicKey(l);

		final GroupVector<SchnorrProof, ZqGroup> schnorrProofs = generateSchnorrProofs(l);

		return new ControlComponentPublicKeys(nodeId, ccrChoiceReturnCodesEncryptionPublicKey, schnorrProofs, ccmElectionPublicKey, schnorrProofs);
	}

	private GroupVector<SchnorrProof, ZqGroup> generateSchnorrProofs(final int l) {
		final ZqGroup zqGroup = ZqGroup.sameOrderAs(gqGroup);
		final ZqElement e = ZqElement.create(2, zqGroup);
		final ZqElement z = ZqElement.create(2, zqGroup);
		final SchnorrProof schnorrProof = new SchnorrProof(e, z);
		return Collections.nCopies(l, schnorrProof).stream().collect(GroupVector.toGroupVector());
	}

}
