/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent;

public interface PartialChoiceReturnCodeAllowList {

	boolean exists(final String longVoteCastReturnCode);
}
