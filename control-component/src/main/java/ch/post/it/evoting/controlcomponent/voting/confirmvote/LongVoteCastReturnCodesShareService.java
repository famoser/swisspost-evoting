/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.voting.confirmvote;

import static ch.post.it.evoting.cryptoprimitives.domain.ControlComponentConstants.NODE_IDS;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.function.Supplier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.controlcomponent.BallotBoxEntity;
import ch.post.it.evoting.controlcomponent.BallotBoxService;
import ch.post.it.evoting.controlcomponent.ElectionContextEntity;
import ch.post.it.evoting.controlcomponent.ElectionContextService;
import ch.post.it.evoting.controlcomponent.ElectionEventService;
import ch.post.it.evoting.controlcomponent.VerificationCardEntity;
import ch.post.it.evoting.controlcomponent.VerificationCardService;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;
import ch.post.it.evoting.domain.voting.confirmvote.ConfirmationKey;
import ch.post.it.evoting.domain.voting.confirmvote.LongVoteCastReturnCodesShare;
import ch.post.it.evoting.evotinglibraries.domain.common.ContextIds;

@Service
public class LongVoteCastReturnCodesShareService {
	private static final Logger LOGGER = LoggerFactory.getLogger(LongVoteCastReturnCodesShareService.class);

	private static final String GROUP = "group";

	private final LongVoteCastReturnCodesShareRepository longVoteCastReturnCodesShareRepository;
	private final VerificationCardService verificationCardService;
	private final ElectionEventService electionEventService;
	private final ObjectMapper objectMapper;
	private final BallotBoxService ballotBoxService;
	private final ElectionContextService electionContextService;

	public LongVoteCastReturnCodesShareService(
			final LongVoteCastReturnCodesShareRepository longVoteCastReturnCodesShareRepository,
			final VerificationCardService verificationCardService,
			final ElectionEventService electionEventService,
			final ObjectMapper objectMapper,
			final BallotBoxService ballotBoxService,
			final ElectionContextService electionContextService) {
		this.longVoteCastReturnCodesShareRepository = longVoteCastReturnCodesShareRepository;
		this.verificationCardService = verificationCardService;
		this.electionEventService = electionEventService;
		this.objectMapper = objectMapper;
		this.ballotBoxService = ballotBoxService;
		this.electionContextService = electionContextService;
	}

	@Transactional
	public void saveLVCCShare(final ConfirmationKey confirmationKey, final CreateLVCCShareOutput createLVCCShareOutput) {
		checkNotNull(confirmationKey);
		checkNotNull(createLVCCShareOutput);

		final ContextIds contextIds = confirmationKey.contextIds();
		final VerificationCardEntity verificationCardEntity = verificationCardService.getVerificationCardEntity(contextIds.verificationCardId());

		final byte[] longVoteCastReturnCodeShareSerialised;
		final byte[] voterVoteCastReturnCodeGenerationPublicKeySerialised;
		final byte[] exponentiationProofSerialised;
		try {
			longVoteCastReturnCodeShareSerialised = objectMapper.writeValueAsBytes(createLVCCShareOutput.getLongVoteCastReturnCodeShare());
			voterVoteCastReturnCodeGenerationPublicKeySerialised = objectMapper.writeValueAsBytes(
					createLVCCShareOutput.getVoterVoteCastReturnCodeGenerationPublicKey());
			exponentiationProofSerialised = objectMapper.writeValueAsBytes(createLVCCShareOutput.getExponentiationProof());
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException("Failed to serialize long Vote Cast Return Code Share.", e);
		}

		final BigInteger confirmationKeySerialised = confirmationKey.element().getValue();

		final LongVoteCastReturnCodesShareEntity longVoteCastReturnCodesShareEntity = new LongVoteCastReturnCodesShareEntity(verificationCardEntity,
				longVoteCastReturnCodeShareSerialised, voterVoteCastReturnCodeGenerationPublicKeySerialised, exponentiationProofSerialised,
				confirmationKeySerialised.toString());
		longVoteCastReturnCodesShareRepository.save(longVoteCastReturnCodesShareEntity);
		LOGGER.info("Saved the Long Vote Cast Return Codes Share. [contextIds: {}]", contextIds);
	}

	@Transactional
	public LongVoteCastReturnCodesShare load(final ConfirmationKey confirmationKey, final int nodeId) {
		checkNotNull(confirmationKey);
		checkArgument(NODE_IDS.contains(nodeId), "The node id must be part of the known node ids. [nodeId: %s]", nodeId);

		final ContextIds contextIds = confirmationKey.contextIds();
		final String electionEventId = confirmationKey.contextIds().electionEventId();
		final String verificationCardId = confirmationKey.contextIds().verificationCardId();
		final String verificationCardSetId = confirmationKey.contextIds().verificationCardSetId();

		final LongVoteCastReturnCodesShareEntity longVoteCastReturnCodesShareEntity =
				longVoteCastReturnCodesShareRepository.findByVerificationCardIdAndConfirmationKey(verificationCardId, confirmationKey.element()
								.getValue().toString())
						.orElseThrow(() -> new IllegalStateException(
								String.format("Long Vote Cast Return Codes Share not found. [verificationCardId: %s]", verificationCardId)));

		final GqGroup encryptionGroup = electionEventService.getEncryptionGroup(electionEventId);

		final GqElement longVoteCastReturnCodeShare = deserializeLongVoteCastReturnCodeShare(
				longVoteCastReturnCodesShareEntity.getLongVoteCastReturnCodeShare(), contextIds, encryptionGroup);

		final ExponentiationProof exponentiationProof = deserializeExponentiationProof(longVoteCastReturnCodesShareEntity.getExponentiationProof(),
				contextIds, encryptionGroup);

		return new LongVoteCastReturnCodesShare(electionEventId, verificationCardSetId, verificationCardId, nodeId, longVoteCastReturnCodeShare,
				exponentiationProof);
	}

	@Transactional
	public void validateConfirmationIsAllowed(final String electionEventId, final String verificationCardId, final Supplier<LocalDateTime> now) {
		final ElectionContextEntity electionContextEntity = electionContextService.getElectionContextEntity(electionEventId);
		final BallotBoxEntity ballotBox = ballotBoxService.getBallotBox(
				verificationCardService.getVerificationCardEntity(verificationCardId).getVerificationCardSetEntity());

		final LocalDateTime electionStartTime = electionContextEntity.getStartTime();
		final LocalDateTime electionEndTime = electionContextEntity.getFinishTime();
		final LocalDateTime currentTime = now.get();

		final boolean afterStartTime = currentTime.isAfter(electionStartTime) || currentTime.isEqual(electionStartTime);
		final boolean beforeEndTime = currentTime.isBefore(electionEndTime.plusSeconds(ballotBox.getGracePeriod())) || currentTime
				.isEqual(electionEndTime.plusSeconds(ballotBox.getGracePeriod()));

		checkState(afterStartTime && beforeEndTime,
				"Impossible to confirm vote before or after the dedicated time. [electionEventId: %s, ballotBoxId: %s, verificationCardId: %s, "
						+ "startTime: %s, finishTime: %s, gracePeriod: %s]",
				electionEventId, ballotBox.getBallotBoxId(), verificationCardId, electionStartTime, electionEndTime, ballotBox.getGracePeriod());
		checkState(!ballotBox.isMixed(),
				"Impossible to confirm vote in an already mixed ballot box. [electionEventId: %s, ballotBoxId: %s, verificationCardId: %s]",
				electionEventId, ballotBox.getBallotBoxId(), verificationCardId);
	}

	private GqElement deserializeLongVoteCastReturnCodeShare(final byte[] longVoteCastReturnCodeShareBytes, final ContextIds contextIds,
			final GqGroup encryptionGroup) {
		final GqElement longVoteCastReturnCodeShare;
		try {
			longVoteCastReturnCodeShare = objectMapper.reader().withAttribute(GROUP, encryptionGroup)
					.readValue(longVoteCastReturnCodeShareBytes, GqElement.class);
		} catch (final IOException e) {
			throw new UncheckedIOException(String.format("Failed to deserialize long vote cast return code share. [contextIds: %s]", contextIds), e);
		}
		return longVoteCastReturnCodeShare;
	}

	private ExponentiationProof deserializeExponentiationProof(final byte[] exponentiationProofBytes, final ContextIds contextIds,
			final GqGroup encryptionGroup) {
		final ExponentiationProof exponentiationProof;
		try {
			exponentiationProof = objectMapper.reader().withAttribute(GROUP, encryptionGroup)
					.readValue(exponentiationProofBytes, ExponentiationProof.class);
		} catch (final IOException e) {
			throw new UncheckedIOException(String.format("Failed to deserialize exponentiation proof. [contextIds: %s]", contextIds), e);
		}
		return exponentiationProof;
	}

}
