/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.voting.sendvote;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;

/**
 * Holds the output of the CreateLCCShare<sub>j</sub> algorithm.
 */
public class CreateLCCShareOutput {

	private final GroupVector<GqElement, GqGroup> hashedPartialChoiceReturnCodes;
	private final GroupVector<GqElement, GqGroup> longChoiceReturnCodeShare;
	private final GqElement voterChoiceReturnCodeGenerationPublicKey;
	private final ExponentiationProof exponentiationProof;

	CreateLCCShareOutput(final GroupVector<GqElement, GqGroup> hashedPartialChoiceReturnCodes,
			final GroupVector<GqElement, GqGroup> longChoiceReturnCodeShare, final GqElement voterChoiceReturnCodeGenerationPublicKey,
			final ExponentiationProof exponentiationProof) {

		checkNotNull(hashedPartialChoiceReturnCodes);
		checkNotNull(longChoiceReturnCodeShare);
		checkNotNull(voterChoiceReturnCodeGenerationPublicKey);
		checkNotNull(exponentiationProof);

		// Cross group checks.
		checkArgument(hashedPartialChoiceReturnCodes.getGroup().equals(longChoiceReturnCodeShare.getGroup()),
				"The hashed partial Choice Return Codes and the long Choice Return Code shares must have the same group.");
		checkArgument(longChoiceReturnCodeShare.getGroup().equals(voterChoiceReturnCodeGenerationPublicKey.getGroup()),
				"The long Choice Return Code shares and the voter Choice Return Code Generation public key must have the same group.");
		checkArgument(longChoiceReturnCodeShare.getGroup().hasSameOrderAs(exponentiationProof.getGroup()),
				"The long Choice Return Code shares and exponentiation proof must have the same group order.");

		this.hashedPartialChoiceReturnCodes = hashedPartialChoiceReturnCodes;
		this.longChoiceReturnCodeShare = longChoiceReturnCodeShare;
		this.voterChoiceReturnCodeGenerationPublicKey = voterChoiceReturnCodeGenerationPublicKey;
		this.exponentiationProof = exponentiationProof;
	}

	GroupVector<GqElement, GqGroup> getHashedPartialChoiceReturnCodes() {
		return hashedPartialChoiceReturnCodes;
	}

	public GroupVector<GqElement, GqGroup> getLongChoiceReturnCodeShare() {
		return longChoiceReturnCodeShare;
	}

	GqElement getVoterChoiceReturnCodeGenerationPublicKey() {
		return voterChoiceReturnCodeGenerationPublicKey;
	}

	ExponentiationProof getExponentiationProof() {
		return exponentiationProof;
	}
}