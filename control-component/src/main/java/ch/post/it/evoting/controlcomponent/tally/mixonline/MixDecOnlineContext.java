/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.tally.mixonline;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;

/**
 * Regroups the context values needed by the MixDecOnline algorithm.
 *
 * <ul>
 * <li>encryptionGroup, the encryption group. Not null.</li>
 * <li>ee, the election event id. Not null and valid UUID.</li>
 * <li>bb, the ballot box id. Not null and a valid UUID.</li>
 * <li>delta_hat, the number of allowed write-ins plus one. Strictly positive.</li>
 * </ul>
 */
public class MixDecOnlineContext {

	private final GqGroup encryptionGroup;
	private final String electionEventId;
	private final String ballotBoxId;
	private final int numberOfAllowedWriteInsPlusOne;

	private MixDecOnlineContext(final GqGroup encryptionGroup, final String electionEventId, final String ballotBoxId,
			final int numberOfAllowedWriteInsPlusOne) {
		this.encryptionGroup = encryptionGroup;
		this.electionEventId = electionEventId;
		this.ballotBoxId = ballotBoxId;
		this.numberOfAllowedWriteInsPlusOne = numberOfAllowedWriteInsPlusOne;
	}

	public GqGroup getEncryptionGroup() {
		return encryptionGroup;
	}

	public String getElectionEventId() {
		return electionEventId;
	}

	public String getBallotBoxId() {
		return ballotBoxId;
	}

	public int getNumberOfAllowedWriteInsPlusOne() {
		return numberOfAllowedWriteInsPlusOne;
	}

	/**
	 * Builder performing context validations before constructing a {@link MixDecOnlineContext}.
	 */
	public static class Builder {

		private GqGroup encryptionGroup;
		private String electionEventId;
		private String ballotBoxId;
		private int numberOfAllowedWriteInsPlusOne;

		public Builder setEncryptionGroup(final GqGroup encryptionGroup) {
			this.encryptionGroup = encryptionGroup;
			return this;
		}

		public Builder setElectionEventId(final String electionEventId) {
			this.electionEventId = electionEventId;
			return this;
		}

		public Builder setBallotBoxId(String ballotBoxId) {
			this.ballotBoxId = ballotBoxId;
			return this;
		}

		public Builder setNumberOfAllowedWriteInsPlusOne(int numberOfAllowedWriteInsPlusOne) {
			this.numberOfAllowedWriteInsPlusOne = numberOfAllowedWriteInsPlusOne;
			return this;
		}

		/**
		 * Constructs a MixDecryptContext object.
		 *
		 * @throws NullPointerException      if any id is null.
		 * @throws IllegalArgumentException  if the number of allowed write-ins plus one is negative or equal to zero.
		 * @throws FailedValidationException if the election event id or the ballot box id are not a valid UUID.
		 */
		public MixDecOnlineContext build() {
			checkNotNull(encryptionGroup);
			validateUUID(electionEventId);
			validateUUID(ballotBoxId);
			checkArgument(numberOfAllowedWriteInsPlusOne > 0, "The number of allowed write-ins + 1 must be strictly positive.");
			return new MixDecOnlineContext(encryptionGroup, electionEventId, ballotBoxId, numberOfAllowedWriteInsPlusOne);
		}
	}
}
