/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.voting.confirmvote;

import static ch.post.it.evoting.cryptoprimitives.utils.Conversions.integerToByteArray;
import static ch.post.it.evoting.cryptoprimitives.utils.Conversions.integerToString;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.util.List;
import java.util.stream.Stream;

import org.springframework.stereotype.Service;

import ch.post.it.evoting.controlcomponent.VerificationCardStateService;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableList;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableString;
import ch.post.it.evoting.cryptoprimitives.math.Base64;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.utils.KeyDerivation;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ZeroKnowledgeProof;

/**
 * Implements the CreateLVCCShare<sub>j</sub> algorithm.
 */
@Service
public class CreateLVCCShareAlgorithm {

	static final int MAX_CONFIRMATION_ATTEMPTS = 5;

	private final KeyDerivation keyDerivation;
	private final Hash hash;
	private final Base64 base64;
	private final ZeroKnowledgeProof zeroKnowledgeProof;
	private final VerificationCardStateService verificationCardStateService;

	public CreateLVCCShareAlgorithm(
			final KeyDerivation keyDerivation,
			final Hash hash,
			final Base64 base64,
			final ZeroKnowledgeProof zeroKnowledgeProof,
			final VerificationCardStateService verificationCardStateService) {
		this.keyDerivation = keyDerivation;
		this.hash = hash;
		this.base64 = base64;
		this.zeroKnowledgeProof = zeroKnowledgeProof;
		this.verificationCardStateService = verificationCardStateService;
	}

	/**
	 * Generates the long Vote Cast Return Code shares.
	 * <p>
	 * By contract the context ids are verified prior to calling this method.
	 * </p>
	 *
	 * @param context the control component context, wrapping the election event id, verification card set id and control component id. Not null.
	 * @param input   the {@link CreateLVCCShareInput}. Not null.
	 * @return the long vote cast return code share, voter vote cast return code generation public key and exponentiation proof encapsulated in a
	 * {@link CreateLVCCShareOutput}.
	 * @throws NullPointerException     if any input parameter is null.
	 * @throws IllegalArgumentException if
	 *                                  <ul>
	 *                                      <li>The context and input do not have the same group.</li>
	 *                                      <li>The verification card is not in L<sub>sentVotes,j</sub>.</li>
	 *                                      <li>The verification card is in in L<sub>confirmedVotes, j</sub></li>
	 *                                  </ul>
	 */
	@SuppressWarnings("java:S117")
	public CreateLVCCShareOutput createLVCCShare(final LVCCContext context, final CreateLVCCShareInput input) {
		checkNotNull(context);
		checkNotNull(input);

		// Context.
		final GqGroup encryptionGroup = context.encryptionGroup();
		final BigInteger q = encryptionGroup.getQ();
		final GqElement g = encryptionGroup.getGenerator();
		final int j = context.nodeId();
		final String ee = context.electionEventId();
		final String vcs = context.verificationCardSetId();

		// Input.
		final GqElement CK_id = input.confirmationKey();
		final ZqElement k_j_prime = input.ccrjReturnCodesGenerationSecretKey();
		final String vc_id = input.verificationCardId();

		// Cross-checks.
		checkArgument(encryptionGroup.equals(CK_id.getGroup()), "The context and input must have the same group.");

		// Require.
		// Ensure vc_id ∈ L_sentVotes,j.
		checkArgument(verificationCardStateService.isSentVote(vc_id),
				String.format(
						"The CCR_j cannot create the LVCC Share since it did not compute the long Choice Return Code shares for the verification card. [vc_id: %s]",
						vc_id));
		// Ensure vc_id ∉ L_confirmedVotes,j
		checkArgument(verificationCardStateService.isNotConfirmedVote(vc_id),
				String.format("The CCR_j already confirmed the vote for this verification card. [vc_id: %s]", vc_id));

		// Operation.
		final int attempts_id = verificationCardStateService.getConfirmationAttempts(vc_id);

		checkArgument(attempts_id < MAX_CONFIRMATION_ATTEMPTS, String.format("Max confirmation attempts of %s exceeded.", MAX_CONFIRMATION_ATTEMPTS));

		final byte[] PRK = integerToByteArray(k_j_prime.getValue());

		final List<String> info_CK = List.of("VoterVoteCastReturnCodeGeneration", ee, vcs, vc_id);

		final ZqElement kc_j_id = keyDerivation.KDFToZq(PRK, info_CK, q);

		final GqElement Kc_j_id = g.exponentiate(kc_j_id);

		final GqElement hCK_id = hash.hashAndSquare(CK_id.getValue(), CK_id.getGroup());

		final GqElement lVCC_id_j = hCK_id.exponentiate(kc_j_id);

		final List<String> i_aux_1 = List.of("CreateLVCCShare", ee, vcs, vc_id, integerToString(j));

		final HashableList i_aux_1_hashable = HashableList.from(i_aux_1.stream().map(HashableString::from).toList());
		final String hlVCC_id_j = base64.base64Encode(hash.recursiveHash(i_aux_1_hashable, lVCC_id_j));

		final List<String> i_aux_2 = Stream.concat(i_aux_1.stream(), Stream.of(integerToString(attempts_id))).toList();

		final GroupVector<GqElement, GqGroup> bases = GroupVector.of(g, hCK_id);
		final GroupVector<GqElement, GqGroup> exponentiations = GroupVector.of(Kc_j_id, lVCC_id_j);
		final ExponentiationProof pi_expLVCC_j_id = zeroKnowledgeProof.genExponentiationProof(bases, kc_j_id, exponentiations, i_aux_2);

		// Corresponds to L_confirmationAttempts,j(vc_id) ← attempts_id + 1
		verificationCardStateService.incrementConfirmationAttempts(vc_id);

		// Output.
		return new CreateLVCCShareOutput(hCK_id, lVCC_id_j, hlVCC_id_j, Kc_j_id, pi_expLVCC_j_id, attempts_id);
	}
}
