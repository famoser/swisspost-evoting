/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public interface BallotBoxRepository extends CrudRepository<BallotBoxEntity, Long> {

	Optional<BallotBoxEntity> findByBallotBoxId(final String ballotBoxId);

	boolean existsByBallotBoxId(final String ballotBoxId);

	Optional<BallotBoxEntity> findByVerificationCardSetEntity(final VerificationCardSetEntity verificationCardSetEntity);

}
