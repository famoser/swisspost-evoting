/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;

import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTable;
import ch.post.it.evoting.cryptoprimitives.domain.election.VerificationCardSetContext;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;

@Service
public class BallotBoxService {

	private final ObjectMapper objectMapper;
	private final BallotBoxRepository ballotBoxRepository;
	private final VerificationCardSetService verificationCardSetService;

	public BallotBoxService(final ObjectMapper objectMapper, final BallotBoxRepository ballotBoxRepository,
			final VerificationCardSetService verificationCardSetService) {
		this.objectMapper = objectMapper;
		this.ballotBoxRepository = ballotBoxRepository;
		this.verificationCardSetService = verificationCardSetService;
	}

	@Transactional
	public BallotBoxEntity save(final String ballotBoxId, final String verificationCardSetId, final boolean testBallotBox,
			final int numberOfWriteInsPlusOne, final int numberOfVotingCards, final int gracePeriod, final PrimesMappingTable primesMappingTable) {
		validateUUID(ballotBoxId);
		validateUUID(verificationCardSetId);
		checkArgument(numberOfWriteInsPlusOne > 0);
		checkArgument(numberOfVotingCards > 0);
		checkArgument(gracePeriod > 0);
		checkNotNull(primesMappingTable);

		final byte[] primesMappingTableBytes;
		try {
			primesMappingTableBytes = objectMapper.writeValueAsBytes(primesMappingTable);
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException(
					String.format("Failed to serialize primes mapping table. [verificationCardSetId: %s]", verificationCardSetId), e);
		}

		final VerificationCardSetEntity verificationCardSetEntity = verificationCardSetService.getVerificationCardSet(verificationCardSetId);
		final BallotBoxEntity ballotBoxEntity = new BallotBoxEntity(ballotBoxId, verificationCardSetEntity, testBallotBox, numberOfWriteInsPlusOne,
				numberOfVotingCards, gracePeriod, primesMappingTableBytes);

		return ballotBoxRepository.save(ballotBoxEntity);
	}

	@Transactional
	public List<BallotBoxEntity> saveFromContexts(final List<VerificationCardSetContext> verificationCardSetContexts) {
		checkNotNull(verificationCardSetContexts);

		final List<BallotBoxEntity> ballotBoxEntities = verificationCardSetContexts.stream()
				.map(verificationCardSetContext -> {
					final String verificationCardSetId = verificationCardSetContext.verificationCardSetId();
					final VerificationCardSetEntity verificationCardSetEntity = verificationCardSetService.getVerificationCardSet(
							verificationCardSetId);

					final byte[] primesMappingTableBytes;
					try {
						primesMappingTableBytes = objectMapper.writeValueAsBytes(verificationCardSetContext.primesMappingTable());
					} catch (final JsonProcessingException e) {
						throw new UncheckedIOException(
								String.format("Failed to serialize primes mapping table. [verificationCardSetId: %s]", verificationCardSetId), e);
					}

					return new BallotBoxEntity(verificationCardSetContext.ballotBoxId(), verificationCardSetEntity,
							verificationCardSetContext.testBallotBox(), verificationCardSetContext.numberOfWriteInFields() + 1,
							verificationCardSetContext.numberOfVotingCards(), verificationCardSetContext.gracePeriod(), primesMappingTableBytes);
				}).toList();

		return Lists.newArrayList(ballotBoxRepository.saveAll(ballotBoxEntities));
	}

	@Transactional
	public boolean existsForElectionEventId(final String ballotBoxId, final String electionEventId) {
		validateUUID(ballotBoxId);
		validateUUID(electionEventId);

		if (!ballotBoxRepository.existsByBallotBoxId(ballotBoxId)) {
			return false;
		}
		final String ballotBoxElectionEventId = getBallotBox(ballotBoxId).getVerificationCardSetEntity().getElectionEventEntity()
				.getElectionEventId();
		return electionEventId.equals(ballotBoxElectionEventId);
	}

	@Transactional
	public BallotBoxEntity getBallotBox(final String ballotBoxId) {
		validateUUID(ballotBoxId);

		return ballotBoxRepository.findByBallotBoxId(ballotBoxId)
				.orElseThrow(() -> new IllegalStateException(String.format("Ballot box not found. [ballotBoxId: %s]", ballotBoxId)));
	}

	@Transactional
	public BallotBoxEntity getBallotBox(final VerificationCardSetEntity verificationCardSetEntity) {
		checkNotNull(verificationCardSetEntity);

		return ballotBoxRepository.findByVerificationCardSetEntity(verificationCardSetEntity)
				.orElseThrow(() -> new IllegalStateException(
						String.format("Ballot box not found. [verificationCardSetId: %s]", verificationCardSetEntity.getVerificationCardSetId())));
	}

	@Transactional
	public boolean isMixed(final String ballotBoxId) {
		validateUUID(ballotBoxId);

		final BallotBoxEntity ballotBoxEntity = getBallotBox(ballotBoxId);
		return ballotBoxEntity.isMixed();
	}

	@Transactional
	public BallotBoxEntity setMixed(final String ballotBoxId) {
		validateUUID(ballotBoxId);

		final BallotBoxEntity ballotBoxEntity = getBallotBox(ballotBoxId);
		ballotBoxEntity.setMixed(true);

		return ballotBoxRepository.save(ballotBoxEntity);
	}

	@Transactional
	public PrimesMappingTable getBallotBoxPrimesMappingTable(final VerificationCardSetEntity verificationCardSetEntity) {
		checkNotNull(verificationCardSetEntity);

		final BallotBoxEntity ballotBoxEntity = getBallotBox(verificationCardSetEntity);
		final GqGroup encryptionGroup = verificationCardSetEntity.getElectionEventEntity().getEncryptionGroup();
		final String ballotBoxId = ballotBoxEntity.getBallotBoxId();

		final byte[] primesMappingTableBytes = ballotBoxEntity.getPrimesMappingTable();
		try {
			return objectMapper.reader()
					.withAttribute("group", encryptionGroup)
					.readValue(primesMappingTableBytes, PrimesMappingTable.class);
		} catch (final IOException e) {
			throw new UncheckedIOException(String.format("Failed to deserialize primes mapping table. [ballotBoxId: %s]", ballotBoxId), e);
		}
	}

}
