/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;

@Repository
public class KeystoreRepository {

	private final String keystoreLocation;
	private final String keystorePasswordLocation;
	private final Alias alias;

	public KeystoreRepository(
			@Value("${direct.trust.keystore.location}")
			String keystoreLocation,
			@Value("${direct.trust.keystore.password.location}")
			String keystorePasswordLocation,
			@Value("${nodeID}")
			int nodeId) {
		this.keystoreLocation = keystoreLocation;
		this.keystorePasswordLocation = keystorePasswordLocation;
		this.alias = Alias.getByComponentName("control_component_" + nodeId);
	}

	public InputStream getKeyStore() throws IOException {
		return Files.newInputStream(Paths.get(keystoreLocation));
	}

	public char[] getKeystorePassword() throws IOException {
		return new String(Files.readAllBytes(Paths.get(keystorePasswordLocation)), StandardCharsets.UTF_8).toCharArray();
	}

	public Alias getKeystoreAlias() {
		return alias;
	}
}
