/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.controlcomponent.voting.SetupComponentPublicKeysEntity;
import ch.post.it.evoting.cryptoprimitives.domain.election.ControlComponentPublicKeys;
import ch.post.it.evoting.cryptoprimitives.domain.election.SetupComponentPublicKeys;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;

@Service
public class SetupComponentPublicKeysService {

	private static final String GROUP = "group";

	private final ObjectMapper objectMapper;
	private final ElectionEventService electionEventService;
	private final SetupComponentPublicKeysRepository setupComponentPublicKeysRepository;

	public SetupComponentPublicKeysService(
			final ObjectMapper objectMapper,
			final ElectionEventService electionEventService,
			final SetupComponentPublicKeysRepository setupComponentPublicKeysRepository) {
		this.objectMapper = objectMapper;
		this.electionEventService = electionEventService;
		this.setupComponentPublicKeysRepository = setupComponentPublicKeysRepository;
	}

	@Transactional
	public void save(final String electionEventId, final SetupComponentPublicKeys setupComponentPublicKeys) {
		checkNotNull(setupComponentPublicKeys);

		// Save setup component public keys entity.
		final ElectionEventEntity electionEventEntity = electionEventService.getElectionEventEntity(electionEventId);

		final byte[] serializedCombinedControlComponentPublicKeys;
		final byte[] serializedElectoralBoardPublicKey;
		final byte[] serializedElectoralBoardSchnorrProofs;
		final byte[] serializedElectionPublicKey;
		final byte[] serializedChoiceReturnCodesPublicKey;
		try {
			serializedCombinedControlComponentPublicKeys = objectMapper.writeValueAsBytes(
					setupComponentPublicKeys.combinedControlComponentPublicKeys());
			serializedElectoralBoardPublicKey = objectMapper.writeValueAsBytes(setupComponentPublicKeys.electoralBoardPublicKey());
			serializedElectoralBoardSchnorrProofs = objectMapper.writeValueAsBytes(setupComponentPublicKeys.electoralBoardSchnorrProofs());
			serializedElectionPublicKey = objectMapper.writeValueAsBytes(setupComponentPublicKeys.electionPublicKey());
			serializedChoiceReturnCodesPublicKey = objectMapper.writeValueAsBytes(setupComponentPublicKeys.choiceReturnCodesEncryptionPublicKey());

		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException("Failed to serialize setup component public keys.", e);
		}

		final SetupComponentPublicKeysEntity setupComponentPublicKeysEntity = new SetupComponentPublicKeysEntity.Builder()
				.setElectionEventEntity(electionEventEntity)
				.setCombinedControlComponentPublicKey(serializedCombinedControlComponentPublicKeys)
				.setElectoralBoardPublicKey(serializedElectoralBoardPublicKey)
				.setElectoralBoardSchnorrProofs(serializedElectoralBoardSchnorrProofs)
				.setElectionPublicKey(serializedElectionPublicKey)
				.setChoiceReturnCodesEncryptionPublicKey(serializedChoiceReturnCodesPublicKey)
				.build();
		setupComponentPublicKeysRepository.save(setupComponentPublicKeysEntity);
	}

	@Transactional
	public GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> getCcmElectionPublicKeys(final String electionEventId) {
		validateUUID(electionEventId);

		final List<ControlComponentPublicKeys> controlComponentPublicKeys = getCombinedControlComponentPublicKeys(electionEventId);
		return controlComponentPublicKeys.stream()
				.map(ControlComponentPublicKeys::ccmjElectionPublicKey)
				.collect(GroupVector.toGroupVector());
	}

	@Transactional
	public List<ControlComponentPublicKeys> getCombinedControlComponentPublicKeys(final String electionEventId) {
		validateUUID(electionEventId);

		final GqGroup encryptionGroup = electionEventService.getEncryptionGroup(electionEventId);
		final SetupComponentPublicKeysEntity setupComponentPublicKeysEntity = getSetupComponentPublicKeysEntity(electionEventId);
		final byte[] controlComponentsPublicKeysBytes = setupComponentPublicKeysEntity.getCombinedControlComponentPublicKeys();
		final ControlComponentPublicKeys[] controlComponentsPublicKeys;
		try {
			controlComponentsPublicKeys = objectMapper
					.reader().withAttribute(GROUP, encryptionGroup)
					.readValue(controlComponentsPublicKeysBytes, ControlComponentPublicKeys[].class);
		} catch (final IOException e) {
			throw new UncheckedIOException(
					String.format("Failed to deserialize combined control component public keys. [electionEventId: %s]", electionEventId), e);
		}
		return Arrays.asList(controlComponentsPublicKeys);
	}

	@Transactional
	public ElGamalMultiRecipientPublicKey getElectoralBoardPublicKey(final String electionEventId) {
		validateUUID(electionEventId);

		final GqGroup encryptionGroup = electionEventService.getEncryptionGroup(electionEventId);
		final SetupComponentPublicKeysEntity setupComponentPublicKeysEntity = getSetupComponentPublicKeysEntity(electionEventId);
		final byte[] electoralBoardPublicKeyBytes = setupComponentPublicKeysEntity.getElectoralBoardPublicKey();
		final ElGamalMultiRecipientPublicKey electoralBoardPublicKey;
		try {
			electoralBoardPublicKey = objectMapper.reader()
					.withAttribute(GROUP, encryptionGroup)
					.readValue(electoralBoardPublicKeyBytes, ElGamalMultiRecipientPublicKey.class);
		} catch (final IOException e) {
			throw new UncheckedIOException(String.format("Failed to deserialize electoral board public key. [electionEventId: %s]", electionEventId),
					e);
		}
		return electoralBoardPublicKey;
	}

	@Transactional
	public ElGamalMultiRecipientPublicKey getElectionPublicKey(final String electionEventId) {
		validateUUID(electionEventId);

		final GqGroup encryptionGroup = electionEventService.getEncryptionGroup(electionEventId);
		final SetupComponentPublicKeysEntity setupComponentPublicKeysEntity = getSetupComponentPublicKeysEntity(electionEventId);
		final byte[] electionPublicKeyBytes = setupComponentPublicKeysEntity.getElectionPublicKey();
		final ElGamalMultiRecipientPublicKey electionPublicKey;
		try {
			electionPublicKey = objectMapper.reader()
					.withAttribute(GROUP, encryptionGroup)
					.readValue(electionPublicKeyBytes, ElGamalMultiRecipientPublicKey.class);
		} catch (final IOException e) {
			throw new UncheckedIOException(String.format("Failed to deserialize election public key. [electionEventId: %s]", electionEventId), e);
		}
		return electionPublicKey;
	}

	@Transactional
	public ElGamalMultiRecipientPublicKey getChoiceReturnCodesEncryptionPublicKey(final String electionEventId) {
		validateUUID(electionEventId);

		final GqGroup encryptionGroup = electionEventService.getEncryptionGroup(electionEventId);
		final SetupComponentPublicKeysEntity setupComponentPublicKeysEntity = getSetupComponentPublicKeysEntity(electionEventId);
		final byte[] choiceReturnCodesEncryptionPublicKeyBytes = setupComponentPublicKeysEntity.getChoiceReturnCodesEncryptionPublicKey();
		final ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey;
		try {
			choiceReturnCodesEncryptionPublicKey = objectMapper.reader()
					.withAttribute(GROUP, encryptionGroup)
					.readValue(choiceReturnCodesEncryptionPublicKeyBytes, ElGamalMultiRecipientPublicKey.class);
		} catch (final IOException e) {
			throw new UncheckedIOException(
					String.format("Failed to deserialize choice return codes encryption public key. [electionEventId: %s]", electionEventId), e);
		}
		return choiceReturnCodesEncryptionPublicKey;
	}

	@Transactional
	public SetupComponentPublicKeysEntity getSetupComponentPublicKeysEntity(final String electionEventId) {
		validateUUID(electionEventId);

		final Optional<SetupComponentPublicKeysEntity> setupComponentPublicKeysEntity = setupComponentPublicKeysRepository.findByElectionEventId(
				electionEventId);

		return setupComponentPublicKeysEntity.orElseThrow(
				() -> new IllegalStateException(
						String.format("Setup component public keys entity not found. [electionEventId: %s]", electionEventId)));
	}
}
