/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.voting.confirmvote;

import static ch.post.it.evoting.cryptoprimitives.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateBase64Encoded;
import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;
import java.util.stream.Collectors;

import ch.post.it.evoting.controlcomponent.LongVoteCastReturnCodesAllowList;
import ch.post.it.evoting.cryptoprimitives.domain.validations.Validations;

/**
 * Regroups the inputs needed by the VerifyLVCCHash<sub>j</sub> algorithm.
 *
 * <ul>
 * <li>L<sub>lVCC</sub>, the long Vote Case Return Codes allow list. Not null and not empty.</li>
 * <li>hlVCC<sub>id,j</sub>, the CCRj's hashed long Vote Cast Return Code share. Not null.</li>
 * <li>(hlVCC<sub>id, j_1_hat</sub>, hlVCC<sub>id, j_2_hat</sub>, hlVCC<sub>id, j_3_hat</sub>), the other CCRj's hashed long Vote Cast Return Code shares. Not null.</li>
 * <li>vc_id, the verification card id. Not null and a valid UUID.</li>
 * </ul>
 */
public class VerifyLVCCHashInput {

	private final LongVoteCastReturnCodesAllowList longVoteCastReturnCodesAllowList;
	private final String ccrjHashedLongVoteCastReturnCode;
	private final List<String> otherCCRsHashedLongVoteCastReturnCodes;
	private final String verificationCardId;

	private VerifyLVCCHashInput(
			final LongVoteCastReturnCodesAllowList longVoteCastReturnCodesAllowList,
			final String ccrjHashedLongVoteCastReturnCode,
			final List<String> otherCCRsHashedLongVoteCastReturnCodes,
			final String verificationCardId) {
		this.longVoteCastReturnCodesAllowList = longVoteCastReturnCodesAllowList;
		this.ccrjHashedLongVoteCastReturnCode = ccrjHashedLongVoteCastReturnCode;
		this.otherCCRsHashedLongVoteCastReturnCodes = otherCCRsHashedLongVoteCastReturnCodes;
		this.verificationCardId = verificationCardId;
	}

	LongVoteCastReturnCodesAllowList getLongVoteCastReturnCodesAllowList() {
		return longVoteCastReturnCodesAllowList;
	}

	String getCcrjHashedLongVoteCastReturnCode() {
		return ccrjHashedLongVoteCastReturnCode;
	}

	List<String> getOtherCCRsHashedLongVoteCastReturnCodes() {
		return List.copyOf(otherCCRsHashedLongVoteCastReturnCodes);
	}

	String getVerificationCardId() {
		return verificationCardId;
	}

	public static class Builder {
		private LongVoteCastReturnCodesAllowList longVoteCastReturnCodesAllowList;
		private String ccrjHashedLongVoteCastReturnCode;
		private List<String> otherCCRsHashedLongVoteCastReturnCodes;
		private String verificationCardId;

		public Builder setLongVoteCastReturnCodesAllowList(final LongVoteCastReturnCodesAllowList longVoteCastReturnCodesAllowList) {
			this.longVoteCastReturnCodesAllowList = checkNotNull(longVoteCastReturnCodesAllowList);
			return this;
		}

		public Builder setCcrjHashedLongVoteCastReturnCode(final String ccrjHashedLongVoteCastReturnCode) {
			this.ccrjHashedLongVoteCastReturnCode = ccrjHashedLongVoteCastReturnCode;
			return this;
		}

		public Builder setOtherCCRsHashedLongVoteCastReturnCodes(final List<String> otherCCRsHashedLongVoteCastReturnCodes) {
			this.otherCCRsHashedLongVoteCastReturnCodes = List.copyOf(checkNotNull(otherCCRsHashedLongVoteCastReturnCodes));
			return this;
		}

		public Builder setVerificationCardId(final String verificationCardId) {
			this.verificationCardId = verificationCardId;
			return this;
		}

		public VerifyLVCCHashInput build() {
			checkNotNull(longVoteCastReturnCodesAllowList);
			checkNotNull(ccrjHashedLongVoteCastReturnCode);
			checkNotNull(otherCCRsHashedLongVoteCastReturnCodes);
			validateUUID(verificationCardId);

			// Size checks.
			checkArgument(this.otherCCRsHashedLongVoteCastReturnCodes.size() == NODE_IDS.size() - 1,
					"There number of other CCRs hashed long Vote Cast Return Codes must be equal to the number of known node ids - 1.");

			// Cross-size checks.
			checkArgument(otherCCRsHashedLongVoteCastReturnCodes.stream().map(String::length).collect(Collectors.toSet()).size() == 1,
					"The length of all the other CCRj hashed long Vote Cast Return Code shares must be equal.");

			// Base64 checks.
			validateBase64Encoded(this.ccrjHashedLongVoteCastReturnCode);
			this.otherCCRsHashedLongVoteCastReturnCodes.forEach(Validations::validateBase64Encoded);

			return new VerifyLVCCHashInput(longVoteCastReturnCodesAllowList, ccrjHashedLongVoteCastReturnCode, otherCCRsHashedLongVoteCastReturnCodes,
					verificationCardId);
		}
	}

}
