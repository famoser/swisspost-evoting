/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.configuration.setupvoting;

import static ch.post.it.evoting.controlcomponent.ControlComponentsApplicationBootstrap.RABBITMQ_EXCHANGE;
import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateBase64Encoded;
import static ch.post.it.evoting.domain.SharedQueue.GEN_ENC_LONG_CODE_SHARES_REQUEST_PATTERN;
import static ch.post.it.evoting.domain.SharedQueue.GEN_ENC_LONG_CODE_SHARES_RESPONSE_PATTERN;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

import javax.annotation.PostConstruct;
import javax.persistence.PersistenceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.retry.RetryContext;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.retry.support.RetrySynchronizationManager;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.commandmessaging.Context;
import ch.post.it.evoting.controlcomponent.CcrjReturnCodesKeys;
import ch.post.it.evoting.controlcomponent.CcrjReturnCodesKeysService;
import ch.post.it.evoting.controlcomponent.ElectionEventService;
import ch.post.it.evoting.controlcomponent.ExactlyOnceCommand;
import ch.post.it.evoting.controlcomponent.ExactlyOnceCommandExecutor;
import ch.post.it.evoting.controlcomponent.Messages;
import ch.post.it.evoting.controlcomponent.PCCAllowListEntryEntity;
import ch.post.it.evoting.controlcomponent.VerificationCardSetEntity;
import ch.post.it.evoting.controlcomponent.VerificationCardSetService;
import ch.post.it.evoting.cryptoprimitives.domain.election.CombinedCorrectnessInformation;
import ch.post.it.evoting.cryptoprimitives.domain.returncodes.ControlComponentCodeShare;
import ch.post.it.evoting.cryptoprimitives.domain.returncodes.ControlComponentCodeSharesPayload;
import ch.post.it.evoting.cryptoprimitives.domain.returncodes.SetupComponentVerificationData;
import ch.post.it.evoting.cryptoprimitives.domain.returncodes.SetupComponentVerificationDataPayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;

@Service
public class GenEncLongCodeSharesProcessor {

	private static final Logger LOGGER = LoggerFactory.getLogger(GenEncLongCodeSharesProcessor.class);

	@SuppressWarnings("java:S115")
	private static final int l_HB64 = 44;

	private final RabbitTemplate rabbitTemplate;
	private final SignatureKeystore<Alias> signatureKeystoreService;
	private final ObjectMapper objectMapper;
	private final CcrjReturnCodesKeysService ccrjReturnCodesKeysService;
	private final GenEncLongCodeSharesAlgorithm genEncLongCodeSharesAlgorithm;
	private final ExactlyOnceCommandExecutor exactlyOnceCommandExecutor;
	private final ElectionEventService electionEventService;
	private final VerificationCardSetService verificationCardSetService;
	private final PCCAllowListEntryService pccAllowListEntryService;

	private String responseQueue;
	@Value("${nodeID}")
	private int nodeId;

	public GenEncLongCodeSharesProcessor(
			final RabbitTemplate rabbitTemplate,
			final SignatureKeystore<Alias> signatureKeystoreService,
			final ObjectMapper objectMapper,
			final CcrjReturnCodesKeysService ccrjReturnCodesKeysService,
			final GenEncLongCodeSharesAlgorithm genEncLongCodeSharesAlgorithm,
			final ExactlyOnceCommandExecutor exactlyOnceCommandExecutor,
			final ElectionEventService electionEventService,
			final VerificationCardSetService verificationCardSetService,
			final PCCAllowListEntryService pccAllowListEntryService) {
		this.rabbitTemplate = rabbitTemplate;
		this.signatureKeystoreService = signatureKeystoreService;
		this.objectMapper = objectMapper;
		this.ccrjReturnCodesKeysService = ccrjReturnCodesKeysService;
		this.genEncLongCodeSharesAlgorithm = genEncLongCodeSharesAlgorithm;
		this.exactlyOnceCommandExecutor = exactlyOnceCommandExecutor;
		this.electionEventService = electionEventService;
		this.verificationCardSetService = verificationCardSetService;
		this.pccAllowListEntryService = pccAllowListEntryService;
	}

	@PostConstruct
	public void initQueue() {
		responseQueue = String.format("%s%s", GEN_ENC_LONG_CODE_SHARES_RESPONSE_PATTERN, nodeId);
	}

	/**
	 * Since the request is treated per chunk, it is possible that multiple chunks try to create the initial verification card set. In that case, only
	 * one will succeed and the other retry once. When retrying, the verification card set will be created, and they can fetch it.
	 */
	@Retryable(value = PersistenceException.class, maxAttempts = 2, backoff = @Backoff(delay = 1000, multiplier = 2, maxDelay = 10000, random = true))
	@RabbitListener(queues = GEN_ENC_LONG_CODE_SHARES_REQUEST_PATTERN + "${nodeID}", autoStartup = "false", concurrency = "4")
	public void onMessage(final Message message) throws IOException {
		checkNotNull(message);
		checkNotNull(message.getMessageProperties());

		final String correlationId = checkNotNull(message.getMessageProperties().getCorrelationId());
		final byte[] messageBytes = checkNotNull(message.getBody());

		final SetupComponentVerificationDataPayload payload = objectMapper.readValue(messageBytes, SetupComponentVerificationDataPayload.class);
		validateSignature(payload);

		final String electionEventId = payload.getElectionEventId();
		final String verificationCardSetId = payload.getVerificationCardSetId();
		// We do not check the existence of the verificationCardSetID since this information is not yet stored in the control component's database.
		final int chunkId = payload.getChunkId();
		final String contextId = String.join("-", electionEventId, verificationCardSetId, String.valueOf(chunkId));

		LOGGER.info("Received GenEncLongCodeShares request. [contextId: {}, correlationId: {}, chunkId: {}]", contextId, correlationId, chunkId);

		final RetryContext retryContext = RetrySynchronizationManager.getContext();
		final int retryNumber = retryContext.getRetryCount();
		if (retryNumber != 0) {
			LOGGER.warn(
					"Above SQL Error 23505 is expected. Retrying operation because multiple chunks tried to create the initial verification card set at the same time. "
							+ "[retryNumber: {}, chunkId: {}]", retryNumber, chunkId);
		}

		final ExactlyOnceCommand exactlyOnceCommand = new ExactlyOnceCommand.Builder()
				.setCorrelationId(correlationId)
				.setContextId(contextId)
				.setContext(Context.CONFIGURATION_RETURN_CODES_GEN_ENC_LONG_CODE_SHARES.toString())
				.setTask(() -> genEncLongCodeSharesPayload(payload, electionEventId, verificationCardSetId, chunkId))
				.setRequestContent(messageBytes).build();

		final byte[] payloadBytes = exactlyOnceCommandExecutor.process(exactlyOnceCommand);

		final Message responseMessage = Messages.createMessage(correlationId, payloadBytes);

		rabbitTemplate.send(RABBITMQ_EXCHANGE, responseQueue, responseMessage);

		LOGGER.info("GenEncLongCodeShares response sent. [contextId: {}, correlationId: {}]", contextId, correlationId);
	}

	private byte[] genEncLongCodeSharesPayload(final SetupComponentVerificationDataPayload payload, final String electionEventId,
			final String verificationCardSetId, final int chunkId) {
		checkArgument(electionEventService.exists(electionEventId), "The given election event ID does not exist. [electionEventId: %s]",
				electionEventId);
		checkArgument(chunkId == payload.getChunkId(), "The given payload does not correspond to the given chunkId");

		final CcrjReturnCodesKeys ccrjReturnCodesKeys = ccrjReturnCodesKeysService.getCcrjReturnCodesKeys(electionEventId);
		final GqGroup gqGroup = ccrjReturnCodesKeys.ccrjChoiceReturnCodesEncryptionKeyPair().getGroup();

		// Sanity check the partial Choice Return Codes allow list before saving.
		final List<String> payloadValidatedAllowList = validateAllowList(payload);
		final CombinedCorrectnessInformation combinedCorrectnessInformation = payload.getCombinedCorrectnessInformation();

		// Save verification card set if not existing.
		final VerificationCardSetEntity verificationCardSetEntity = verificationCardSetService.getOrCreateVerificationCardSet(electionEventId,
				verificationCardSetId, combinedCorrectnessInformation);

		// Save allow list chunk.
		final List<PCCAllowListEntryEntity> partialChoiceReturnCodeAllowList = payloadValidatedAllowList.stream()
				.map(partialChoiceCode -> new PCCAllowListEntryEntity(verificationCardSetEntity, partialChoiceCode, chunkId)).toList();
		pccAllowListEntryService.saveAll(partialChoiceReturnCodeAllowList);
		LOGGER.info("AllowList saved chunk. [electionEventId: {}, verificationCardSetId: {}, chunkId: {}]", electionEventId, verificationCardSetId,
				chunkId);

		final GenEncLongCodeSharesContext genEncLongCodeSharesContext = new GenEncLongCodeSharesContext.Builder()
				.setElectionEventId(electionEventId)
				.setVerificationCardSetId(verificationCardSetId)
				.setEncryptionGroup(gqGroup)
				.setNodeId(nodeId)
				.setNumberOfVotingOptions(combinedCorrectnessInformation.getTotalNumberOfVotingOptions())
				.build();

		final GenEncLongCodeSharesInput genEncLongCodeSharesInput = new GenEncLongCodeSharesInput.Builder()
				.returnCodesGenerationSecretKey(ccrjReturnCodesKeys.ccrjReturnCodesGenerationSecretKey())
				.verificationCardIds(getVerificationCardIDs(payload))
				.verificationCardPublicKeys(getVerificationCardPublicKeys(payload))
				.encryptedHashedPartialChoiceReturnCodes(getEncryptedHashedPartialChoiceReturnCodes(payload))
				.encryptedHashedConfirmationKeys(getEncryptedHashedConfirmationKeys(payload))
				.build();

		final GenEncLongCodeSharesOutput genEncLongCodeSharesOutput = genEncLongCodeSharesAlgorithm.genEncLongCodeShares(genEncLongCodeSharesContext,
				genEncLongCodeSharesInput);

		final ControlComponentCodeSharesPayload controlComponentCodeSharesPayload =
				toControlComponentCodeSharesPayload(electionEventId, verificationCardSetId, gqGroup, payload, genEncLongCodeSharesOutput);

		try {
			return objectMapper.writeValueAsBytes(controlComponentCodeSharesPayload);
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException(String.format(
					"Could not serialize EncLongCodeShares response payload. [electionEventId: %s, verificationCardSetId: %s,  chunkId: %s]",
					electionEventId, verificationCardSetId, chunkId), e);
		}
	}

	private ControlComponentCodeSharesPayload toControlComponentCodeSharesPayload(final String electionEventId, final String verificationCardSetId,
			final GqGroup gqGroup, final SetupComponentVerificationDataPayload payload, final GenEncLongCodeSharesOutput output) {

		final List<String> vc = getVerificationCardIDs(payload);

		checkArgument(vc.size() == output.getExponentiatedEncryptedHashedPartialChoiceReturnCodes().size());

		final List<ControlComponentCodeShare> returnCodeGenerationOutputs =
				IntStream.range(0, output.getExponentiatedEncryptedHashedPartialChoiceReturnCodes().size())
						.parallel()
						.mapToObj(id -> {
							final String vc_id = vc.get(id);
							final GqElement voterChoiceReturnCodeGenerationPublicKeys = output.getVoterChoiceReturnCodeGenerationPublicKeys().get(id);
							final GqElement voterVoteCastReturnCodeGenerationPublicKeys = output.getVoterVoteCastReturnCodeGenerationPublicKeys()
									.get(id);
							final ElGamalMultiRecipientCiphertext exponentiatedEncryptedHashedPartialChoiceReturnCodes =
									output.getExponentiatedEncryptedHashedPartialChoiceReturnCodes().get(id);
							final ExponentiationProof proofsCorrectExponentiationPartialChoiceReturnCodes =
									output.getProofsCorrectExponentiationPartialChoiceReturnCodes().get(id);
							final ElGamalMultiRecipientCiphertext exponentiatedEncryptedHashedConfirmationKeys =
									output.getExponentiatedEncryptedHashedConfirmationKeys().get(id);
							final ExponentiationProof proofsCorrectExponentiationConfirmationKeys =
									output.getProofsCorrectExponentiationConfirmationKeys().get(id);

							return new ControlComponentCodeShare(
									vc_id,
									new ElGamalMultiRecipientPublicKey(GroupVector.of(voterChoiceReturnCodeGenerationPublicKeys)),
									new ElGamalMultiRecipientPublicKey(GroupVector.of(voterVoteCastReturnCodeGenerationPublicKeys)),
									exponentiatedEncryptedHashedPartialChoiceReturnCodes,
									proofsCorrectExponentiationPartialChoiceReturnCodes,
									exponentiatedEncryptedHashedConfirmationKeys,
									proofsCorrectExponentiationConfirmationKeys);
						})
						.toList();

		final ControlComponentCodeSharesPayload controlComponentCodeSharesPayload =
				new ControlComponentCodeSharesPayload(electionEventId, verificationCardSetId, payload.getChunkId(), gqGroup,
						returnCodeGenerationOutputs, nodeId);

		final CryptoPrimitivesSignature controlComponentCodeSharesPayloadSignature = getPayloadSignature(controlComponentCodeSharesPayload);
		controlComponentCodeSharesPayload.setSignature(controlComponentCodeSharesPayloadSignature);
		LOGGER.info("Successfully signed control component code shares payload [nodeId: {}, electionEventId: {}, verificationCardSetId: {}]", nodeId,
				electionEventId, verificationCardSetId);

		return controlComponentCodeSharesPayload;
	}

	private List<String> getVerificationCardIDs(final SetupComponentVerificationDataPayload payload) {
		final List<SetupComponentVerificationData> returnCodeGenerationInputs = payload.getSetupComponentVerificationData();

		return returnCodeGenerationInputs.stream()
				.parallel()
				.map(SetupComponentVerificationData::verificationCardId)
				.toList();
	}

	private GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> getVerificationCardPublicKeys(final SetupComponentVerificationDataPayload payload) {
		final List<SetupComponentVerificationData> returnCodeGenerationInputs = payload.getSetupComponentVerificationData();

		return returnCodeGenerationInputs.stream().map(SetupComponentVerificationData::verificationCardPublicKey)
				.collect(GroupVector.toGroupVector());
	}

	private GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> getEncryptedHashedPartialChoiceReturnCodes(
			final SetupComponentVerificationDataPayload payload) {
		final List<SetupComponentVerificationData> returnCodeGenerationInputs = payload.getSetupComponentVerificationData();

		return returnCodeGenerationInputs.stream().map(SetupComponentVerificationData::encryptedHashedSquaredPartialChoiceReturnCodes)
				.collect(GroupVector.toGroupVector());
	}

	private GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> getEncryptedHashedConfirmationKeys(
			final SetupComponentVerificationDataPayload payload) {
		final List<SetupComponentVerificationData> returnCodeGenerationInputs = payload.getSetupComponentVerificationData();

		return returnCodeGenerationInputs.stream().map(SetupComponentVerificationData::encryptedHashedSquaredConfirmationKey)
				.collect(GroupVector.toGroupVector());
	}

	private void validateSignature(final SetupComponentVerificationDataPayload payload) {
		final String electionEventId = payload.getElectionEventId();
		final String verificationCardSetId = payload.getVerificationCardSetId();
		final String payloadId = String.format("[electionEventId: %s, verificationCardSetId: %s, chunkID: %s, nodeId: %s]",
				electionEventId, verificationCardSetId, payload.getChunkId(), nodeId);

		final CryptoPrimitivesSignature signature = payload.getSignature();
		checkState(signature != null, "The signature of the setup component verification data payload is null. %s", payloadId);

		final Hashable additionalContextData = ChannelSecurityContextData.setupComponentVerificationData(electionEventId,
				verificationCardSetId);

		LOGGER.info("Checking the signature of payload... {}", payloadId);

		final boolean isSignatureValid;
		try {
			isSignatureValid = signatureKeystoreService.verifySignature(Alias.SDM_CONFIG, payload,
					additionalContextData,
					signature.signatureContents());
		} catch (final SignatureException e) {
			throw new IllegalStateException(String.format("Unable to verify the setup component verification data payload signature. %s", payloadId),
					e);
		}

		if (!isSignatureValid) {
			throw new InvalidPayloadSignatureException(SetupComponentVerificationDataPayload.class, payloadId);
		}

		LOGGER.info("Successfully verified the signature of the setup component verification data payload. {}", payloadId);
	}

	private List<String> validateAllowList(final SetupComponentVerificationDataPayload payload) {
		final List<String> payloadAllowList = payload.getPartialChoiceReturnCodesAllowList();
		final int totalNumberOfVotingOptions = payload.getCombinedCorrectnessInformation().getTotalNumberOfVotingOptions();
		final int numberOfVotingCards = payload.getSetupComponentVerificationData().size();
		checkArgument(totalNumberOfVotingOptions * numberOfVotingCards == payloadAllowList.size(), String.format(
				"The total number of voting options times the number of voting cards must be equal to the size of the partial Choice Return Codes allow list. [voting options: %s, voting cards: %s, allow list: %s]",
				totalNumberOfVotingOptions, numberOfVotingCards, payloadAllowList.size()));
		payloadAllowList.stream()
				.parallel()
				.forEach(element -> checkArgument(validateBase64Encoded(element).length() == l_HB64, String.format(
				"At least one element in the partial Choice Return Codes allow list has incorrect length. [element: %s, allowed length: %s]", element,
				l_HB64)));
		final ArrayList<String> payloadAllowListCopy = new ArrayList<>(payloadAllowList);
		Collections.sort(payloadAllowListCopy);
		checkArgument(payloadAllowList.equals(payloadAllowListCopy), "The allow list is not lexicographically sorted.");

		return payloadAllowList;
	}

	private CryptoPrimitivesSignature getPayloadSignature(final ControlComponentCodeSharesPayload payload) {
		final String electionEventId = payload.getElectionEventId();
		final String verificationCardSetId = payload.getVerificationCardSetId();

		final Hashable additionalContextData = ChannelSecurityContextData.controlComponentCodeShares(nodeId, electionEventId, verificationCardSetId);

		try {
			final byte[] signature = signatureKeystoreService.generateSignature(payload, additionalContextData);

			return new CryptoPrimitivesSignature(signature);
		} catch (final SignatureException se) {
			final String message = String.format(
					"Failed to generate the control component code shares payload signature [nodeId: %s, electionEventId: %s, verificationCardSetId: %s]",
					nodeId, electionEventId, verificationCardSetId);
			throw new IllegalStateException(message, se);
		}
	}
}
