/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.configuration.setupvoting;

import static ch.post.it.evoting.controlcomponent.configuration.setupvoting.GenKeysCCRAlgorithm.GenKeysCCROutput;
import static ch.post.it.evoting.cryptoprimitives.domain.VotingOptionsConstants.MAXIMUM_NUMBER_OF_SELECTABLE_VOTING_OPTIONS;
import static ch.post.it.evoting.cryptoprimitives.math.GroupVector.toGroupVector;
import static ch.post.it.evoting.cryptoprimitives.utils.Conversions.integerToString;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.IntStream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientKeyPair;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ZqGroupGenerator;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.SchnorrProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ZeroKnowledgeProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ZeroKnowledgeProofFactory;

@DisplayName("A GenKeysCCRService")
class GenKeysCCRServiceTest {

	private static final int PHI = MAXIMUM_NUMBER_OF_SELECTABLE_VOTING_OPTIONS;
	private static final int NODE_ID = 1;
	private static final String ELECTION_EVENT_ID = RandomFactory.createRandom().genRandomBase16String(32).toLowerCase(Locale.ENGLISH);

	private static GqGroup encryptionGroup;
	private static ZeroKnowledgeProof zeroKnowledgeProof;
	private static GenKeysCCRAlgorithm genKeysCCRAlgorithm;

	@BeforeAll
	static void setUpAll() {
		zeroKnowledgeProof = ZeroKnowledgeProofFactory.createZeroKnowledgeProof();
		genKeysCCRAlgorithm = new GenKeysCCRAlgorithm(NODE_ID, RandomFactory.createRandom(), zeroKnowledgeProof);
		encryptionGroup = GroupTestData.getLargeGqGroup();
	}

	@Test
	@DisplayName("valid parameter does not throw")
	void validParamDoesNotThrow() {
		assertDoesNotThrow(() -> genKeysCCRAlgorithm.genKeysCCR(encryptionGroup, ELECTION_EVENT_ID));
	}

	@Test
	@DisplayName("null encryption group throws NullPointerException")
	void nullEncryptionGroupThrows() {
		assertThrows(NullPointerException.class, () -> genKeysCCRAlgorithm.genKeysCCR(null, ELECTION_EVENT_ID));
	}

	@Test
	@DisplayName("invalid election event id throws FailedValidationException")
	void invalidElectionEventId() {
		assertThrows(FailedValidationException.class, () -> genKeysCCRAlgorithm.genKeysCCR(encryptionGroup, "123"));
	}

	@Nested
	@DisplayName("A GenKeysCCROutput built with")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class GenKeysCCROutputTest {

		private final Random random = RandomFactory.createRandom();

		private ZqElement generationSecretKey;
		private ElGamalMultiRecipientKeyPair keyPair;
		private GroupVector<SchnorrProof, ZqGroup> schnorrProofs;

		@BeforeAll
		void setUpAll() {
			keyPair = ElGamalMultiRecipientKeyPair.genKeyPair(encryptionGroup, PHI, random);
			final List<String> i_aux = Arrays.asList(ELECTION_EVENT_ID, "GenKeyCCR", integerToString(NODE_ID));
			schnorrProofs = IntStream.range(0, keyPair.size())
					.mapToObj(i -> {
						final ZqElement EL_sk_j_i = keyPair.getPrivateKey().get(i);
						final GqElement EL_pk_j_i = keyPair.getPublicKey().get(i);
						return zeroKnowledgeProof.genSchnorrProof(EL_sk_j_i, EL_pk_j_i, i_aux);
					}).collect(toGroupVector());

			generationSecretKey = new ZqGroupGenerator(ZqGroup.sameOrderAs(encryptionGroup)).genRandomZqElementMember();
		}

		@Test
		@DisplayName("valid param gives expected output")
		void expectOutput() {
			final GenKeysCCROutput genKeysCCROutput = new GenKeysCCROutput(keyPair, generationSecretKey, schnorrProofs);

			final GqGroup keyPairGroup = genKeysCCROutput.ccrjChoiceReturnCodesEncryptionKeyPair().getGroup();
			final ZqGroup generationKeyGroup = genKeysCCROutput.ccrjReturnCodesGenerationSecretKey().getGroup();
			assertTrue(keyPairGroup.hasSameOrderAs(generationKeyGroup));
		}

		@Test
		@DisplayName("any null parameter throws NullPointerException")
		void nullParamThrows() {
			assertThrows(NullPointerException.class, () -> new GenKeysCCROutput(null, generationSecretKey, schnorrProofs));
			assertThrows(NullPointerException.class, () -> new GenKeysCCROutput(keyPair, null, schnorrProofs));
			assertThrows(NullPointerException.class, () -> new GenKeysCCROutput(keyPair, generationSecretKey, null));
		}

		@Test
		@DisplayName("wrong size keypair throws IllegalArgumentException")
		void wrongSizeKeyPairThrows() {
			final ElGamalMultiRecipientKeyPair wrongSizeKeyPair = ElGamalMultiRecipientKeyPair.genKeyPair(encryptionGroup, 1, random);

			final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
					() -> new GenKeysCCROutput(wrongSizeKeyPair, generationSecretKey, schnorrProofs));
			final String message = String.format("The ccrj Choice Return Codes encryption key pair must be of size phi. [phi: %s]", PHI);
			assertEquals(message, Throwables.getRootCause(exception).getMessage());
		}

		@Test
		@DisplayName("wrong size Schnorr proofs throws IllegalArgumentException")
		void wrongSizeSchnorrProofs() {
			final GroupVector<SchnorrProof, ZqGroup> wrongSizeSchnorrProofs = schnorrProofs.stream()
					.limit(1)
					.collect(toGroupVector());

			final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
					() -> new GenKeysCCROutput(keyPair, generationSecretKey, wrongSizeSchnorrProofs));
			final String message = String.format("There must be phi Schnorr proofs. [phi: %s]", PHI);
			assertEquals(message, Throwables.getRootCause(exception).getMessage());
		}

		@Test
		@DisplayName("wrong group generation secret key throws IllegalArgumentException")
		void wrongGroupGenerationSecretKeyThrows() {
			final GqGroup otherGroup = GroupTestData.getDifferentGqGroup(encryptionGroup);
			final ZqElement otherGenerationKey = new ZqGroupGenerator(ZqGroup.sameOrderAs(otherGroup)).genRandomZqElementMember();

			final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
					() -> new GenKeysCCROutput(keyPair, otherGenerationKey, schnorrProofs));
			assertEquals("The ccrj Return Codes generation secret key must have the same order than the ccr Choice Return Codes encryption key pair.",
					Throwables.getRootCause(exception).getMessage());
		}

		@Test
		@DisplayName("wrong group Schnorr proofs throws IllegalArgumentException")
		void wrongGroupSchnorrProofs() {
			final GqGroup otherGroup = GroupTestData.getDifferentGqGroup(encryptionGroup);
			final ElGamalMultiRecipientKeyPair otherKeyPair = ElGamalMultiRecipientKeyPair.genKeyPair(otherGroup, PHI, random);
			final ZqElement otherGenerationKey = new ZqGroupGenerator(ZqGroup.sameOrderAs(otherGroup)).genRandomZqElementMember();

			final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
					() -> new GenKeysCCROutput(otherKeyPair, otherGenerationKey, schnorrProofs));
			assertEquals("The Schnorr proofs must have the same group order as the ccr Choice Return Codes encryption key pair.",
					Throwables.getRootCause(exception).getMessage());
		}

	}

}
