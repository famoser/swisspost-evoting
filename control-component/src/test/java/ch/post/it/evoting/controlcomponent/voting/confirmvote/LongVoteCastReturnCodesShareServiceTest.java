/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */

package ch.post.it.evoting.controlcomponent.voting.confirmvote;

import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;

import org.assertj.core.api.ThrowableAssert.ThrowingCallable;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import ch.post.it.evoting.controlcomponent.ElectionContextEntity;
import ch.post.it.evoting.controlcomponent.ElectionContextService;
import ch.post.it.evoting.controlcomponent.VerificationCardService;
import ch.post.it.evoting.controlcomponent.VerificationCardSetEntity;
import ch.post.it.evoting.controlcomponent.BallotBoxEntity;
import ch.post.it.evoting.controlcomponent.BallotBoxService;

@ExtendWith(MockitoExtension.class)
class LongVoteCastReturnCodesShareServiceTest {

	@Mock(answer = Answers.RETURNS_DEEP_STUBS)
	private VerificationCardService verificationCardService;
	@Mock
	private ElectionContextService electionContextService;
	@Mock
	private BallotBoxService ballotBoxService;
	@InjectMocks
	private LongVoteCastReturnCodesShareService longVoteCastReturnCodesShareService;

	@Nested
	class validateConfirmationIsAllowed {
		private static final String ANY_ID = "53";
		private static final int GRACE_PERIOD = 3600;
		private BallotBoxEntity ballotBoxEntity;
		private LocalDateTime electionStartTime;
		private LocalDateTime electionEndTime;
		private LocalDateTime currentTime;

		@BeforeEach
		void setUp() {
			currentTime = LocalDateTime.now();
			electionStartTime = currentTime.minusSeconds(GRACE_PERIOD);
			electionEndTime = currentTime.plusSeconds(GRACE_PERIOD);
			ballotBoxEntity = mock(BallotBoxEntity.class);
			final ElectionContextEntity electionContextEntity = mock(ElectionContextEntity.class);

			when(electionContextEntity.getStartTime()).thenReturn(electionStartTime);
			when(electionContextEntity.getFinishTime()).thenReturn(electionEndTime);
			when(ballotBoxService.getBallotBox(any(VerificationCardSetEntity.class))).thenReturn(ballotBoxEntity);
			when(electionContextService.getElectionContextEntity(anyString())).thenReturn(electionContextEntity);
			when(verificationCardService.getVerificationCardEntity(anyString()).getVerificationCardSetEntity()).thenReturn(
					new VerificationCardSetEntity());
		}

		@Test
		@DisplayName("Voting at the start of the election event works.")
		void votingAtTheBeginOfElectionWorks() {
			// given
			when(ballotBoxEntity.isMixed()).thenReturn(false);
			when(ballotBoxEntity.getGracePeriod()).thenReturn(GRACE_PERIOD);

			// when
			final ThrowingCallable validation = () -> longVoteCastReturnCodesShareService.validateConfirmationIsAllowed(ANY_ID, ANY_ID,
					() -> electionStartTime);

			// then
			assertThatCode(validation).doesNotThrowAnyException();
		}

		@Test
		@DisplayName("Voting at the end of the election event works.")
		void votingAtTheEndOfElectionWorks() {
			// given
			when(ballotBoxEntity.isMixed()).thenReturn(false);
			when(ballotBoxEntity.getGracePeriod()).thenReturn(GRACE_PERIOD);

			// when
			final ThrowingCallable validation = () -> longVoteCastReturnCodesShareService.validateConfirmationIsAllowed(ANY_ID, ANY_ID,
					() -> electionEndTime);

			// then
			assertThatCode(validation).doesNotThrowAnyException();
		}

		@Test
		@DisplayName("Voting before the election event fails.")
		void votingBeforeElectionFails() {
			// given
			when(ballotBoxEntity.getGracePeriod()).thenReturn(GRACE_PERIOD);
			when(ballotBoxEntity.getBallotBoxId()).thenReturn(ANY_ID);

			// when
			final ThrowingCallable validation = () -> longVoteCastReturnCodesShareService.validateConfirmationIsAllowed(ANY_ID, ANY_ID,
					() -> electionStartTime.minusSeconds(1));

			// then
			assertThatThrownBy(validation)
					.isInstanceOf(IllegalStateException.class)
					.hasMessage(
							"Impossible to confirm vote before or after the dedicated time. [electionEventId: %s, ballotBoxId: %s, verificationCardId: %s, "
									+ "startTime: %s, finishTime: %s, gracePeriod: %s]", ANY_ID, ANY_ID, ANY_ID, electionStartTime, electionEndTime,
							GRACE_PERIOD);
		}

		@Test
		@DisplayName("Voting after the election event period fails.")
		void votingAfterElectionFails() {
			// given
			when(ballotBoxEntity.getGracePeriod()).thenReturn(GRACE_PERIOD);
			when(ballotBoxEntity.getBallotBoxId()).thenReturn(ANY_ID);

			// when
			final ThrowingCallable validation = () -> longVoteCastReturnCodesShareService.validateConfirmationIsAllowed(ANY_ID, ANY_ID,
					() -> electionEndTime.plusSeconds(GRACE_PERIOD).plusSeconds(1));

			// then
			assertThatThrownBy(validation)
					.isInstanceOf(IllegalStateException.class)
					.hasMessage("Impossible to confirm vote before or after the dedicated time. [electionEventId: %s, ballotBoxId: %s, "
									+ "verificationCardId: %s, startTime: %s, finishTime: %s, gracePeriod: %s]", ANY_ID, ANY_ID, ANY_ID, electionStartTime,
							electionEndTime,
							GRACE_PERIOD);
		}

		@Test
		@DisplayName("Voting in a mixed ballot box fails.")
		void votingInMixedBallotBoxFails() {
			// given
			when(ballotBoxEntity.isMixed()).thenReturn(true);
			when(ballotBoxEntity.getBallotBoxId()).thenReturn(ANY_ID);

			// when
			final ThrowingCallable validation = () -> longVoteCastReturnCodesShareService.validateConfirmationIsAllowed(ANY_ID, ANY_ID,
					() -> currentTime);

			// then
			assertThatThrownBy(validation)
					.isInstanceOf(IllegalStateException.class)
					.hasMessage("Impossible to confirm vote in an already mixed ballot box. [electionEventId: %s, ballotBoxId: %s, "
									+ "verificationCardId: %s]",
							ANY_ID, ANY_ID, ANY_ID);
		}
	}
}