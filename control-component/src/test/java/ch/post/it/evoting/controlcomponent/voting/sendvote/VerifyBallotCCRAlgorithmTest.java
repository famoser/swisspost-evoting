/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.voting.sendvote;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Locale;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTable;
import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTableEntry;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.test.tools.TestGroupSetup;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.PlaintextEqualityProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ZeroKnowledgeProof;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.votingoptions.GetActualVotingOptionsAlgorithm;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.votingoptions.GetEncodedVotingOptionsAlgorithm;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.votingoptions.GetSemanticInformationAlgorithm;

/**
 * Tests of VerifyBallotCCRAlgorithm.
 */
@DisplayName("VerifyBallotCCRService")
class VerifyBallotCCRAlgorithmTest extends TestGroupSetup {

	private static final int NODE_ID = 1;
	private static final int PHI = 6;
	private static final int PSI = 5;
	private static final int DELTA = 1;
	private static final int l_ID = 32;
	private static final ZeroKnowledgeProof zeroKnowledgeProof = mock(ZeroKnowledgeProof.class);
	private static final GetEncodedVotingOptionsAlgorithm getEncodedVotingOptionsAlgorithm = new GetEncodedVotingOptionsAlgorithm();
	private static final GetActualVotingOptionsAlgorithm getActualVotingOptionsAlgorithm = new GetActualVotingOptionsAlgorithm();
	private static final GetSemanticInformationAlgorithm getSemanticInformationAlgorithm = new GetSemanticInformationAlgorithm();
	private static VerifyBallotCCRAlgorithm verifyBallotCCRAlgorithm;
	private final Random random = RandomFactory.createRandom();
	private final ElGamalGenerator elGamalGenerator = new ElGamalGenerator(gqGroup);
	private VerifyBallotCCRContext context;
	private VerifyBallotCCRInput input;
	private String electionEventId;
	private String verificationCardSetId;
	private VerifyBallotCCRInput.Builder builder;

	@BeforeAll
	static void setUpAll() {
		verifyBallotCCRAlgorithm = new VerifyBallotCCRAlgorithm(zeroKnowledgeProof, getEncodedVotingOptionsAlgorithm, getActualVotingOptionsAlgorithm,
				getSemanticInformationAlgorithm);
	}

	@BeforeEach
	void setUp() {
		electionEventId = random.genRandomBase16String(l_ID).toLowerCase(Locale.ENGLISH);
		verificationCardSetId = random.genRandomBase16String(l_ID).toLowerCase(Locale.ENGLISH);
		final GroupVector<PrimeGqElement, GqGroup> smallPrimeGroupMembers = PrimeGqElement.PrimeGqElementFactory.getSmallPrimeGroupMembers(gqGroup,
				1);
		final PrimesMappingTable primesMappingTable = PrimesMappingTable.from(
				List.of(new PrimesMappingTableEntry("actualVotingOption", smallPrimeGroupMembers.get(0), "semantic")));
		context = new VerifyBallotCCRContext(gqGroup, NODE_ID, electionEventId, verificationCardSetId, PSI, DELTA, primesMappingTable);

		builder = new VerifyBallotCCRInput.Builder();
		final String verificationCardId = random.genRandomBase16String(l_ID).toLowerCase(Locale.ENGLISH);
		final ElGamalMultiRecipientCiphertext encryptedVote = elGamalGenerator.genRandomCiphertext(1);
		final ZqElement k_id = zqGroupGenerator.genRandomZqElementMember();
		final ElGamalMultiRecipientCiphertext exponentiatedEncryptedVote = encryptedVote.getCiphertextExponentiation(k_id);
		final ElGamalMultiRecipientCiphertext encryptedPartialChoiceReturnCodes = elGamalGenerator.genRandomCiphertext(PSI);
		final GqElement verificationCardPublicKey = gqGroupGenerator.genMember();
		final ElGamalMultiRecipientPublicKey electionPublicKey = new ElGamalMultiRecipientPublicKey(
				gqGroupGenerator.genRandomGqElementVector(DELTA));
		final ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey = new ElGamalMultiRecipientPublicKey(
				gqGroupGenerator.genRandomGqElementVector(PHI));
		final ExponentiationProof exponentiationProof = new ExponentiationProof(zqGroupGenerator.genRandomZqElementMember(),
				zqGroupGenerator.genRandomZqElementMember());
		final PlaintextEqualityProof plaintextEqualityProof = new PlaintextEqualityProof(zqGroupGenerator.genRandomZqElementMember(),
				zqGroupGenerator.genRandomZqElementVector(2));

		input = builder.setVerificationCardId(verificationCardId)
				.setEncryptedVote(encryptedVote)
				.setExponentiatedEncryptedVote(exponentiatedEncryptedVote)
				.setEncryptedPartialChoiceReturnCodes(encryptedPartialChoiceReturnCodes)
				.setVerificationCardPublicKey(verificationCardPublicKey)
				.setElectionPublicKey(electionPublicKey)
				.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey)
				.setExponentiationProof(exponentiationProof)
				.setPlaintextEqualityProof(plaintextEqualityProof)
				.build();
	}

	@Test
	@DisplayName("valid parameters does not throw")
	void validParams() {
		when(zeroKnowledgeProof.verifyExponentiation(any(), any(), any(), any())).thenReturn(true);
		when(zeroKnowledgeProof.verifyPlaintextEquality(any(), any(), any(), any(), any(), any())).thenReturn(true);

		assertTrue(verifyBallotCCRAlgorithm.verifyBallotCCR(context, input));
	}

	@Test
	@DisplayName("invalid proofs return false")
	void invalidExponentiationProof() {
		when(zeroKnowledgeProof.verifyExponentiation(any(), any(), any(), any())).thenReturn(false);
		when(zeroKnowledgeProof.verifyPlaintextEquality(any(), any(), any(), any(), any(), any())).thenReturn(true);
		assertFalse(verifyBallotCCRAlgorithm.verifyBallotCCR(context, input));

		when(zeroKnowledgeProof.verifyExponentiation(any(), any(), any(), any())).thenReturn(true);
		when(zeroKnowledgeProof.verifyPlaintextEquality(any(), any(), any(), any(), any(), any())).thenReturn(false);
		assertFalse(verifyBallotCCRAlgorithm.verifyBallotCCR(context, input));

		when(zeroKnowledgeProof.verifyExponentiation(any(), any(), any(), any())).thenReturn(false);
		when(zeroKnowledgeProof.verifyPlaintextEquality(any(), any(), any(), any(), any(), any())).thenReturn(false);
		assertFalse(verifyBallotCCRAlgorithm.verifyBallotCCR(context, input));
	}

	@Test
	@DisplayName("any null parameter throws NullPointerException")
	void nullParams() {
		assertThrows(NullPointerException.class, () -> verifyBallotCCRAlgorithm.verifyBallotCCR(context, null));
		assertThrows(NullPointerException.class, () -> verifyBallotCCRAlgorithm.verifyBallotCCR(null, input));
	}

	@Test
	@DisplayName("context and input with different groups throws IllegalArgumentException")
	void differentGroupContextInput() {
		final GroupVector<PrimeGqElement, GqGroup> smallPrimeGroupMembers = PrimeGqElement.PrimeGqElementFactory.getSmallPrimeGroupMembers(
				otherGqGroup, 1);
		final PrimesMappingTable otherPrimesMappingTable = PrimesMappingTable.from(
				List.of(new PrimesMappingTableEntry("actualVotingOption", smallPrimeGroupMembers.get(0), "semantic")));
		final VerifyBallotCCRContext otherContext = new VerifyBallotCCRContext(otherGqGroup, NODE_ID, electionEventId,
				verificationCardSetId, PSI, DELTA, otherPrimesMappingTable);

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> verifyBallotCCRAlgorithm.verifyBallotCCR(otherContext, input));
		assertEquals("The context and input must have the same group.", Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("wrong size encrypted partial Choice Return Codes")
	void wrongSizeEncryptedPartialChoiceReturnCodes() {
		final VerifyBallotCCRContext contextWrongSizePsi = new VerifyBallotCCRContext(context.encryptionGroup(), context.nodeId(),
				context.electionEventId(), context.verificationCardSetId(), 4, context.numberOfAllowedWriteInsPlusOne(),
				context.primesMappingTable());

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> verifyBallotCCRAlgorithm.verifyBallotCCR(contextWrongSizePsi, input));
		assertEquals(String.format(
						"The encrypted partial Choice Return Codes size must be equal to number of selectable voting options. [psi: %s]", 4),
				Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("numberOfSelectableVotingOptions bigger than phi throws IllegalArgumentException")
	void wrongSizePsi() {
		final ElGamalMultiRecipientCiphertext diff = elGamalGenerator.genRandomCiphertext(PHI + 1);
		final VerifyBallotCCRInput.Builder otherInput = builder.setEncryptedPartialChoiceReturnCodes(diff);

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, otherInput::build);
		assertEquals(String.format("numberOfSelectableVotingOptions must be smaller or equal to phi. [numberOfSelectableVotingOptions: %s, phi: %s]",
						PHI + 1, PHI),
				Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("numberOfAllowedWriteInsPlusOne bigger than delta throws IllegalArgumentException")
	void wrongSizeDeltaHat() {
		final VerifyBallotCCRContext contextWrongSizeDeltaHat = new VerifyBallotCCRContext(context.encryptionGroup(), context.nodeId(),
				context.electionEventId(), context.verificationCardSetId(), context.numberOfSelectableVotingOptions(), DELTA + 1,
				context.primesMappingTable());

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> verifyBallotCCRAlgorithm.verifyBallotCCR(contextWrongSizeDeltaHat, input));
		assertEquals(String.format("The encrypted vote size must be equal to the number of allowed write-ins + 1. [delta_hat: %s]",
						DELTA + 1),
				Throwables.getRootCause(exception).getMessage());
	}

}
