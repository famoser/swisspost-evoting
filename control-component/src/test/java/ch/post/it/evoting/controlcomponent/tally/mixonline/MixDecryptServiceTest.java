/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.tally.mixonline;

import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.assertj.core.api.ThrowableAssert.ThrowingCallable;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import ch.post.it.evoting.controlcomponent.BallotBoxEntity;
import ch.post.it.evoting.controlcomponent.BallotBoxService;
import ch.post.it.evoting.controlcomponent.ElectionContextEntity;
import ch.post.it.evoting.controlcomponent.ElectionContextService;
import ch.post.it.evoting.controlcomponent.SetupComponentPublicKeysService;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ControlComponentShufflePayload;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.mixnet.VerifiableShuffle;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.VerifiableShuffleGenerator;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.VerifiableDecryptionGenerator;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.VerifiableDecryptions;

@ExtendWith(MockitoExtension.class)
class MixDecryptServiceTest {

	@Mock
	private BallotBoxService ballotBoxService;
	@Mock(answer = Answers.RETURNS_DEEP_STUBS)
	private ElectionContextService electionContextService;
	@Mock(answer = Answers.RETURNS_DEEP_STUBS)
	private SetupComponentPublicKeysService setupComponentPublicKeysService;
	@InjectMocks
	private MixDecryptService mixDecryptService;

	@Nested
	class ValidateMixIsAllowed {
		private static final String ANY_ID = "53";
		private static final int GRACE_PERIOD = 3600;
		private BallotBoxEntity ballotBoxEntity;

		private LocalDateTime electionEndTime;
		private LocalDateTime currentTime;

		@BeforeEach
		void setUp() {
			currentTime = LocalDateTime.now();
			electionEndTime = currentTime.plusSeconds(3600);
			ballotBoxEntity = mock(BallotBoxEntity.class);
			final ElectionContextEntity electionContextEntity = mock(ElectionContextEntity.class);

			when(ballotBoxEntity.getGracePeriod()).thenReturn(GRACE_PERIOD);
			when(electionContextEntity.getFinishTime()).thenReturn(electionEndTime);
			when(ballotBoxService.getBallotBox(anyString())).thenReturn(ballotBoxEntity);
			when(electionContextService.getElectionContextEntity(anyString())).thenReturn(electionContextEntity);
		}

		@Test
		@DisplayName("Test ballot box can be mixed at any time.")
		void testBallotBox() {
			// given
			when(ballotBoxEntity.isTestBallotBox()).thenReturn(true);

			// when
			final ThrowingCallable validation = () -> mixDecryptService.validateMixIsAllowed(ANY_ID, ANY_ID, () -> currentTime);

			// then
			assertThatCode(validation).doesNotThrowAnyException();
		}

		@Test
		@DisplayName("Possible to mix after the grace period has expired.")
		void prodBallotBoxWithElectionFinished() {
			// given
			when(ballotBoxEntity.isTestBallotBox()).thenReturn(false);

			// when
			final ThrowingCallable validation = () -> mixDecryptService.validateMixIsAllowed(ANY_ID, ANY_ID,
					() -> electionEndTime.plusSeconds(GRACE_PERIOD).plusSeconds(1));

			// then
			assertThatCode(validation).doesNotThrowAnyException();
		}

		@Test
		@DisplayName("Not possible to mix before the election finish, including grace period has expired")
		void prodBallotBoxWithElectionNotFinished() {
			// given
			when(ballotBoxEntity.isTestBallotBox()).thenReturn(false);

			// when
			final ThrowingCallable validation = () -> mixDecryptService.validateMixIsAllowed(ANY_ID, ANY_ID,
					() -> electionEndTime.plusSeconds(GRACE_PERIOD));

			// then
			assertThatThrownBy(validation)
					.isInstanceOf(IllegalStateException.class)
					.hasMessage("The ballot box can not be mixed. [isTestBallotBox: %s, finishTime: %s, electionEventId: %s, ballotBoxId: %s]",
							ballotBoxEntity.isTestBallotBox(), electionEndTime, ANY_ID, ANY_ID);
		}
	}

	@Nested
	class ValidateShufflePayload {

		private static final String ELECTION_EVENT_ID = "00000000000000000000000000000000";
		private static final String BALLOT_BOX_ID = "11111111111111111111111111111111";
		private static final GqGroup GQ_GROUP = GroupTestData.getGqGroup();

		@BeforeEach
		void setUp() {
			when(setupComponentPublicKeysService.getElectionPublicKey(anyString()).getGroup()).thenReturn(GQ_GROUP);
		}

		@ParameterizedTest
		@DisplayName("Valid payload pass validation")
		@ValueSource(ints = { 1, 2, 3, 4 })
		void validPayloadPassValidation(final int nodeId) throws IllegalAccessException {
			// given
			FieldUtils.writeField(mixDecryptService, "nodeId", nodeId, true);
			final List<ControlComponentShufflePayload> shufflePayloads = createPayloads(GQ_GROUP, ELECTION_EVENT_ID, BALLOT_BOX_ID, nodeId);

			// when
			final ThrowingCallable validation = () -> mixDecryptService.validateShufflePayload(GQ_GROUP, ELECTION_EVENT_ID, BALLOT_BOX_ID,
					shufflePayloads);

			// then
			assertThatCode(validation).doesNotThrowAnyException();
		}

		@ParameterizedTest
		@DisplayName("Incorrect payload count is given according node ID")
		@MethodSource("incorrectPayloadCountAccordingNodeIdProvider")
		void incorrectPayloadCountAccordingNodeId(final int nodeId, final int payloadCount) throws IllegalAccessException {
			// given
			FieldUtils.writeField(mixDecryptService, "nodeId", nodeId, true);
			final List<ControlComponentShufflePayload> shufflePayloads = new ArrayList<>();
			IntStream.range(0, payloadCount)
					.forEach(value -> shufflePayloads.add(createPayload(GQ_GROUP, ELECTION_EVENT_ID, BALLOT_BOX_ID, nodeId)));

			// when
			final ThrowingCallable validation = () -> mixDecryptService.validateShufflePayload(GQ_GROUP, ELECTION_EVENT_ID, BALLOT_BOX_ID,
					shufflePayloads);

			// then
			assertThatThrownBy(validation)
					.isInstanceOf(IllegalArgumentException.class)
					.hasMessage("There must be exactly the expected number of shuffle payloads. [expected: %s, actual: %s]", nodeId - 1,
							shufflePayloads.size());
		}

		static Stream<Arguments> incorrectPayloadCountAccordingNodeIdProvider() {
			return Stream.of(
					Arguments.of(1, 1),
					Arguments.of(2, 0),
					Arguments.of(2, 2),
					Arguments.of(3, 1),
					Arguments.of(3, 3),
					Arguments.of(4, 2),
					Arguments.of(4, 5)
			);
		}

		@ParameterizedTest
		@DisplayName("Payload with incorrect election event id failed validation (except node 1 which has no payload)")
		@ValueSource(ints = { 2, 3, 4 })
		void payloadWithIncorrectElectionEventIdFailedValidation(final int nodeId) throws IllegalAccessException {
			// given
			FieldUtils.writeField(mixDecryptService, "nodeId", nodeId, true);
			final String wrongElectionEventId = "22222222222222222222222222222222";
			final List<ControlComponentShufflePayload> shufflePayloads = createPayloads(GQ_GROUP, wrongElectionEventId, BALLOT_BOX_ID, nodeId);

			// when
			final ThrowingCallable validation = () -> mixDecryptService.validateShufflePayload(GQ_GROUP, ELECTION_EVENT_ID, BALLOT_BOX_ID,
					shufflePayloads);

			// then
			assertThatThrownBy(validation)
					.isInstanceOf(IllegalStateException.class)
					.hasMessage("Election event ID must be identical in shuffle payload. [expected: %s, actual: %s]", ELECTION_EVENT_ID,
							wrongElectionEventId);
		}

		@ParameterizedTest
		@DisplayName("Payload with incorrect election event id failed validation (except node 1 which has no payload)")
		@ValueSource(ints = { 2, 3, 4 })
		void payloadWithIncorrectBallotBoxIdFailedValidation(final int nodeId) throws IllegalAccessException {
			// given
			FieldUtils.writeField(mixDecryptService, "nodeId", nodeId, true);
			final String wrongBallotBoxId = "22222222222222222222222222222222";
			final List<ControlComponentShufflePayload> shufflePayloads = createPayloads(GQ_GROUP, ELECTION_EVENT_ID, wrongBallotBoxId, nodeId);

			// when
			final ThrowingCallable validation = () -> mixDecryptService.validateShufflePayload(GQ_GROUP, ELECTION_EVENT_ID, BALLOT_BOX_ID,
					shufflePayloads);

			// then
			assertThatThrownBy(validation)
					.isInstanceOf(IllegalStateException.class)
					.hasMessage("Ballot box ID must be identical in shuffle payload. [expected: %s, actual: %s]", BALLOT_BOX_ID,
							wrongBallotBoxId);
		}

		@ParameterizedTest
		@DisplayName("Payload with incorrect encryption group failed validation (except node 1 which has no payload)")
		@ValueSource(ints = { 2, 3, 4 })
		void payloadWithIncorrectGqGroupFailedValidation(final int nodeId) throws IllegalAccessException {
			// given
			FieldUtils.writeField(mixDecryptService, "nodeId", nodeId, true);
			final GqGroup wrongGqGroup = GroupTestData.getLargeGqGroup();
			final List<ControlComponentShufflePayload> shufflePayloads = createPayloads(wrongGqGroup, ELECTION_EVENT_ID, BALLOT_BOX_ID, nodeId);

			// when
			final ThrowingCallable validation = () -> mixDecryptService.validateShufflePayload(GQ_GROUP, ELECTION_EVENT_ID, BALLOT_BOX_ID,
					shufflePayloads);

			// then
			assertThatThrownBy(validation)
					.isInstanceOf(IllegalStateException.class)
					.hasMessage("Gq groups must be identical in shuffle payload. [expected: %s, actual: %s]", GQ_GROUP,
							wrongGqGroup);
		}

		@ParameterizedTest
		@DisplayName("Payload with incorrect node id failed validation (except node 1 which has no payload)")
		@ValueSource(ints = { 2, 3, 4 })
		void payloadWithIncorrectNodeIdFailedValidation(final int nodeId) throws IllegalAccessException {
			// given
			FieldUtils.writeField(mixDecryptService, "nodeId", nodeId, true);
			final List<ControlComponentShufflePayload> shufflePayloads = Collections.nCopies(nodeId - 1,
					createPayload(GQ_GROUP, ELECTION_EVENT_ID, BALLOT_BOX_ID, nodeId));
			final int[] expectedNodeIds = IntStream.range(1, nodeId).toArray();
			final int[] actualNodeIds = new int[] { nodeId };

			// when
			final ThrowingCallable validation = () -> mixDecryptService.validateShufflePayload(GQ_GROUP, ELECTION_EVENT_ID, BALLOT_BOX_ID,
					shufflePayloads);

			// then
			assertThatThrownBy(validation)
					.isInstanceOf(IllegalStateException.class)
					.hasMessage("Payloads must come from expected nodes. [expected: %s, actual: %s]", Arrays.toString(expectedNodeIds),
							Arrays.toString(actualNodeIds));
		}

		private List<ControlComponentShufflePayload> createPayloads(final GqGroup gqGroup, final String electionEventId, final String ballotBoxId,
				final int nodeId) {

			if (nodeId == 1) {
				return Collections.emptyList();
			}

			final List<ControlComponentShufflePayload> shufflePayloads = new ArrayList<>();
			IntStream.range(1, nodeId)
					.forEach(n -> shufflePayloads.add(createPayload(gqGroup, electionEventId, ballotBoxId, n)));
			return shufflePayloads;
		}

		private ControlComponentShufflePayload createPayload(final GqGroup gqGroup, final String electionEventId, final String ballotBoxId,
				final int nodeId) {
			final VerifiableDecryptions verifiableDecryptions = new VerifiableDecryptionGenerator(gqGroup).genVerifiableDecryption(5, 5);
			final VerifiableShuffle verifiableShuffle = new VerifiableShuffleGenerator(gqGroup).genVerifiableShuffle(5, 5);
			return new ControlComponentShufflePayload(gqGroup, electionEventId, ballotBoxId, nodeId, verifiableDecryptions, verifiableShuffle);
		}
	}
}