/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.tally.mixonline;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Locale;

import org.junit.jupiter.api.Test;

import ch.post.it.evoting.controlcomponent.BallotBoxEntity;
import ch.post.it.evoting.controlcomponent.VerificationCardSetEntity;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;

class BallotBoxEntityTest {

	private static final Random RANDOM_SERVICE = RandomFactory.createRandom();

	@Test
	void constructWithoutParametersDoesNotThrow() {
		final BallotBoxEntity ballotBoxEntity = new BallotBoxEntity();
		assertFalse(ballotBoxEntity.isMixed());
		assertFalse(ballotBoxEntity.isTestBallotBox());
	}

	@Test
	void constructWithValidParametersDoesNotThrow() {
		final String ballotBoxId = RANDOM_SERVICE.genRandomBase16String(32).toLowerCase(Locale.ENGLISH);
		final VerificationCardSetEntity verificationCardSetEntity = new VerificationCardSetEntity();
		final BallotBoxEntity ballotBoxEntity = new BallotBoxEntity(ballotBoxId, verificationCardSetEntity, true, 1, 10, 900, new byte[] {});
		assertEquals(ballotBoxId, ballotBoxEntity.getBallotBoxId());
		assertEquals(verificationCardSetEntity, ballotBoxEntity.getVerificationCardSetEntity());
		assertFalse(ballotBoxEntity.isMixed());
		assertTrue(ballotBoxEntity.isTestBallotBox());
	}
}