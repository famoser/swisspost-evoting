/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.tally.mixonline;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.after;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.security.SignatureException;
import java.util.Collections;
import java.util.UUID;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.ClassOrderer;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestClassOrder;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.annotation.DirtiesContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.controlcomponent.CcmjElectionKeysService;
import ch.post.it.evoting.controlcomponent.ElectionEventService;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.tally.MixDecryptOnlineRequestPayload;
import ch.post.it.evoting.domain.tally.MixDecryptOnlineResponsePayload;

@TestClassOrder(ClassOrderer.OrderAnnotation.class)
@Order(2)
@SpringBootTest(properties = { "nodeID=2" })
@DisplayName("MixDecryptProcessor on node 2 consuming")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
class MixDecryptProcessorCC2ITCase extends MixDecryptProcessorTestBase {

	private static final int NODE_ID_2 = 2;

	@Autowired
	private RabbitTemplate rabbitTemplate;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private ElectionEventService electionEventService;

	@SpyBean
	private MixDecryptService mixDecryptService;

	@SpyBean
	private MixDecryptProcessor mixDecryptProcessor;

	@SpyBean
	private SignatureKeystore<Alias> signatureKeystoreService;

	@SpyBean
	private VerifyMixDecOnlineAlgorithm verifyMixDecOnlineAlgorithm;

	@BeforeAll
	static void setUpAll(
			@Autowired
			final CcmjElectionKeysService ccmjElectionKeysService) {

		// Each node needs to save its ccmj election key pair.
		ccmjElectionKeysService.save(electionEventId, getCcmjKeyPair(NODE_ID_2));
	}

	@Test
	@DisplayName("a mixDecryptOnlineRequestPayload correctly mixes")
	void request() throws IOException, SignatureException {
		doReturn(true).when(signatureKeystoreService).verifySignature(any(), any(), any(), any());

		final MixDecryptResponse mixDecryptResponseNode1 = getResponseMessage(1);
		final String electionEventId = mixDecryptResponseNode1.getElectionEventId();
		final String ballotBoxIdToMix = mixDecryptResponseNode1.getBallotBoxId();
		final Message responseMessageNode1 = mixDecryptResponseNode1.getMessage();
		final GqGroup gqGroup = electionEventService.getEncryptionGroup(electionEventId);

		final MixDecryptOnlineResponsePayload mixDecryptOnlineResponsePayload = objectMapper.reader()
				.withAttribute("group", gqGroup)
				.readValue(responseMessageNode1.getBody(), MixDecryptOnlineResponsePayload.class);

		// Send to node 2.
		final MixDecryptOnlineRequestPayload request = new MixDecryptOnlineRequestPayload(electionEventId, ballotBoxIdToMix, NODE_ID_2,
				Collections.singletonList(mixDecryptOnlineResponsePayload.controlComponentShufflePayload()));
		final byte[] requestBytes = objectMapper.writeValueAsBytes(request);

		final MessageProperties messageProperties = new MessageProperties();
		final String correlationId = UUID.randomUUID().toString();
		messageProperties.setCorrelationId(correlationId);

		// Sends a request to the processor.
		final Message message = new Message(requestBytes, messageProperties);
		rabbitTemplate.send(RABBITMQ_EXCHANGE, MIX_DEC_ONLINE_REQUEST_QUEUE_2, message);

		// Collects the response of the processor.
		final Message responseMessage = rabbitTemplate.receive(MIX_DEC_ONLINE_RESPONSE_QUEUE_2, 5000);

		assertNotNull(responseMessage);
		assertEquals(correlationId, responseMessage.getMessageProperties().getCorrelationId());

		verify(mixDecryptProcessor, after(5000).times(1)).onMessage(any());
		verify(mixDecryptService).performMixDecrypt(any(), any(), any());
		verify(verifyMixDecOnlineAlgorithm).verifyMixDecOnline(any(), any());
		verify(signatureKeystoreService, times(1)).verifySignature(any(), any(), any(), any());
		verify(signatureKeystoreService, times(2)).generateSignature(any(), any());

		// Saves the response so the next nodes can use it.
		addResponseMessage(NODE_ID_2, electionEventId, ballotBoxIdToMix, responseMessage);
	}

}