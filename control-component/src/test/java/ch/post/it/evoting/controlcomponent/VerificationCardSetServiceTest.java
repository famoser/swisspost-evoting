/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.persistence.PersistenceException;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import ch.post.it.evoting.cryptoprimitives.domain.election.CombinedCorrectnessInformation;
import ch.post.it.evoting.cryptoprimitives.domain.election.CorrectnessInformation;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;

@SpringBootTest
@ContextConfiguration(initializers = TestKeyStoreInitializer.class)
@ActiveProfiles("test")
@DisplayName("A verificationCardSetService")
class VerificationCardSetServiceTest {
	private static final Random random = RandomFactory.createRandom();
	private static final String ELECTION_EVENT_ID = random.genRandomBase16String(32);
	private static final String VERIFICATION_CARD_SET_ID = random.genRandomBase16String(32);
	private static final String ALREADY_SET_VERIFICATION_CARD_SET_ID = random.genRandomBase16String(32);

	@Autowired
	private VerificationCardSetService verificationCardSetService;

	@BeforeAll
	static void setUpElection(
			@Autowired
			final ElectionEventService electionEventService,
			@Autowired
			final VerificationCardSetRepository verificationCardSetRepository) {

		final GqGroup encryptionGroup = GroupTestData.getGqGroup();

		// Save election event.
		final ElectionEventEntity savedElectionEventEntity = electionEventService.save(ELECTION_EVENT_ID, encryptionGroup);

		// Create and save verification card sets
		final CombinedCorrectnessInformation combinedCorrectnessInformation = new CombinedCorrectnessInformation(
				Collections.singletonList(new CorrectnessInformation("correctnessId", 1, 1, Collections.emptyList())));
		final VerificationCardSetEntity verificationCardSet1 = new VerificationCardSetEntity(VERIFICATION_CARD_SET_ID, savedElectionEventEntity,
				combinedCorrectnessInformation);

		final VerificationCardSetEntity verificationCardSet2 = new VerificationCardSetEntity(ALREADY_SET_VERIFICATION_CARD_SET_ID,
				savedElectionEventEntity, combinedCorrectnessInformation);

		verificationCardSetRepository.saveAll(Arrays.asList(verificationCardSet1, verificationCardSet2));
	}

	@AfterAll
	static void cleanUpAll(
			@Autowired
			final TestDatabaseCleanUpService testDatabaseCleanUpService) {

		testDatabaseCleanUpService.cleanUp();
	}

	@DisplayName("setting a long vote cast return codes allow list behaves as expected.")
	@Test
	void happyPathTest() {
		final List<String> longVoteCastReturnCodesAllowList = Arrays.asList("A", "B", "C");

		assertDoesNotThrow(
				() -> verificationCardSetService.setLongVoteCastReturnCodesAllowList(VERIFICATION_CARD_SET_ID, longVoteCastReturnCodesAllowList));

		assertEquals(longVoteCastReturnCodesAllowList, verificationCardSetService.getLongVoteCastReturnCodesAllowList(VERIFICATION_CARD_SET_ID));
	}

	@DisplayName("setting with an invalid input throws.")
	@Test
	void invalidInputValidationTest() {
		final List<String> longVoteCastReturnCodesAllowList = Collections.emptyList();

		assertAll(
				() -> assertThrows(NullPointerException.class,
						() -> verificationCardSetService.setLongVoteCastReturnCodesAllowList(null, longVoteCastReturnCodesAllowList)),
				() -> assertThrows(FailedValidationException.class,
						() -> verificationCardSetService.setLongVoteCastReturnCodesAllowList("invalidVerificationCardSetId",
								longVoteCastReturnCodesAllowList)),
				() -> assertThrows(NullPointerException.class,
						() -> verificationCardSetService.setLongVoteCastReturnCodesAllowList(VERIFICATION_CARD_SET_ID, null))
		);
	}

	@DisplayName("setting a long vote cast return codes allow list with non-matching verification card set ids throws.")
	@Test
	void nonMatchingVerificationCardSetThrows() {
		final String verificationCardSetId = random.genRandomBase16String(32);
		final List<String> longVoteCastReturnCodesAllowList = Collections.emptyList();

		final IllegalStateException illegalStateException = assertThrows(IllegalStateException.class,
				() -> verificationCardSetService.setLongVoteCastReturnCodesAllowList(verificationCardSetId, longVoteCastReturnCodesAllowList));

		assertEquals(String.format("Could not find any matching verification card set [verificationCardSetId: %s]", verificationCardSetId),
				illegalStateException.getMessage());

	}

	@DisplayName("setting a long vote cast return codes allow list on a verification card set already containing throws.")
	@Test
	void alreadyExistsInputValidationTest() {
		final String longVoteCastReturnCode = random.genRandomBase64String(48);
		final List<String> longVoteCastReturnCodesAllowList = List.of(longVoteCastReturnCode);
		verificationCardSetService.setLongVoteCastReturnCodesAllowList(ALREADY_SET_VERIFICATION_CARD_SET_ID, longVoteCastReturnCodesAllowList);

		final PersistenceException exception = assertThrows(PersistenceException.class,
				() -> verificationCardSetService.setLongVoteCastReturnCodesAllowList(ALREADY_SET_VERIFICATION_CARD_SET_ID,
						longVoteCastReturnCodesAllowList));
	}

}
