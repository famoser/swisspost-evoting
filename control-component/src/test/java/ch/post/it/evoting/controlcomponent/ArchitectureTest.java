/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;

@AnalyzeClasses(packages = "ch.post.it.evoting.controlcomponent")
class ArchitectureTest {

	@ArchTest
	static final ArchRule NO_DEPS_FROM_CONFIGURATION_PACKAGE = noClasses().that()
			.resideInAPackage("ch.post.it.evoting.controlcomponent.configuration..")
			.should().dependOnClassesThat()
			.resideInAnyPackage("ch.post.it.evoting.controlcomponent.voting..", "ch.post.it.evoting.controlcomponent.tally..");

	@ArchTest
	static final ArchRule NO_DEPS_FROM_TALLY_PACKAGE = noClasses().that()
			.resideInAPackage("ch.post.it.evoting.controlcomponent.tally..")
			.should().dependOnClassesThat()
			.resideInAnyPackage("ch.post.it.evoting.controlcomponent.voting..", "ch.post.it.evoting.controlcomponent.configuration..");

	@ArchTest
	static final ArchRule NO_DEPS_FROM_VOTING_PACKAGE = noClasses().that()
			.resideInAPackage("ch.post.it.evoting.controlcomponent.voting..")
			.should().dependOnClassesThat()
			.resideInAnyPackage("ch.post.it.evoting.controlcomponent.tally..", "ch.post.it.evoting.controlcomponent.configuration..");

	@ArchTest
	static final ArchRule CLASSES_IN_SENDVOTE_SHOULD_NOT_DEPEND_ON_CLASSES_IN_CONFIRMVOTE = noClasses().that()
			.resideInAPackage("ch.post.it.evoting.controlcomponent.voting.sendvote")
			.should().dependOnClassesThat()
			.resideInAPackage("ch.post.it.evoting.controlcomponent.voting.confirmvote");

	@ArchTest
	static final ArchRule CLASSES_IN_CONFIRMEVOTE_SHOULD_NOT_DEPEND_ON_CLASSES_IN_SENDVOTE = noClasses().that()
			.resideInAPackage("ch.post.it.evoting.controlcomponent.voting.confirmvote")
			.should().dependOnClassesThat()
			.resideInAPackage("ch.post.it.evoting.controlcomponent.voting.sendvote");

}
