/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.election.SetupComponentPublicKeys;
import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.SetupComponentPublicKeysPayload;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.votingserver.orchestrator.OrchestratorFacade;

@DisplayName("ElectionEventContextServiceTest")
class SetupComponentPublicKeysServiceTest {

	private static final SetupComponentPublicKeysRepository SETUP_COMPONENT_PUBLIC_KEYS_REPOSITORY = mock(SetupComponentPublicKeysRepository.class);
	private static final ElectionEventService ELECTION_EVENT_SERVICE = mock(ElectionEventService.class);
	private static final OrchestratorFacade ORCHESTRATOR_FACADE = mock(OrchestratorFacade.class);

	private static SetupComponentPublicKeysService setupComponentPublicKeysService;
	private static SetupComponentPublicKeysPayload setupComponentPublicKeysPayload;
	private static SetupComponentPublicKeys setupComponentPublicKeys;
	private static String electionEventId;
	private static ElectionEventEntity electionEventEntity;

	@BeforeAll
	static void setUpAll() throws IOException {
		final ObjectMapper mapper = DomainObjectMapper.getNewInstance();
		final URL setupComponentPublicKeysPayloadUrl = SetupComponentPublicKeysServiceTest.class.getResource(
				"/processor/setupComponentPublicKeysServiceTest/setup-component-public-keys-payload.json");
		setupComponentPublicKeysPayload = mapper.readValue(setupComponentPublicKeysPayloadUrl, SetupComponentPublicKeysPayload.class);
		setupComponentPublicKeys = setupComponentPublicKeysPayload.getSetupComponentPublicKeys();
		electionEventId = setupComponentPublicKeysPayload.getElectionEventId();
		setupComponentPublicKeysService = new SetupComponentPublicKeysService(mapper, ORCHESTRATOR_FACADE, ELECTION_EVENT_SERVICE,
				SETUP_COMPONENT_PUBLIC_KEYS_REPOSITORY);

		electionEventEntity = new ElectionEventEntity(electionEventId, setupComponentPublicKeysPayload.getEncryptionGroup());
		final byte[] electionPublicKey = mapper.writeValueAsBytes(setupComponentPublicKeys.electionPublicKey());
		final byte[] ccrEncryptionPublicKey = mapper.writeValueAsBytes(setupComponentPublicKeys.choiceReturnCodesEncryptionPublicKey());
		final SetupComponentPublicKeysEntity setupComponentPublicKeysEntity = new SetupComponentPublicKeysEntity(electionEventEntity, new byte[] {},
				new byte[] {}, new byte[] {}, electionPublicKey, ccrEncryptionPublicKey);
		when(SETUP_COMPONENT_PUBLIC_KEYS_REPOSITORY.findById(electionEventId)).thenReturn(Optional.of(setupComponentPublicKeysEntity));

		when(ELECTION_EVENT_SERVICE.retrieveElectionEventEntity(electionEventId)).thenReturn(electionEventEntity);
	}

	@Test
	@DisplayName("Retrieving the voting client public keys with invalid IDs throws")
	void retrievingInvalidIdsThrows() {
		assertAll(
				() -> assertThrows(NullPointerException.class,
						() -> setupComponentPublicKeysService.getVotingClientPublicKeys(null)),
				() -> assertThrows(FailedValidationException.class,
						() -> setupComponentPublicKeysService.getVotingClientPublicKeys("invalid electionEventId"))
		);
	}

	@Test
	@DisplayName("Retrieving not saved election event context throws")
	void retrievingNotSavedElectionEventContextThrows() {
		when(SETUP_COMPONENT_PUBLIC_KEYS_REPOSITORY.findById(electionEventId)).thenReturn(Optional.empty());
		assertThrows(IllegalStateException.class, () -> setupComponentPublicKeysService.getVotingClientPublicKeys(electionEventId));
	}

	@Test
	@DisplayName("Retrieving the voting client public keys for a saved election event context does not throw")
	void retrievingSavedElectionEventContextDoesNotThrow() {
		final VotingClientPublicKeys expected = new VotingClientPublicKeys(setupComponentPublicKeysPayload.getEncryptionGroup(),
				setupComponentPublicKeys.electionPublicKey(), setupComponentPublicKeys.choiceReturnCodesEncryptionPublicKey());

		final VotingClientPublicKeys result;
		result = assertDoesNotThrow(() -> setupComponentPublicKeysService.getVotingClientPublicKeys(electionEventId));
		verify(SETUP_COMPONENT_PUBLIC_KEYS_REPOSITORY, times(1)).findById(electionEventId);
		assertEquals(expected.encryptionParameters(), result.encryptionParameters());
		assertEquals(expected.electionPublicKey().getKeyElements(), result.electionPublicKey().getKeyElements());
		assertEquals(expected.choiceReturnCodesEncryptionPublicKey().getKeyElements(),
				result.choiceReturnCodesEncryptionPublicKey().getKeyElements());
	}
}
