/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.orchestrator.tally;

import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.IntConsumer;
import java.util.stream.IntStream;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ControlComponentShufflePayload;
import ch.post.it.evoting.domain.SharedQueue;
import ch.post.it.evoting.domain.tally.BallotBoxStatus;
import ch.post.it.evoting.domain.tally.MixDecryptOnlinePayload;
import ch.post.it.evoting.domain.tally.MixDecryptOnlineRequestPayload;
import ch.post.it.evoting.domain.tally.MixDecryptOnlineResponsePayload;
import ch.post.it.evoting.evotinglibraries.domain.tally.ControlComponentBallotBoxPayload;
import ch.post.it.evoting.votingserver.orchestrator.IntegrationTestSupport;

@DisplayName("MixDecryptOnlineController end to end integration test")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class MixDecryptOnlineControllerIT extends IntegrationTestSupport {

	private static final Logger LOGGER = LoggerFactory.getLogger(MixDecryptOnlineControllerIT.class);
	private static final String ballotBoxId = "e3e3c2fd8a16489291c5c2222222222e";
	private static final String MIX_DEC_ONLINE_PATH = "tally/mixonline";
	private static final String BASE_URL = "/api/v1/" + MIX_DEC_ONLINE_PATH + "/electionevent/{electionevent}/ballotbox/{ballotboxid}";
	private static final String MIX_URL = BASE_URL + "/mix";
	private static final String STATUS_URL = BASE_URL + "/status";
	private static final String DOWNLOAD_URL = BASE_URL + "/download";

	private static ObjectMapper domainMapper;
	private static List<ControlComponentBallotBoxPayload> controlComponentBallotBoxPayloads;
	private static List<ControlComponentShufflePayload> responseControlComponentShufflePayloads;

	@Autowired
	private WebTestClient webTestClient;

	@Autowired
	private MixnetPayloadService mixnetPayloadService;

	@Autowired
	private ControlComponentBallotBoxPayloadRepository controlComponentBallotBoxPayloadRepository;

	@Autowired
	private ShufflePayloadRepository shufflePayloadRepository;

	@Autowired
	private RabbitTemplate rabbitTemplate;

	@BeforeAll
	public static void setup() throws IOException {
		domainMapper = DomainObjectMapper.getNewInstance();

		// Responses for each node
		final Resource controlComponentBallotBoxPayloadResource = new ClassPathResource(
				"/orchestrator/tally/mixonline/control-component-ballot-box-payloads.json");
		controlComponentBallotBoxPayloads = domainMapper.readValue(controlComponentBallotBoxPayloadResource.getFile(),
				new TypeReference<>() {
				});

		final Resource shufflePayloadsResource = new ClassPathResource("/orchestrator/tally/mixonline/control-component-shuffle-payloads.json");
		responseControlComponentShufflePayloads = domainMapper.readValue(shufflePayloadsResource.getFile(),
				new TypeReference<>() {
				});
	}

	@AfterEach
	void cleanUpDatabase() {
		shufflePayloadRepository.deleteAll();
		controlComponentBallotBoxPayloadRepository.deleteAll();
	}

	@Test()
	@Order(1)
	@DisplayName("mix happy path")
	void happyPath() throws Exception {

		final CompletableFuture<WebTestClient.ResponseSpec> resultFuture = new CompletableFuture<>();

		final ExecutorService executorService = Executors.newFixedThreadPool(1, new CustomizableThreadFactory("http-pool-"));

		//Send the HTTP request in a separate thread and wait for the results.
		executorService.execute(() -> {
			try {
				final WebTestClient.ResponseSpec response = webTestClient.put()
						.uri(uriBuilder -> uriBuilder
								.path(MIX_URL)
								.build(electionEventId, ballotBoxId))
						.accept(MediaType.APPLICATION_JSON)
						.exchange();

				resultFuture.complete(response);

			} catch (final Exception ex) {
				resultFuture.completeExceptionally(ex);
			}
		});

		IntStream.rangeClosed(1, 4).forEach(listenAndWriteToQueues(responseControlComponentShufflePayloads));

		final WebTestClient.ResponseSpec response = resultFuture.get();

		response.expectStatus().isCreated();

		assertNull(response.expectBody(MixDecryptOnlineRequestPayload.class).returnResult().getResponseBody());

		// Wait till the status is MIXED
		await()
				.atMost(3, TimeUnit.SECONDS)
				.pollDelay(100, TimeUnit.MILLISECONDS)
				.until(() -> Objects.equals((webTestClient.get()
								.uri(uriBuilder -> uriBuilder
										.path(STATUS_URL)
										.build(electionEventId, ballotBoxId))
								.accept(MediaType.APPLICATION_JSON)
								.exchange()
								.expectBody(BallotBoxStatus.class)
								.returnResult()
								.getResponseBody()
						), BallotBoxStatus.MIXED)
				);

		// Download the processed MixDecryptOnlineRequestPayload
		final WebTestClient.ResponseSpec responseSpec = webTestClient.get()
				.uri(uriBuilder -> uriBuilder
						.path(DOWNLOAD_URL)
						.build(electionEventId, ballotBoxId))
				.accept(MediaType.APPLICATION_JSON)
				.exchange();

		responseSpec.expectStatus().isOk();

		final MixDecryptOnlinePayload payload = responseSpec.expectBody(MixDecryptOnlinePayload.class).returnResult().getResponseBody();

		assertNotNull(payload);
		assertEquals(electionEventId, payload.electionEventId());
		assertEquals(ballotBoxId, payload.ballotBoxId());
		assertNotNull(payload.controlComponentShufflePayloads());
		assertEquals(4, payload.controlComponentShufflePayloads().size());
	}

	private IntConsumer listenAndWriteToQueues(final List<ControlComponentShufflePayload> mixnetShufflePayloads) {

		return nodeId -> {

			// Wait for request
			final String queueName = SharedQueue.MIX_DEC_ONLINE_REQUEST_PATTERN + nodeId;
			final Message requestMessage = rabbitTemplate.receive(queueName, 5000);

			LOGGER.debug("Message[nodeId: {}, queueName: {}]: {}", nodeId, queueName, requestMessage);

			// Check request
			assertNotNull(requestMessage);
			assertNotNull(requestMessage.getMessageProperties().getCorrelationId());
			assertNotNull(requestMessage.getBody());
			LOGGER.debug("Message received[CorrelationId:{}]\n{}", requestMessage.getMessageProperties().getCorrelationId(),
					new String(requestMessage.getBody()));

			// Create response
			Message responseMessage = null;

			switch (nodeId) {
			case 1 -> { // IN: MixDecryptOnlineRequestPayload (w/ ee, bbid), OUT: MixDecryptOnlineRequestPayload (w/ InitialPayload & ShufflePayload)
				try {
					final MixDecryptOnlineRequestPayload payload = domainMapper.readValue(requestMessage.getBody(),
							MixDecryptOnlineRequestPayload.class);

					assert (payload.controlComponentShufflePayloads().isEmpty());

					final ControlComponentBallotBoxPayload controlComponentBallotBoxPayload1 = controlComponentBallotBoxPayloads.get(0);
					final ControlComponentShufflePayload shuffle1 = mixnetShufflePayloads.get(0);

					LOGGER.debug("node: 1, received: {}", payload);
					LOGGER.debug("         response: {}", shuffle1);

					final byte[] byteContent = domainMapper.writeValueAsBytes(
							new MixDecryptOnlineResponsePayload(controlComponentBallotBoxPayload1, shuffle1));

					responseMessage = MessageBuilder
							.withBody(byteContent)
							.setCorrelationId(requestMessage.getMessageProperties().getCorrelationId())
							.build();
				} catch (final IOException e) {
					e.printStackTrace();
				}
			}
			case 2 -> { // IN: MixDecryptOnlineRequestPayload (w/ ShufflePayload 1), OUT: MixDecryptOnlineRequestPayload (w/ InitialPayload? & ShufflePayload)
				try {
					final MixDecryptOnlineRequestPayload payload = domainMapper.readValue(requestMessage.getBody(),
							MixDecryptOnlineRequestPayload.class);

					assert (payload.controlComponentShufflePayloads().size() == 1);

					final ControlComponentBallotBoxPayload controlComponentBallotBoxPayload2 = controlComponentBallotBoxPayloads.get(nodeId - 1);
					final ControlComponentShufflePayload shuffle_2 = mixnetShufflePayloads.get(nodeId - 1);

					LOGGER.debug("node: 2, received: #{}, {}", payload.controlComponentShufflePayloads().size(),
							payload.controlComponentShufflePayloads().toArray());
					LOGGER.debug("         response: {}", shuffle_2);

					final byte[] byteContent = domainMapper.writeValueAsBytes(
							new MixDecryptOnlineResponsePayload(controlComponentBallotBoxPayload2, shuffle_2));

					responseMessage = MessageBuilder
							.withBody(byteContent)
							.setCorrelationId(requestMessage.getMessageProperties().getCorrelationId())
							.build();
				} catch (final IOException e) {
					e.printStackTrace();
				}
			}
			case 3 -> { // IN: MixDecryptOnlineRequestPayload (w/ ShufflePayload 1 & 2), OUT: MixDecryptOnlineRequestPayload (w/ InitialPayload? & ShufflePayload)
				try {
					final MixDecryptOnlineRequestPayload payload = domainMapper.readValue(requestMessage.getBody(),
							MixDecryptOnlineRequestPayload.class);

					assert (payload.controlComponentShufflePayloads().size() == 2);

					final ControlComponentBallotBoxPayload controlComponentBallotBoxPayload3 = controlComponentBallotBoxPayloads.get(nodeId - 1);
					final ControlComponentShufflePayload shuffle_3 = mixnetShufflePayloads.get(nodeId - 1);

					LOGGER.debug("node: 3, received: #{}, {}", payload.controlComponentShufflePayloads().size(),
							payload.controlComponentShufflePayloads().toArray());
					LOGGER.debug("         response: {}", shuffle_3);

					final byte[] byteContent = domainMapper.writeValueAsBytes(
							new MixDecryptOnlineResponsePayload(controlComponentBallotBoxPayload3, shuffle_3));

					responseMessage = MessageBuilder
							.withBody(byteContent)
							.setCorrelationId(requestMessage.getMessageProperties().getCorrelationId())
							.build();
				} catch (final IOException e) {
					e.printStackTrace();
				}
			}
			case 4 -> { // IN: MixDecryptOnlineRequestPayload (w/ ShufflePayload 1, 2 & 3), OUT: MixDecryptOnlineRequestPayload (w/ InitialPayload? & ShufflePayload)
				try {
					final MixDecryptOnlineRequestPayload payload = domainMapper.readValue(requestMessage.getBody(),
							MixDecryptOnlineRequestPayload.class);

					assert (payload.controlComponentShufflePayloads().size() == 3);

					final ControlComponentBallotBoxPayload controlComponentBallotBoxPayload4 = controlComponentBallotBoxPayloads.get(nodeId - 1);
					final ControlComponentShufflePayload shuffle_4 = mixnetShufflePayloads.get(nodeId - 1);

					LOGGER.debug("node: 4, received: #{}, {}", payload.controlComponentShufflePayloads().size(),
							payload.controlComponentShufflePayloads().toArray());

					final byte[] byteContent = domainMapper.writeValueAsBytes(
							new MixDecryptOnlineResponsePayload(controlComponentBallotBoxPayload4, shuffle_4));

					responseMessage = MessageBuilder
							.withBody(byteContent)
							.setCorrelationId(requestMessage.getMessageProperties().getCorrelationId())
							.build();
				} catch (final IOException e) {
					e.printStackTrace();
				}
			}
			}

			// Send response
			final String routingKey = SharedQueue.MIX_DEC_ONLINE_RESPONSE_PATTERN + nodeId;
			assert responseMessage != null;
			LOGGER.debug("Response[CorrelationId:{}]: {}", responseMessage.getMessageProperties().getCorrelationId(), responseMessage);
			rabbitTemplate.send(routingKey, responseMessage);
			LOGGER.debug("Response sent[CorrelationId:{}]", responseMessage.getMessageProperties().getCorrelationId());
		};
	}

	@Test
	@Order(2)
	@DisplayName("check status when not yet Started")
	void checkStatusWhenNotStarted() {

		//Send the HTTP request in a separate thread and wait for the results.
		final BallotBoxStatus result = webTestClient.get()
				.uri(uriBuilder -> uriBuilder
						.path(STATUS_URL)
						.build(electionEventId, ballotBoxId))
				.accept(MediaType.APPLICATION_JSON)
				.exchange()
				.expectBody(BallotBoxStatus.class)
				.returnResult()
				.getResponseBody();

		assertEquals(BallotBoxStatus.MIXING_NOT_STARTED, result);
	}

	@Test
	@Order(3)
	@DisplayName("check status when uncompleted")
	void checkStatusWhenUncompleted() {

		IntStream.rangeClosed(1, 2).forEach(node ->
				mixnetPayloadService.saveShufflePayload(responseControlComponentShufflePayloads.get(node - 1))
		);

		//Send the HTTP request in a separate thread and wait for the results.
		final BallotBoxStatus result = webTestClient.get()
				.uri(uriBuilder -> uriBuilder
						.path(STATUS_URL)
						.build(electionEventId, ballotBoxId))
				.accept(MediaType.APPLICATION_JSON)
				.exchange()
				.expectBody(BallotBoxStatus.class)
				.returnResult()
				.getResponseBody();

		assertEquals(BallotBoxStatus.MIXING, result);
	}

	@Test
	@Order(4)
	@DisplayName("check status when done processing")
	void checkStatusWhenDoneProcessing() {

		IntStream.rangeClosed(1, responseControlComponentShufflePayloads.size()).forEach(node ->
				mixnetPayloadService.saveShufflePayload(responseControlComponentShufflePayloads.get(node - 1))
		);

		//Send the HTTP request in a separate thread and wait for the results.
		final BallotBoxStatus result = webTestClient.get()
				.uri(uriBuilder -> uriBuilder
						.path(STATUS_URL)
						.build(electionEventId, ballotBoxId))
				.accept(MediaType.APPLICATION_JSON)
				.exchange()
				.expectBody(BallotBoxStatus.class)
				.returnResult()
				.getResponseBody();

		assertEquals(BallotBoxStatus.MIXED, result);
	}

	@Test
	@Order(5)
	@DisplayName("download MixDecryptOnlineRequestPayload when status is uncompleted")
	void downloadMixDecryptOnlinePayloadWhenUncompleted() throws Exception {

		final CompletableFuture<WebTestClient.ResponseSpec> resultFuture = new CompletableFuture<>();

		final ExecutorService executorService = Executors.newFixedThreadPool(1, new CustomizableThreadFactory("http-pool-"));

		IntStream.rangeClosed(1, 2).forEach(node ->
				mixnetPayloadService.saveShufflePayload(responseControlComponentShufflePayloads.get(node - 1))
		);

		//Send the HTTP request in a separate thread and wait for the results.
		executorService.execute(() -> {

			try {
				final WebTestClient.ResponseSpec response = webTestClient.get()
						.uri(uriBuilder -> uriBuilder
								.path(DOWNLOAD_URL)
								.build(electionEventId, ballotBoxId))
						.accept(MediaType.APPLICATION_JSON)
						.exchange();

				resultFuture.complete(response);

			} catch (final Exception ex) {
				resultFuture.completeExceptionally(ex);
			}
		});

		final WebTestClient.ResponseSpec response = resultFuture.get();

		response.expectStatus().isNotFound();

		final MixDecryptOnlineRequestPayload payload = response.expectBody(MixDecryptOnlineRequestPayload.class).returnResult().getResponseBody();

		assertNull(payload);
	}

	@Test
	@Order(6)
	@DisplayName("download MixDecryptOnlineRequestPayload when status completed")
	void downloadMixDecryptOnlinePayloadWhenCompleted() throws Exception {

		final CompletableFuture<WebTestClient.ResponseSpec> resultFuture = new CompletableFuture<>();

		final ExecutorService executorService = Executors.newFixedThreadPool(1, new CustomizableThreadFactory("http-pool-"));

		IntStream.rangeClosed(1, controlComponentBallotBoxPayloads.size()).forEach(node ->
				mixnetPayloadService.saveControlComponentBallotBoxPayload(controlComponentBallotBoxPayloads.get(node - 1))
		);

		IntStream.rangeClosed(1, responseControlComponentShufflePayloads.size()).forEach(node ->
				mixnetPayloadService.saveShufflePayload(responseControlComponentShufflePayloads.get(node - 1))
		);

		//Send the HTTP request in a separate thread and wait for the results.
		executorService.execute(() -> {

			try {
				final WebTestClient.ResponseSpec response = webTestClient.get()
						.uri(uriBuilder -> uriBuilder
								.path(DOWNLOAD_URL)
								.build(electionEventId, ballotBoxId))
						.accept(MediaType.APPLICATION_JSON)
						.exchange();

				resultFuture.complete(response);

			} catch (final Exception ex) {
				resultFuture.completeExceptionally(ex);
			}
		});

		final WebTestClient.ResponseSpec response = resultFuture.get();

		response.expectStatus().isOk();

		final MixDecryptOnlinePayload payload = response.expectBody(MixDecryptOnlinePayload.class).returnResult().getResponseBody();

		assertNotNull(payload);
		assertEquals(electionEventId, payload.electionEventId());
		assertEquals(ballotBoxId, payload.ballotBoxId());
		assertNotNull(payload.controlComponentBallotBoxPayloads());
		assertNotNull(payload.controlComponentShufflePayloads());
		assertEquals(4, payload.controlComponentShufflePayloads().size());
	}

}
