/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.URL;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.election.SetupComponentPublicKeys;
import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.SetupComponentPublicKeysPayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;

@DisplayName("ElectionEventContextController")
@ExtendWith(MockitoExtension.class)
class SetupComponentPublicKeysControllerTest {

	private static SetupComponentPublicKeysPayload setupComponentPublicKeysPayload;
	private static String tenantId;
	private static String electionEventId;

	@InjectMocks
	private SetupComponentPublicKeysController setupComponentPublicKeysController;

	@Mock
	private SetupComponentPublicKeysService setupComponentPublicKeysService;

	@Mock
	private ElectionEventService encryptionParametersService;

	@Mock
	private SignatureKeystore<Alias> signatureKeystoreService;

	@BeforeAll
	static void setUpAll() throws IOException {
		final ObjectMapper mapper = DomainObjectMapper.getNewInstance();
		final URL setupComponentPublicKeysPayloadUrl = SetupComponentPublicKeysControllerTest.class.getResource(
				"/processor/setupComponentPublicKeysServiceTest/setup-component-public-keys-payload.json");
		setupComponentPublicKeysPayload = mapper.readValue(setupComponentPublicKeysPayloadUrl, SetupComponentPublicKeysPayload.class);

		electionEventId = setupComponentPublicKeysPayload.getElectionEventId();
		tenantId = electionEventId;
	}

	@Test
	@DisplayName("retrieve voting client public keys with valid parameters")
	void retrieveVotingClientPublicKeysHappyPath() {
		final SetupComponentPublicKeys setupComponentPublicKeys = setupComponentPublicKeysPayload.getSetupComponentPublicKeys();
		final VotingClientPublicKeys votingClientPublicKeys = new VotingClientPublicKeys(setupComponentPublicKeysPayload.getEncryptionGroup(),
				setupComponentPublicKeys.electionPublicKey(), setupComponentPublicKeys.choiceReturnCodesEncryptionPublicKey());

		when(setupComponentPublicKeysService.getVotingClientPublicKeys(electionEventId)).thenReturn(votingClientPublicKeys);

		final VotingClientPublicKeys response = setupComponentPublicKeysController.getVotingClientPublicKeys(tenantId, electionEventId);

		verify(setupComponentPublicKeysService, times(1)).getVotingClientPublicKeys(anyString());

		assertEquals(votingClientPublicKeys, response);
	}

	@Test
	@DisplayName("retrieve voting client public keys with invalid parameters throws")
	void retrieveVotingClientPublicKeysInvalidParametersThrows() {
		assertAll(
				() -> assertThrows(NullPointerException.class,
						() -> setupComponentPublicKeysController.getVotingClientPublicKeys(null, electionEventId)),
				() -> assertThrows(NullPointerException.class,
						() -> setupComponentPublicKeysController.getVotingClientPublicKeys(tenantId, null)),
				() -> assertThrows(FailedValidationException.class,
						() -> setupComponentPublicKeysController.getVotingClientPublicKeys(tenantId, "invalid electionEventId"))
		);
	}
}
