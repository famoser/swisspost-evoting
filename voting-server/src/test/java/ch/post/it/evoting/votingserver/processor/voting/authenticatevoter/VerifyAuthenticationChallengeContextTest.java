/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor.voting.authenticatevoter;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.security.SecureRandom;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.votingserver.processor.voting.AuthenticationStep;
import ch.post.it.evoting.votingserver.processor.voting.VerifyAuthenticationChallengeContext;

class VerifyAuthenticationChallengeContextTest {

	private static final SecureRandom SECURE_RANDOM = new SecureRandom();
	private static final Random RANDOM = RandomFactory.createRandom();

	private String electionEventId;
	private AuthenticationStep authenticationStep;

	@BeforeEach
	void setup() {
		electionEventId = RANDOM.genRandomBase16String(32);
		authenticationStep = AuthenticationStep.values()[SECURE_RANDOM.nextInt(AuthenticationStep.values().length)];
	}

	@Test
	void constructWithNullArgumentsThrows() {
		assertThrows(NullPointerException.class, () -> new VerifyAuthenticationChallengeContext(null, authenticationStep));
		assertThrows(NullPointerException.class, () -> new VerifyAuthenticationChallengeContext(electionEventId, null));
	}

	@Test
	void constructWithElectionEventIdNotValidUuidThrows() {
		final String tooLongElectionEventId = electionEventId + "1";
		assertThrows(FailedValidationException.class, () -> new VerifyAuthenticationChallengeContext(tooLongElectionEventId, authenticationStep));
		final String tooShortElectionEventId = electionEventId.substring(1);
		assertThrows(FailedValidationException.class, () -> new VerifyAuthenticationChallengeContext(tooShortElectionEventId, authenticationStep));
		final String electionEventIdBadCharacter = "$" + electionEventId.substring(1);
		assertThrows(FailedValidationException.class, () -> new VerifyAuthenticationChallengeContext(electionEventIdBadCharacter, authenticationStep));
	}

	@Test
	void constructWithValidInputsDoesNotThrow() {
		assertDoesNotThrow(() -> new VerifyAuthenticationChallengeContext(electionEventId, authenticationStep));
	}
}
