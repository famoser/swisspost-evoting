/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.votingserver.orchestrator.OrchestratorFacade;

@DisplayName("ElectionEventContextServiceTest")
class ElectionEventContextServiceTest {

	private static final ElectionEventService ELECTION_EVENT_SERVICE = mock(ElectionEventService.class);
	private static final ElectionEventContextRepository ELECTION_EVENT_CONTEXT_REPOSITORY = mock(ElectionEventContextRepository.class);
	private static final OrchestratorFacade ORCHESTRATOR_FACADE = mock(OrchestratorFacade.class);
	private static final VerificationCardSetService VERIFICATION_CARD_SET_SERVICE = mock(VerificationCardSetService.class);

	private static ElectionEventContextService electionEventContextService;
	private static ElectionEventContextPayload electionEventContextPayload;

	@BeforeAll
	static void setUpAll() throws IOException {
		final ObjectMapper mapper = DomainObjectMapper.getNewInstance();
		final URL electionContextPayloadUrl = ElectionEventContextServiceTest.class.getResource(
				"/processor/electionEventContextServiceTest/election-event-context-payload.json");
		electionEventContextPayload = mapper.readValue(electionContextPayloadUrl, ElectionEventContextPayload.class);
		final String electionEventId = electionEventContextPayload.getElectionEventContext().electionEventId();
		electionEventContextService = new ElectionEventContextService(ORCHESTRATOR_FACADE, ELECTION_EVENT_SERVICE,
				VERIFICATION_CARD_SET_SERVICE, ELECTION_EVENT_CONTEXT_REPOSITORY);

		final ElectionEventEntity electionEventEntity = new ElectionEventEntity(electionEventId, electionEventContextPayload.getEncryptionGroup());
		final ElectionEventContextEntity electionEventContextEntity = new ElectionEventContextEntity(electionEventEntity, LocalDateTime.now(),
				LocalDateTime.now(), electionEventContextPayload.getElectionEventContext().electionEventAlias(),
				electionEventContextPayload.getElectionEventContext().electionEventDescription());
		when(ELECTION_EVENT_CONTEXT_REPOSITORY.findById(electionEventEntity.getElectionEventId())).thenReturn(
				Optional.of(electionEventContextEntity));

		when(ELECTION_EVENT_SERVICE.retrieveElectionEventEntity(electionEventId)).thenReturn(electionEventEntity);
	}

	@Test
	@DisplayName("Saving with null parameters throws NullPointerException")
	void savingNullThrows() {
		assertThrows(NullPointerException.class, () -> electionEventContextService.saveElectionEventContext(null));
	}

	@Test
	@DisplayName("Saving a valid election event context does not throw")
	void savingValidElectionEventContextDoesNotThrow() {
		doNothing().when(ORCHESTRATOR_FACADE).uploadElectionEventContext(any(), any());

		assertDoesNotThrow(() -> electionEventContextService.saveElectionEventContext(electionEventContextPayload));

		verify(ELECTION_EVENT_CONTEXT_REPOSITORY, times(1)).save(any());
		verify(ORCHESTRATOR_FACADE, times(1)).uploadElectionEventContext(any(), any());
	}
}
