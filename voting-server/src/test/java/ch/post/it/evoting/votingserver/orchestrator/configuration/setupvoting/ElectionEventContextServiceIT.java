/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.orchestrator.configuration.setupvoting;

import static ch.post.it.evoting.domain.SharedQueue.ELECTION_CONTEXT_REQUEST_PATTERN;
import static ch.post.it.evoting.domain.SharedQueue.ELECTION_CONTEXT_RESPONSE_PATTERN;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.commandmessaging.Context;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.domain.configuration.ElectionContextResponsePayload;
import ch.post.it.evoting.votingserver.orchestrator.BroadcastIntegrationTestService;
import ch.post.it.evoting.votingserver.orchestrator.IntegrationTestSupport;

@DisplayName("ElectionEventContextService integration test")
class ElectionEventContextServiceIT extends IntegrationTestSupport {

	@Autowired
	private ElectionEventContextService electionEventContextService;

	@Autowired
	private BroadcastIntegrationTestService broadcastIntegrationTestService;

	@Autowired
	private ObjectMapper objectMapper;

	@AfterEach
	void cleanUp() {
		broadcastIntegrationTestService.cleanUpOrchestrator();
	}

	@Test
	@DisplayName("Process ElectionEventContextPayload")
	void firstTimeCommand() throws IOException, InterruptedException {

		final Context context = Context.CONFIGURATION_ELECTION_CONTEXT;

		final CountDownLatch serviceCountDownLatch = new CountDownLatch(1);

		final Resource payloadsResource = new ClassPathResource("/orchestrator/configuration/electioncontext/election-event-context-payload.json");

		final ElectionEventContextPayload requestPayload = objectMapper.readValue(payloadsResource.getFile(), ElectionEventContextPayload.class);

		final ExecutorService executorService = Executors.newFixedThreadPool(1, new CustomizableThreadFactory("service-pool-"));

		// Call the service in a separate thread and wait for the results.
		executorService.execute(() -> {
			electionEventContextService.uploadElectionEventContextPayload(electionEventId, requestPayload);
			serviceCountDownLatch.countDown();
		});

		broadcastIntegrationTestService.awaitBroadcastRequestsSaved(context, electionEventId, 30, SECONDS);

		final Resource ccPayloadsResource = new ClassPathResource(
				"/orchestrator/configuration/electioncontext/election-context-response-payloads.json");
		final List<ElectionContextResponsePayload> responsePayloads = objectMapper.readValue(ccPayloadsResource.getFile(),
				new TypeReference<>() {
				});

		broadcastIntegrationTestService.
				respondWith(ELECTION_CONTEXT_REQUEST_PATTERN, ELECTION_CONTEXT_RESPONSE_PATTERN, nodeId -> responsePayloads.get(nodeId - 1));

		assertTrue(serviceCountDownLatch.await(30, SECONDS));
	}
}
