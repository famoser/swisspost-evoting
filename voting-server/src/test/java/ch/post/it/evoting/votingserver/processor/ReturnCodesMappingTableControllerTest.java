/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.URL;
import java.security.SignatureException;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.domain.configuration.SetupComponentCMTablePayload;
import ch.post.it.evoting.votingserver.processor.idempotence.IdempotenceService;
import ch.post.it.evoting.votingserver.processor.idempotence.IdempotentExecutionRepository;

@DisplayName("ReturnCodesMappingTableController")
class ReturnCodesMappingTableControllerTest {

	private static SetupComponentCMTablePayload setupComponentCMTablePayload;
	private static String electionEventId;
	private static String verificationCardSetId;
	private static int chunkId;
	private static SignatureKeystore<Alias> signatureKeystoreService;
	private static IdempotentExecutionRepository idempotentExecutionRepository;
	private static IdempotenceService idempotenceService;
	private static ReturnCodesMappingTableService returnCodesMappingTableService;
	private static ReturnCodesMappingTableController returnCodesMappingTableController;

	@BeforeAll
	static void setUpAll() throws IOException {
		signatureKeystoreService = mock(SignatureKeystore.class);
		idempotentExecutionRepository = mock(IdempotentExecutionRepository.class);
		idempotenceService = new IdempotenceService(idempotentExecutionRepository);
		returnCodesMappingTableService = mock(ReturnCodesMappingTableService.class);
		returnCodesMappingTableController = new ReturnCodesMappingTableController(
				signatureKeystoreService,
				returnCodesMappingTableService, idempotenceService);

		final ObjectMapper mapper = DomainObjectMapper.getNewInstance();
		final URL returnCodesMappingTablePayloadUrl = ReturnCodesMappingTableControllerTest.class.getResource(
				"/processor/returnCodesMappingTableResourceTest/setupComponentCMTablePayload.0.json");
		setupComponentCMTablePayload = mapper.readValue(returnCodesMappingTablePayloadUrl, SetupComponentCMTablePayload.class);
		electionEventId = setupComponentCMTablePayload.getElectionEventId();
		verificationCardSetId = setupComponentCMTablePayload.getVerificationCardSetId();
		chunkId = setupComponentCMTablePayload.getChunkId();
	}

	@Test
	@DisplayName("calling save with valid parameters")
	void saveReturnCodesMappingTableHappyPath() throws SignatureException {
		when(signatureKeystoreService.verifySignature(any(), any(), any(), any())).thenReturn(true);
		when(idempotentExecutionRepository.existsById(any())).thenReturn(false);
		returnCodesMappingTableController.saveReturnCodesMappingTable(electionEventId, verificationCardSetId, chunkId, setupComponentCMTablePayload);

		verify(returnCodesMappingTableService, times(1)).saveReturnCodesMappingTable(any());
	}

	@Test
	@DisplayName("calling save with valid parameters but idempotent service say it was already executed")
	void saveReturnCodesMappingTableIdempotentAlreadyExist() throws SignatureException {
		when(signatureKeystoreService.verifySignature(any(), any(), any(), any())).thenReturn(true);
		when(idempotentExecutionRepository.existsById(any())).thenReturn(true);
		returnCodesMappingTableController.saveReturnCodesMappingTable(electionEventId, verificationCardSetId, chunkId, setupComponentCMTablePayload);

		verify(returnCodesMappingTableService, times(0)).saveReturnCodesMappingTable(any());
		verify(idempotentExecutionRepository, times(1)).existsById(any());
	}

	@Test
	@DisplayName("calling save with invalid signature")
	void saveReturnCodesMappingTableWithInvalidSignature() throws SignatureException {
		when(signatureKeystoreService.verifySignature(any(), any(), any(), any())).thenReturn(false);

		final InvalidPayloadSignatureException exception = assertThrows(InvalidPayloadSignatureException.class,
				() -> returnCodesMappingTableController.saveReturnCodesMappingTable(electionEventId, verificationCardSetId, chunkId,
						setupComponentCMTablePayload));

		final String errorMessage = String.format("Signature of payload %s is invalid. [electionEventId: %s, verificationCardSetId: %s]",
				SetupComponentCMTablePayload.class.getSimpleName(), setupComponentCMTablePayload.getElectionEventId(),
				setupComponentCMTablePayload.getVerificationCardSetId());

		assertEquals(errorMessage, exception.getMessage());
	}

	@Test
	@DisplayName("calling save with invalid signature's content")
	void saveReturnCodesMappingTableWithInvalidSignatureContent() throws SignatureException {
		when(signatureKeystoreService.verifySignature(any(), any(), any(), any())).thenThrow(SignatureException.class);

		final IllegalStateException exception = assertThrows(IllegalStateException.class,
				() -> returnCodesMappingTableController.saveReturnCodesMappingTable(electionEventId, verificationCardSetId, chunkId,
						setupComponentCMTablePayload));

		final String errorMessage = String.format(
				"Couldn't verify the signature of the setup component CMTable payload. [electionEventId: %s, verificationCardSetId: %s]",
				electionEventId, verificationCardSetId);

		assertEquals(errorMessage, exception.getMessage());
	}
}
