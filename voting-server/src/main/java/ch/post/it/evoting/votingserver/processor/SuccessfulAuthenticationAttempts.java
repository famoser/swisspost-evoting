/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import ch.post.it.evoting.cryptoprimitives.domain.validations.Validations;

public record SuccessfulAuthenticationAttempts(List<String> successfulChallenges) {

	public SuccessfulAuthenticationAttempts {
		checkNotNull(successfulChallenges);
		successfulChallenges = List.copyOf(successfulChallenges);
		successfulChallenges.forEach(Validations::validateBase64Encoded);
	}

	@Override
	public List<String> successfulChallenges() {
		return List.copyOf(successfulChallenges);
	}
}
