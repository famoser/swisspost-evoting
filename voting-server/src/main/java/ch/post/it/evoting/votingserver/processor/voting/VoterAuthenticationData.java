/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor.voting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;

import java.util.List;

import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableList;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableString;

public record VoterAuthenticationData(String electionEventId, String verificationCardSetId, String votingCardSetId, String ballotBoxId,
									  String ballotId, String verificationCardId, String votingCardId, String credentialId)
		implements HashableList {

	public VoterAuthenticationData {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		validateUUID(votingCardSetId);
		validateUUID(ballotBoxId);
		validateUUID(ballotId);
		validateUUID(verificationCardId);
		validateUUID(votingCardId);
		validateUUID(credentialId);
	}

	@Override
	public List<? extends Hashable> toHashableForm() {
		return List.of(HashableString.from(electionEventId),
				HashableString.from(verificationCardSetId),
				HashableString.from(votingCardSetId),
				HashableString.from(ballotBoxId),
				HashableString.from(ballotId),
				HashableString.from(verificationCardId),
				HashableString.from(votingCardId),
				HashableString.from(credentialId));
	}

}
