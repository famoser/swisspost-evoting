/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor;

import java.io.IOException;
import java.io.UncheckedIOException;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.EncryptionGroupUtils;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;

@Converter
public class EncryptionGroupConverter implements AttributeConverter<GqGroup, byte[]> {

	private final ObjectMapper objectMapper;

	public EncryptionGroupConverter(final ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
	}

	@Override
	public byte[] convertToDatabaseColumn(final GqGroup encryptionGroup) {
		try {
			return objectMapper.writeValueAsBytes(encryptionGroup);
		} catch (JsonProcessingException e) {
			throw new UncheckedIOException("Failed to serialize the encryption group.", e);
		}
	}

	@Override
	public GqGroup convertToEntityAttribute(final byte[] bytes) {
		// Use object mapper cache to avoid deserializing too many times the GqGroups.
		final JsonNode encryptionGroupNode;
		try {
			encryptionGroupNode = objectMapper.readTree(bytes);
		} catch (final IOException e) {
			throw new UncheckedIOException("Could not retrieve the group.", e);
		}

		return EncryptionGroupUtils.getEncryptionGroup(objectMapper, encryptionGroupNode);
	}

}
