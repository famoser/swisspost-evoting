/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.election.Ballot;
import ch.post.it.evoting.domain.configuration.BallotDataPayload;

@Service
public class BallotDataService {

	private static final Logger LOGGER = LoggerFactory.getLogger(BallotDataService.class);

	private final ObjectMapper objectMapper;
	private final ElectionEventService electionEventService;
	private final BallotDataRepository ballotDataRepository;

	public BallotDataService(
			final ObjectMapper objectMapper,
			final ElectionEventService electionEventService,
			final BallotDataRepository ballotDataRepository) {
		this.objectMapper = objectMapper;
		this.electionEventService = electionEventService;
		this.ballotDataRepository = ballotDataRepository;
	}

	/**
	 * Saves the ballot data.
	 *
	 * @param ballotDataPayload the ballot data payload. Must be non-null.
	 * @throws UncheckedIOException if an error occurs while serializing the ballot.
	 */
	@Transactional
	public void saveBallotData(final BallotDataPayload ballotDataPayload) throws UncheckedIOException {
		checkNotNull(ballotDataPayload);

		final byte[] serializedBallot;
		try {
			serializedBallot = objectMapper.writeValueAsBytes(ballotDataPayload.ballot());
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException("Failed to serialize ballot.", e);
		}

		final String electionEventId = ballotDataPayload.electionEventId();
		final ElectionEventEntity electionEventEntity = electionEventService.retrieveElectionEventEntity(electionEventId);

		final String ballotId = ballotDataPayload.ballotId();
		final BallotDataEntity ballotDataEntity = new BallotDataEntity(ballotId, electionEventEntity, serializedBallot,
				ballotDataPayload.ballotTextsJson().getBytes(StandardCharsets.UTF_8));

		ballotDataRepository.save(ballotDataEntity);
		LOGGER.info("Ballot and ballotTexts successfully saved. [electionEventId: {}, ballotId: {}]", electionEventId, ballotId);
	}

	@Transactional
	public Ballot findBallotByElectionEventIdAndBallotId(final String electionEventId, final String ballotId) {
		validateUUID(electionEventId);
		validateUUID(ballotId);

		final BallotDataEntity ballotDataEntity = findBallotDataEntity(electionEventId, ballotId);

		try {
			return objectMapper.readValue(ballotDataEntity.getBallotJson(), Ballot.class);
		} catch (final IOException e) {
			throw new UncheckedIOException(
					String.format("Cannot deserialize the ballot from ballot data. [electionEventId: %s, ballotId: %s]", electionEventId,
							ballotId), e);
		}
	}

	@Transactional
	public String findBallotTextsByElectionEventIdAndBallotId(final String electionEventId, final String ballotId) {
		validateUUID(electionEventId);
		validateUUID(ballotId);

		return findBallotDataEntity(electionEventId, ballotId).getBallotTextsJson();
	}

	private BallotDataEntity findBallotDataEntity(final String electionEventId, final String ballotId) {
		validateUUID(electionEventId);
		validateUUID(ballotId);

		return ballotDataRepository.findByElectionEventIdAndBallotId(electionEventId, ballotId)
				.orElseThrow(
						() -> new IllegalStateException(String.format("Ballot data not found. [electionEventId: %s, ballotId: %s]", electionEventId,
								ballotId)));
	}

}
