/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor.idempotence;

import java.util.function.Supplier;

public enum IdempotenceContext implements Supplier<String> {
	SAVE_RETURN_CODES_MAPPING_TABLE,
	SAVE_VOTER_AUTHENTICATION_DATA,
	SAVE_SETUP_COMPONENT_PUBLIC_KEYS,
	SAVE_SETUP_COMPONENT_VERIFICATION_CARD_KEYSTORES;

	@Override
	public String get() {
		return this.name();
	}
}
