/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor;

import static com.google.common.base.Preconditions.checkNotNull;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "SETUP_COMPONENT_PUBLIC_KEYS")
public class SetupComponentPublicKeysEntity {

	@Id
	private String electionEventId;

	@MapsId
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ELECTION_EVENT_ID", referencedColumnName = "ELECTION_EVENT_ID")
	private ElectionEventEntity electionEventEntity;

	private byte[] choiceReturnCodesEncryptionPublicKey;

	private byte[] combinedControlComponentPublicKeys;

	private byte[] electionPublicKey;

	private byte[] electoralBoardPublicKey;

	private byte[] electoralBoardSchnorrProofs;

	@Version
	private Integer changeControlId;

	public SetupComponentPublicKeysEntity() {
	}

	public SetupComponentPublicKeysEntity(final ElectionEventEntity electionEventEntity, final byte[] combinedControlComponentPublicKeys,
			final byte[] electoralBoardPublicKey, final byte[] electoralBoardSchnorrProofs, final byte[] electionPublicKey,
			final byte[] choiceReturnCodesEncryptionPublicKey) {
		this.electionEventEntity = checkNotNull(electionEventEntity);
		this.combinedControlComponentPublicKeys = checkNotNull(combinedControlComponentPublicKeys);
		this.electoralBoardPublicKey = checkNotNull(electoralBoardPublicKey);
		this.electoralBoardSchnorrProofs = checkNotNull(electoralBoardSchnorrProofs);
		this.electionPublicKey = checkNotNull(electionPublicKey);
		this.choiceReturnCodesEncryptionPublicKey = checkNotNull(choiceReturnCodesEncryptionPublicKey);
	}

	public String getElectionEventId() {
		return electionEventId;
	}

	public ElectionEventEntity getElectionEventEntity() {
		return electionEventEntity;
	}

	public byte[] getChoiceReturnCodesEncryptionPublicKey() {
		return choiceReturnCodesEncryptionPublicKey;
	}

	public byte[] getCombinedControlComponentPublicKeys() {
		return combinedControlComponentPublicKeys;
	}

	public byte[] getElectionPublicKey() {
		return electionPublicKey;
	}

	public byte[] getElectoralBoardPublicKey() {
		return electoralBoardPublicKey;
	}

	public byte[] getElectoralBoardSchnorrProofs() {
		return electoralBoardSchnorrProofs;
	}

}
