/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor.voting.authenticatevoter;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;

import java.io.UncheckedIOException;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import ch.post.it.evoting.cryptoprimitives.domain.election.Ballot;
import ch.post.it.evoting.cryptoprimitives.domain.election.CombinedCorrectnessInformation;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.domain.configuration.SetupComponentVoterAuthenticationData;
import ch.post.it.evoting.domain.configuration.VerificationCardKeystore;
import ch.post.it.evoting.votingserver.processor.BallotDataService;
import ch.post.it.evoting.votingserver.processor.SetupComponentPublicKeysService;
import ch.post.it.evoting.votingserver.processor.SetupComponentVerificationCardKeystoreService;
import ch.post.it.evoting.votingserver.processor.VerificationCardService;
import ch.post.it.evoting.votingserver.processor.VerificationCardState;
import ch.post.it.evoting.votingserver.processor.VotingClientPublicKeys;
import ch.post.it.evoting.votingserver.processor.configuration.setupvoting.VoterAuthenticationDataService;
import ch.post.it.evoting.votingserver.processor.voting.VoterAuthenticationData;

@Service
public class AuthenticateVoterService {

	private static final String CI_SELECTIONS = "ciSelections";
	private static final String CI_VOTING_OPTIONS = "ciVotingOptions";
	private static final String ACTUAL_VOTING_OPTIONS = "actualVotingOptions";
	private static final String SEMANTIC_INFORMATION = "semanticInformation";
	private static final String ENCODED_VOTING_OPTIONS = "encodedVotingOptions";
	private static final String TOTAL_NUMBER_OF_WRITE_INS = "totalNumberOfWriteIns";
	private static final String VERIFICATION_CARD_KEYSTORE = "verificationCardKeystore";

	private final ObjectMapper objectMapper;
	private final BallotDataService ballotDataService;
	private final VerificationCardService verificationCardService;
	private final VoterAuthenticationDataService voterAuthenticationDataService;
	private final SetupComponentPublicKeysService setupComponentPublicKeysService;
	private final SetupComponentVerificationCardKeystoreService setupComponentVerificationCardKeystoreService;

	public AuthenticateVoterService(
			final ObjectMapper objectMapper,
			final BallotDataService ballotDataService,
			final VerificationCardService verificationCardService,
			final VoterAuthenticationDataService voterAuthenticationDataService,
			final SetupComponentPublicKeysService setupComponentPublicKeysService,
			final SetupComponentVerificationCardKeystoreService setupComponentVerificationCardKeystoreService) {
		this.objectMapper = objectMapper;
		this.ballotDataService = ballotDataService;
		this.verificationCardService = verificationCardService;
		this.voterAuthenticationDataService = voterAuthenticationDataService;
		this.setupComponentPublicKeysService = setupComponentPublicKeysService;
		this.setupComponentVerificationCardKeystoreService = setupComponentVerificationCardKeystoreService;
	}

	/**
	 * Returns the {@code AuthenticateVoterResponsePayload} containing the necessary data.
	 *
	 * @param electionEventId the election event id. Must be non-null and a valid UUID.
	 * @param credentialId    the credential id.    Must be non-null and a valid UUID.
	 * @return the payload containing the necessary data. It's content varies depending on if a re-login is performed or not.
	 * @throws NullPointerException      if any parameter is null.
	 * @throws FailedValidationException if the {@code electionEventId} is invalid.
	 */
	public AuthenticateVoterResponsePayload retrieveAuthenticateVoterPayload(final String electionEventId, final String credentialId) {
		validateUUID(credentialId);
		validateUUID(electionEventId);

		// Construct response VoterAuthenticationData, which does not contain the baseAuthenticationChallenge.
		final SetupComponentVoterAuthenticationData setupComponentVoterAuthenticationData = voterAuthenticationDataService.getVoterAuthenticationData(
				electionEventId, credentialId);

		final String verificationCardSetId = setupComponentVoterAuthenticationData.verificationCardSetId();
		final String verificationCardId = setupComponentVoterAuthenticationData.verificationCardId();
		final String ballotId = setupComponentVoterAuthenticationData.ballotId();
		final String votingCardSetId = setupComponentVoterAuthenticationData.votingCardSetId();
		final String ballotBoxId = setupComponentVoterAuthenticationData.ballotBoxId();
		final String votingCardId = setupComponentVoterAuthenticationData.votingCardId();
		final VoterAuthenticationData voterAuthenticationData = new VoterAuthenticationData(electionEventId, verificationCardSetId, votingCardSetId,
				ballotBoxId, ballotId, verificationCardId, votingCardId, credentialId);

		// Construct authenticate voter response payload depending on the verification card state (re-login).
		final VerificationCardState verificationCardState = verificationCardService.getVerificationCardState(credentialId);

		return switch (verificationCardState) {
			case INITIAL -> {
				final Ballot ballot = ballotDataService.findBallotByElectionEventIdAndBallotId(electionEventId, ballotId);
				final String ballotTexts = ballotDataService.findBallotTextsByElectionEventIdAndBallotId(electionEventId, ballotId);
				final String verificationCardJson = getVerificationCardJson(electionEventId, verificationCardSetId, verificationCardId, ballotId);
				final VotingClientPublicKeys votingClientPublicKeys = setupComponentPublicKeysService.getVotingClientPublicKeys(electionEventId);
				final VoterMaterial voterMaterial = new VoterMaterial(ballot, ballotTexts);

				yield new AuthenticateVoterResponsePayload(verificationCardState, voterMaterial, voterAuthenticationData, verificationCardJson,
						votingClientPublicKeys);
			}
			case SENT -> {
				final Ballot ballot = ballotDataService.findBallotByElectionEventIdAndBallotId(electionEventId, ballotId);
				final String ballotTexts = ballotDataService.findBallotTextsByElectionEventIdAndBallotId(electionEventId, ballotId);
				final String verificationCardJson = getVerificationCardJson(electionEventId, verificationCardSetId, verificationCardId, ballotId);
				final VotingClientPublicKeys votingClientPublicKeys = setupComponentPublicKeysService.getVotingClientPublicKeys(electionEventId);

				final List<String> shortChoiceReturnCodes = verificationCardService.getShortChoiceReturnCodes(credentialId);
				final VoterMaterial voterMaterial = new VoterMaterial(ballot, ballotTexts, shortChoiceReturnCodes);

				yield new AuthenticateVoterResponsePayload(verificationCardState, voterMaterial, voterAuthenticationData, verificationCardJson,
						votingClientPublicKeys);
			}
			case CONFIRMED -> {
				final VoterMaterial voterMaterial = new VoterMaterial(verificationCardService.getShortVoteCastReturnCode(credentialId));

				yield new AuthenticateVoterResponsePayload(verificationCardState, voterMaterial);
			}
			default -> throw new IllegalStateException(
					String.format("Invalid verification card state. [verificationCardId: %s, verificationCardState: %s]", verificationCardId,
							verificationCardState));
		};
	}

	private String getVerificationCardJson(final String electionEventId, final String verificationCardSetId, final String verificationCardId,
			final String ballotId) {

		final VerificationCardKeystore verificationCardKeystore = setupComponentVerificationCardKeystoreService.loadVerificationCardKeystore(
				electionEventId, verificationCardSetId, verificationCardId);

		final Ballot ballot = ballotDataService.findBallotByElectionEventIdAndBallotId(electionEventId, ballotId);
		final CombinedCorrectnessInformation combinedCorrectnessInformation = new CombinedCorrectnessInformation(ballot);

		final JsonNode encodedVotingOptionsJson;
		final JsonNode actualVotingOptionsJson;
		final JsonNode semanticInformationJson;
		final JsonNode ciSelectionsJson;
		final JsonNode ciVotingOptionsJson;
		try {
			encodedVotingOptionsJson = objectMapper.readTree(objectMapper.writeValueAsString(ballot.getEncodedVotingOptions()));
			actualVotingOptionsJson = objectMapper.readTree(objectMapper.writeValueAsString(ballot.getActualVotingOptions()));
			semanticInformationJson = objectMapper.readTree(objectMapper.writeValueAsString(ballot.getSemanticInformation()));

			ciSelectionsJson = objectMapper.readTree(
					objectMapper.writeValueAsString(combinedCorrectnessInformation.getCorrectnessInformationSelections()));
			ciVotingOptionsJson = objectMapper.readTree(
					objectMapper.writeValueAsString(combinedCorrectnessInformation.getCorrectnessInformationVotingOptions()));
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException("Failed to create verification card data nodes.", e);
		}

		final ObjectNode verificationCardNode = objectMapper.createObjectNode();
		verificationCardNode.put("id", verificationCardId);
		verificationCardNode.put(VERIFICATION_CARD_KEYSTORE, verificationCardKeystore.verificationCardKeystore());
		verificationCardNode.set(ENCODED_VOTING_OPTIONS, encodedVotingOptionsJson);
		verificationCardNode.set(ACTUAL_VOTING_OPTIONS, actualVotingOptionsJson);
		verificationCardNode.set(SEMANTIC_INFORMATION, semanticInformationJson);
		verificationCardNode.set(CI_SELECTIONS, ciSelectionsJson);
		verificationCardNode.set(CI_VOTING_OPTIONS, ciVotingOptionsJson);
		verificationCardNode.put(TOTAL_NUMBER_OF_WRITE_INS, combinedCorrectnessInformation.getTotalNumberOfWriteInOptions());

		return verificationCardNode.toString();
	}

}
