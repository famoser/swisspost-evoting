/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.orchestrator.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.domain.SharedQueue.LONG_VOTE_CAST_RETURN_CODES_ALLOW_LIST_REQUEST_PATTERN;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Arrays;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.commandmessaging.Context;
import ch.post.it.evoting.domain.configuration.setupvoting.LongVoteCastReturnCodesAllowListResponsePayload;
import ch.post.it.evoting.domain.configuration.setupvoting.SetupComponentLVCCAllowListPayload;
import ch.post.it.evoting.votingserver.orchestrator.voting.BroadcastCommand;
import ch.post.it.evoting.votingserver.orchestrator.voting.BroadcastCommandProducer;

@RestController
@RequestMapping("/api/v1/configuration/setupvoting")
public class LongVoteCastReturnCodesAllowListController {

	private static final Logger LOGGER = LoggerFactory.getLogger(LongVoteCastReturnCodesAllowListController.class);

	private final ObjectMapper objectMapper;
	private final BroadcastCommandProducer broadcastCommandProducer;

	public LongVoteCastReturnCodesAllowListController(
			final ObjectMapper objectMapper,
			final BroadcastCommandProducer broadcastCommandProducer) {
		this.objectMapper = objectMapper;
		this.broadcastCommandProducer = broadcastCommandProducer;
	}

	@PostMapping("/longvotecastreturncodesallowlist/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}")
	public void uploadLongVoteCastReturnCodesAllowList(
			@PathVariable
			final String electionEventId,
			@PathVariable
			final String verificationCardSetId,
			@RequestBody
			final SetupComponentLVCCAllowListPayload setupComponentLVCCAllowListPayload) {

		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		checkNotNull(setupComponentLVCCAllowListPayload);
		checkArgument(electionEventId.equals(setupComponentLVCCAllowListPayload.getElectionEventId()), "Election event id mismatch.");
		checkArgument(verificationCardSetId.equals(setupComponentLVCCAllowListPayload.getVerificationCardSetId()),
				"Verification card set id mismatch.");

		final String contextId = String.join("-", Arrays.asList(electionEventId, verificationCardSetId));
		final String correlationId = UUID.randomUUID().toString();

		LOGGER.info("Uploading long vote cast return codes allow list... [contextId: {}, correlationId: {}]", contextId, correlationId);

		final BroadcastCommand<LongVoteCastReturnCodesAllowListResponsePayload> broadcastCommand = new BroadcastCommand.Builder<LongVoteCastReturnCodesAllowListResponsePayload>()
				.contextId(contextId)
				.context(Context.CONFIGURATION_SETUP_VOTING_LONG_VOTE_CAST_RETURN_CODES_ALLOW_LIST)
				.payload(setupComponentLVCCAllowListPayload)
				.pattern(LONG_VOTE_CAST_RETURN_CODES_ALLOW_LIST_REQUEST_PATTERN)
				.deserialization(this::deserializePayload)
				.build();

		broadcastCommandProducer.sendMessagesAwaitingNotification(broadcastCommand);

		LOGGER.info("Successfully uploaded long vote cast return codes allow list. [contextId: {}, correlationId: {}]", contextId, correlationId);
	}

	private LongVoteCastReturnCodesAllowListResponsePayload deserializePayload(final byte[] payload) {
		try {
			return objectMapper.readValue(payload, LongVoteCastReturnCodesAllowListResponsePayload.class);
		} catch (final IOException e) {
			throw new UncheckedIOException(e);
		}
	}
}
