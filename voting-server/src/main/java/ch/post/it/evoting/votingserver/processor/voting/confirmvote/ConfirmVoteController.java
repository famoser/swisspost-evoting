/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor.voting.confirmvote;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.post.it.evoting.evotinglibraries.domain.common.ContextIds;
import ch.post.it.evoting.votingserver.common.Constants;
import ch.post.it.evoting.votingserver.processor.IdentifierValidationService;
import ch.post.it.evoting.votingserver.processor.voting.AuthenticationChallenge;
import ch.post.it.evoting.votingserver.processor.voting.AuthenticationStep;
import ch.post.it.evoting.votingserver.processor.voting.VerifyAuthenticationChallengeService;

/**
 * Web service for retrieving the short Vote Cast Return Code (in collaboration with the control components).
 */
@RestController
@RequestMapping("api/v1/processor/voting/confirmvote")
public class ConfirmVoteController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ConfirmVoteController.class);

	private final VoteCastReturnCodeService voteCastReturnCodeService;
	private final IdentifierValidationService identifierValidationService;
	private final VerifyAuthenticationChallengeService verifyAuthenticationChallengeService;

	public ConfirmVoteController(
			final VoteCastReturnCodeService voteCastReturnCodeService,
			final IdentifierValidationService identifierValidationService,
			final VerifyAuthenticationChallengeService verifyAuthenticationChallengeService) {
		this.voteCastReturnCodeService = voteCastReturnCodeService;
		this.identifierValidationService = identifierValidationService;
		this.verifyAuthenticationChallengeService = verifyAuthenticationChallengeService;
	}

	/**
	 * Retrieves the short Vote Cast Return Code for a given election event id and voting card id using a Confirmation Key.
	 *
	 * @param electionEventId       the election event identifier.
	 * @param verificationCardSetId the verification card set identifier.
	 * @param credentialId          the credential id
	 * @param verificationCardId    the verification card identifier
	 * @param confirmVotePayload    the confirmation key payload.
	 * @return the short Vote Cast Return Code.
	 */
	@PostMapping("electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}/credentialId/{credentialId}/verificationcard/{verificationCardId}")
	public ConfirmVoteResponsePayload retrieveShortVoteCastReturnCode(
			@PathVariable(Constants.PARAMETER_VALUE_ELECTION_EVENT_ID)
			final String electionEventId,
			@PathVariable(Constants.PARAMETER_VALUE_VERIFICATION_CARD_SET_ID)
			final String verificationCardSetId,
			@PathVariable(Constants.PARAMETER_VALUE_CREDENTIAL_ID)
			final String credentialId,
			@PathVariable(Constants.PARAMETER_VALUE_VERIFICATION_CARD_ID)
			final String verificationCardId,
			@RequestBody
			final ConfirmVotePayload confirmVotePayload) {

		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		validateUUID(credentialId);
		validateUUID(verificationCardId);

		final ContextIds contextIds = confirmVotePayload.contextIds();
		LOGGER.info("Received request to retrieve short Vote Cast Return Code. [contextIds: {}, credentialId: {}]", contextIds, credentialId);

		// Cross validate ids.
		checkArgument(electionEventId.equals(contextIds.electionEventId()),
				"The request election event id does not match the payload election event id.");
		checkArgument(verificationCardSetId.equals(contextIds.verificationCardSetId()),
				"The request verification card set id does not match the payload verification card set id.");
		checkArgument(verificationCardId.equals(contextIds.verificationCardId()),
				"The request verification card id does not match the payload verification card id.");

		final AuthenticationChallenge authenticationChallenge = confirmVotePayload.authenticationChallenge();
		final String payloadCredentialId = authenticationChallenge.derivedVoterIdentifier();
		checkArgument(credentialId.equals(payloadCredentialId), "The request credential id does not match the payload credential id.");

		// Validate context ids coherence.
		identifierValidationService.validateContextIds(contextIds, credentialId);
		LOGGER.debug("Validated context ids. [contextIds: {}, credentialId: {}]", contextIds, credentialId);

		// Verify authentication challenge.
		verifyAuthenticationChallengeService.verifyAuthenticationChallenge(electionEventId, AuthenticationStep.CONFIRM_VOTE, authenticationChallenge);

		// Retrieve Vote Cast Code.
		final String shortVoteCastReturnCode = voteCastReturnCodeService.retrieveShortVoteCastCode(contextIds, confirmVotePayload.confirmationKey());

		return new ConfirmVoteResponsePayload(shortVoteCastReturnCode);
	}

}
