/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.nio.charset.StandardCharsets;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "BALLOT_DATA")
public class BallotDataEntity {

	@Id
	private String ballotId;

	@ManyToOne
	@JoinColumn(name = "ELECTION_EVENT_FK_ID", referencedColumnName = "ELECTION_EVENT_ID")
	private ElectionEventEntity electionEventEntity;

	private byte[] ballotJson;

	private byte[] ballotTextsJson;

	@Version
	private Integer changeControlId;

	public BallotDataEntity() {
	}

	public BallotDataEntity(final String ballotId, final ElectionEventEntity electionEventEntity, final byte[] ballotJson,
			final byte[] ballotTextsJson) {
		this.ballotId = validateUUID(ballotId);
		this.electionEventEntity = checkNotNull(electionEventEntity);
		this.ballotJson = checkNotNull(ballotJson);
		this.ballotTextsJson = checkNotNull(ballotTextsJson);
	}

	public String getBallotId() {
		return ballotId;
	}

	public ElectionEventEntity getElectionEventEntity() {
		return electionEventEntity;
	}

	public byte[] getBallotJson() {
		return ballotJson;
	}

	public String getBallotTextsJson() {
		return new String(ballotTextsJson, StandardCharsets.UTF_8);
	}

	public Integer getChangeControlId() {
		return changeControlId;
	}

}
