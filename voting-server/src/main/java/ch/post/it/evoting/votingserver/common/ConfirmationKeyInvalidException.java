/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.common;

public class ConfirmationKeyInvalidException extends RuntimeException {

	private final int remainingAttempts;

	public ConfirmationKeyInvalidException(final String message, final int remainingAttempts) {
		super(message);
		this.remainingAttempts = remainingAttempts;
	}

	public int getRemainingAttempts() {
		return remainingAttempts;
	}

}
