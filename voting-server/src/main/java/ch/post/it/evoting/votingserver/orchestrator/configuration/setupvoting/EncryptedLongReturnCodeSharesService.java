/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.orchestrator.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.domain.SharedQueue.GEN_ENC_LONG_CODE_SHARES_REQUEST_PATTERN;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.commandmessaging.Context;
import ch.post.it.evoting.cryptoprimitives.domain.returncodes.ControlComponentCodeSharesPayload;
import ch.post.it.evoting.cryptoprimitives.domain.returncodes.SetupComponentVerificationDataPayload;
import ch.post.it.evoting.domain.configuration.setupvoting.ComputingStatus;
import ch.post.it.evoting.votingserver.orchestrator.voting.BroadcastCommand;
import ch.post.it.evoting.votingserver.orchestrator.voting.BroadcastCommandProducer;

@Service
public class EncryptedLongReturnCodeSharesService {

	private static final Logger LOGGER = LoggerFactory.getLogger(EncryptedLongReturnCodeSharesService.class);

	private final BroadcastCommandProducer broadcastCommandProducer;
	private final ObjectMapper objectMapper;
	private final EncLongCodeShareRepository encLongCodeShareRepository;


	public EncryptedLongReturnCodeSharesService(
			final BroadcastCommandProducer broadcastCommandProducer,
			final ObjectMapper objectMapper,
			final EncLongCodeShareRepository encLongCodeShareRepository) {
		this.broadcastCommandProducer = broadcastCommandProducer;
		this.objectMapper = objectMapper;
		this.encLongCodeShareRepository = encLongCodeShareRepository;
	}

	void computeGenEncLongCodeShares(final String electionEventId, final String verificationCardSetId,final int chunkId,
			final SetupComponentVerificationDataPayload setupComponentVerificationDataPayload) {

		final String contextId = String.join("-", electionEventId, verificationCardSetId, String.valueOf(chunkId));

		final BroadcastCommand<SetupComponentVerificationDataPayload> broadcastCommand = new BroadcastCommand.Builder<SetupComponentVerificationDataPayload>()
				.contextId(contextId)
				.context(Context.CONFIGURATION_RETURN_CODES_GEN_ENC_LONG_CODE_SHARES)
				.payload(setupComponentVerificationDataPayload)
				.pattern(GEN_ENC_LONG_CODE_SHARES_REQUEST_PATTERN)
				.build();

		broadcastCommandProducer.sendMessagesAndForget(broadcastCommand);
	}

	@Transactional
	public List<ControlComponentCodeSharesPayload> getEncLongCodeShares(final String electionEventId, final String verificationCardSetId,
			final int chunkId) {

		final List<EncLongCodeShareEntity> encLongCodeShareEntities = encLongCodeShareRepository.findByElectionEventIdAndVerificationCardSetIdAndChunkId(
				electionEventId, verificationCardSetId, chunkId);

		final int numberOfEncLongCodeShares = encLongCodeShareEntities.size();
		final int numberOfControlComponents = NODE_IDS.size();

		if (numberOfEncLongCodeShares % numberOfControlComponents != 0) {
			throw new IllegalStateException(String.format(
					"The number of enc long code shares doesn't match the number of nodes. [numberOfEncLongCodeShares: %s, numberOfControlComponents: %s]",
					numberOfEncLongCodeShares, numberOfControlComponents));
		}

		return encLongCodeShareEntities.stream()
				.map(encLongCodeShareEntity -> {
					final byte[] bytes = encLongCodeShareEntity.getEncLongCodeShare();
					try {
						return objectMapper.readValue(bytes, ControlComponentCodeSharesPayload.class);
					} catch (final IOException e) {
						throw new UncheckedIOException(String.format(
								"Failed to deserialize the enc long code shares. [electionEventId: %s, verificationCardSetId: %s, chunkId: %s]",
								electionEventId, verificationCardSetId, chunkId), e);
					}
				})
				.toList();
	}

	@Transactional
	public ComputingStatus getEncLongCodeSharesComputingStatus(final String electionEventId, final String verificationCardSetId,
			final int chunkCount) {

		final int expectedCount = NODE_IDS.size() * chunkCount;
		final long actualCount = encLongCodeShareRepository.countByElectionEventIdAndVerificationCardSetId(electionEventId, verificationCardSetId);

		LOGGER.debug("Asked for computing status. [electionEventId: {}, verificationCardSetId: {}, expectedCount: {}, actualCount: {}]",
				electionEventId, verificationCardSetId, expectedCount, actualCount);

		if (actualCount < expectedCount) {
			return ComputingStatus.COMPUTING;
		} else if (actualCount == expectedCount) {
			return ComputingStatus.COMPUTED;
		} else {
			return ComputingStatus.COMPUTING_ERROR;
		}
	}

	@Transactional
	public void saveControlComponentCodeSharesPayload(final ControlComponentCodeSharesPayload controlComponentCodeSharesPayload) {
		final String electionEventId = controlComponentCodeSharesPayload.getElectionEventId();
		final String verificationCardSetId = controlComponentCodeSharesPayload.getVerificationCardSetId();
		final int chunkId = controlComponentCodeSharesPayload.getChunkId();
		final int nodeId = controlComponentCodeSharesPayload.getNodeId();

		final byte[] encLongCodeShare;
		try {
			encLongCodeShare = objectMapper.writeValueAsBytes(controlComponentCodeSharesPayload);
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException(String.format(
					"Failed to serialize control component code shares payload. [electionEventId: %s, verificationCardSetId: %s, chunkId: %s, nodeId: %s]",
					electionEventId, verificationCardSetId, chunkId, nodeId), e);
		}

		final EncLongCodeShareEntity encLongCodeShareEntity = new EncLongCodeShareEntity(electionEventId, verificationCardSetId, chunkId, nodeId,
				encLongCodeShare);
		encLongCodeShareRepository.save(encLongCodeShareEntity);

		LOGGER.debug("Saved enc long code share entity. [electionEventId: {}, verificationCardSetId: {}, chunkId: {}, nodeId: {}]",
				electionEventId, verificationCardSetId, chunkId, nodeId);
	}
}
