/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor.voting;

import java.util.Optional;

import ch.post.it.evoting.votingserver.processor.voting.VerifyAuthenticationChallengeOutput.VerifyAuthenticationChallengeStatus;

public class VerifyAuthenticationChallengeException extends RuntimeException {

	private final VerifyAuthenticationChallengeStatus errorStatus;
	private final Integer remainingAttempts;

	public VerifyAuthenticationChallengeException(final VerifyAuthenticationChallengeStatus errorStatus) {
		this.errorStatus = errorStatus;
		this.remainingAttempts = null;
	}

	public VerifyAuthenticationChallengeException(final VerifyAuthenticationChallengeStatus errorStatus,
			final Integer remainingAttempts) {
		this.errorStatus = errorStatus;
		this.remainingAttempts = remainingAttempts;
	}

	public VerifyAuthenticationChallengeStatus getErrorStatus() {
		return errorStatus;
	}

	public Optional<Integer> getRemainingAttempts() {
		return Optional.ofNullable(remainingAttempts);
	}

}
