/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor.idempotence;

import java.util.function.Supplier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class IdempotenceService<T extends Supplier<String>> {

	private static final Logger LOGGER = LoggerFactory.getLogger(IdempotenceService.class);
	private final IdempotentExecutionRepository idempotentExecutionRepository;

	public IdempotenceService(final IdempotentExecutionRepository idempotentExecutionRepository) {
		this.idempotentExecutionRepository = idempotentExecutionRepository;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public <U> U execute(final T context, final String executionKey, final Supplier<U> execution, final Supplier<U> getter) {
		final IdempotentExecutionId idempotentExecutionId = new IdempotentExecutionId(context.get(), executionKey);
		if (!exists(idempotentExecutionId)) {
			final U result = execution.get();
			save(idempotentExecutionId);
			return result;
		} else {
			LOGGER.warn("Execution bypassed. [context: {}, executionKey: {}]", context.get(), executionKey);
			return getter.get();
		}
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void execute(final T context, final String executionKey, final Runnable execution) {
		final IdempotentExecutionId idempotentExecutionId = new IdempotentExecutionId(context.get(), executionKey);
		if (!exists(idempotentExecutionId)) {
			execution.run();
			save(idempotentExecutionId);
		} else {
			LOGGER.warn("Execution bypassed. [context: {}, executionKey: {}]", context.get(), executionKey);
		}
	}

	private boolean exists(final IdempotentExecutionId idempotentExecutionId) {
		return idempotentExecutionRepository.existsById(idempotentExecutionId);
	}

	private void save(final IdempotentExecutionId idempotentExecutionId) {
		final IdempotentExecution idempotentExecutionEntity = new IdempotentExecution(idempotentExecutionId.getContext(),
				idempotentExecutionId.getExecutionKey());
		idempotentExecutionRepository.save(idempotentExecutionEntity);
	}
}
