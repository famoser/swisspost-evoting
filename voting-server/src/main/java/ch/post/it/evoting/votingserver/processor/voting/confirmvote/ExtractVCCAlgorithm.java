/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor.voting.confirmvote;

import static ch.post.it.evoting.cryptoprimitives.utils.Conversions.byteArrayToString;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableByteArray;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableString;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.symmetric.Symmetric;
import ch.post.it.evoting.cryptoprimitives.utils.KeyDerivation;
import ch.post.it.evoting.votingserver.processor.voting.ReturnCodesMappingTable;

/**
 * Implements the ExtractVCC algorithm.
 */
@Service
public class ExtractVCCAlgorithm {

	static final int NUMBER_OF_CONTROL_COMPONENTS = 4;

	private static final int KEY_DERIVATION_BYTES_LENGTH = 32;

	private final Hash hash;
	private final Symmetric symmetric;
	private final KeyDerivation keyDerivation;

	public ExtractVCCAlgorithm(
			final Hash hash,
			final Symmetric symmetric,
			final KeyDerivation keyDerivation) {
		this.hash = hash;
		this.symmetric = symmetric;
		this.keyDerivation = keyDerivation;
	}

	/**
	 * Extracts the short Vote Cast Return Code VCC<sub>id</sub> from the Return Codes Mapping table CMtable.
	 *
	 * @param context the {@link ExtractVCCContext} containing necessary group and ids. Must be non-null.
	 * @param input   the {@link ExtractVCCInput} containing all needed inputs. Must be non-null.
	 * @return the short Vote Cast Return Code VCC<sub>id</sub>.
	 * @throws NullPointerException     if any of the fields is null.
	 * @throws IllegalArgumentException if the context and input do not have the same group.
	 * @throws IllegalStateException    if the corresponding short Choice Return Codes cannot be found.
	 */
	@SuppressWarnings("java:S117")
	public ExtractVCCOutput extractVCC(final ExtractVCCContext context, final ExtractVCCInput input) {
		checkNotNull(context);
		checkNotNull(input);

		// Cross group check.
		checkArgument(context.encryptionGroup().equals(input.getGroup()), "The context and input must have the same group.");

		// Variables.
		final GqGroup gqGroup = context.encryptionGroup();
		final GqElement identity = GqElement.GqElementFactory.fromValue(BigInteger.ONE, gqGroup);
		final String ee = context.electionEventId();
		final String vc_id = input.verificationCardId();
		final List<GqElement> lVCC_id_vector = input.longVoteCastReturnCodeShares();
		final ReturnCodesMappingTable CMtable = input.returnCodesMappingTable();

		// Operations.
		final GqElement pVCC_id = lVCC_id_vector.stream().reduce(identity, GqElement::multiply);

		final byte[] lVCC_id = hash.recursiveHash(pVCC_id, HashableString.from(vc_id), HashableString.from(ee));

		final String key = Base64.getEncoder().encodeToString(hash.recursiveHash(HashableByteArray.from(lVCC_id)));

		final Optional<String> encryptedVoteCastReturnCode = CMtable.get(key);
		if (encryptedVoteCastReturnCode.isEmpty()) {
			throw new IllegalStateException(
					String.format("Encrypted short Vote Cast Return Code not found in CMtable. [electionEventId: %s, verificationCardId: %s]", ee,
							vc_id));

		} else {
			final String ctVCC_id_i_encoded = encryptedVoteCastReturnCode.get();

			final byte[] ctVCC_id_combined = Base64.getDecoder().decode(ctVCC_id_i_encoded);

			final int length = ctVCC_id_combined.length;

			final int split = length - symmetric.getNonceLength();

			final byte[] ctVCC_id_ciphertext = Arrays.copyOfRange(ctVCC_id_combined, 0, split);

			final byte[] ctVCC_id_nonce = Arrays.copyOfRange(ctVCC_id_combined, split, length);

			final byte[] skvcc_id = keyDerivation.KDF(lVCC_id, List.of(), KEY_DERIVATION_BYTES_LENGTH);

			final byte[] VCC_id_bytes = symmetric.getPlaintextSymmetric(skvcc_id, ctVCC_id_ciphertext, ctVCC_id_nonce, List.of());

			final String VCC_id = byteArrayToString(VCC_id_bytes);

			return new ExtractVCCOutput(VCC_id);
		}
	}

}
