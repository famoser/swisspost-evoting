/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor;

public enum VerificationCardState {
	INITIAL,
	SENT,
	CONFIRMED,
	BLOCKED,
	AUTHENTICATION_ATTEMPTS_EXCEEDED,
	CONFIRMATION_ATTEMPTS_EXCEEDED;

	public boolean isInactive() {
		return BLOCKED.equals(this) || AUTHENTICATION_ATTEMPTS_EXCEEDED.equals(this) || CONFIRMATION_ATTEMPTS_EXCEEDED.equals(this);
	}

}
