/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public interface ElectionEventContextRepository extends CrudRepository<ElectionEventContextEntity, String> {

	@Transactional(isolation = Isolation.SERIALIZABLE)
	@Query("select new ch.post.it.evoting.votingserver.processor.ElectionEventDTO("
			+ "eece.electionEventId, "
			+ "eece.electionEventAlias, "
			+ "eece.electionEventDescription) "
			+ "from ElectionEventContextEntity eece")
	List<ElectionEventDTO> findAllElectionEvents();
}
