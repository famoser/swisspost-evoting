/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor.idempotence;

import static com.google.common.base.Preconditions.checkNotNull;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Version;

@Entity
@IdClass(IdempotentExecutionId.class)
public class IdempotentExecution {

	@Id
	private String context;

	@Id
	private String executionKey;

	@Version
	private Long changeControlId;

	protected IdempotentExecution() {
	}

	public IdempotentExecution(final String context, final String executionKey) {
		this.context = checkNotNull(context);
		this.executionKey = checkNotNull(executionKey);
	}

	public String getContext() {
		return context;
	}

	public String getExecutionKey() {
		return executionKey;
	}
}
