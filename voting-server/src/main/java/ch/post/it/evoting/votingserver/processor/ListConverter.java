/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.List;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@Converter
public class ListConverter implements AttributeConverter<List<String>, byte[]> {

	private final ObjectMapper objectMapper;

	public ListConverter(final ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
	}

	@Override
	public byte[] convertToDatabaseColumn(final List<String> list) {
		try {
			return objectMapper.writeValueAsBytes(list);
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException("Failed to serialize list to bytes.", e);
		}
	}

	@Override
	public List<String> convertToEntityAttribute(final byte[] bytes) {
		try {
			return objectMapper.readValue(bytes, new TypeReference<>() {
			});
		} catch (final IOException e) {
			throw new UncheckedIOException("Failed to deserialize bytes into list.", e);
		}
	}

}
