/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.orchestrator.configuration.setupvoting;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.commandmessaging.Context;
import ch.post.it.evoting.domain.configuration.setupvoting.LongVoteCastReturnCodesAllowListResponsePayload;
import ch.post.it.evoting.votingserver.orchestrator.aggregator.AggregatorService;
import ch.post.it.evoting.votingserver.orchestrator.voting.CommandFacade;

@Component
public class LongVoteCastReturnCodesAllowListConsumer {

	private static final Logger LOGGER = LoggerFactory.getLogger(LongVoteCastReturnCodesAllowListConsumer.class);

	private final CommandFacade commandFacade;
	private final ObjectMapper objectMapper;
	private final AggregatorService aggregatorService;

	public LongVoteCastReturnCodesAllowListConsumer(final CommandFacade commandFacade, final ObjectMapper objectMapper,
			final AggregatorService aggregatorService) {
		this.commandFacade = commandFacade;
		this.objectMapper = objectMapper;
		this.aggregatorService = aggregatorService;
	}

	@RabbitListener(queues = "#{queueNameResolver.get(\"LONG_VOTE_CAST_RETURN_CODES_ALLOW_LIST_RESPONSE_PATTERN\")}")
	public void consume(final Message message) throws IOException {

		final String correlationId = message.getMessageProperties().getCorrelationId();
		checkNotNull(correlationId);

		final byte[] encodedResponse = message.getBody();
		final LongVoteCastReturnCodesAllowListResponsePayload payload = objectMapper.readValue(encodedResponse,
				LongVoteCastReturnCodesAllowListResponsePayload.class);

		final int nodeId = payload.nodeId();
		final String electionEventId = payload.electionEventId();
		final String verificationCardSetId = payload.verificationCardSetId();
		final String contextId = String.join("-", Arrays.asList(electionEventId, verificationCardSetId));

		LOGGER.info("Received long vote cast return codes allow list response. [contextId: {}, correlationId: {}, nodeId: {}]", contextId,
				correlationId, nodeId);

		commandFacade.saveResponse(encodedResponse, correlationId, contextId,
				Context.CONFIGURATION_SETUP_VOTING_LONG_VOTE_CAST_RETURN_CODES_ALLOW_LIST, nodeId);

		aggregatorService.notifyPartialResponseReceived(correlationId, contextId);
	}
}
