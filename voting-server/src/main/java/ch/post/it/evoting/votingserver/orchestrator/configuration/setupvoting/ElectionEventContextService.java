/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.orchestrator.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.domain.SharedQueue.ELECTION_CONTEXT_REQUEST_PATTERN;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.commandmessaging.Context;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.domain.configuration.ElectionContextResponsePayload;
import ch.post.it.evoting.votingserver.orchestrator.voting.BroadcastCommand;
import ch.post.it.evoting.votingserver.orchestrator.voting.BroadcastCommandProducer;

@Service
public class ElectionEventContextService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ElectionEventContextService.class);

	private final ObjectMapper objectMapper;
	private final BroadcastCommandProducer broadcastCommandProducer;

	public ElectionEventContextService(
			final ObjectMapper objectMapper,
			final BroadcastCommandProducer broadcastCommandProducer) {
		this.objectMapper = objectMapper;
		this.broadcastCommandProducer = broadcastCommandProducer;
	}

	public void uploadElectionEventContextPayload(final String electionEventId, final ElectionEventContextPayload electionEventContextPayload) {
		validateUUID(electionEventId);
		checkNotNull(electionEventContextPayload);

		final String contextId = electionEventId;
		final String correlationId = UUID.randomUUID().toString();

		LOGGER.debug("Uploading election event context... [electionEventId: {}, correlationId: {}]", electionEventId, correlationId);

		final BroadcastCommand<ElectionContextResponsePayload> broadcastCommand = new BroadcastCommand.Builder<ElectionContextResponsePayload>()
				.contextId(contextId)
				.context(Context.CONFIGURATION_ELECTION_CONTEXT)
				.payload(electionEventContextPayload)
				.pattern(ELECTION_CONTEXT_REQUEST_PATTERN)
				.deserialization(this::deserializePayload)
				.build();

		broadcastCommandProducer.sendMessagesAwaitingNotification(broadcastCommand);

		LOGGER.info("Successfully uploaded election event context. [electionEventId: {}, correlationId: {}]", electionEventId, correlationId);
	}

	private ElectionContextResponsePayload deserializePayload(final byte[] payload) {
		try {
			return objectMapper.readValue(payload, ElectionContextResponsePayload.class);
		} catch (final IOException e) {
			throw new UncheckedIOException(e);
		}
	}

}
