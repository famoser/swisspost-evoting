/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.UncheckedIOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.election.SetupComponentPublicKeys;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.SetupComponentPublicKeysPayload;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.votingserver.orchestrator.OrchestratorFacade;

@Service
public class SetupComponentPublicKeysService {

	private static final String GROUP = "group";
	private static final Logger LOGGER = LoggerFactory.getLogger(SetupComponentPublicKeysService.class);

	private final ObjectMapper objectMapper;
	private final OrchestratorFacade orchestratorFacade;
	private final ElectionEventService electionEventService;
	private final SetupComponentPublicKeysRepository setupComponentPublicKeysRepository;

	public SetupComponentPublicKeysService(
			final ObjectMapper objectMapper,
			final OrchestratorFacade orchestratorFacade,
			final ElectionEventService electionEventService,
			final SetupComponentPublicKeysRepository setupComponentPublicKeysRepository) {
		this.objectMapper = objectMapper;
		this.orchestratorFacade = orchestratorFacade;
		this.electionEventService = electionEventService;
		this.setupComponentPublicKeysRepository = setupComponentPublicKeysRepository;
	}

	/**
	 * Saves the setup component public keys and uploads it to the control components.
	 *
	 * @param setupComponentPublicKeysPayload the request payload. Must be non null.
	 * @throws NullPointerException             if {@code setupComponentPublicKeysPayload} is null.
	 * @throws IllegalStateException            if an error occurred while verifying the signature of the election event context payload.
	 * @throws InvalidPayloadSignatureException if the signature of the election event context payload is invalid.
	 * @throws UncheckedIOException             if an error occurs while serializing the election event context.
	 */
	@Transactional
	public void saveSetupComponentPublicKeys(final SetupComponentPublicKeysPayload setupComponentPublicKeysPayload) {
		checkNotNull(setupComponentPublicKeysPayload);

		final String electionEventId = setupComponentPublicKeysPayload.getElectionEventId();

		// Upload setup component public keys to control components.
		orchestratorFacade.uploadSetupComponentPublicKeys(electionEventId, setupComponentPublicKeysPayload);
		LOGGER.info("Setup component public keys successfully uploaded to the control components. [electionEventId: {}]", electionEventId);

		// save setup component public keys in vote verification
		setupComponentPublicKeysRepository.save(
				buildSetupComponentPublicKeysEntity(electionEventId, setupComponentPublicKeysPayload.getSetupComponentPublicKeys()));
		LOGGER.info("Setup component public keys successfully saved. [electionEventId: {}]", electionEventId);
	}

	/**
	 * Recovers the voting client public keys from the setup component public keys entity using the given election event id.
	 *
	 * @param electionEventId the election event id. Must be non-null and a valid UUID.
	 * @return the voting client public keys.
	 * @throws FailedValidationException if {@code electionEventId} is not valid.
	 */
	@Transactional
	public VotingClientPublicKeys getVotingClientPublicKeys(final String electionEventId) {
		validateUUID(electionEventId);

		// Retrieve the setup component public keys.
		final ElectionEventEntity electionEventEntity = electionEventService.retrieveElectionEventEntity(electionEventId);
		final SetupComponentPublicKeysEntity setupComponentPublicKeysEntity = setupComponentPublicKeysRepository
				.findById(electionEventId)
				.orElseThrow(() -> new IllegalStateException(
						String.format("Setup component public keys not found. [electionEventId: %s]", electionEventId)));
		LOGGER.info("Setup component public keys found. [electionEventId: {}]", electionEventId);

		// Retrieve group.
		final GqGroup encryptionGroup = electionEventEntity.getEncryptionGroup();

		// Deserialize the voting client public keys.
		final ElGamalMultiRecipientPublicKey electionPublicKey;
		final ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey;
		try {
			electionPublicKey = objectMapper.reader()
					.withAttribute(GROUP, encryptionGroup)
					.readValue(setupComponentPublicKeysEntity.getElectionPublicKey(), ElGamalMultiRecipientPublicKey.class);
			choiceReturnCodesEncryptionPublicKey = objectMapper.reader()
					.withAttribute(GROUP, encryptionGroup)
					.readValue(setupComponentPublicKeysEntity.getChoiceReturnCodesEncryptionPublicKey(), ElGamalMultiRecipientPublicKey.class);
		} catch (final IOException e) {
			throw new UncheckedIOException(String.format("Failed to deserialize the voting client public keys. [electionEventId: %s]",
					setupComponentPublicKeysEntity.getElectionEventEntity().getElectionEventId()), e);
		}

		LOGGER.info("Voting client public keys recovered. [electionEventId: {}]", electionEventId);
		return new VotingClientPublicKeys(encryptionGroup, electionPublicKey, choiceReturnCodesEncryptionPublicKey);
	}

	private SetupComponentPublicKeysEntity buildSetupComponentPublicKeysEntity(final String electionEventId,
			final SetupComponentPublicKeys setupComponentPublicKeys) {

		final byte[] serializedCombinedControlComponentPublicKeys;
		final byte[] serializedElectoralBoardPublicKey;
		final byte[] serializedElectoralBoardSchnorrProofs;
		final byte[] serializedElectionPublicKey;
		final byte[] serializedChoiceReturnCodesPublicKey;
		final ElectionEventEntity electionEventEntity;
		try {
			serializedCombinedControlComponentPublicKeys = objectMapper.writeValueAsBytes(
					setupComponentPublicKeys.combinedControlComponentPublicKeys());
			serializedElectoralBoardPublicKey = objectMapper.writeValueAsBytes(setupComponentPublicKeys.electoralBoardPublicKey());
			serializedElectoralBoardSchnorrProofs = objectMapper.writeValueAsBytes(setupComponentPublicKeys.electoralBoardSchnorrProofs());
			serializedElectionPublicKey = objectMapper.writeValueAsBytes(setupComponentPublicKeys.electionPublicKey());
			serializedChoiceReturnCodesPublicKey = objectMapper.writeValueAsBytes(setupComponentPublicKeys.choiceReturnCodesEncryptionPublicKey());
			electionEventEntity = electionEventService.retrieveElectionEventEntity(electionEventId);
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException("Failed to serialize setup component public keys.", e);
		}

		return new SetupComponentPublicKeysEntity(electionEventEntity, serializedCombinedControlComponentPublicKeys,
				serializedElectoralBoardPublicKey,
				serializedElectoralBoardSchnorrProofs, serializedElectionPublicKey, serializedChoiceReturnCodesPublicKey);
	}

}
