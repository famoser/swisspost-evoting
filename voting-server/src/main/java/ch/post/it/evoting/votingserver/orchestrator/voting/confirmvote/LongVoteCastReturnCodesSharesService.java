/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.orchestrator.voting.confirmvote;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.domain.SharedQueue.CREATE_LVCC_SHARE_HASH_REQUEST_PATTERN;
import static ch.post.it.evoting.domain.SharedQueue.VERIFY_LVCC_SHARE_HASH_REQUEST_PATTERN;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.math.BigInteger;
import java.util.Base64;
import java.util.Comparator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.commandmessaging.Context;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableBigInteger;
import ch.post.it.evoting.domain.voting.confirmvote.ControlComponenthlVCCPayload;
import ch.post.it.evoting.domain.voting.confirmvote.ControlComponentlVCCSharePayload;
import ch.post.it.evoting.domain.voting.confirmvote.VotingServerConfirmPayload;
import ch.post.it.evoting.votingserver.orchestrator.voting.BroadcastCommand;
import ch.post.it.evoting.votingserver.orchestrator.voting.BroadcastCommandProducer;
import ch.post.it.evoting.votingserver.processor.VerificationCardService;

@Service
public class LongVoteCastReturnCodesSharesService {

	private static final Logger LOGGER = LoggerFactory.getLogger(LongVoteCastReturnCodesSharesService.class);

	private final Hash hash;
	private final ObjectMapper objectMapper;
	private final VerificationCardService verificationCardService;
	private final BroadcastCommandProducer broadcastProducerService;

	public LongVoteCastReturnCodesSharesService(
			final Hash hash,
			final ObjectMapper objectMapper,
			final VerificationCardService verificationCardService,
			final BroadcastCommandProducer broadcastProducerService) {
		this.hash = hash;
		this.objectMapper = objectMapper;
		this.verificationCardService = verificationCardService;
		this.broadcastProducerService = broadcastProducerService;
	}

	public List<ControlComponentlVCCSharePayload> getLongVoteCastReturnCodesContributions(final String electionEventId,
			final String verificationCardSetId, final String verificationCardId, final VotingServerConfirmPayload votingServerConfirmPayload) {

		validateUUID(electionEventId);
		validateUUID(verificationCardId);
		checkNotNull(votingServerConfirmPayload);
		LOGGER.info("Received request to compute LVCC share [electionEventId : {}, verificationCardSetId : {}, verificationCardId : {}]",
				electionEventId, verificationCardSetId, verificationCardId);

		final BigInteger confirmationKeyValue = votingServerConfirmPayload.getConfirmationKey().element().getValue();
		final String confirmationKeyAsString = Base64.getEncoder().encodeToString(hash.recursiveHash(HashableBigInteger.from(confirmationKeyValue)));

		final String contextId = String.join("-", electionEventId, verificationCardSetId, verificationCardId, confirmationKeyAsString);

		final BroadcastCommand<ControlComponenthlVCCPayload> createLVCCCommand = new BroadcastCommand.Builder<ControlComponenthlVCCPayload>()
				.contextId(contextId)
				.context(Context.VOTING_RETURN_CODES_CREATE_LVCC_SHARE_HASH)
				.payload(votingServerConfirmPayload)
				.pattern(CREATE_LVCC_SHARE_HASH_REQUEST_PATTERN)
				.deserialization(this::deserializeHashPayload)
				.build();

		final List<ControlComponenthlVCCPayload> longReturnCodesShareHashPayloads = broadcastProducerService.sendMessagesAwaitingNotification(
				createLVCCCommand);

		LOGGER.info("Received Long Vote Cast Return Codes Share hashes [electionEventId : {}, verificationCardSetId : {}, verificationCardId : {}]",
				electionEventId, verificationCardSetId, verificationCardId);

		final BroadcastCommand<ControlComponentlVCCSharePayload> verifyLVCCCommand = new BroadcastCommand.Builder<ControlComponentlVCCSharePayload>()
				.contextId(contextId)
				.context(Context.VOTING_RETURN_CODES_VERIFY_LVCC_SHARE_HASH)
				.payload(longReturnCodesShareHashPayloads)
				.pattern(VERIFY_LVCC_SHARE_HASH_REQUEST_PATTERN)
				.deserialization(this::deserializeControlComponentlVCCSharePayload)
				.build();

		return broadcastProducerService.sendMessagesAwaitingNotification(verifyLVCCCommand);
	}

	private ControlComponenthlVCCPayload deserializeHashPayload(final byte[] payload) {
		try {
			return objectMapper.readValue(payload, ControlComponenthlVCCPayload.class);
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
	}

	private ControlComponentlVCCSharePayload deserializeControlComponentlVCCSharePayload(final byte[] payload) {
		try {
			return objectMapper.readValue(payload, ControlComponentlVCCSharePayload.class);
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
	}

}
