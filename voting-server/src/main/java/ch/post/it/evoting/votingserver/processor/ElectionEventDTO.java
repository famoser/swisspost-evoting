/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateNonBlankUCS;
import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;

public record ElectionEventDTO(String electionEventId, String electionEventAlias, String electionEventDescription) {

	public ElectionEventDTO {
		validateUUID(electionEventId);
		validateNonBlankUCS(electionEventAlias);
		validateNonBlankUCS(electionEventDescription);
	}
}
