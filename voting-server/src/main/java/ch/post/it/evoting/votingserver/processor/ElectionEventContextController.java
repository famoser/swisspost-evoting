/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkState;

import java.security.SignatureException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.votingserver.common.Constants;

/**
 * Web service for saving the election event context.
 */
@RestController
@RequestMapping("api/v1/processor/configuration/electioncontext/")
public class ElectionEventContextController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ElectionEventContextController.class);

	private final ElectionEventService electionEventService;
	private final SignatureKeystore<Alias> signatureKeystoreService;
	private final ElectionEventContextService electionEventContextService;

	public ElectionEventContextController(
			final ElectionEventContextService electionEventContextService,
			final ElectionEventService electionEventService,
			final SignatureKeystore<Alias> signatureKeystoreService) {
		this.electionEventContextService = electionEventContextService;
		this.electionEventService = electionEventService;
		this.signatureKeystoreService = signatureKeystoreService;
	}

	@Transactional
	@PostMapping("electionevent/{electionEventId}")
	public void saveElectionEventContext(
			@PathVariable(Constants.PARAMETER_VALUE_ELECTION_EVENT_ID)
			final String electionEventId,
			@RequestBody
			final ElectionEventContextPayload electionEventContextPayload) {

		validateUUID(electionEventId);
		verifyPayloadSignature(electionEventContextPayload);

		electionEventService.save(electionEventId, electionEventContextPayload.getEncryptionGroup());
		electionEventContextService.saveElectionEventContext(electionEventContextPayload);
		LOGGER.info("Successfully uploaded and saved the election event context. [electionEventId: {}]", electionEventId);
	}

	private void verifyPayloadSignature(final ElectionEventContextPayload electionEventContextPayload) {
		final String electionEventId = electionEventContextPayload.getElectionEventContext().electionEventId();

		final CryptoPrimitivesSignature signature = electionEventContextPayload.getSignature();

		checkState(signature != null, "The signature of the election event context payload is null. [electionEventId: %s]", electionEventId);

		final Hashable additionalContextData = ChannelSecurityContextData.electionEventContext(electionEventId);

		final boolean isSignatureValid;
		try {
			isSignatureValid = signatureKeystoreService.verifySignature(Alias.SDM_CONFIG, electionEventContextPayload,
					additionalContextData, signature.signatureContents());
		} catch (final SignatureException e) {
			throw new IllegalStateException(
					String.format("Could not verify the signature of the election event context. [electionEventId: %s]", electionEventId));
		}

		if (!isSignatureValid) {
			throw new InvalidPayloadSignatureException(ElectionEventContextPayload.class, String.format("[electionEventId: %s]", electionEventId));
		}
	}
}
