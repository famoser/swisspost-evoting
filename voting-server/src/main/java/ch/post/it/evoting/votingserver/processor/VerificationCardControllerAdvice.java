/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class VerificationCardControllerAdvice extends ResponseEntityExceptionHandler {

	private static final Logger LOG = LoggerFactory.getLogger(VerificationCardControllerAdvice.class);

	@ExceptionHandler(value = { IllegalStateException.class })
	public ResponseEntity<VotingCardDto> handleIllegalStateException(final HttpServletRequest httpServletRequest, final IllegalStateException ex) {
		LOG.error("Failed to process request. [request: {}]", httpServletRequest.getRequestURI(), ex);
		return new ResponseEntity<>(HttpStatus.PRECONDITION_FAILED);
	}

	@ExceptionHandler(value = { TooManyVerificationCardsException.class })
	public ResponseEntity<VotingCardDto> handleToManyVotingCardsException(final HttpServletRequest httpServletRequest,
			final TooManyVerificationCardsException ex) {
		LOG.debug("Failed to process request due too many voting cards found. [request: {}]", httpServletRequest.getRequestURI(), ex);
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(value = { VerificationCardNotFoundException.class })
	public ResponseEntity<VotingCardDto> handleVerificationCardNotFoundException(final HttpServletRequest httpServletRequest,
			final VerificationCardNotFoundException ex) {
		LOG.debug("Failed to process request due too many voting cards found. [request: {}]", httpServletRequest.getRequestURI(), ex);
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
}
