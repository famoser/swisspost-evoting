/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.orchestrator.aggregator;

public record BroadcastAggregatorMessage(String correlationId) {
}
