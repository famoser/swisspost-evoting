/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor.voting.sendvote;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;

/**
 * Regroups the context values needed by the ExtractCRC algorithm.
 */
public record ExtractCRCContext(GqGroup encryptionGroup, String electionEventId, String ballotId) {
	/**
	 * @throws NullPointerException      if any of the fields is null.
	 * @throws FailedValidationException if any id is invalid.
	 */
	public ExtractCRCContext {
		checkNotNull(encryptionGroup);
		validateUUID(electionEventId);
		validateUUID(ballotId);
	}

	public static class Builder {

		private GqGroup encryptionGroup;
		private String electionEventId;
		private String ballotId;

		public Builder setEncryptionGroup(final GqGroup encryptionGroup) {
			this.encryptionGroup = checkNotNull(encryptionGroup);
			return this;
		}

		public Builder setElectionEventId(final String electionEventId) {
			this.electionEventId = validateUUID(electionEventId);
			return this;
		}

		public Builder setBallotId(final String ballotId) {
			this.ballotId = validateUUID(ballotId);
			return this;
		}

		public ExtractCRCContext build() {
			return new ExtractCRCContext(encryptionGroup, electionEventId, ballotId);
		}

	}

}
