/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.UncheckedIOException;
import java.time.LocalDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ch.post.it.evoting.cryptoprimitives.domain.election.ElectionEventContext;
import ch.post.it.evoting.cryptoprimitives.domain.election.VerificationCardSetContext;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.votingserver.orchestrator.OrchestratorFacade;

@Service
public class ElectionEventContextService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ElectionEventContextService.class);

	private final OrchestratorFacade orchestratorFacade;
	private final ElectionEventService electionEventService;
	private final VerificationCardSetService verificationCardSetService;
	private final ElectionEventContextRepository electionEventContextRepository;

	public ElectionEventContextService(
			final OrchestratorFacade orchestratorFacade,
			final ElectionEventService electionEventService,
			final VerificationCardSetService verificationCardSetService,
			final ElectionEventContextRepository electionEventContextRepository) {
		this.orchestratorFacade = orchestratorFacade;
		this.electionEventService = electionEventService;
		this.verificationCardSetService = verificationCardSetService;
		this.electionEventContextRepository = electionEventContextRepository;
	}

	/**
	 * Saves the election event context and uploads it to the control components.
	 *
	 * @param electionEventContextPayload the request payload. Must be non null.
	 * @throws NullPointerException             if {@code electionEventContextPayload} is null.
	 * @throws IllegalStateException            if an error occurred while verifying the signature of the election event context payload.
	 * @throws IllegalArgumentException         if the election event finish date is in the past.
	 * @throws InvalidPayloadSignatureException if the signature of the election event context payload is invalid.
	 * @throws UncheckedIOException             if an error occurs while serializing the election event context.
	 */
	@Transactional
	public void saveElectionEventContext(final ElectionEventContextPayload electionEventContextPayload) {
		checkNotNull(electionEventContextPayload);

		final ElectionEventContext electionEventContext = electionEventContextPayload.getElectionEventContext();
		final String electionEventId = electionEventContext.electionEventId();

		checkArgument(electionEventContext.finishTime().isAfter(LocalDateTime.now()),
				"The election event period should not be finished yet. [electionEventId: %s]", electionEventId);

		// Save election event context.
		electionEventContextRepository.save(createElectionEventContextEntity(electionEventContext));
		LOGGER.info("Election event context successfully saved. [electionEventId: {}]", electionEventId);

		// Save verification card sets.
		final List<VerificationCardSetContext> verificationCardSetContexts = electionEventContext.verificationCardSetContexts();
		verificationCardSetService.saveAllFromContext(electionEventId, verificationCardSetContexts);

		// Upload election event context to control components.
		orchestratorFacade.uploadElectionEventContext(electionEventId, electionEventContextPayload);

		LOGGER.info("Election event context successfully uploaded to the control components. [electionEventId: {}]", electionEventId);
	}

	/**
	 * Gets the election event context entity.
	 *
	 * @param electionEventEntity the related election event entity. Must be non-null.
	 * @return the election event context entity.
	 * @throws NullPointerException  if {@code ElectionEventEntity} is null.
	 * @throws IllegalStateException if the election event context entity does not exist.
	 */
	@Transactional
	public ElectionEventContextEntity getElectionEventContextEntity(final ElectionEventEntity electionEventEntity) {
		checkNotNull(electionEventEntity);

		return electionEventContextRepository.findById(electionEventEntity.getElectionEventId())
				.orElseThrow(() -> new IllegalStateException(
						String.format("No election event context entity found. [electionEventId: %s]", electionEventEntity.getElectionEventId())));
	}

	private ElectionEventContextEntity createElectionEventContextEntity(final ElectionEventContext electionEventContext) {
		final ElectionEventEntity electionEventEntity = electionEventService.retrieveElectionEventEntity(electionEventContext.electionEventId());

		return new ElectionEventContextEntity(electionEventEntity, electionEventContext.startTime(), electionEventContext.finishTime(),
				electionEventContext.electionEventAlias(), electionEventContext.electionEventDescription());
	}

	/**
	 * Retrieves all election events.
	 */
	@Transactional
	public List<ElectionEventDTO> retrieveAll() {
		return electionEventContextRepository.findAllElectionEvents();
	}

}
