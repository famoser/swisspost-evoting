/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor.voting.sendvote;

import static ch.post.it.evoting.cryptoprimitives.utils.Conversions.byteArrayToString;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.election.Ballot;
import ch.post.it.evoting.cryptoprimitives.domain.election.CombinedCorrectnessInformation;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableByteArray;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableString;
import ch.post.it.evoting.cryptoprimitives.math.Base64;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.symmetric.Symmetric;
import ch.post.it.evoting.cryptoprimitives.utils.KeyDerivation;
import ch.post.it.evoting.votingserver.processor.BallotDataService;
import ch.post.it.evoting.votingserver.processor.voting.ReturnCodesMappingTable;

/**
 * Implements the ExtractCRC algorithm.
 */
@Service
public class ExtractCRCAlgorithm {

	static final int CHOICE_RETURN_CODES_LENGTH = 4;
	static final int NUMBER_OF_CONTROL_COMPONENTS = 4;

	private static final int KEY_DERIVATION_BYTES_LENGTH = 32;

	private final Hash hash;
	private final Base64 base64;
	private final Symmetric symmetric;
	private final KeyDerivation keyDerivation;
	private final BallotDataService ballotDataService;

	public ExtractCRCAlgorithm(
			final Hash hash,
			final Base64 base64,
			final Symmetric symmetric,
			final KeyDerivation keyDerivation,
			final BallotDataService ballotDataService) {
		this.hash = hash;
		this.base64 = base64;
		this.symmetric = symmetric;
		this.keyDerivation = keyDerivation;
		this.ballotDataService = ballotDataService;
	}

	/**
	 * Extracts the short Choice Return Codes CC<sub>id</sub> from the Return Codes Mapping table CMtable.
	 *
	 * @param context the {@link ExtractCRCContext} containing necessary group and ids. Must be non-null.
	 * @param input   the {@link ExtractCRCInput} containing all needed inputs. Must be non-null.
	 * @return the short Choice Return Codes CC<sub>id</sub>.
	 * @throws NullPointerException     if any of the fields is null.
	 * @throws IllegalArgumentException if the context and input do not have the same group.
	 */
	@SuppressWarnings("java:S117")
	public ExtractCRCOutput extractCRC(final ExtractCRCContext context, final ExtractCRCInput input) {
		checkNotNull(context);
		checkNotNull(input);

		// Cross group check.
		checkArgument(context.encryptionGroup().equals(input.getGroup()), "The context and input must have the same group.");

		// Variables.
		final int psi = input.longChoiceReturnCodeShares().get(0).size();
		final GqGroup gqGroup = context.encryptionGroup();
		final GqElement identity = GqElement.GqElementFactory.fromValue(BigInteger.ONE, gqGroup);
		final String ee = context.electionEventId();
		final String vc_id = input.verificationCardId();
		final ReturnCodesMappingTable CMtable = input.returnCodesMappingTable();

		final CombinedCorrectnessInformation combinedCorrectnessInformation = new CombinedCorrectnessInformation(getBallot(context));

		// Operations.
		final List<String> CC_id = new ArrayList<>();

		for (int i = 0; i < psi; i++) {

			final int final_i = i;
			final GqElement pC_id_i = input.longChoiceReturnCodeShares().stream().map(lCC_j_id -> lCC_j_id.get(final_i))
					.reduce(identity, GqElement::multiply);

			final byte[] lCC_id_i = hash.recursiveHash(pC_id_i, HashableString.from(vc_id), HashableString.from(ee),
					HashableString.from(combinedCorrectnessInformation.getCorrectnessIdForSelectionIndex(i)));

			final String key = base64.base64Encode(hash.recursiveHash(HashableByteArray.from(lCC_id_i)));

			final Optional<String> encryptedShortChoiceReturnCode = CMtable.get(key);
			if (encryptedShortChoiceReturnCode.isEmpty()) {
				throw new IllegalStateException(
						String.format(
								"Encrypted short Choice Return Code not found in CMtable. [electionEventId: %s, verificationCardId: %s, index: %s]",
								ee, vc_id, i));

			} else {
				final String ctCC_id_i_encoded = encryptedShortChoiceReturnCode.get();

				final byte[] ctCC_id_i_combined = base64.base64Decode(ctCC_id_i_encoded);

				final int length = ctCC_id_i_combined.length;

				final int split = length - symmetric.getNonceLength();

				final byte[] ctCC_id_i_ciphertext = Arrays.copyOfRange(ctCC_id_i_combined, 0, split);

				final byte[] ctCC_id_i_nonce = Arrays.copyOfRange(ctCC_id_i_combined, split, length);

				final byte[] skcc_id_i = keyDerivation.KDF(lCC_id_i, List.of(), KEY_DERIVATION_BYTES_LENGTH);

				final byte[] CC_id_i_bytes = symmetric.getPlaintextSymmetric(skcc_id_i, ctCC_id_i_ciphertext, ctCC_id_i_nonce, List.of());

				final String CC_id_i = byteArrayToString(CC_id_i_bytes);
				CC_id.add(CC_id_i);
			}
		}

		return new ExtractCRCOutput(CC_id);
	}

	private Ballot getBallot(final ExtractCRCContext context) {
		return ballotDataService.findBallotByElectionEventIdAndBallotId(context.electionEventId(), context.ballotId());
	}

}
