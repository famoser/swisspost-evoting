/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor.voting.sendvote;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.security.SignatureException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.google.common.annotations.VisibleForTesting;

import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.domain.configuration.SetupComponentVoterAuthenticationData;
import ch.post.it.evoting.domain.voting.sendvote.CombinedControlComponentPartialDecryptPayload;
import ch.post.it.evoting.domain.voting.sendvote.ControlComponentLCCSharePayload;
import ch.post.it.evoting.domain.voting.sendvote.ControlComponentPartialDecryptPayload;
import ch.post.it.evoting.domain.voting.sendvote.LongChoiceReturnCodesShare;
import ch.post.it.evoting.domain.voting.sendvote.PartiallyDecryptedEncryptedPCC;
import ch.post.it.evoting.domain.voting.sendvote.VotingServerEncryptedVotePayload;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.evotinglibraries.domain.common.ContextIds;
import ch.post.it.evoting.evotinglibraries.domain.common.EncryptedVerifiableVote;
import ch.post.it.evoting.votingserver.orchestrator.OrchestratorFacade;
import ch.post.it.evoting.votingserver.processor.ElectionEventService;
import ch.post.it.evoting.votingserver.processor.VerificationCardService;
import ch.post.it.evoting.votingserver.processor.VerificationCardState;
import ch.post.it.evoting.votingserver.processor.configuration.setupvoting.VoterAuthenticationDataService;
import ch.post.it.evoting.votingserver.processor.voting.ReturnCodesMappingTable;
import ch.post.it.evoting.votingserver.processor.voting.ReturnCodesMappingTableSupplier;

/**
 * Generate the short Choice Return Codes based on the encrypted partial choice return codes - in interaction with the control components.
 */
@Service
public class ChoiceReturnCodesService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ChoiceReturnCodesService.class);

	private final ExtractCRCAlgorithm extractCRCAlgorithm;
	private final SignatureKeystore<Alias> signatureKeystoreService;
	private final OrchestratorFacade orchestratorFacade;
	private final ElectionEventService electionEventService;
	private final VerificationCardService verificationCardService;
	private final VoterAuthenticationDataService voterAuthenticationDataService;
	private final ReturnCodesMappingTableSupplier returnCodesMappingTableSupplier;

	ChoiceReturnCodesService(
			final ExtractCRCAlgorithm extractCRCAlgorithm,
			final SignatureKeystore<Alias> signatureKeystoreService,
			final OrchestratorFacade orchestratorFacade,
			final ElectionEventService electionEventService,
			final VerificationCardService verificationCardService,
			final VoterAuthenticationDataService voterAuthenticationDataService,
			final ReturnCodesMappingTableSupplier returnCodesMappingTableSupplier) {
		this.extractCRCAlgorithm = extractCRCAlgorithm;
		this.signatureKeystoreService = signatureKeystoreService;
		this.orchestratorFacade = orchestratorFacade;
		this.electionEventService = electionEventService;
		this.verificationCardService = verificationCardService;
		this.voterAuthenticationDataService = voterAuthenticationDataService;
		this.returnCodesMappingTableSupplier = returnCodesMappingTableSupplier;
	}

	/**
	 * Retrieves the short Choice Return Codes from the vote with the help of the control components.
	 *
	 * @param contextIds              the context ids.
	 * @param encryptedVerifiableVote the encrypted verifiable vote from which to get the Short Choice Return Codes.
	 * @param credentialId            the credential id.
	 * @return the list of short Choice Return Codes.
	 * @throws NullPointerException      if any parameter is null.
	 * @throws FailedValidationException if {@code credentialId} is an invalid UUID.
	 */
	@SuppressWarnings("java:S117")
	public List<String> retrieveShortChoiceReturnCodes(final ContextIds contextIds, final String credentialId,
			final EncryptedVerifiableVote encryptedVerifiableVote) {

		checkNotNull(encryptedVerifiableVote);
		validateUUID(credentialId);

		final String electionEventId = contextIds.electionEventId();
		final String verificationCardSetId = contextIds.verificationCardSetId();
		final String verificationCardId = contextIds.verificationCardId();

		final GqGroup encryptionGroup = electionEventService.getEncryptionGroup(electionEventId);
		checkArgument(encryptionGroup.equals(encryptedVerifiableVote.encryptedVote().getGroup()),
				"The encryption group does not match the encrypted verifiable vote's group. [contextIds: %s]", contextIds);

		LOGGER.debug("Requesting Long Choice Return Codes to the control components... [contextIds: {}]", contextIds);

		// Create and sign VotingServerEncryptedVotePayload with secret signing key.
		final GqGroup gqGroup = encryptedVerifiableVote.encryptedVote().getGroup();
		final VotingServerEncryptedVotePayload votingServerEncryptedVotePayload = new VotingServerEncryptedVotePayload(gqGroup,
				encryptedVerifiableVote);

		final CryptoPrimitivesSignature signature = getPayloadSignature(votingServerEncryptedVotePayload);
		votingServerEncryptedVotePayload.setSignature(signature);

		// Ask the control components to partially decrypt the pCC.
		final List<ControlComponentPartialDecryptPayload> controlComponentPartialDecryptPayloads = decryptEncryptedPartialChoiceReturnCodes(
				contextIds, votingServerEncryptedVotePayload);

		verifyPCCPayloads(controlComponentPartialDecryptPayloads);

		// Combine response payloads.
		final CombinedControlComponentPartialDecryptPayload combinedPCCPayloads = new CombinedControlComponentPartialDecryptPayload(
				controlComponentPartialDecryptPayloads);

		// Ask the control components to compute the Long Choice Return Codes shares. The DecryptPCC_j will be done at same time by the CCs.
		final List<ControlComponentLCCSharePayload> controlComponentLCCSharePayloads = retrieveLongChoiceReturnCodesShares(contextIds,
				combinedPCCPayloads);

		// Retrieve corresponding ballot id.
		final SetupComponentVoterAuthenticationData voterAuthenticationData = voterAuthenticationDataService.getVoterAuthenticationData(
				electionEventId, credentialId);
		final String ballotId = voterAuthenticationData.ballotId();

		// Retrieve short Choice Return Codes by combining CCR shares and looking up the Return Codes Mapping Table.
		final ExtractCRCContext extractCRCContext = new ExtractCRCContext.Builder()
				.setEncryptionGroup(gqGroup)
				.setElectionEventId(electionEventId)
				.setBallotId(ballotId)
				.build();

		final List<GroupVector<GqElement, GqGroup>> lCCShares = getLCCShares(controlComponentLCCSharePayloads);
		final ReturnCodesMappingTable returnCodesMappingTable = returnCodesMappingTableSupplier.get(verificationCardSetId);
		final ExtractCRCInput extractCRCInput = new ExtractCRCInput(lCCShares, verificationCardId, returnCodesMappingTable);

		final ExtractCRCOutput shortChoiceReturnCodesOutput = extractCRCAlgorithm.extractCRC(extractCRCContext, extractCRCInput);
		LOGGER.info("Successfully retrieved short Choice Return Codes. [contextIds: {}]", contextIds);

		// Save the short Choice Return Codes for later use in case of a re-login and transition to SENT.
		final List<String> shortChoiceReturnCodes = shortChoiceReturnCodesOutput.getShortChoiceReturnCodes();
		verificationCardService.saveSentState(verificationCardId, shortChoiceReturnCodes);
		LOGGER.info("Successfully saved state. [contextIds: {}, state: {}]", contextIds, VerificationCardState.SENT);

		return shortChoiceReturnCodes;
	}

	private CryptoPrimitivesSignature getPayloadSignature(final VotingServerEncryptedVotePayload payload) {
		final ContextIds contextIds = payload.getEncryptedVerifiableVote().contextIds();

		final Hashable additionalContextData = ChannelSecurityContextData.votingServerEncryptedVote(contextIds.electionEventId(),
				contextIds.verificationCardSetId(), contextIds.verificationCardId());
		try {
			final byte[] signature = signatureKeystoreService.generateSignature(payload, additionalContextData);
			return new CryptoPrimitivesSignature(signature);
		} catch (final SignatureException e) {
			throw new IllegalStateException(
					String.format("Could not generate voting server encrypted vote payload signature. [contextIds: %s]", contextIds));
		}
	}

	private void verifyPCCPayloads(final List<ControlComponentPartialDecryptPayload> controlComponentPartialDecryptPayloads) {
		final boolean isGqGroupEquals = controlComponentPartialDecryptPayloads.stream()
				.map(ControlComponentPartialDecryptPayload::getEncryptionGroup)
				.distinct()
				.count() == 1;

		if (!isGqGroupEquals) {
			throw new IllegalStateException("GqGroup is not identical for all the payloads.");
		}

		final boolean isContextIdsEquals = controlComponentPartialDecryptPayloads.stream()
				.map(ControlComponentPartialDecryptPayload::getPartiallyDecryptedEncryptedPCC)
				.map(PartiallyDecryptedEncryptedPCC::contextIds)
				.distinct()
				.count() == 1;

		if (!isContextIdsEquals) {
			throw new IllegalStateException("ContextIds are not identical for all the payloads.");
		}
	}

	/**
	 * Calls the orchestrator which asks the control components to execute PartialDecryptPCC_j algorithm and collects their contributions.
	 */
	private List<ControlComponentPartialDecryptPayload> decryptEncryptedPartialChoiceReturnCodes(final ContextIds contextIds,
			final VotingServerEncryptedVotePayload votingServerEncryptedVotePayload) {

		final String electionEventId = contextIds.electionEventId();
		final String verificationCardSetId = contextIds.verificationCardSetId();
		final String verificationCardId = contextIds.verificationCardId();

		// Call orchestrator.
		final List<ControlComponentPartialDecryptPayload> controlComponentPartialDecryptPayloads = orchestratorFacade.getChoiceReturnCodesPartialDecryptContributions(
				electionEventId, verificationCardSetId, verificationCardId, votingServerEncryptedVotePayload);
		LOGGER.info("Partial decryptions received from the control-components. [contextIds: {}]", contextIds);

		return controlComponentPartialDecryptPayloads;
	}

	/**
	 * Extracts the lCC shares from the contributions.
	 */
	@VisibleForTesting
	List<GroupVector<GqElement, GqGroup>> getLCCShares(final List<ControlComponentLCCSharePayload> longReturnCodesSharePayloads) {
		return longReturnCodesSharePayloads.stream()
				.map(ControlComponentLCCSharePayload::getLongChoiceReturnCodesShare)
				.map(LongChoiceReturnCodesShare::longChoiceReturnCodeShare)
				.toList();
	}

	/**
	 * Asks the control components to compute the Long Choice Return Codes.
	 */
	private List<ControlComponentLCCSharePayload> retrieveLongChoiceReturnCodesShares(final ContextIds contextIds,
			final CombinedControlComponentPartialDecryptPayload combinedControlComponentPartialDecryptPayload) {

		final String electionEventId = contextIds.electionEventId();
		final String verificationCardSetId = contextIds.verificationCardSetId();
		final String verificationCardId = contextIds.verificationCardId();

		final List<ControlComponentLCCSharePayload> controlComponentLCCSharePayloads = orchestratorFacade.getLongChoiceReturnCodesContributions(
				electionEventId, verificationCardSetId, verificationCardId, combinedControlComponentPartialDecryptPayload);
		LOGGER.info("Retrieved the long Choice Return Code shares payloads. [contextIds: {}]", contextIds);

		verifySharesPayloadSignatures(contextIds, controlComponentLCCSharePayloads);

		return controlComponentLCCSharePayloads;
	}

	/**
	 * Verifies the signatures of the received {@link ControlComponentLCCSharePayload}s.
	 */
	private void verifySharesPayloadSignatures(final ContextIds contextIds,
			final List<ControlComponentLCCSharePayload> controlComponentLCCSharePayloads) {
		for (final ControlComponentLCCSharePayload payload : controlComponentLCCSharePayloads) {
			final int nodeId = payload.getLongChoiceReturnCodesShare().nodeId();
			final Hashable additionalContextData = ChannelSecurityContextData.controlComponentLCCShare(nodeId, contextIds.electionEventId(),
					contextIds.verificationCardSetId(), contextIds.verificationCardId());

			final CryptoPrimitivesSignature signature = payload.getSignature();

			checkState(signature != null, "The signature of the long return codes share payload is null. [nodeId: %s, contextIds: %s]",
					nodeId, contextIds);

			final boolean isSignatureValid;
			try {
				isSignatureValid = signatureKeystoreService.verifySignature(Alias.getControlComponentByNodeId(nodeId), payload, additionalContextData,
						signature.signatureContents());
			} catch (final SignatureException e) {
				throw new IllegalStateException(
						String.format("Could not verify the signature of the long return codes share payload. [nodeId: %s, contextIds: %s]",
								nodeId, contextIds));
			}

			if (!isSignatureValid) {
				throw new InvalidPayloadSignatureException(ControlComponentLCCSharePayload.class,
						String.format("[nodeId: %s, contextIds: %s]", nodeId, contextIds));
			}
		}
	}

}
