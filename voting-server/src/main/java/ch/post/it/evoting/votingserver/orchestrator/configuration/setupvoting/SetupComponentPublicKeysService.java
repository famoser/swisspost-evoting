/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.orchestrator.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.domain.SharedQueue.SETUP_COMPONENT_PUBLIC_KEYS_REQUEST_PATTERN;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.commandmessaging.Context;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.SetupComponentPublicKeysPayload;
import ch.post.it.evoting.domain.configuration.SetupComponentPublicKeysResponsePayload;
import ch.post.it.evoting.votingserver.orchestrator.voting.BroadcastCommand;
import ch.post.it.evoting.votingserver.orchestrator.voting.BroadcastCommandProducer;

@Service
public class SetupComponentPublicKeysService {

	private static final Logger LOGGER = LoggerFactory.getLogger(SetupComponentPublicKeysService.class);

	private final ObjectMapper objectMapper;
	private final BroadcastCommandProducer broadcastCommandProducer;

	public SetupComponentPublicKeysService(
			final ObjectMapper objectMapper,
			final BroadcastCommandProducer broadcastCommandProducer) {
		this.objectMapper = objectMapper;
		this.broadcastCommandProducer = broadcastCommandProducer;
	}

	public void uploadSetupComponentPublicKeys(final String electionEventId, final SetupComponentPublicKeysPayload setupComponentPublicKeysPayload) {
		validateUUID(electionEventId);
		checkNotNull(setupComponentPublicKeysPayload);
		checkArgument(electionEventId.equals(setupComponentPublicKeysPayload.getElectionEventId()), "Election event id mismatch.");

		final String contextId = electionEventId;
		final String correlationId = UUID.randomUUID().toString();

		LOGGER.debug("Uploading setup component public keys... [contextId: {}, correlationId: {}]", contextId, correlationId);

		final BroadcastCommand<SetupComponentPublicKeysResponsePayload> broadcastCommand = new BroadcastCommand.Builder<SetupComponentPublicKeysResponsePayload>()
				.contextId(contextId)
				.context(Context.CONFIGURATION_SETUP_COMPONENT_PUBLIC_KEYS)
				.payload(setupComponentPublicKeysPayload)
				.pattern(SETUP_COMPONENT_PUBLIC_KEYS_REQUEST_PATTERN)
				.deserialization(this::deserializePayload)
				.build();

		broadcastCommandProducer.sendMessagesAwaitingNotification(broadcastCommand);

		LOGGER.info("Successfully uploaded setup component public keys. [contextId: {}, correlationId: {}]", contextId, correlationId);
	}

	private SetupComponentPublicKeysResponsePayload deserializePayload(final byte[] payload) {
		try {
			return objectMapper.readValue(payload, SetupComponentPublicKeysResponsePayload.class);
		} catch (final IOException e) {
			throw new UncheckedIOException(e);
		}
	}

}
