#init schema
echo "Starting server"
database_port=9093
database_url=jdbc:h2:tcp://localhost:$database_port
nohup java -cp /data/appl/libs/h2*.jar org.h2.tools.Server -tcp -tcpPort $database_port -pg -web -ifNotExists -tcpAllowOthers &

sleep 3

echo "Creating users"
java -cp /data/appl/libs/h2*.jar org.h2.tools.RunScript -url "$database_url/~/h2db1;MODE=Oracle"  -user '' -password '' -script create-schemas.sql

echo "Creating schemas"
# Declare an array of string with type
declare -a StringArray=(
                        "VOTING_SERVER"
                        "CONTROL_COMPONENT"
                        )
for schema in ${StringArray[@]}; do
    if [ "$schema" = "CONTROL_COMPONENT" ]; then
      echo "CONTROL_COMPONENT"
        for i in {1..4}
        do
          control_component_schema=$schema"_"$i
          ./flyway-commandline/flyway baseline -url="$database_url/~/h2db1;MODE=Oracle" -user=${control_component_schema,,} -password=${control_component_schema,,} -schemas=$control_component_schema
          ./flyway-commandline/flyway clean -url="$database_url/~/h2db1;MODE=Oracle" -user=${control_component_schema,,} -password=${control_component_schema,,} -schemas=$control_component_schema
          ./flyway-commandline/flyway migrate -url="$database_url/~/h2db1;MODE=Oracle" -user=${control_component_schema,,} -password=${control_component_schema,,} -schemas=$control_component_schema -locations=filesystem:sql/control_components
        done
      continue
    fi

     lowercase_schema=$(echo "$schema" | tr '[:upper:]' '[:lower:]')

    ./flyway-commandline/flyway baseline -url="$database_url/~/h2db1;MODE=Oracle" -user=${schema,,} -password=${schema,,} -schemas=$schema
    ./flyway-commandline/flyway clean -url="$database_url/~/h2db1;MODE=Oracle" -user=${schema,,} -password=${schema,,} -schemas=$schema
    ./flyway-commandline/flyway migrate -url="$database_url/~/h2db1;MODE=Oracle" -user=${schema,,} -password=${schema,,} -schemas=$schema -locations=filesystem:sql/$lowercase_schema
    echo "./flyway-commandline/flyway migrate -url="$database_url/~/h2db1;MODE=Oracle" -user=${schema,,} -password=${schema,,} -schemas=$schema -locations=filesystem:sql/$lowercase_schema"

done
