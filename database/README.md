# Database Module

The database module streamlines end-to-end testing by gathering different SQL files and creating an H2 database. H2 is a lightweight, open-source
database that is commonly used for development and testing purposes due to its simplicity and ease of use. On the other hand, in production
environments, we use Oracle databases, which are known for their high performance, scalability, and reliability. Oracle databases are enterprise-grade
databases that can handle large amounts of data and support complex transactions, making them ideal for mission-critical applications. It's important
to note that the passwords stored in this repository are only meant for testing and development purposes and are never used in a production
environment.

## Usage

Used in the [end-to-end](https://gitlab.com/swisspost-evoting/e-voting/evoting-e2e-dev/-/blob/master/README.md) repository.
