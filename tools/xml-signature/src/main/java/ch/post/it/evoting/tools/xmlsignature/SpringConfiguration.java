/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.tools.xmlsignature;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystoreFactory;
import ch.post.it.evoting.cryptoprimitives.utils.VerificationResult;
import ch.post.it.evoting.evotinglibraries.direct.trust.KeystoreValidator;
import ch.post.it.evoting.tools.xmlsignature.keystore.KeystoreRepository;

@Configuration
@PropertySource("application.properties")
public class SpringConfiguration {

	private static final Logger LOGGER = LoggerFactory.getLogger(SpringConfiguration.class);

	@Value("${direct.trust.signingAlias:#{null}}")
	private Alias signingAlias;

	@Value("${direct.trust.keystore.location}")
	private String keystoreLocation;
	@Value("${direct.trust.keystore.password.location}")
	private String keystorePasswordLocation;

	@Bean
	public KeystoreRepository keystoreRepository() {
		return new KeystoreRepository(keystoreLocation, keystorePasswordLocation);
	}

	@Bean
	SignatureKeystore<Alias> keystoreService(
			@Qualifier("keystoreRepository")
			final KeystoreRepository repository) throws IOException {
		return SignatureKeystoreFactory.createSignatureKeystore(repository.getKeyStore(), "PKCS12", repository.getKeystorePassword(),
				keystore -> {
					final VerificationResult result = KeystoreValidator.validateKeystore(keystore, signingAlias);
					if (!result.isVerified()) {
						LOGGER.error("Keystore validation failed. [alias: {}]", signingAlias.get());
						result.getErrorMessages().descendingIterator().forEachRemaining(LOGGER::error);
					}
					return result.isVerified();
				}, signingAlias != null ? signingAlias : Alias.CANTON);
	}
}
