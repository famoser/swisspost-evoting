/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.tools.xmlsignature;

import java.io.UncheckedIOException;
import java.nio.file.Path;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.signers.CantonConfigSigner;
import ch.post.it.evoting.domain.signers.SetupComponentEvotingPrintSigner;
import ch.post.it.evoting.domain.signers.XmlSigner;

public class Main {

	private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);
	private static final String USAGE_TEXT = "Usage : java -Ddirect.trust.keystore.location=<direct-trust-keystoreFile> "
			+ "-Ddirect.trust.keystore.password.location=<direct-trust-passFile> -jar xml-signature.jar <"
			+ String.join("|", Arrays.stream(Signers.values()).map(Signers::name).toList())
			+ "> <sign|verify> <filePath>";
	private static final String ACTION_SIGN = "sign";
	private static final String ACTION_VERIFY = "verify";

	public static void main(final String[] args) {
		try {
			final boolean isExistingSigner = args.length > 0 &&
					Arrays.stream(Signers.values()).anyMatch(s -> s.name().equals(args[0]));

			if (isExistingSigner && args.length > 2) {
				final Signers selectedSigner = Signers.valueOf(args[0]);

				// prepare spring configuration accordingly to what action will be executed
				if (args[1].equals(ACTION_SIGN)) {
					System.setProperty("direct.trust.signingAlias", selectedSigner.signingAlias.name());
				}

				final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Main.class.getPackageName());
				final var signatureKeystore = context.getBean(SignatureKeystore.class);

				final XmlSigner<?> xmlSigner = selectedSigner.signerClass.getDeclaredConstructor(SignatureKeystore.class)
						.newInstance(signatureKeystore);

				switch (args[1]) {
				case ACTION_SIGN -> xmlSigner.sign(Path.of(args[2]));
				case ACTION_VERIFY -> verify(xmlSigner, Path.of(args[2]));
				default -> LOGGER.error("Unknown action. Use sign or verify");
				}
			} else {
				LOGGER.error(USAGE_TEXT);
			}
		} catch (final Exception e) {
			LOGGER.error("Unable to process the requested action", e);
		}
	}

	private static void verify(final XmlSigner<?> xmlSigner, final Path filePath) {
		try {
			final boolean verify = xmlSigner.verify(filePath);
			logResultAsJson(verify, null);
		} catch (final Exception e) {
			logResultAsJson(false, e.getMessage());
		}
	}

	private static void logResultAsJson(final boolean signatureVerified, final String exceptionMessage) {
		try {
			final String s = new ObjectMapper().writeValueAsString(new Result(signatureVerified, exceptionMessage));
			if (exceptionMessage != null) {
				LOGGER.error(s);
			} else {
				LOGGER.info(s);
			}
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException(String.format("Could not log the final result. [signatureVerified: %s]", signatureVerified), e);
		}
	}

	private enum Signers {
		CONFIG(CantonConfigSigner.class, Alias.CANTON),
		PRINT(SetupComponentEvotingPrintSigner.class, Alias.SDM_CONFIG);

		private final Class<? extends XmlSigner<?>> signerClass;
		private final Alias signingAlias;

		Signers(final Class<? extends XmlSigner<?>> signerClass, final Alias signingAlias) {
			this.signerClass = signerClass;
			this.signingAlias = signingAlias;
		}
	}

	@JsonPropertyOrder({ "signatureVerified", "exceptionMessage" })
	private record Result(boolean signatureVerified, String exceptionMessage) {
	}
}
