/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.config.integrationtests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import ch.post.it.evoting.config.Application;
import ch.post.it.evoting.config.Config;
import ch.post.it.evoting.config.ConfigurationCommandLine;
import ch.post.it.evoting.config.integrationtests.keystore.TestKeyStoreInitializer;

@SpringJUnitConfig(classes = Application.class)
@ContextConfiguration(classes = Config.class, loader = AnnotationConfigContextLoader.class, initializers = TestKeyStoreInitializer.class)
class ConfigurationCommandLineTest {

	@TempDir
	Path tempDir;

	@Autowired
	private ConfigurationCommandLine commandLine;

	@Test
	void genEncryptionParametersAndPrimes() throws Exception {
		final Path seedPath = Paths.get(Objects.requireNonNull(this.getClass().getResource("/seed.txt")).toURI());

		commandLine
				.run("-encryptionParameters",
						"-seed_path", seedPath.toString(),
						"-out", tempDir.toString());

		final List<Path> files = Files.list(tempDir).toList();

		assertEquals(1, files.size());

		final String expectedFilename = Path.of(tempDir.toString(), "/encryptionParametersPayload.json").toString();

		assertEquals(expectedFilename, files.get(0).toString());
	}

	@Test
	void genEncryptionParametersAndPrimes_invalidSeedPath_throwsIllegalArgumentException() throws URISyntaxException {
		final Path basePath = Paths.get(Objects.requireNonNull(this.getClass().getResource(".")).toURI());
		final String seedPath = Paths.get(basePath + "/inexistent_seed_file.txt").toString();
		final String outPath = tempDir.toString();

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> commandLine.run("-genEncryptionParametersAndPrimes", "-seed_path", seedPath, "-out", outPath));

		final String expectedMessage = String.format("Seed path is invalid. [path: %s]", seedPath);

		assertEquals(expectedMessage, exception.getMessage());
	}

}
