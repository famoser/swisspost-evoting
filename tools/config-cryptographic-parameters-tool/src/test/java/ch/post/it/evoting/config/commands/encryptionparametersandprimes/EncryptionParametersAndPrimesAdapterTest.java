/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.config.commands.encryptionparametersandprimes;

import static ch.post.it.evoting.config.commands.encryptionparametersandprimes.ParametersMapGenerator.getMapWithAllParameters;
import static ch.post.it.evoting.config.commands.encryptionparametersandprimes.ParametersMapGenerator.getMapWithMandatoryParameters;
import static ch.post.it.evoting.config.commands.encryptionparametersandprimes.ParametersMapGenerator.mapToParameters;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.nio.file.Paths;
import java.util.Map;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import ch.post.it.evoting.config.Parameters;

class EncryptionParametersAndPrimesAdapterTest {

	final EncryptionParametersAndPrimesAdapter adapter = new EncryptionParametersAndPrimesAdapter();

	@Test
	void provideAllValidParameters_holderCreatedWithoutIssue() {
		// given
		final Parameters currentParameter = mapToParameters(getMapWithAllParameters());

		// when
		final EncryptionParametersAndPrimesParameters holder = adapter.adapt(currentParameter);

		// then
		assertThat(holder.seedPath()).isEqualTo(Paths.get(ParametersMapGenerator.SEED_PATH_VALUE));
		assertThat(holder.outputPath()).isEqualTo(Paths.get(ParametersMapGenerator.OUT_VALUE));
	}

	@ParameterizedTest(name = "{0} is expected to be invalid.")
	@MethodSource("invalidParametersProvider")
	void provideOneMissingParameter_holderThrowAnException(final String missingParameterName, Map<String, String> parameters) {
		// given
		final Parameters currentParameter = mapToParameters(parameters);

		// when / then
		assertThatThrownBy(() -> adapter.adapt(currentParameter))
				.isInstanceOf(IllegalArgumentException.class)
				.hasMessageContaining(missingParameterName);
	}

	static Stream<Arguments> invalidParametersProvider() {
		return getMapWithMandatoryParameters().keySet().stream()
				.map(s -> {
					final Map<String, String> map = getMapWithAllParameters();
					map.remove(s);
					return Arguments.of(s, map);
				});
	}
}