/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.config;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;

import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;

class MainParametersProcessorTest {

	@Test
	void processEmptyCommand() throws IOException {
		final Command process;
		try (final ByteArrayOutputStream output = new ByteArrayOutputStream()) {

			System.setOut(new PrintStream(output, true, StandardCharsets.UTF_8));
			process = MainParametersProcessor.process(new String[0]);
		}
		assertThat(process == null, CoreMatchers.is(true));
	}

	@Test
	void parseCommandWithParameterWithoutRequiredValue() throws IOException {
		final Command process;
		try (final ByteArrayOutputStream output = new ByteArrayOutputStream()) {

			System.setOut(new PrintStream(output, true, StandardCharsets.UTF_8));
			final String[] params = new String[] { "-genUUID", "-num", };
			process = MainParametersProcessor.process(params);
		}
		assertThat(process == null, CoreMatchers.is(true));
	}

	@Test
	void parseHelpCommand() throws IOException {
		final Command process;
		try (final ByteArrayOutputStream output = new ByteArrayOutputStream()) {

			System.setOut(new PrintStream(output, true, StandardCharsets.UTF_8));
			final String[] params = new String[] { "-h", };
			process = MainParametersProcessor.process(params);
		}

		assertNotNull(process);
		assertEquals(MutuallyExclusiveCommand.HELP, process.getIdentifier());
	}

	@Test
	void parseGenEncryptionParametersAndPrimesCommand() {
		final String seedPath = "some/path/seed.txt";
		final String outPath = "out";
		final String[] params = new String[] { "-encryptionParameters", "-seed_path", seedPath, "-out", outPath };

		final Command result = MainParametersProcessor.process(params);

		assertNotNull(result);
		assertEquals(MutuallyExclusiveCommand.GEN_ENCRYPTION_AND_PRIMES_PARAM, result.getIdentifier());
		assertEquals(seedPath, result.getParameters().getParam(CommandParameter.SEED_PATH.getParameterName()));
		assertEquals(outPath, result.getParameters().getParam(CommandParameter.OUT.getParameterName()));
	}

	@Test
	void parseGenEncryptionParametersAndPrimesCommandWithoutRequiredParameter() {
		final String[] params = new String[] { "-encryptionParameters", "-seed_path", "" };

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> MainParametersProcessor.process(params));
		assertEquals("encryptionParameters command requires seed_path parameter.", exception.getMessage());
	}

	@Test
	void parseWrongCommand() throws IOException {
		final Command process;
		try (final ByteArrayOutputStream output = new ByteArrayOutputStream()) {

			System.setOut(new PrintStream(output, true, StandardCharsets.UTF_8));
			final String[] params = new String[] { "-genUUID", "-genEncryptionParametersAndPrimes", "-p", };
			process = MainParametersProcessor.process(params);
		}
		assertThat(process == null, CoreMatchers.is(true));
	}
}
