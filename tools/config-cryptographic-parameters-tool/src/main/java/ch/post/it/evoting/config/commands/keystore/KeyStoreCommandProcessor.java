/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.config.commands.keystore;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.function.Consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.config.Parameters;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.signing.GenKeysAndCert;
import ch.post.it.evoting.cryptoprimitives.signing.KeysAndCert;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureFactory;

@Service
public class KeyStoreCommandProcessor implements Consumer<Parameters> {

	private static final Logger LOGGER = LoggerFactory.getLogger(KeyStoreCommandProcessor.class);

	private final Random random;
	private final KeyStoreParametersAdapter keyStoreParametersAdapter;

	public KeyStoreCommandProcessor(final Random random, final KeyStoreParametersAdapter keyStoreParametersAdapter) {
		this.random = random;
		this.keyStoreParametersAdapter = keyStoreParametersAdapter;
	}

	@Override
	public void accept(final Parameters parameters) {

		LOGGER.info("Starting the generation of key store");

		final KeyStoreParametersContainer container = keyStoreParametersAdapter.adapt(parameters);
		final SignatureFactory signatureFactory = SignatureFactory.getInstance();
		final GenKeysAndCert genKeysAndCertService = signatureFactory.createGenKeysAndCert(container.getAuthorityInformation());

		final Path keystorePath = container.getOutputPath().resolve("signing_keystore_" + container.getAlias() + ".p12");
		final Path passwordFilePath = container.getOutputPath().resolve("signing_pw_" + container.getAlias() + ".txt");
		final Path certificatePath = container.getOutputPath().resolve("signing_certificate_" + container.getAlias() + ".crt");

		LOGGER.info("Parameters collected");

		createFileStructure(container.getOutputPath(), keystorePath, passwordFilePath, certificatePath);

		LOGGER.info("File structure created");

		final KeysAndCert output = genKeysAndCertService.genKeysAndCert(container.getValidFrom(), container.getValidUntil());

		LOGGER.info("Key and cert generated");

		final String password = random.genRandomBase64String(container.getPasswordLength());

		LOGGER.info("Password generated");

		final KeyStore keyStore = createKeyStore(container, output, password);

		LOGGER.info("KeyStore created");

		writeOutputsToFile(keystorePath, passwordFilePath, certificatePath, output, password, keyStore);

		LOGGER.info("Data written to output dir '{}'...", container.getOutputPath());
	}

	private void writeOutputsToFile(final Path keystorePath, final Path passwordFilePath, final Path certificatePath,
			final KeysAndCert output, final String password, final KeyStore keyStore) {
		try (final FileOutputStream keyStoreFos = new FileOutputStream(keystorePath.toFile());
				final FileOutputStream passwordFileFos = new FileOutputStream(passwordFilePath.toFile());
				final FileOutputStream certificateFos = new FileOutputStream(certificatePath.toFile())) {
			keyStore.store(keyStoreFos, password.toCharArray());
			passwordFileFos.write(password.getBytes(StandardCharsets.UTF_8));
			certificateFos.write(output.certificate().getEncoded());
		} catch (final IOException | CertificateException | KeyStoreException | NoSuchAlgorithmException e) {
			throw new KeyStoreGeneratorException("Error while writing outputs to files.", e);
		}
	}

	private void createFileStructure(final Path containingDirectory, final Path keystorePath, final Path passwordFilePath,
			final Path certificatePath) {
		try {
			Files.createDirectories(containingDirectory);
		} catch (final FileAlreadyExistsException e) {
			LOGGER.info("Directory already exist.");
		} catch (final IOException e) {
			throw new UncheckedIOException(e);
		}
		Arrays.asList(keystorePath, passwordFilePath, certificatePath).forEach(file -> {
			try {
				Files.createFile(file);
			} catch (final IOException e) {
				throw new UncheckedIOException(e);
			}
		});
	}

	private KeyStore createKeyStore(final KeyStoreParametersContainer parameters, final KeysAndCert output, final String password) {
		try {
			final KeyStore keyStore = KeyStore.getInstance("PKCS12");
			keyStore.load(null, password.toCharArray());
			keyStore.setKeyEntry(parameters.getAlias(), output.privateKey(), "".toCharArray(),
					new Certificate[] { output.certificate() });
			return keyStore;
		} catch (final KeyStoreException | IOException | NoSuchAlgorithmException | CertificateException e) {
			throw new KeyStoreGeneratorException("Error while creating the key store.", e);
		}
	}
}
