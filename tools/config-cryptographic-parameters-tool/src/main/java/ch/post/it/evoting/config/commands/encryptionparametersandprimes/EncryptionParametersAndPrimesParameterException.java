/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.config.commands.encryptionparametersandprimes;

public class EncryptionParametersAndPrimesParameterException extends RuntimeException {

	public EncryptionParametersAndPrimesParameterException(final String message) {
		super(message);
	}

	public EncryptionParametersAndPrimesParameterException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public EncryptionParametersAndPrimesParameterException(final Throwable cause) {
		super(cause);
	}
}
