/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.config.commands.encryptionparametersandprimes;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamal;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;

/**
 * Implements the GetElectionEventEncryptionParameters algorithm
 */
@Service
public class GetElectionEventEncryptionParametersAlgorithm {

	private final ElGamal elGamal;

	public GetElectionEventEncryptionParametersAlgorithm(final ElGamal elGamal) {
		this.elGamal = elGamal;
	}

	/**
	 * Gets the election event's encryption parameters consisting of the encryption group and the small primes.
	 *
	 * @param maximumNumberOfPossibleVotingOptions &omega;, the number of small primes to be generated. Must be strictly positive.
	 * @param seed                                 the seed to be used for generating the encryption group. Must be non-null.
	 * @return the {@link GetElectionEventEncryptionParametersOutput} containing the encryption group and the small primes.
	 */
	@SuppressWarnings("java:S117")
	public GetElectionEventEncryptionParametersOutput getElectionEventEncryptionParameters(final int maximumNumberOfPossibleVotingOptions,
			final String seed) {
		checkNotNull(seed);
		checkArgument(maximumNumberOfPossibleVotingOptions > 0);
		final int omega = maximumNumberOfPossibleVotingOptions;

		final GqGroup p_q_g = elGamal.getEncryptionParameters(seed);
		final GroupVector<PrimeGqElement, GqGroup> p_vector = PrimeGqElement.PrimeGqElementFactory.getSmallPrimeGroupMembers(p_q_g, omega);

		return new GetElectionEventEncryptionParametersOutput(p_q_g, p_vector);
	}
}
