/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.config.commands.encryptionparametersandprimes;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.function.Consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.common.annotations.VisibleForTesting;

import ch.post.it.evoting.config.Parameters;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.EncryptionParametersPayload;

@Service
public class EncryptionParametersAndPrimesCommandProcessor implements Consumer<Parameters> {

	private static final Logger LOGGER = LoggerFactory.getLogger(EncryptionParametersAndPrimesCommandProcessor.class);

	private final EncryptionParametersAndPrimesOutputSerializer configOutputSerializer;
	final EncryptionParametersAndPrimesGenerator configEncryptionParametersGenerator;
	private final EncryptionParametersAndPrimesAdapter configEncryptionParametersAdapter;

	@Value("${direct.trust.keystore.location.config}")
	private String keystoreLocationConfig;
	@Value("${direct.trust.keystore.password.location.config}")
	private String keystorePasswordLocationConfig;

	public EncryptionParametersAndPrimesCommandProcessor(final EncryptionParametersAndPrimesOutputSerializer configOutputSerializer,
			final EncryptionParametersAndPrimesGenerator configEncryptionParametersGenerator,
			final EncryptionParametersAndPrimesAdapter configEncryptionParametersAdapter) {
		this.configOutputSerializer = configOutputSerializer;
		this.configEncryptionParametersGenerator = configEncryptionParametersGenerator;
		this.configEncryptionParametersAdapter = configEncryptionParametersAdapter;
	}

	@VisibleForTesting
	EncryptionParametersAndPrimesGenerator getConfigEncryptionParametersGenerator() {
		return configEncryptionParametersGenerator;
	}

	@Override
	public void accept(final Parameters parameters) {

		try {
			LOGGER.info("Starting to generate the encryption parameters...");

			final EncryptionParametersAndPrimesParameters holder = configEncryptionParametersAdapter.adapt(parameters);
			final Path keystoreLocationConfigPath = Paths.get(keystoreLocationConfig);
			final Path keystorePasswordLocationConfigPath = Paths.get(keystorePasswordLocationConfig);

			LOGGER.info("EncryptionParameters and primes parameters validated");

			final EncryptionParametersPayload encryptionParametersPayload =
					configEncryptionParametersGenerator.generate(holder.seedPath(), keystoreLocationConfigPath, keystorePasswordLocationConfigPath);
			LOGGER.info("Encryption parameters generated");

			final Path writtenFilePath = configOutputSerializer.serialize(encryptionParametersPayload, holder.outputPath());
			LOGGER.info("Output processed");

			LOGGER.info("The encryption Parameters payload was generated correctly. It can be found in: {}", writtenFilePath);
		} catch (IOException e) {
			throw new EncryptionParametersAndPrimesParameterException(e);
		}
	}
}
