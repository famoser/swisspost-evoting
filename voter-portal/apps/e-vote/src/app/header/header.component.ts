/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */

import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '@swiss-post/backend';
import { ConfigurationService } from '@swiss-post/shared/configuration';
import { getConfig, LanguageSelectorActions } from '@swiss-post/shared/state';
import { FAQService } from '@swiss-post/shared/ui';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

@Component({
  selector: 'swp-header',
  templateUrl: './header.component.html',
  styleUrls: [ './header.component.scss' ],
})
export class HeaderComponent {
  availableLanguages = environment.availableLanguages;
  currentLang$: Observable<string>;
  skipLink$: Observable<string>;
  isCollapsed = true;

  constructor(
    public readonly configuration: ConfigurationService,
    private readonly store: Store,
    private readonly translate: TranslateService,
    private readonly faqService: FAQService,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
  ) {
    this.currentLang$ = this.store
      .select(getConfig)
      .pipe(
        map(config => config?.lang || translate.defaultLang),
        tap(lang => translate.use(lang)),
        tap(lang => document.documentElement.lang = lang)
      );

    this.skipLink$ = this.route.fragment.pipe(
      map(fragment => this.router.url.replace(new RegExp(`#${fragment}$`), ''))
    );
  }

  translateApp(lang: string) {
    this.store.dispatch(LanguageSelectorActions.languageClicked({ lang }));
  }

  showFAQ(): void {
    this.faqService.showFAQ();
  }
}
