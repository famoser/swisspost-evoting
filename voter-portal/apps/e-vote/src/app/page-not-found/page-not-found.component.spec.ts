/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReplacePlaceholdersPipe } from '@swiss-post/shared/ui';
import { MockPipe } from 'ng-mocks';
import { TranslateTestingModule } from 'ngx-translate-testing';

import { PageNotFoundComponent } from './page-not-found.component';

describe('PageNotFoundComponent', () => {
  let fixture: ComponentFixture<PageNotFoundComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        TranslateTestingModule.withTranslations({}),
      ],
      declarations: [
        PageNotFoundComponent,
        MockPipe(ReplacePlaceholdersPipe, value => value),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageNotFoundComponent);
    fixture.detectChanges();
  });

  it('should show message incorrecturl', () => {
    expect(fixture.nativeElement.textContent).toContain(
      'pagenotfound.incorrecturl'
    );
  });
});
