/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { LocationStrategy } from '@angular/common';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { SHARED_FEATURE_KEY, SharedActions } from '@swiss-post/shared/state';
import { ProcessCancellationService } from '@swiss-post/shared/ui';
import { MockComponent, MockProvider } from 'ng-mocks';
import { TranslateTestingModule } from 'ngx-translate-testing';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { StepperComponent } from './stepper/stepper.component';

describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>;
  let app: AppComponent;
  let store: MockStore;
  let popStateCallback: () => void;

  beforeEach(async () => {
    const onPopState = jest.fn().mockImplementation((callback) => popStateCallback = callback);

    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        TranslateTestingModule.withTranslations({}),
      ],
      declarations: [
        AppComponent,
        MockComponent(HeaderComponent),
        MockComponent(StepperComponent),
      ],
      providers: [
        provideMockStore({ initialState: { [SHARED_FEATURE_KEY]: {} } }),
        MockProvider(ProcessCancellationService),
        MockProvider(LocationStrategy, { onPopState }),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    app = fixture.componentInstance;
    store = TestBed.inject(MockStore);
  });

  function setState(newState: object) {
    store.setState({ [SHARED_FEATURE_KEY]: newState });
  }

  describe('store registration', () => {
    it('should not register a store in the window object if "Cypress" is undefined', () => {
      window.Cypress = undefined;

      app.ngOnInit();

      expect(window.store).toBeFalsy();
    });

    it('should register the store in the window object if "Cypress" is defined', () => {
      window.Cypress = {};

      app.ngOnInit();

      expect(window.store).toEqual(store);
    });
  });

  describe('route exit handling', () => {
    let cancellationService: ProcessCancellationService;

    beforeEach(() => {
      cancellationService = TestBed.inject(ProcessCancellationService);
      app.ngOnInit();
    });

    it('should mark the back button as pressed with every new "popstate" event', () => {
      expect(cancellationService.backButtonPressed).toBeFalsy();

      popStateCallback();

      expect(cancellationService.backButtonPressed).toBeTruthy();
    });

    it('should dispatch a logout action on init when the user is authenticated', () => {
      const dispatchSpy = jest.spyOn(store, 'dispatch');

      setState({ isAuthenticated: true });
      app.ngOnInit();

      expect(dispatchSpy).toHaveBeenCalledWith(SharedActions.loggedOut());
    });
  });

  it('should not dispatch a logout action on init when the user is not authenticated ', () => {
    const dispatchSpy = jest.spyOn(store, 'dispatch');

    setState({ isAuthenticated: false });
    app.ngOnInit();

    expect(dispatchSpy).not.toHaveBeenCalledWith(SharedActions.loggedOut());
  });
});
