/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { SharedIconsModule } from '@swiss-post/shared/icons';
import { SharedStateModule } from '@swiss-post/shared/state';
import { SharedUiModule } from '@swiss-post/shared/ui';
import { NgxMaskModule } from 'ngx-mask';
import { ExtendedFactorComponent } from './extended-factor/extended-factor.component';
import { StartVotingRoutingModule } from './start-voting-routing.module';
import { StartVotingComponent } from './start-voting/start-voting.component';

@NgModule({
  imports: [
    CommonModule,
    StartVotingRoutingModule,
    SharedStateModule,
    ReactiveFormsModule,
    NgbDatepickerModule,
    SharedIconsModule,
    SharedUiModule,
    TranslateModule,
    FormsModule,
    NgxMaskModule.forRoot({ validation: false }),
  ],
  declarations: [ StartVotingComponent, ExtendedFactorComponent ],
})
export class StartVotingModule {
}
