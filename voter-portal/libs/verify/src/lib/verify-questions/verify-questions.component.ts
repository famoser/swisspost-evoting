/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import {Component, Input} from '@angular/core';
import {ContestVerifyData} from '@swiss-post/types';

@Component({
  selector: 'swp-verify-questions',
  templateUrl: './verify-questions.component.html',
  styleUrls: ['./verify-questions.component.scss'],
})
export class VerifyQuestionsComponent {
  @Input() contestVerifyData: ContestVerifyData | undefined;
}
