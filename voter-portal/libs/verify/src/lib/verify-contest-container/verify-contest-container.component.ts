/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import {Component, Input} from '@angular/core';
import {ContestVerifyData, TemplateType} from '@swiss-post/types';

@Component({
  selector: 'swp-verify-contest-container',
  templateUrl: './verify-contest-container.component.html',
  styleUrls: ['./verify-contest-container.component.scss'],
})
export class VerifyContestContainerComponent {
  readonly TemplateType = TemplateType;

  @Input() contestVerifyData: ContestVerifyData | undefined;
}
