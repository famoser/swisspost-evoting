# backend

This library was generated with [Nx](https://nx.dev).

It provides a connection to the backend through the Online Voting API.

`ovApi` requires voter's `startVotingKey`, `dateOfBirth` & `config` object to initiate;

```typescript
    // working voter
    const voter: IVoter = {
        intitialCode: '9et8hsech5jbb47yhdhs',
        dob: '01061944'
    }

    // working config
    const config = {
      lang: 'FR',
      electionEventId: '1806033169574409bf7cc5753246dd8a',
    };
```

## Running unit tests

Run `nx test backend` to execute the unit tests.
