/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
export const votingCardPentagon = 'M5.22,22.45L1,9.52l11-8,11,8L18.78,22.45H5.22Z';
