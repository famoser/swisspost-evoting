/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { votingCardTriangle } from './voting-card-triangle';
import { votingCardDiamond } from './voting-card-diamond';
import { votingCardPentagon } from './voting-card-pentagon';
import { votingCardStar } from './voting-card-star';

export default {
  'voting-card-triangle': votingCardTriangle,
  'voting-card-diamond': votingCardDiamond,
  'voting-card-pentagon': votingCardPentagon,
  'voting-card-star': votingCardStar,
};
