# shared-state

This library was generated with [Nx](https://nx.dev).

It manages `SharedState` for **FeatureModules**: `StartVoting`, `Choose`;

It imports `AuthModule` that provides `authService`;

`authService` exposes `requestBallot(startVotingKey: string)` that promises `Ballot`;

`Ballot` is placed in `ngrx` `store` while still on `StartVotingComponent` before navigating to `choose` `route`;

`ChooseComponent` selects `Contests` from `Ballot` in `ngrx` `store` and displays the list

## Running unit tests

Run `nx test shared-state` to execute the unit tests.
