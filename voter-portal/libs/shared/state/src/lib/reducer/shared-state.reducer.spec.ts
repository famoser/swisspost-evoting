/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { Action } from '@ngrx/store';
import { StartVotingActions } from '../actions/shared-state.actions';

import { initialState, reducer, SharedState } from './shared-state.reducer';

describe('SharedState Reducer', () => {

  describe('valid SharedState actions', () => {
    it('loadSvkSuccess should return authentication token', () => {

      const mockVoteCastCode = 'mockVoteCastCode';
      const action = StartVotingActions.voteCastReturnCodeLoaded({ voteCastReturnCode: mockVoteCastCode });

      const result: SharedState = reducer(initialState, action);

      expect(result.voteCastReturnCode?.length).toBeGreaterThan(0);
    });
  });

  describe('unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as Action;

      const result = reducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });
});
