/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { BackendConfig, BackendError, Ballot, BallotUserData, Voter } from '@swiss-post/types';

export const LanguageSelectorActions = createActionGroup({
  source: 'Language Selector',
  events: {
    'Language Clicked': props<{ lang: string }>(),
    'Ballot Translated': props<{ ballot: Ballot }>(),
    'Ballot Translation Failed': props<{ error: BackendError }>(),
  },
});

export const LegalTermsActions = createActionGroup({
  source: 'Legal Terms Page',
  events: {
    'Agree Clicked': props<{ config: BackendConfig }>(),
  },
});

export const StartVotingActions = createActionGroup({
  source: 'Start Voting Page',
  events: {
    'Start Clicked': props<{ voter: Voter }>(),
    'Ballot Loaded': props<{ ballot: Ballot }>(),
    'Choice Return Codes Loaded': props<{ choiceReturnCodes: string[], ballot: Ballot }>(),
    'Vote Cast Return Code Loaded': props<{ voteCastReturnCode: string }>(),
    'Authentication Failed': props<{ error: BackendError }>(),
  },
});

export const ChooseActions = createActionGroup({
  source: 'Choose Page',
  events: {
    'Review Clicked': props<{ ballotUserData: BallotUserData }>(),
  },
});

export const ReviewActions = createActionGroup({
  source: 'Review Page',
  events: {
    'Seal Vote Clicked': emptyProps(),
    'Seal Vote Canceled': emptyProps(),
    'Sealed Vote Loaded': props<{ choiceReturnCodes: string[] }>(),
    'Sealed Vote Load Failed': props<{ error: BackendError }>(),
  },
});

export const VerifyActions = createActionGroup({
  source: 'Review Page',
  events: {
    'Cast Vote Clicked': props<{ confirmationKey: string }>(),
    'Cast Vote Loaded': props<{ voteCastReturnCode: string }>(),
    'Cast Vote Load Failed': props<{ error: BackendError }>(),
  },
});

export const SharedActions = createActionGroup({
  source: 'Generic',
  events: {
    'Logged out': emptyProps(),
    'Server Error Cleared': emptyProps(),
  },
});
