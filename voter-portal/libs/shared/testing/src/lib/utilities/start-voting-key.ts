/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { RandomString } from './random';

export const RandomStartVotingKey = () => {
  return RandomString(24);
};
