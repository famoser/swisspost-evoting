/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
export class MockResizeObserver {
  observe() { /* do nothing */
  }

  unobserve() { /* do nothing */
  }

  disconnect() { /* do nothing */
  }
}
