/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { Ballot } from '@swiss-post/types';

let choiceCodeIndex = 0;

const MockChoiceCode = (): string => {
  choiceCodeIndex++;
  return `choicecode-${choiceCodeIndex}`;
};

export const MockChoiceReturnCodes = (ballot: Ballot): string[] => {
  return ballot.contests.reduce((allChoiceReturnCodes, contest) => {
    let choiceReturnCodes: string[] = [];

    if (contest.questions) {
      const questionChoiceReturnCodes = contest.questions.map(MockChoiceCode);
      choiceReturnCodes = [ ...choiceReturnCodes, ...questionChoiceReturnCodes ];
    }

    if (contest.listQuestion?.maxChoices) {
      const listChoiceReturnCodes = Array.from({ length: contest.listQuestion?.maxChoices }, MockChoiceCode);
      choiceReturnCodes = [ ...choiceReturnCodes, ...listChoiceReturnCodes ];
    }

    if (contest.candidatesQuestion?.maxChoices) {
      const candidateChoiceReturnCodes = Array.from({ length: contest.candidatesQuestion?.maxChoices }, MockChoiceCode);
      choiceReturnCodes = [ ...choiceReturnCodes, ...candidateChoiceReturnCodes ];
    }

    return [ ...allChoiceReturnCodes, ...choiceReturnCodes ];
  }, [] as string[]);
};
