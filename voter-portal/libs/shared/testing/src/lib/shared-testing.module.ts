/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {QuestionsTestingComponent} from './components/questions-testing.component';

@NgModule({
  declarations: [
    QuestionsTestingComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    QuestionsTestingComponent
  ]
})
export class SharedTestingModule {
}
