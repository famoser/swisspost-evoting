/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
export * from './lib/shared-configuration.module';
export * from './lib/configuration.service';
