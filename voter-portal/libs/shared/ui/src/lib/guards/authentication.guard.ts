/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { Injectable } from '@angular/core';
import { CanLoad, Router, UrlTree } from '@angular/router';
import { Store } from '@ngrx/store';
import { getConfig, getIsAuthenticated } from '@swiss-post/shared/state';
import { merge, Observable, partition } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationGuard implements CanLoad {
  constructor(private readonly store: Store, private readonly router: Router) {
  }

  canLoad(): Observable<boolean | UrlTree> {
    const [ canLoad$, cannotLoad$ ] = partition(
      this.store.select(getIsAuthenticated),
      Boolean,
    );

    const redirectToLandingPage$ = cannotLoad$.pipe(
      mergeMap(() => this.store.select(getConfig)),
      map((config) => {
        if (config) {
          return this.router.createUrlTree([
            'legal-terms',
            config.electionEventId,
          ]);
        }
        return this.router.createUrlTree(['']);
      })
    );

    return merge(canLoad$, redirectToLandingPage$);
  }
}
