/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { Component } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { getElectionEventId } from '@swiss-post/shared/state';
import { ProcessCancellationService } from '@swiss-post/shared/ui';
import { BackAction } from '@swiss-post/types';
import { MockProvider } from 'ng-mocks';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { DeactivationGuard } from './deactivation.guard';

describe('DeactivationGuard', () => {
  let guard: DeactivationGuard;
  let store: MockStore;
  let cancellationService: ProcessCancellationService;
  let hasOpenModals = false;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ RouterTestingModule ],
      providers: [
        provideMockStore({}),
        MockProvider(ProcessCancellationService),
        MockProvider(NgbModal, { hasOpenModals: () => hasOpenModals }),
      ],
    });
    guard = TestBed.inject(DeactivationGuard);
    store = TestBed.inject(MockStore);
    cancellationService = TestBed.inject(ProcessCancellationService);
    cancellationService.backButtonPressed = true;
  });

  describe('allowed back urls', () => {
    const setupAndCallCanDeactivate = (urlAllowed: string[], url: string) => {
      const routeData = {
        data: {allowedBackPaths: urlAllowed},
      } as unknown;
      const backTo = {url} as RouterStateSnapshot;
      return guard.canDeactivate(
        {} as Component,
        routeData as ActivatedRouteSnapshot,
        {} as RouterStateSnapshot,
        backTo
      );
    };

    it('should allow backbutton in case url is allowed', () => {
      const backButtonAllowed = setupAndCallCanDeactivate(
        ['allowed-url'],
        'allowed-url'
      );
      expect(backButtonAllowed).toBeTruthy();
    });

    it('should not allow backbutton if url is not whitelisted', () => {
      const backButtonAllowed = setupAndCallCanDeactivate(
        ['allowed-url'],
        'some-not-whitelisted-url'
      );
      expect(backButtonAllowed).toBeFalsy();
    });

    it('should not trigger guard if backbutton has not been pressed', () => {
      cancellationService.backButtonPressed = false;

      const routeData = {
        data: {allowedBackPaths: ['allowedurl']},
      } as unknown;
      const backTo = {url: 'notallowed'} as RouterStateSnapshot;

      const backButtonAllowed = guard.canDeactivate(
        {} as Component,
        routeData as ActivatedRouteSnapshot,
        {} as RouterStateSnapshot,
        backTo
      );
      expect(backButtonAllowed).toBeTruthy();
    });

    it('should not allow backbutton if any modal dialog is open', () => {
      hasOpenModals = true;
      const backButtonAllowed = setupAndCallCanDeactivate(
        ['allowed-url'],
        'allowed-url'
      );
      expect(backButtonAllowed).toBeFalsy();
      hasOpenModals = false;
    });
  });

  describe('show dialogs', () => {
    const setupAndCallCanDeactivate = (cancel: boolean) => {
      jest.spyOn(cancellationService, cancel ? 'cancelVote' : 'leaveProcess');

      const routeData = {
        data: {
          backAction: cancel
            ? BackAction.ShowCancelVoteDialog
            : BackAction.ShowLeaveProcessDialog,
        },
      } as unknown;
      const backTo = {url: 'notallowed'} as RouterStateSnapshot;

      return guard.canDeactivate(
        {} as Component,
        routeData as ActivatedRouteSnapshot,
        {} as RouterStateSnapshot,
        backTo
      );
    };

    it('should show ShowCancelVoteDialog', () => {
      const backButtonAllowed = setupAndCallCanDeactivate(true);
      expect(backButtonAllowed).toBeFalsy();
      expect(cancellationService.cancelVote).toBeCalled();
    });

    it('should show ShowLeaveProcessDialog', () => {
      const backButtonAllowed = setupAndCallCanDeactivate(false);
      expect(backButtonAllowed).toBeFalsy();
      expect(cancellationService.leaveProcess).toBeCalled();
    });
  });

  describe('go pack to specific pages', () => {
    it('should go back to start voting page', () => {
      const routeData = {
        data: {backAction: BackAction.GoToStartVotingPage},
      } as unknown;

      const urlTree = guard.canDeactivate(
        {} as Component,
        routeData as ActivatedRouteSnapshot,
        {} as RouterStateSnapshot
      ) as UrlTree;

      expect(urlTree.toString()).toContain('start-voting');
    });

    it('should go back to legal-page', () => {
      store.overrideSelector(getElectionEventId, '123456');
      const routeData = {
        data: {backAction: BackAction.GoToLegalTermsPage},
      } as unknown;

      const urlTree$ = guard.canDeactivate(
        {} as Component,
        routeData as ActivatedRouteSnapshot,
        {} as RouterStateSnapshot
      ) as Observable<UrlTree>;

      urlTree$.pipe(take(1)).subscribe((urlTree) => {
        expect(urlTree.toString()).toContain('legal-terms/123456');
      });
    });
  });
});
