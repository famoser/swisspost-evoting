/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { AfterViewInit, Directive, ElementRef, Input, Renderer2 } from '@angular/core';
import { NgbAccordion } from '@ng-bootstrap/ng-bootstrap';

@Directive({
  selector: 'ngb-accordion'
})
export class AccordionAccessibilityDirective implements AfterViewInit {
  @Input() headersLevel: number = 2;

  constructor(
      private readonly el: ElementRef<HTMLElement>,
      private readonly accordion: NgbAccordion,
      private readonly renderer: Renderer2,
  ) {
    this.accordion.destroyOnHide = false;
  }

  ngAfterViewInit() {
    this.el.nativeElement.querySelectorAll('.accordion-item').forEach(item => {
      const currentHeader = item.querySelector('.accordion-header');
      if (!currentHeader) {
        return;
      }

      const headerButton = currentHeader.querySelector('button');
      if (!headerButton) {
        return;
      }

      headerButton.id = `${currentHeader.id}-button`;

      const collapsibleElement = item.querySelector('[role=tabpanel]');
      if (!collapsibleElement) {
        return;
      }

      collapsibleElement.setAttribute('role', 'region');
      collapsibleElement.setAttribute('aria-labelledby', headerButton.id);

      this.replaceWithProperHeader(currentHeader);
    });
  }

  private replaceWithProperHeader(accordionHeader: Element) {
    const newHeader: HTMLElement = this.renderer.createElement(`h${this.headersLevel}`);
    newHeader.classList.add(accordionHeader.classList.toString());
    accordionHeader.childNodes.forEach(child => newHeader.appendChild(child));

    accordionHeader.replaceWith(newHeader);
  }
}
