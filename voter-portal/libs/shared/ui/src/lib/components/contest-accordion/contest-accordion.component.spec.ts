/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { Component, DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { NgbAccordionModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateTestingModule } from 'ngx-translate-testing';

import { ContestAccordionComponent } from './contest-accordion.component';

let accordionIndex = 0;

@Component({
  template: `
      <swp-contest-accordion
          [id]="id"
          [title]="title"
          [subtitle]="subtitle"
          [cardClass]="cardClass"
      >{{contents}}</swp-contest-accordion>
  `,
})
class TestHostComponent {
  id!: string;
  title: string | undefined;
  subtitle: string | undefined;
  cardClass: string | undefined;
  contents: string | undefined;
}

describe('AccordionComponent', () => {
  let testHost: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;
  let accordionHeader: DebugElement;
  let accordionBody: DebugElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        ContestAccordionComponent,
        TestHostComponent,
      ],
      imports: [
        NgbAccordionModule,
        TranslateTestingModule.withTranslations({}),
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    testHost = fixture.componentInstance;

    testHost.id = `test-id-${accordionIndex++}`;

    fixture.detectChanges();

    accordionHeader = fixture.debugElement.query(By.css(`#${testHost.id}-header`));
    accordionBody = fixture.debugElement.query(By.css(`#${testHost.id}`));
  });

  it('should be open by default', () => {
    const displayedBody = accordionBody.nativeElement as HTMLElement;
    expect(displayedBody.classList).toContain('show');
  });

  it('should show "Hide" in the header', () => {
    const displayedText = accordionHeader.query(By.css('span:last-child')).nativeElement as HTMLElement;
    expect(displayedText.textContent).toBe('common.hide');
  });

  it('should properly show provided title', () => {
    const inputTitle = testHost.title = 'Test Title';

    fixture.detectChanges();

    const displayedTitle = accordionHeader.query(By.css('.h4')).nativeElement as HTMLElement;
    expect(displayedTitle.textContent).toBe(inputTitle);
  });

  it('should properly show provided subtitle', () => {
    const inputSubtitle = testHost.subtitle = 'Test Subtitle';

    fixture.detectChanges();

    const displayedSubtitle = accordionHeader.query(By.css('.h4 + span')).nativeElement as HTMLElement;
    expect(displayedSubtitle.textContent).toBe(inputSubtitle);
  });

  it('should not show a subtitle if none is provided', () => {
    testHost.subtitle = undefined;

    fixture.detectChanges();

    const accordionSubtitle = accordionHeader.query(By.css('.h4 + span'));
    expect(accordionSubtitle).toBeFalsy();
  });

  it('should properly show provided contents as the accordion body', () => {
    const inputContents = testHost.contents = 'Test Contents';

    fixture.detectChanges();

    const displayedBody = accordionBody.nativeElement as HTMLElement;
    expect(displayedBody.textContent).toBe(inputContents);
  });

  it('should properly pass the provided card class to the accordion', () => {
    const cardClass = testHost.cardClass = 'test-class';

    fixture.detectChanges();

    const displayedAccordion = fixture.debugElement.query(By.css('.accordion-item')).nativeElement as HTMLElement;
    expect(displayedAccordion.classList).toContain(cardClass);
  });


  describe('header click', () => {
    beforeEach(() => {
      const headerButton = accordionHeader.query(By.css('button')).nativeElement as HTMLElement;
      headerButton.click();

      fixture.detectChanges();
    });

    it('should close the accordion body', () => {
      const displayedBody = accordionBody.nativeElement as HTMLElement;
      expect(displayedBody.classList).not.toContain('show');
    });

    it('should display "Show" in the header', () => {
      const displayedText = accordionHeader.query(By.css('span:last-child')).nativeElement as HTMLElement;
      expect(displayedText.textContent).toBe('common.show');
    });
  });
});
