/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

function checkContents(contents: string | string[]) {
  if (!contents || !contents.length) {
    throw new Error('The ConfirmationModalComponent requires contents.');
  }
}

@Component({
  selector: 'swp-confirmation-modal',
  templateUrl: './confirmation-modal.component.html',
  styleUrls: ['./confirmation-modal.component.scss'],
})
export class ConfirmationModalComponent implements OnInit {
  @Input() title!: string;
  @Input() confirmLabel!: string;
  @Input() cancelLabel!: string;

  constructor(
    public readonly activeModal: NgbActiveModal,
  ) {
  }

  private _content: string[] = [];

  get content(): string[] {
    return this._content;
  }

  @Input() set content(value: string | string[]) {
    checkContents(value);
    this._content = Array.isArray(value) ? value : [value];
  }

  ngOnInit() {
    checkContents(this._content);
  }
}
