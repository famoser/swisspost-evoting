/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import '@angular/localize/init';
import { provideMockStore } from '@ngrx/store/testing';
import { ConfigurationService } from '@swiss-post/shared/configuration';
import { MockDirective, MockProvider } from 'ng-mocks';
import { TranslateTestingModule } from 'ngx-translate-testing';
import { ParagraphsDirective } from '../../directives/paragraphs.directive';

import { BackendErrorComponent } from './backend-error.component';

describe('BackendErrorComponent', () => {
    let component: BackendErrorComponent;
    let fixture: ComponentFixture<BackendErrorComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [
                TranslateTestingModule.withTranslations({}).withDefaultLanguage('fr'),
                HttpClientTestingModule,
            ],
            declarations: [
                BackendErrorComponent,
                MockDirective(ParagraphsDirective),
            ],
            providers: [
                MockProvider(ConfigurationService),
                provideMockStore({}),
            ],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(BackendErrorComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
