/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
export interface RouteData {
  stepKey: string;
  allowedBackPaths?: string[];
  backAction?: BackAction;
}

export enum BackAction {
  ShowCancelVoteDialog = 'ShowCancelDialog',
  ShowLeaveProcessDialog = 'ShowLeaveDialog',
  GoToStartVotingPage = 'GoToStartVotingPage',
  GoToLegalTermsPage = 'GoToLegalTermsPage',
}
