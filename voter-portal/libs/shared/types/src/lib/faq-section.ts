/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
export enum FAQSection {
  HowToStart = 'how-to-start',
  WhatIsVotingCard = 'what-is-voting-card',
  WhatIsStartVotingKey = 'what-is-start-voting-key',
  HowToMakeAChoice = 'how-to-make-a-choice',
  CanIChangeSelection = 'can-i-change-selection',
  WhatAreChoiceReturnCodes = 'what-are-choice-codes',
  WhatIsConfirmationKey = 'what-is-confirmation-key',
  WhatIsVoteCastReturnCode = 'what-is-vote-cast-return-code',
  WhatIfCodesDoNotMatch = 'what-if-codes-do-not-match',
  WhatIsDifferenceSealAndCast = 'what-is-difference-seal-and-cast',
  CanILeaveAnytime = 'can-i-leave-anytime',
  HowToUseWriteIns = 'how-to-use-write-ins',
}
