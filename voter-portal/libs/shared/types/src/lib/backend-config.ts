/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
export interface BackendConfig {
  lib: string;
  lang: string;
  electionEventId: string;
}
