/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
export enum ExtendedFactor {
  YearOfBirth = 'yob',
  DateOfBirth = 'dob',
}
