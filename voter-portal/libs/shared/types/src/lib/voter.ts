/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
export interface Voter {
  startVotingKey: string;
  extendedFactor: string;
}
