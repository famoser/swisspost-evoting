/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import {Renderer} from 'marked';

export interface Environment {
  production: boolean,
  voterPortalConfig: string,
  availableLanguages: Language[],
  markdownDenylist: (keyof Renderer)[],
  progressOverlayCloseDelay: number | Date,
  progressOverlayNavigateDelay: number | Date,
}

export interface Language {
  id: string;
  label: string;
}
