/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {IconComponent} from '@swiss-post/shared/icons';
import {MockCandidatesContest} from '@swiss-post/shared/testing';
import {ClearableInputComponent} from '@swiss-post/shared/ui';
import {ElectionContest} from '@swiss-post/types';
import {MockComponent, MockProvider} from 'ng-mocks';
import {TranslateTestingModule} from 'ngx-translate-testing';
import {CandidateSelectorComponent} from '../candidate-selector/candidate-selector.component';

import {CandidateSelectionModalComponent} from './candidate-selection-modal.component';

describe('CandidateSelectionModalComponent', () => {
  let component: CandidateSelectionModalComponent;
  let fixture: ComponentFixture<CandidateSelectionModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        CandidateSelectionModalComponent,
        MockComponent(ClearableInputComponent),
        MockComponent(CandidateSelectorComponent),
        MockComponent(IconComponent),
      ],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        TranslateTestingModule.withTranslations({}),
      ],
      providers: [MockProvider(NgbActiveModal)]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CandidateSelectionModalComponent);
    component = fixture.componentInstance;

    const contest = MockCandidatesContest({
      numSeats: 12,
      cumulAllowed: false,
      numLists: 3,
      numCandidatesPerList: 4,
      hasListQuestion: false,
    });
    component.contest = new ElectionContest(contest);

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
