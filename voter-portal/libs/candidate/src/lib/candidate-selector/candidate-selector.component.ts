/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {ElectionContest, Candidate, ContestUserData} from '@swiss-post/types';

@Component({
  selector: 'swp-candidate-selector',
  templateUrl: './candidate-selector.component.html',
  styleUrls: ['./candidate-selector.component.scss'],
})
export class CandidateSelectorComponent implements OnInit {
  @Input() electionContest!: ElectionContest;
  @Input() contestUserData!: ContestUserData;
  @Input() candidateIndex!: number;
  @Input() candidate!: Candidate;
  candidateCumul!: number;
  isAliasSelected!: boolean;

  constructor(
    private readonly modal: NgbActiveModal
  ) {
  }

  get candidatesUserData() {
    return this.contestUserData.candidates ?? [];
  }

  get candidateMaxAllowedCumul() {
    return this.candidate.allRepresentations?.length || 1;
  }

  get isCumulAllowed() {
    return this.candidateMaxAllowedCumul > 1;
  }

  get isCandidateSelected() {
    return this.candidateCumul > 0;
  }

  get isSelectedOnCurrentPosition() {
    return this.candidate.id === this.candidatesUserData[this.candidateIndex]?.candidateId;
  }

  get isCumulMessageShown() {
    return !this.candidate.isBlank && (this.isCumulAllowed && this.candidateCumul > 0);
  }

  ngOnInit() {
    this.isAliasSelected = this.selectedAlias;
    this.candidateCumul = this.candidatesUserData.filter(({candidateId}) => {
      return candidateId === this.candidate.id;
    }).length;
  }

  private get selectedAlias(): boolean {
    const candidateAliases = this.electionContest.candidatesQuestion?.fusions.reduce((aliases, fusion) => {
      return this.candidate.alias && fusion.includes(this.candidate.alias) ? [...aliases, ...fusion] : aliases;
    }, []);

    return this.candidatesUserData.some(({candidateId}) => {
      const candidate = this.electionContest.getCandidate(candidateId);
      const hasAlias = !!(candidate?.alias);
      const isSameCandidate = candidate && candidate.id === this.candidate.id;
      return hasAlias && !isSameCandidate && candidateAliases?.includes(candidate.alias as string);
    });
  }

  selectCandidate() {
    this.modal.close(this.candidate);
  }
}
