/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {NgbTooltipModule} from '@ng-bootstrap/ng-bootstrap';
import {TranslateModule} from '@ngx-translate/core';
import {SharedUiModule} from '@swiss-post/shared/ui';
import {CandidateSelectionModalComponent} from './candidate-selection-modal/candidate-selection-modal.component';
import {CandidatesComponent} from './candidates/candidates.component';
import {SharedIconsModule} from '@swiss-post/shared/icons';
import {CandidateSelectorComponent} from './candidate-selector/candidate-selector.component';
import {CandidateDetailsComponent} from './candidate-details/candidate-details.component';
import { CandidateWriteInComponent } from './candidate-write-in/candidate-write-in.component';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    RouterModule,
    FormsModule,
    NgbTooltipModule,
    SharedIconsModule,
    SharedUiModule,
    ReactiveFormsModule,
  ],
  declarations: [
    CandidateSelectionModalComponent,
    CandidateSelectorComponent,
    CandidatesComponent,
    CandidateDetailsComponent,
    CandidateWriteInComponent,
  ],
  exports: [CandidateSelectionModalComponent, CandidatesComponent],
})
export class CandidateModule {
}
