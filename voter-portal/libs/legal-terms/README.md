# legal-terms

This library was generated with [Nx](https://nx.dev).

It exports `LegalTermsModule` that contains 1st page of the `app`

## Running unit tests

Run `nx test legal-terms` to execute the unit tests.
