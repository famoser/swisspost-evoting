/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LegalTermsComponent} from './legal-terms/legal-terms.component';

const routes: Routes = [
  {path: '', component: LegalTermsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LegalTermsRoutingModule {
}
