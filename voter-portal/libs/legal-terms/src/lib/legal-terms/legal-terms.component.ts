/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { ConfigurationService } from '@swiss-post/shared/configuration';
import { LegalTermsActions } from '@swiss-post/shared/state';
import { BackendConfig } from '@swiss-post/types';

@Component({
  selector: 'swp-legal-terms',
  templateUrl: './legal-terms.component.html',
  styleUrls: [ './legal-terms.component.scss' ],
})
export class LegalTermsComponent implements OnInit {
  private readonly libPath = 'crypto.ov-api.min.js';
  electionEventId!: string;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly store: Store,
    private readonly configuration: ConfigurationService,
    private readonly translate: TranslateService
  ) {
  }

  ngOnInit(): void {
    this.electionEventId = this.route.snapshot.paramMap.get(
      'electionEventId'
    ) as string;
  }

  agree(): void {
    const config: BackendConfig = {
      lib: this.libPath,
      lang: this.translate.currentLang,
      electionEventId: this.electionEventId,
    };

    this.store.dispatch(LegalTermsActions.agreeClicked({ config }));
  }
}
