/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {CandidateList, CandidateListDetails} from '@swiss-post/types';

@Component({
  selector: 'swp-list-details',
  templateUrl: './list-details.component.html',
  styleUrls: ['./list-details.component.scss'],
})
export class ListDetailsComponent implements OnChanges {
  @Input() list: CandidateList | null | undefined;
  @Input() listPlaceholder: string | null | undefined;
  @Input() headingLevel: number | null | undefined;
  @Input() headingDisplayLevel: number | null | undefined;
  @Input() screenReaderLabel: string | null | undefined;
  @Input() showAllDetails: boolean | null | undefined;
  listDetails: string[] = [];

  get headingClass(): string {
    const headingLevel = this.headingDisplayLevel ?? this.headingLevel;
    return headingLevel ? `h${headingLevel}` : '';
  }

  ngOnChanges({list}: SimpleChanges) {
    const newList = list?.currentValue;
    this.listDetails = newList ? this.getListDetails(newList) : [];
  }

  private getListDetails(list: CandidateList): string[] {
    const listDetailKeys: (keyof CandidateListDetails)[] = [
      "listType_apparentment",
      "listType_sousApparentment",
      "listType_attribute2",
      "listType_attribute3",
    ];

    return listDetailKeys.reduce((listDetails, detailKey) => {
      const detail = list.details ? list.details[detailKey] : undefined;
      return detail ? [...listDetails, detail] : listDetails;
    }, [] as string[]);
  }
}
