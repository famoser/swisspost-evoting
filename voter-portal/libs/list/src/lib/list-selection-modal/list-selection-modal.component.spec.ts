/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {IconComponent} from '@swiss-post/shared/icons';
import {ClearableInputComponent} from '@swiss-post/shared/ui';
import {ElectionContest} from '@swiss-post/types';
import {MockComponent, MockProvider} from 'ng-mocks';
import {TranslateTestingModule} from 'ngx-translate-testing';

import {ListSelectionModalComponent} from './list-selection-modal.component';

describe('ListSelectionModalComponent', () => {
  let component: ListSelectionModalComponent;
  let fixture: ComponentFixture<ListSelectionModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        ListSelectionModalComponent,
        MockComponent(ClearableInputComponent),
        MockComponent(IconComponent),
      ],
      providers: [MockProvider(NgbActiveModal)],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        TranslateTestingModule.withTranslations({}),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListSelectionModalComponent);
    component = fixture.componentInstance;

    component.electionContest = {} as ElectionContest;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
