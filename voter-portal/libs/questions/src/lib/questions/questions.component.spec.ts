/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import {Component, ViewChild} from '@angular/core';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {MockContest, MockContestUserData, MockQuestions} from '@swiss-post/shared/testing';
import {MarkdownPipe} from '@swiss-post/shared/ui';
import {Contest, ContestUserData, Question} from '@swiss-post/types';
import {MockPipe} from 'ng-mocks';

import {QuestionsComponent} from './questions.component';

@Component({
  template: `
    <swp-questions
      [contest]="contest"
      [contestUserData]="contestUserData"
      [leftColumnTemplate]="leftColumnTemplate"
      [rightColumnTemplate]="rightColumnTemplate"
    ></swp-questions>
    <ng-template #leftColumnTemplate>{{leftColumnTemplateContent}}</ng-template>
    <ng-template #rightColumnTemplate>{{rightColumnTemplateContent}}</ng-template>
  `
})
class TestHostComponent {
  @ViewChild(QuestionsComponent) questionsComponent!: QuestionsComponent;
  contest: Contest | undefined;
  contestUserData: ContestUserData | undefined;
  leftColumnTemplateContent = 'Left column contents';
  rightColumnTemplateContent = 'Right column contents';
}

describe('QuestionComponent', () => {
  let testHost: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;
  let questions: Question[];
  let markdownTransformFn: jest.Mock<string>;

  beforeEach(async () => {
    markdownTransformFn = jest.fn(x => `transformed ${x}`);

    await TestBed.configureTestingModule({
      declarations: [
        QuestionsComponent,
        TestHostComponent,
        MockPipe(MarkdownPipe, markdownTransformFn)
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    testHost = fixture.componentInstance;

    questions = MockQuestions(['Question A', 'Question B']);
    testHost.contest = MockContest(questions);

    fixture.detectChanges();
  });

  it('should show all questions provided in the contest', () => {
    const displayedQuestions = fixture.debugElement.queryAll(By.css('.question'));
    expect(displayedQuestions.length).toBe(questions.length);
  });

  it('should show questions text transformed using the markdown pipe', () => {
    questions.forEach(question => {
      const displayedQuestion = fixture.debugElement.query(By.css(`#question-${question.id}`));
      const displayedTitle = displayedQuestion.query(By.css('.question--title'));
      expect(displayedTitle.nativeElement.textContent).toBe(markdownTransformFn(question.text));
    });
  });

  it('should correctly map answers to questions when contest user data is provided', () => {
    const contestUserData = testHost.contestUserData = MockContestUserData(questions);

    fixture.detectChanges();

    questions.forEach((question, i) => {
      const chosenOption = contestUserData.questions?.find(q => q.id === question.id)?.chosenOption;
      const expectedAnswer = question.options.find(o => o.id === chosenOption)?.text ?? question.blankOption.text;

      const {answer} = testHost.questionsComponent.getTemplateContext(question, i);

      expect(answer).toBe(expectedAnswer);
    });
  });

  it('should not show answer if there is no contestUserData', () => {
    testHost.contestUserData = undefined;

    fixture.detectChanges();

    questions.forEach((question, i) => {
      const {answer} = testHost.questionsComponent.getTemplateContext(question, i);
      expect(answer).toBe('');
    });
  });

  describe('column templates', () => {
    let leftColumn: HTMLElement;
    let rightColumn: HTMLElement;

    beforeEach(() => {
      const displayedQuestion = fixture.debugElement.query(By.css('.question'));
      const questionColumns = displayedQuestion.queryAll(By.css('.col, [class^="col-"]'));
      leftColumn = questionColumns[0].nativeElement as HTMLElement;
      rightColumn = questionColumns[questionColumns.length - 1].nativeElement as HTMLElement;
    });

    it('should properly render the template provided for the left column', () => {
      expect(leftColumn.textContent).toContain(testHost.leftColumnTemplateContent);
    });

    it('should properly render the template provided for the right column', () => {
      expect(rightColumn.textContent).toContain(testHost.rightColumnTemplateContent);
    });
  });
});
