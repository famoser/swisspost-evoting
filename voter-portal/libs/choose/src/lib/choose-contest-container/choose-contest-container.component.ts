/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {FormArray, FormGroup} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {CandidateUserData, Contest, TemplateType} from '@swiss-post/types';
import {BehaviorSubject, Subject} from 'rxjs';
import {startWith, takeUntil} from 'rxjs/operators';

@Component({
  selector: 'swp-choose-contest-container',
  templateUrl: './choose-contest-container.component.html',
  styleUrls: ['./choose-contest-container.component.scss'],
})
export class ChooseContestContainerComponent implements OnInit, OnDestroy {
  @Input() contest!: Contest;
  @Input() contestFormGroup!: FormGroup;
  @Input() formSubmitted: boolean | undefined;
  readonly TemplateType = TemplateType;
  accordionSubtitle = new BehaviorSubject('');
  destroy$ = new Subject<void>();

  get candidates(): FormArray {
    return this.contestFormGroup.get('candidates') as FormArray;
  }

  constructor(
    private readonly translate: TranslateService
  ) {
  }

  ngOnInit() {
    if (this.contest.template === TemplateType.ListsAndCandidates) {
      this.candidates.valueChanges
        .pipe(
          takeUntil(this.destroy$),
          startWith(this.candidates.value as CandidateUserData[])
        )
        .subscribe((candidatesUserData: CandidateUserData[]) => {
          const selectedCandidateCount = candidatesUserData.filter(({candidateId}) => !!candidateId).length;
          const interpolateParams = {
            nbChoices: selectedCandidateCount,
            maxChoices: this.contest?.candidatesQuestion?.maxChoices,
          };
          const updatedSubtitle = this.translate.instant('choose.candidates.subtitle', interpolateParams);
          this.accordionSubtitle.next(updatedSubtitle);
        });
    }
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
