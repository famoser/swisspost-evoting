/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import {Component, Input} from '@angular/core';
import {FormArray, FormGroup} from '@angular/forms';
import {Contest} from '@swiss-post/types';

@Component({
  selector: 'swp-choose-questions',
  templateUrl: './choose-questions.component.html',
  styleUrls: ['./choose-questions.component.scss'],
})
export class ChooseQuestionsComponent {
  @Input() contest: Contest | undefined;
  @Input() contestFormGroup: FormGroup | undefined;

  getQuestionFormGroup(questionIndex: number): FormGroup | undefined {
    const contestQuestions = this.contestFormGroup?.get('questions') as FormArray;
    return contestQuestions?.controls[questionIndex] as FormGroup;
  }

  getChosenOption(questionIndex: number): string | undefined {
    return this.getQuestionFormGroup(questionIndex)?.get('chosenOption')?.value;
  }

  resetRadioButtons(questionIndex: number): void {
    this.getQuestionFormGroup(questionIndex)?.get('chosenOption')?.reset();

    const question = document.querySelectorAll('.question--options').item(questionIndex) as HTMLElement;
    const firstOption = question?.querySelectorAll('.question--option').item(0) as HTMLElement;
    firstOption.focus();
  }
}
