/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import {Component, Input} from '@angular/core';
import {ContestAndContestUserData} from '@swiss-post/types';

@Component({
  selector: 'swp-review-questions',
  templateUrl: './review-questions.component.html',
  styleUrls: ['./review-questions.component.scss'],
})
export class ReviewQuestionsComponent {
  @Input() contestAndValues: ContestAndContestUserData | undefined;
}
