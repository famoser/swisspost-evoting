/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import {DebugElement} from '@angular/core';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {provideMockStore} from '@ngrx/store/testing';
import {MockContest, MockContestUserData, MockQuestions} from '@swiss-post/shared/testing';
import {ContestAccordionComponent} from '@swiss-post/shared/ui';
import {ContestAndContestUserData, ContestUserData, Question, TemplateType} from '@swiss-post/types';
import {MockComponent} from 'ng-mocks';
import {ReviewCandidateListComponent} from '../review-candidate-list/review-candidate-list.component';
import {ReviewQuestionsComponent} from '../review-questions/review-questions.component';

import {ReviewContestContainerComponent} from './review-contest-container.component';

describe('ContestReviewContainerComponent', () => {
  let component: ReviewContestContainerComponent;
  let fixture: ComponentFixture<ReviewContestContainerComponent>;
  let accordionComponent: DebugElement;
  let questionListComponent: DebugElement;
  let questions: Question[];
  let contestAndValues: ContestAndContestUserData;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        ReviewContestContainerComponent,
        MockComponent(ContestAccordionComponent),
        MockComponent(ReviewQuestionsComponent),
        MockComponent(ReviewCandidateListComponent),
      ],
      providers: [provideMockStore({})],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewContestContainerComponent);
    component = fixture.componentInstance;

    questions = MockQuestions(['Choose Container Question A', 'Choose Container Question B']);
    contestAndValues = component.contestAndValues = {
      contest: MockContest(questions),
      contestUserData: MockContestUserData(questions),
    }

    fixture.detectChanges();

    accordionComponent = fixture.debugElement.query(By.css('swp-contest-accordion'));
    questionListComponent = fixture.debugElement.query(By.css('swp-review-questions'));
  });

  function setContestUserData(newContesUserData: ContestUserData) {
    component.contestAndValues = {...contestAndValues, contestUserData: newContesUserData};
    fixture.detectChanges();
  }

  it('should pass an id derived from that of the contest provided to the accordion component', () => {
    expect(accordionComponent.componentInstance.id).toContain(component.contestAndValues?.contest?.id);
  });

  it('should pass the provided contest title as the title of the accordion component', () => {
    expect(accordionComponent.componentInstance.title).toContain(component.contestAndValues?.contest?.title);
  });

  it('should pass the provided contest and contest user data to the question list component', () => {
    expect(questionListComponent.componentInstance.contestAndValues).toBe(component.contestAndValues);
  });

  it('should not pass a card class to the accordion component if not all questions are blank', () => {
    setContestUserData(MockContestUserData(questions, {blankAnswerOnly: false}));

    accordionComponent = fixture.debugElement.query(By.css('swp-contest-accordion'));
    expect(accordionComponent.componentInstance.cardClass).toBeFalsy();
  });

  it('should not show the question list component if the provided contest has candidate choices', () => {
    contestAndValues.contest.template = TemplateType.ListsAndCandidates;

    fixture.detectChanges();

    questionListComponent = fixture.debugElement.query(By.css('swp-choose-questions'));
    expect(questionListComponent).toBeFalsy();
  });
});
