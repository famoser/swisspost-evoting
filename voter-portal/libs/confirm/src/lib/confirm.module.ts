/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { SharedIconsModule } from '@swiss-post/shared/icons';
import { SharedUiModule } from '@swiss-post/shared/ui';
import { ConfirmRoutingModule } from './confirm-routing/confirm-routing.module';
import { ConfirmComponent } from './confirm/confirm.component';

@NgModule({
  imports: [
    CommonModule,
    ConfirmRoutingModule,
    TranslateModule,
    SharedUiModule,
    SharedIconsModule,
  ],
  declarations: [ ConfirmComponent ],
})
export class ConfirmModule {
}
