/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.commandmessaging;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.security.SecureRandom;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class CommandServiceTest {

	private static final String CONTEXT_KEY_GENERATION = "key-generation";
	private static final String CONTEXT_ID_ONE = "context-id-1";
	private static final String CORRELATION_ID_ONE = "1";
	private static final String CORRELATION_ID_THREE = "3";
	private static final int NODE_ID_ONE = 1;
	private static final int NODE_ID_2 = 2;
	private static SecureRandom secureRandom;

	@Autowired
	CommandService commandService;

	@Autowired
	CommandRepository commandRepository;

	@BeforeAll
	public static void bootstrap(
			@Autowired
			final
			CommandService commandService) {

		final CommandId commandIdOneNodeOne = CommandId.builder()
				.contextId(CONTEXT_ID_ONE)
				.context(CONTEXT_KEY_GENERATION)
				.correlationId(CORRELATION_ID_ONE)
				.nodeId(NODE_ID_ONE)
				.build();
		final CommandId commandIdOneNodeTwo = CommandId.builder()
				.contextId(CONTEXT_ID_ONE)
				.context(CONTEXT_KEY_GENERATION)
				.correlationId(CORRELATION_ID_ONE)
				.nodeId(NODE_ID_2)
				.build();

		secureRandom = new SecureRandom();
		final byte[] requestPayload = new byte[10];

		secureRandom.nextBytes(requestPayload);
		commandService.saveRequest(commandIdOneNodeOne, requestPayload);
		commandService.saveRequest(commandIdOneNodeTwo, requestPayload);
	}

	@Test
	void findIdenticalCommand() {
		final CommandId commandIdOneNodeOne = CommandId.builder().contextId(CONTEXT_ID_ONE).context(CONTEXT_KEY_GENERATION).correlationId(
						CORRELATION_ID_ONE).nodeId(NODE_ID_ONE)
				.build();

		final Optional<Command> identicalCommand = commandService.findIdenticalCommand(commandIdOneNodeOne);
		assertTrue(identicalCommand.isPresent());
	}

	@Test
	void findSemanticallyIdenticalCommand() {
		final CommandId commandIdOneNodeTwo = CommandId.builder().contextId(CONTEXT_ID_ONE).context(CONTEXT_KEY_GENERATION).correlationId("2").nodeId(
						NODE_ID_ONE)
				.build();

		final Optional<Command> identicalCommand = commandService.findIdenticalCommand(commandIdOneNodeTwo);
		assertFalse(identicalCommand.isPresent());

		final List<Command> semanticallyIdenticalCommand = commandService.findSemanticallyIdenticalCommand(commandIdOneNodeTwo);
		assertEquals(1, semanticallyIdenticalCommand.size());
	}

	@Test
	void failToFindCommand() {
		final CommandId commandIdTwoNodeTwo = CommandId.builder().contextId("unique-id-2").context(CONTEXT_KEY_GENERATION).correlationId(
						CORRELATION_ID_ONE).nodeId(NODE_ID_ONE)
				.build();

		final Optional<Command> identicalCommand = commandService.findIdenticalCommand(commandIdTwoNodeTwo);
		assertFalse(identicalCommand.isPresent());

		final List<Command> semanticallyIdenticalCommand = commandService.findSemanticallyIdenticalCommand(commandIdTwoNodeTwo);
		assertEquals(0, semanticallyIdenticalCommand.size());
	}

	@Test
	void saveResponseSuccessfullyFindRequest() {
		final CommandId commandIdOneNodeOne = CommandId.builder()
				.contextId(CONTEXT_ID_ONE)
				.context(CONTEXT_KEY_GENERATION)
				.correlationId(CORRELATION_ID_ONE)
				.nodeId(NODE_ID_ONE)
				.build();

		final boolean requestForContextIdAndContextAlreadyPresent = commandService.isRequestForContextIdAndContextAlreadyPresent(CONTEXT_ID_ONE,
				CONTEXT_KEY_GENERATION);
		assertTrue(requestForContextIdAndContextAlreadyPresent);

		final byte[] responsePayload = new byte[10];
		secureRandom.nextBytes(responsePayload);

		final Command command = commandService.saveResponse(commandIdOneNodeOne, responsePayload);
		assertNotNull(command.getResponsePayload());

	}

	@Test
	void saveResponseFailingToFindRequest() {
		final CommandId commandIdOneNodeOne = CommandId.builder()
				.contextId(CONTEXT_ID_ONE)
				.context(CONTEXT_KEY_GENERATION)
				.correlationId(CORRELATION_ID_THREE)
				.nodeId(NODE_ID_ONE)
				.build();

		final byte[] responsePayload = new byte[10];
		secureRandom.nextBytes(responsePayload);
		assertThrows(IllegalStateException.class, () -> commandService.saveResponse(commandIdOneNodeOne, responsePayload));

	}

	@Test
	void saveSuccessfully() {
		final CommandId commandIdOneNodeOne = CommandId.builder()
				.contextId(CONTEXT_ID_ONE)
				.context(CONTEXT_KEY_GENERATION)
				.correlationId(CORRELATION_ID_THREE)
				.nodeId(NODE_ID_ONE)
				.build();

		final byte[] requestPayload = new byte[10];
		secureRandom.nextBytes(requestPayload);
		final LocalDateTime requestDateTime = LocalDateTime.now();
		final byte[] responsePayload = new byte[10];
		secureRandom.nextBytes(responsePayload);
		final LocalDateTime responseDateTime = LocalDateTime.now();

		final Command command = commandService.save(commandIdOneNodeOne, requestPayload, requestDateTime, responsePayload, responseDateTime);
		assertNotNull(command.getResponsePayload());

	}

	@Test
	void findAllMessagesWithCorrelationId() {
		final List<Command> allMessagesWithCorrelationId = commandService.findAllCommandsWithCorrelationId(CORRELATION_ID_ONE);
		assertEquals(NODE_ID_2, allMessagesWithCorrelationId.size());
	}
}