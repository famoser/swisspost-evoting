/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.commandmessaging;

import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestCommandApplication {
}
